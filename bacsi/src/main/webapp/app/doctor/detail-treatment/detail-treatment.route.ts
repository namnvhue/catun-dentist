import {Route} from '@angular/router';
import {UserRouteAccessService} from '../../shared/auth/user-route-access-service';
import {DetailTreatmentComponent} from './detail-treatment.component';

export const DETAIL_TREATMENT_ROUTE: Route = {
    path: 'treatment-detail',
    component: DetailTreatmentComponent,
    data: {
        authorities: ['ROLE_ADMIN', 'ROLE_DOCTOR'],
        pageTitle: 'CatunDentistApp.detailTreatmentEntity.title'
    },
    canActivate: [UserRouteAccessService]
};
