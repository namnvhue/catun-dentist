export class DetailTreatmentDataResponse {
    patientInfo: PatientResposne[];
    categoryServices: CategoryServiceResponse[];
    toothNames: string[];
    amounts: number[];
}

export class CategoryServiceResponse {
    id?: string;
    name?: string;
    services?: ServiceResponse[];
}

export class ServiceResponse {
    id?: string;
    name?: string;
    price?: number;
    toothNames?: string[];
    amounts: number;
    // products?: ProductResponse[];

    constructor() {
        this.amounts = 1;
    }
}

export class ProductResponse {
    id?: string;
    name?: string;
    price?: number;
}

export class PatientResposne {
    name?: string;
    numberPhone: string;
}
