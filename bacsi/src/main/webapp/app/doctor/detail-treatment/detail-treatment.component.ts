import {Component, OnDestroy, OnInit} from '@angular/core';
import {HttpResponse} from '@angular/common/http';
import {TranslateService} from '@ngx-translate/core';
import {DetailTreatmentService} from './detail-treatment.service';
import {DetailTreatmentDataResponse, PatientResposne, ProductResponse, ServiceResponse} from './detail-treatment.model';
import {Patient} from '../treatment/treatment.model';
import {Treatment} from '../treatment-history/treatment-history.model';
import {TreatmentService} from '../treatment/treatment.service';
import {Router} from '@angular/router';

@Component({
    selector: 'jhi-confirm',
    templateUrl: './detail-treatment.component.html',
    styleUrls: ['./detail-treatment.component.css']
})
export class DetailTreatmentComponent implements OnInit, OnDestroy {

    selectedPatient: Patient;
    selectedTreatment: Treatment;
    patientSubscription: any;
    treatmentSubscription: any;

    initData: DetailTreatmentDataResponse;
    serviceCateList: CategoryServiceDTO[];
    serviceList: ServiceDTO[];

    selectedPatients = [];
    selectedDiscount: number;
    doctorNote: string;
    patientNote: string;
    totalPrice: number = 0;
    finalPrice: number = 0;

    discountValues = [
        5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 80, 85, 90, 95, 100
    ];

    message;
    staticAlertClosed = false;
    staticAlertType = 'success';

    noService: boolean;
    noServiceMsg: string;

    patientList;
    isPatientAvailable = false;
    dropdownSettings = {};

    toothList;
    isToothAvailable = false;
    toothDropdownSettings = {};
    selectedTooth: string[] = [];

    isServiceAvailable: boolean = false;
    serviceDropdownSettings = {};
    serviceDropdownList = [];
    selectedService: any[];
    requestService: SelectedService[] = [];


    constructor(private translateService: TranslateService,
                private detailTreatmentService: DetailTreatmentService,
                private treatmentService: TreatmentService,
                private router: Router) {
        this.patientSubscription = this.treatmentService.selectedPatient.subscribe((patient) => {
            this.selectedPatient = patient;
        });
        this.treatmentSubscription = this.treatmentService.selectedTreatment.subscribe((treatment) => {
            this.selectedTreatment = treatment;
        });
    }

    ngOnInit() {
        this.checkSelectedValue();
        this.getInitData();
    }

    ngOnDestroy(): void {
        this.treatmentSubscription.unsubscribe();
        this.patientSubscription.unsubscribe();
    }

    checkSelectedValue() {
        if (!this.selectedPatient.id) {
            this.router.navigate(['/treatment']);
        }
        if (!this.selectedTreatment.id) {
            this.router.navigate(['/treatment']);
        }
    }

    /**
     * Fetch {ServiceCategory, Service And Product} data object from server
     */
    getInitData() {
        this.detailTreatmentService.fetchDetailTreatmentData().subscribe(
            (res) => {
                this.initData = res;
                this.initPatientSelect(res.patientInfo);
                this.initToothSelect(res.toothNames);
                this.classifyInitData();
            },
            (err) => {
                console.log(err.errorMessage); // TODO
            }
        );
    }

    initToothSelect(toothInfoRes: string[]) {
        this.toothList = [];
        toothInfoRes.forEach((t) => {
            this.toothList.push({
                'id': t,
                'name': t
            });
        });
        this.isToothAvailable = true;
        this.selectedTooth = [];
        this.toothDropdownSettings = {
            singleSelection: false,
            idField: 'id',
            textField: 'name',
            selectAllText: 'Chọn hết',
            unSelectAllText: 'Bỏ chọn hết',
            itemsShowLimit: 2,
            allowSearchFilter: true
        };
    }

    /**
     * Init Patient Selector
     * 1. Parse patient response data to patient-list object for selector
     * 2. Init selector config
     * @param {PatientResposne[]} patientInfoRes
     */
    initPatientSelect(patientInfoRes: PatientResposne[]) {
        this.patientList = [];
        if (patientInfoRes) {
            patientInfoRes.forEach((patientInfo) => {
                this.patientList.push({
                    'id': patientInfo.numberPhone,
                    'name': patientInfo.name + '(' + patientInfo.numberPhone + ')'
                });
            });
            this.isPatientAvailable = true;
            this.selectedPatients = [];
            this.dropdownSettings = {
                singleSelection: true,
                idField: 'id',
                textField: 'name',
                selectAllText: 'Chọn hết',
                unSelectAllText: 'Bỏ chọn hết',
                itemsShowLimit: 3,
                allowSearchFilter: true
            };
        }
    }

    /**
     * Classify ServiceCategory, Service And Product data to seperate lists
     */
    classifyInitData() {
        if (this.initData) {
            this.serviceCateList = [];
            this.serviceList = [];

            //this.serviceCateList.push(new CategoryServiceDTO('all', 'All', true)); // Add default 'All'
            this.initData.categoryServices.forEach((serviceCate) => {

                // Add service categories to show-list
                this.serviceCateList.push(new CategoryServiceDTO(serviceCate.id, serviceCate.name));

                // Add services & products to show-list
                serviceCate.services.forEach((service) => {
                    // init data in case service is selected
                    service.amounts = 1;
                    service.toothNames = [];

                    // Do not push existed service to list, just add service category id
                    let existedService = this.serviceList.find((s) => s.service.id === service.id);
                    if (existedService) {
                        existedService.addServiceCategoryId(serviceCate.id);
                        return;
                    }

                    let serviceDto = new ServiceDTO(service, serviceCate.id);
                    this.serviceList.push(serviceDto);
                    this.serviceDropdownList.push({
                        'id': serviceDto.service.id,
                        'name': serviceCate.name + ' - ' + serviceDto.service.name
                    });
                });
            });
            this.initServiceSelect();
        }
    }

    initServiceSelect() {
        this.isServiceAvailable = true;
        this.selectedService = [];
        this.serviceDropdownSettings = {
            singleSelection: true,
            idField: 'id',
            textField: 'name',
            selectAllText: 'Chọn hết',
            unSelectAllText: 'Bỏ chọn hết',
            itemsShowLimit: 1,
            allowSearchFilter: true
        };
    }

    /**
     * Calculate totalPrice
     * @returns {number}
     */
    sumPrice() {
        let totalPriceValue = 0;
        this.requestService.forEach((s) => {
            if (s.service) {
                totalPriceValue += +s.service.price * +s.service.amounts;
            }
        });
        this.totalPrice = totalPriceValue;
        return totalPriceValue;
    }

    /**
     * Calculate finalPrice
     * Final Price = Total Price & Discount(%)
     * @returns {number}
     */
    calculateFinalPrice() {
        let finalPriceValue: number = 0;
        if (this.selectedDiscount) {
            finalPriceValue = this.totalPrice - ((this.totalPrice * this.selectedDiscount) / 100);
        } else {
            finalPriceValue = this.totalPrice;
        }
        this.finalPrice = finalPriceValue;
        return finalPriceValue;
    }

    /**
     * Convert ServiceList DTO to DetailTreatment Request to server
     * @returns {{services: ServiceResponse[]; totalPrice: number; discount: number; finalPrice: number; phone: string; notedFromDoctor: string; notedFromPatient: string}}
     */
    createDetailTreatmentRequest() {
        let services: ServiceResponse[] = [];

        this.requestService.forEach((s) => {
            if (s.service) {
                let selectedService = Object.assign({}, s.service);
                services.push(selectedService);
            }
        });

        let request = {
            services: services,
            totalPrice: this.totalPrice,
            discount: this.selectedDiscount,
            finalPrice: this.finalPrice,
            patientId: this.selectedPatient.id,
            treatmentId: this.selectedTreatment.id,
            notedFromDoctor: this.doctorNote,
            notedFromPatient: this.patientNote
        };
        return request;
    }

    addService() {
        this.requestService.push(new SelectedService());
    }

    chooseServiceOnAdd(event, selectedService: SelectedService) {
        let existedService = this.requestService.find((s) => {
            if (s.service) {
                return s.service.id === event.id;
            }
        });
        if (!existedService) {
            this.serviceList.forEach((serviceDto) => {
                if (serviceDto.service.id == event.id) {
                    selectedService.service = serviceDto.service;
                    return;
                }
            });
        } else {
            this.requestService.splice(this.requestService.length-1, 1);
        }
    }

    removeServiceFromRequest(service: SelectedService) {
        let i = this.requestService.findIndex((s) => s.service.id == service.service.id);
        if (i > -1) {
            this.requestService.splice(i, 1);
        }
    }


    /**
     * Send POST request to server for saving Detail Treatment
     */
    save() {
        let detailTreatment = this.createDetailTreatmentRequest();
        if (detailTreatment.services.length == 0) {
            this.noService = true;
            this.noServiceMsg = this.translateService.instant('CatunDentistApp.detailTreatmentEntity.message.noService');
            return;
        }
        console.log(detailTreatment);
        this.detailTreatmentService.saveDetailTreament(detailTreatment)
            .subscribe(
                (response: HttpResponse<any>) => {
                    if (response.status == 200) {
                        this.message = this.translateService.instant('CatunDentistApp.detailTreatmentEntity.message.saved'),
                            this.staticAlertType = 'success';
                    }
                    this.staticAlertClosed = false;
                    setTimeout(() => this.staticAlertClosed = true, 4000);
                    this.resetDetailTreatment();
                    this.router.navigate(['/treatment/' + this.selectedTreatment.id]);
                },
                () => {
                });
    }

    resetDetailTreatment() {
        this.requestService = [];
        this.selectedPatients = [];
        this.selectedDiscount = null;
        this.doctorNote = '';
        this.patientNote = '';
        this.totalPrice = 0;
        this.finalPrice = 0;

    }
}

export class CategoryServiceDTO {
    id: string;
    name: string;
    isSelected: boolean;

    constructor(id: string, name: string, isSelected?: boolean) {
        this.id = id;
        this.name = name;
        this.isSelected = isSelected ? isSelected : false;
    }
}

export class SelectedService {
    service: ServiceResponse;
}

export class ServiceDTO {
    service: ServiceResponse;
    serviceCategoryId: string[];
    isSelected: boolean;
    isShown: boolean;

    constructor(service: ServiceResponse, serviceCategoryId: string) {
        this.service = service;
        this.isSelected = false;
        this.isShown = false;
        this.addServiceCategoryId(serviceCategoryId);
    }

    addServiceCategoryId(serviceCategoryId: string) {
        if (this.serviceCategoryId) {
            if (!this.isServiceCategoryExist(serviceCategoryId)) {
                this.serviceCategoryId.push(serviceCategoryId);
            }
        } else {
            // this.serviceCategoryId = ['all', serviceCategoryId];
            this.serviceCategoryId = [serviceCategoryId];
        }
    }

    isServiceCategoryExist(serviceCategoryId: string) {
        return this.serviceCategoryId.find((id) => id == serviceCategoryId);
    }
}
