import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule} from '@angular/router';
import { CatunDentistAppSharedModule } from '../../shared/shared.module';
import {DETAIL_TREATMENT_ROUTE} from './detail-treatment.route';
import {DetailTreatmentComponent} from './detail-treatment.component';
import {DetailTreatmentService} from './detail-treatment.service';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
    imports: [
        RouterModule.forChild([DETAIL_TREATMENT_ROUTE]),
        CatunDentistAppSharedModule,
        NgMultiSelectDropDownModule
    ],
    declarations: [
        DetailTreatmentComponent
    ],
    entryComponents: [
    ],
    providers: [
        DetailTreatmentService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CatunDentistDetailTreatmentModule {}
