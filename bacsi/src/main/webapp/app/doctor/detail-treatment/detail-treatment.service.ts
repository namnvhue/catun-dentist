import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';
import {DetailTreatmentDataResponse} from './detail-treatment.model';

@Injectable()
export class DetailTreatmentService {
    private resourceUrl = SERVER_API_URL + '/doctor';

    constructor(private http: HttpClient) {
    }

    fetchDetailTreatmentData(): Observable<DetailTreatmentDataResponse> {
        return this.http.get<DetailTreatmentDataResponse>(this.resourceUrl + '/treatment-detail-services');
    }

    saveDetailTreament(detail): Observable<any> {
        return this.http.post(
            this.resourceUrl + '/treatment-detail', JSON.stringify(detail), {
                responseType: 'blob',
                headers: new HttpHeaders({'Content-Type': 'application/json'}),
                observe: 'response'
            });
    }
}
