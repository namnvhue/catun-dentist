import {Component, OnInit} from '@angular/core';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {TranslateService} from '@ngx-translate/core';
import {TreatmentService} from '../treatment.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Pageable, Patient} from '../treatment.model';
import {Treatment, TreatmentResponse} from "../../treatment-history/treatment-history.model";
import {NgbModal, NgbModalRef} from "@ng-bootstrap/ng-bootstrap";
import {Principal} from "../../../shared/auth/principal.service";

@Component({
    selector: 'jhi-confirm',
    templateUrl: './treatment-list.component.html',
    styleUrls: ['./treatment-list.component.css']
})
export class TreatmentListComponent implements OnInit {

    routeData: any;

    treatments: Treatment[];
    pageable: Pageable;

    predicate: any;
    reverse: any;
    page: any = 1;
    search: any = '';

    message;
    staticAlertClosed = false;
    staticAlertType = 'success';

    tempTreatment: Treatment;
    finishTreatmentModal: NgbModalRef;
    deleteTreatmentModal: NgbModalRef;

    isDoctor: boolean;
    isReception: boolean;
    authorities: any;

    constructor(private activatedRoute: ActivatedRoute,
                private router: Router,
                private modalService: NgbModal,
                private translateService: TranslateService,
                private treatmentService: TreatmentService,
                private principal: Principal) {
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
    }

    ngOnInit() {
        this.loadAll();
        this.getRole();
    }

    loadAll() {
        this.treatmentService.fetchUnfinishedTreatments({
            search: this.search,
            page: this.page,
            sort: this.reverse ? 'asc' : 'desc'
        }).subscribe(
            (res: HttpResponse<TreatmentResponse>) => {
                this.onSuccess(res.body);
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    private getRole() {
        this.isDoctor = false;
        this.isReception = false;

        this.principal.identity(false).then((account) => {
            if (account !== null) {
                this.authorities = account.authorities;
                if (account.authorities.indexOf('ROLE_RECEPTIONIST') > -1) {
                    this.isReception = true;
                } else if (account.authorities.indexOf('ROLE_DOCTOR') > -1) {
                    this.isDoctor = true;
                }
            }
        });
    }

    private setSelectedValue(treatment: Treatment) {
        this.treatmentService.setTreatment(treatment);
        let patient = new Patient();
        patient.id = treatment.patientId;
        patient.phone = treatment.phone;
        patient.email = treatment.email;
        patient.name = treatment.name;
        patient.birthDay = treatment.birthDay;
        this.treatmentService.setPatient(patient);
    }

    finish(content: any, treatment: Treatment) {
        this.tempTreatment = treatment;
        this.finishTreatmentModal = this.modalService.open(content);
    }

    confirmFinish() {
        this.treatmentService.changeTreatmentStatus({
            treatmentId: this.tempTreatment.id,
            finishFlag: true
        }).subscribe(
            (res: HttpResponse<any>) => {
                this.tempTreatment = null;
                this.finishTreatmentModal.close();
                this.transition();
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    closeFinishModal() {
        this.tempTreatment = null;
        this.finishTreatmentModal.close();
    }

    delete(content: any, treatment: Treatment) {
        this.tempTreatment = treatment;
        this.deleteTreatmentModal = this.modalService.open(content);
    }

    confirmDelete() {
        this.treatmentService.deleteTreatment(this.tempTreatment.id).subscribe(
            (res: HttpResponse<any>) => {
                this.tempTreatment = null;
                this.deleteTreatmentModal.close();
                this.transition();
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    closeDeleteModal() {
        this.tempTreatment = null;
        this.deleteTreatmentModal.close();
    }

    select(treatment: Treatment) {
        this.setSelectedValue(treatment);
        this.router.navigate(['/treatment/' + treatment.id]);
    }

    createTreatment() {
        this.router.navigate(['/create-treatment/']);
    }

    transition() {
        this.router.navigate(['/treatment'], {
            queryParams:
                {
                    search: this.search,
                    page: this.page,
                    sort: (this.reverse ? 'asc' : 'desc')
                }
        });
        this.loadAll();
    }

    sort() {
        return (this.reverse ? 'asc' : 'desc');
    }

    private onSuccess(data: TreatmentResponse) {
        this.treatments = data.treatments;
        this.pageable = data.pageable;
    }

    private onError(error) {
        this.message = error.message;
        this.staticAlertType = 'danger';
        this.staticAlertClosed = false;
        setTimeout(() => this.staticAlertClosed = true, 4000);
    }
}
