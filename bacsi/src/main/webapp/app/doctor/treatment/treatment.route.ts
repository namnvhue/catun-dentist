import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes} from '@angular/router';
import {UserRouteAccessService} from '../../shared/auth/user-route-access-service';
import {Injectable} from '@angular/core';
import {JhiPaginationUtil} from 'ng-jhipster';
import {HistoryListComponent} from './history-list/history-list.component';
import {HistoryDetailComponent} from './history-detail/history-detail.component';
import {TreatmentListComponent} from "./treatment-list/treatment-list.component";
import {CreateTreatmentComponent} from "./create-treatment/create-treatment.component";

@Injectable()
export class TreatmentResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const TREATMENT_ROUTE: Routes = [
    {
        path: 'treatment',
        component: TreatmentListComponent,
        resolve: {
            'pagingParams': TreatmentResolvePagingParams
        },
        data: {
            authorities: ['ROLE_ADMIN', 'ROLE_DOCTOR','ROLE_RECEPTIONIST'],
            pageTitle: 'CatunDentistApp.treatment.titleTreatment'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'treatment/:treatmentId',
        component: HistoryListComponent,
        data: {
            authorities: ['ROLE_ADMIN', 'ROLE_DOCTOR','ROLE_RECEPTIONIST'],
            pageTitle: 'CatunDentistApp.treatment.titleTreatmentDetailList'
        },
        resolve: {
            'pagingParams': TreatmentResolvePagingParams
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'create-treatment',
        component: CreateTreatmentComponent,
        data: {
            authorities: ['ROLE_ADMIN', 'ROLE_DOCTOR'],
            pageTitle: 'CatunDentistApp.treatment.titleTreatmentDetailList'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'treatment-detail-note/:id',
        component: HistoryDetailComponent,
        data: {
            authorities: ['ROLE_ADMIN', 'ROLE_DOCTOR'],
            pageTitle: 'CatunDentistApp.medicalHistoryDetailEntity.noteTitle'
        },
        canActivate: [UserRouteAccessService]
    }
];
