import {Component, OnInit, ViewChild} from '@angular/core';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {TranslateService} from '@ngx-translate/core';
import {CusinfoEntity} from '../../../shared/model/cusinfo-entity';
import {BookingEntity} from '../../../shared/model/booking-entity';
import * as moment_ from 'moment';
import {ServiceEntityService} from '../../../entities/service-entity/service-entity.service';
import {ServiceEntity} from '../../../entities/service-entity/service-entity.model';
import {
    DateService,
    DD_MM_YYYY_PATTERN,
    DD_PATTERN,
    MM_PATTERN
} from '../../../shared/shared.date.service';
import {AvailabilitySettingService} from '../../../entities/availability-setting/availability-setting.service';
import {DateOffEntity} from '../../../entities/availability-setting/availability-setting.model';
import {TreatmentService} from '../treatment.service';
import {Router} from "@angular/router";

const moment = moment_;
const today = moment();

@Component({
    selector: 'jhi-create-treatment',
    templateUrl: './create-treatment.component.html',
    styleUrls: ['./create-treatment.component.css']
})
export class CreateTreatmentComponent implements OnInit {

    errorMessage: string;

    message;
    staticAlertClosed = false;
    staticAlertType = 'success';

    selectedItems = [];
    dropdownSettings = {};
    serviceList;
    isDataAvailable = false;

    selectedTimeSlot;
    timeSlotDropdownSettings = {};
    timeSlotList;
    isTimeSlotDataAvailable = false;

    cusinfo: CusinfoEntity = new CusinfoEntity();
    booking: BookingEntity;

    public loading = false;
    public isDateOffSelected = false;
    public dateOffSelectedMsg = '';

    @ViewChild('dob')
    dobDp: any; // DOB date picker
    minDate = {year: 1900, month: 1, day: 1};
    maxDate = {year: today.year(), month: today.month() + 1, day: today.date()};

    @ViewChild('date')
    dateDp: any; // Date-book date picker
    bookMinDate = {year: today.year(), month: today.month() + 1, day: today.date()};

    bookDateObj: any = this.bookMinDate;

    dayoffs: DateOffEntity[];
    holidays: DateOffEntity[];

    constructor(private translateService: TranslateService,
                private serviceEntityService: ServiceEntityService,
                private dateService: DateService,
                private availabilitySettingService: AvailabilitySettingService,
                private treatmentService: TreatmentService,
                private router: Router) {
        this.getSettings();
    }

    ngOnInit() {
        this.booking = new BookingEntity();
        this.loadServices();

    }

    book() {
        this.booking.dateVal = this.dateService.joinDateElement(this.bookDateObj, true);
        this.booking.timeVal = this.selectedTimeSlot[0].time;
        this.booking.services = this.selectedItems;
        this.booking.nameVal = this.cusinfo.nameVal;
        this.booking.dobVal = this.cusinfo.getDobStr();
        this.booking.emailVal = this.cusinfo.emailVal;
        this.booking.phoneVal = this.cusinfo.phoneVal;
        this.booking.historyVal = this.cusinfo.historyVal;
        this.booking.currentVal = this.cusinfo.currentVal;
        this.booking.ortherRequest = this.cusinfo.ortherRequest;

        this.loading = true;
        this.treatmentService.addTreatment(this.booking).subscribe(
            (response) => {
                if (response.status === 200) {
                    this.resetData();
                    this.router.navigate(['/treatment']);
                }
            },
            (error) => {
                this.showALert(error.error.errorMessage, 'danger');
                this.loading = false;
            });
    }

    /**
     * Show alert box
     * @param msg
     * @param type
     */
    showALert(msg: string, type: string) {
        this.message = msg;
        this.staticAlertType = type;
        this.staticAlertClosed = false;
        setTimeout(() => this.staticAlertClosed = true, 10000);
    }

    /**
     * Service dropdown's methods
     */
    loadServices() {
        this.serviceList = [];
        this.serviceEntityService.query({
            page: 0,
            size: 9999,
            sort: ['id', 'asc']
        }).subscribe(
            (res: HttpResponse<ServiceEntity[]>) => this.onSuccess(res.body),
            (res: HttpErrorResponse) => this.onError(res.message),
            () => this.initServiceSelect() //  init service select after services-loading
        );
    }

    /**
     * Service dropdown: Init method
     */
    initServiceSelect() {
        this.isTimeSlotDataAvailable = true;
        this.selectedItems = [];
        this.dropdownSettings = {
            singleSelection: false,
            idField: 'id',
            textField: 'name',
            selectAllText: 'Chọn hết',
            unSelectAllText: 'Bỏ chọn hết',
            itemsShowLimit: 3,
            allowSearchFilter: false
        };
    }

    /**
     * Service dropdown: Init method
     */
    initTimeslotSelect() {
        this.isTimeSlotDataAvailable = true;
        this.timeSlotDropdownSettings = {
            singleSelection: true,
            idField: 'id',
            textField: 'time',
            itemsShowLimit: 1,
            allowSearchFilter: true
        };
    }

    dobDpOnClick() {
        this.dobDp.toggle();
        this.dobDp.navigateTo({year: 1990, month: 1});
    }

    dateDpOnClick() {
        this.dateDp.toggle();
    }

    selectBookDate() {
        if (this.dayoffs && this.holidays) {

            if (this.checkDayOffSelected(this.bookDateObj, this.dayoffs) ||
                this.checkDayOffSelected(this.bookDateObj, this.holidays)) {
                this.dateOffSelectedMsg = this.translateService.instant('CatunDentistApp.treatment.create.message.dayoffSelected');
                this.bookDateObj = {};
            } else {
                this.dateOffSelectedMsg = '';
            }
        }
    }

    checkDayOffSelected(date, listDayOff) {
        let tempDate: number;
        let tempMonth: number;
        let flg = false;
        listDayOff.forEach((d) => {
            if (!flg) {
                tempDate = +moment(d.dateOff, DD_MM_YYYY_PATTERN).format(DD_PATTERN);
                tempMonth = +moment(d.dateOff, DD_MM_YYYY_PATTERN).format(MM_PATTERN);
                if (tempDate == date.day && tempMonth == date.month) {
                    flg = true;
                    return;
                } else {
                    flg = false;
                    return;
                }
            }
        });
        return flg;
    }

    /**
     * Fetch saved-setting from server
     */
    getSettings() {
        this.availabilitySettingService.fetchSettingData().subscribe(
            (res) => {
                if (res.timeWorking) {
                    this.dayoffs = res.dateOffs;
                    this.holidays = res.holidayOffs;
                    this.timeSlotList = this.availabilitySettingService.slitWorkTime(res.timeWorking);
                    this.initTimeslotSelect();
                }
            },
            (err) => {
                console.log(err.errorMessage); // TODO
            }
        );
    }

    private resetData() {
        this.booking = new BookingEntity();
        this.cusinfo = new CusinfoEntity();
        this.bookDateObj = {};
        this.selectedTimeSlot = [];
        this.loading = false;
    }

    private onSuccess(data) {
        for (let i = 0; i < data.length; i++) {
            let service = {id: data[i].id, name: data[i].name};
            this.serviceList.push(service);
        }
        this.isDataAvailable = true;
    }

    private onError(error) {
        console.log(error); // TODO
    }
}
