import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {HttpResponse} from '@angular/common/http';
import {TranslateService} from '@ngx-translate/core';
import {TreatmentService} from '../treatment.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NgxGalleryAnimation, NgxGalleryImage, NgxGalleryOptions, NgxGalleryOrder} from 'ngx-gallery';
import {HistoryDetailRequest, HistoryDetailResponse, Patient} from '../treatment.model';
import {DateService, DD_MM_YYYY_HH_MM_PATTERN} from '../../../shared/shared.date.service';
import {Principal} from '../../../shared';
import {Treatment, TreatmentDetail} from "../../treatment-history/treatment-history.model";
import {TreatmentHistoryService} from "../../treatment-history/treatment-history.service";

export const CREATE = 'new';
const BASE_64_IMG_PREFIX = 'data:image/jpeg;base64,';

@Component({
    selector: 'jhi-confirm',
    templateUrl: './history-detail.component.html',
    styleUrls: ['./history-detail.component.css']
})
export class HistoryDetailComponent implements OnInit, OnDestroy {

    private patientSubscription;
    private treatmentDetailSubscription;
    private treatmentSubscription;

    //  Values for POST request
    historyId: any;
    treatmentDetailId: any;
    patient: Patient;
    treatmentDetail: TreatmentDetail;
    treatment: Treatment;
    uploadedFiles: any[] = [];
    detailsReq: HistoryDetailRequest = new HistoryDetailRequest();

    //  DOM values
    isAddNew = true;
    galleryOptions: NgxGalleryOptions[];
    galleryImages: NgxGalleryImage[] = [];
    minuteStep = 15;

    discountValues = [
        5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 80, 85, 90, 95, 100
    ];
    doctorName: any;
    timeIn = {hour: 6, minute: 0};
    timeOut = {hour: 6, minute: 0};
    @ViewChild('dateInDp')
    dateInDp: any; //  Date-book date picker
    @ViewChild('dateOutDp')
    dateOutDp: any; //  Date-book date picker
    dateInVal: any;
    dateOutVal: any;

    timeValid = false;
    timeInMsg: any;
    timeOutMsg: any;

    message;
    staticAlertClosed = false;
    staticAlertType = 'success';

    constructor(private activatedRoute: ActivatedRoute,
                private router: Router,
                private translateService: TranslateService,
                private treatmentService: TreatmentService,
                private treatmentHistoryService: TreatmentHistoryService,
                private dateService: DateService,
                private principal: Principal) {
        this.initGalleryOptions();
        this.getLoginUserInfo();
    }

    ngOnInit() {
        this.getSelectedValues();
        this.historyId = this.activatedRoute.snapshot.paramMap.get('id');
        this.isAddNew = this.historyId == CREATE;
        if (!this.isAddNew) {
            this.treatmentDetailId = this.historyId;
        }
        this.initMedicalHistoryDetails();
    }

    ngOnDestroy() {
        if (this.patientSubscription) {
            this.patientSubscription.unsubscribe();
        }
    }

    getSelectedValues() {
        this.patientSubscription = this.treatmentHistoryService.selectedPatient.subscribe((patient) => {
            this.patient = patient;
            this.detailsReq.userId = this.patient.id;
        });
        this.treatmentDetailSubscription = this.treatmentHistoryService.selectedTreatmentDetail.subscribe((td) => {
            this.treatmentDetail = td;
            this.detailsReq.treatmentDetailId = this.treatmentDetail.treatmentDetailId;
            this.treatmentDetailId = this.treatmentDetail.treatmentDetailId;
        });

        this.treatmentSubscription = this.treatmentHistoryService.selectedTreatment.subscribe((t) => {
            this.treatment = t;
        });
    }

    checkSelectedValue() {
        if (!this.patient || !this.patient.id) {
            this.router.navigate(['/treatment']);
        }
        if (!this.treatmentDetail || !this.treatmentDetail.treatmentDetailId) {
            this.router.navigate(['/treatment/', this.patient.id]);
        }
    }

    initMedicalHistoryDetails() {
        this.uploadedFiles = [];
        this.galleryImages = [];
        if (!this.isAddNew) {
            this.detailsReq.id = this.historyId;
            this.treatmentService.fetchMedicalHistoryDetail(this.historyId).subscribe(
                (res) => {
                    this.mappingInitData(res);
                },
                (err) => {
                    this.showALert(err.error.message, 'danger');
                }
            );
        } else {
            let temp = this.dateService.today().toDate();
            // let temp = this.dateService.addDaysToToday(-14);
            this.dateInVal = {year: temp.getFullYear(), month: temp.getMonth() + 1, day: temp.getDate()};
            this.dateOutVal = {year: temp.getFullYear(), month: temp.getMonth() + 1, day: temp.getDate()};
        }
    }

    getLoginUserInfo() {
        this.principal.identity(false).then((account) => {
            if (account !== null) {
                this.doctorName = account.lastName + ' ' + account.firstName;
            }
        });
    }

    checkTime() {
        const msg = this.translateService.instant('CatunDentistApp.medicalHistoryDetailEntity.validateMessage.timeInvalid');
        this.timeValid = false;

        let inValid = false;
        let outValid = false;
        if (this.timeIn) {
            if (this.timeIn.hour >= 6 && this.timeIn.hour <= 23) {
                this.timeInMsg = null;
                inValid = true;
            } else {
                this.timeInMsg = msg;
            }
        }
        if (this.timeOut) {
            if (this.timeOut.hour >= 6 && this.timeOut.hour <= 23) {
                this.timeOutMsg = null;
                outValid = true;
            } else {
                this.timeOutMsg = msg;
            }
        }
        this.timeValid = inValid && outValid;
        this.calWorkingTime();
    }

    calWorkingTime() {
        if (this.timeValid) {
            this.detailsReq.timeWorking = (this.timeOut.hour - this.timeIn.hour) + '';
        }
    }

    mappingInitData(res: HistoryDetailResponse) {
        this.detailsReq.treatmentDetailId = res.medicalHistory.treatmentDetailId;
        this.detailsReq.userId = res.medicalHistory.userId;
        this.detailsReq.timeIn = res.medicalHistory.timeIn;
        this.detailsReq.timeOut = res.medicalHistory.timeOut;
        this.detailsReq.timeWorking = res.medicalHistory.timeAmount;
        // this.detailsReq.tipName = res.medicalHistory.tipName;
        // this.detailsReq.totalCost = res.medicalHistory.therapeuticCost;
        // this.detailsReq.discount = res.medicalHistory.discount;
        // this.detailsReq.realCost = res.medicalHistory.realCost;
        this.detailsReq.noted = res.medicalHistory.description;
        this.doctorName = res.medicalHistory.doctorName;

        this.dateInVal = this.dateService.getDatePickerObject(res.medicalHistory.timeIn, DD_MM_YYYY_HH_MM_PATTERN);
        this.dateOutVal = this.dateService.getDatePickerObject(res.medicalHistory.timeOut, DD_MM_YYYY_HH_MM_PATTERN);
        this.timeIn = this.dateService.getHourAndMinFromString(res.medicalHistory.timeIn, DD_MM_YYYY_HH_MM_PATTERN);
        this.timeOut = this.dateService.getHourAndMinFromString(res.medicalHistory.timeOut, DD_MM_YYYY_HH_MM_PATTERN);

        if (res.images && res.images.length > 0) {
            for (let i = 0; i < res.images.length; i++) {
                this.galleryImages.push({
                    small: BASE_64_IMG_PREFIX + res.images[i],
                    medium: BASE_64_IMG_PREFIX + res.images[i],
                    big: BASE_64_IMG_PREFIX + res.images[i]
                });
                this.addFile(this.base64ToBlob(res.images[i], 'jpg'));
            }
        }
    }

    save() {
        this.createHistoryDetailRequest();
        this.treatmentService.saveHistoryDetail(this.detailsReq).subscribe(
            (response: HttpResponse<any>) => {
                if (response.status == 200) {
                    this.router.navigate(['treatment-history/' + this.treatment.id]);
                }
            },
            (e) => {
                this.showALert(e.error.message, 'danger');
            });
    }

    /**
     * Show alert box
     * @param msg
     * @param type
     */
    showALert(msg: string, type: string) {
        this.message = msg;
        this.staticAlertType = type;
        this.staticAlertClosed = false;
        setTimeout(() => this.staticAlertClosed = true, 4000);
    }

    reset() {
        this.dateInVal = {};
        this.dateOutVal = {};
        this.timeIn = {hour: 6, minute: 0};
        this.timeOut = {hour: 6, minute: 0};
        this.uploadedFiles = [];
        this.galleryImages = [];
        this.detailsReq = new HistoryDetailRequest();
    }

    createHistoryDetailRequest() {
        this.detailsReq.timeIn = this.generateDateTimeStr(this.dateInVal, this.timeIn);
        this.detailsReq.timeOut = this.generateDateTimeStr(this.dateOutVal, this.timeOut);
        this.detailsReq.files = this.uploadedFiles.map((f) => f.data);
    }

    /**
     * Format: dd/MM/yyyy HH:mm
     * @param dateObj
     * @param timeObj
     */
    generateDateTimeStr(dateObj: any, timeObj: any) {
        let result = '';
        if (dateObj && timeObj) {
            result = this.dateService.joinDateElement(dateObj, true);
            result += ' ' + this.dateService.joinTimeElement(timeObj);
        }
        return result;
    }

    /**
     * Event when select files
     * @param ev
     */
    onFileSelect(ev: any, type: string) {
        let files: any;
        if (type == 'select') {          // select by input
            files = ev.target.files;
        } else {                         // select by drop
            ev.preventDefault();
            ev.stopPropagation();
            files = ev.dataTransfer.files;
        }

        for (let i = 0; i < files.length; i++) {
            if (this.checkFileExt(files[i].name)) {
                this.addImage(files[i]);
                this.addFile(files[i]);
            }
        }
    }

    /**
     * Only JPG, JPEG, PNG allowed
     * @param fileName
     */
    checkFileExt(fileName) {
        const ext = fileName.split('.').pop().toLowerCase();
        if (ext.includes('jpg') || ext.includes('jpeg') || ext.includes('png')) {
            return true;
        }
        return false;
    }

    /**
     * Add uploaded file to files arrays for form-data when POST to server
     * @param imageFile
     */
    addFile(imageFile: any) {
        this.uploadedFiles.push({
            name: imageFile.name,
            data: imageFile
        });
    }

    /**
     * Remove a file from form-data files array
     * @param fileName
     */
    removeFile(fileName: string) {
        this.uploadedFiles = this.uploadedFiles.filter((file) => file.name != fileName);
    }

    /**
     * Add seelcted image for preview
     * @param imageFile
     */
    addImage(imageFile: any) {
        let reader = new FileReader();
        let image: any;

        reader.onload = (e: any) => {
            image = e.target.result;
            this.galleryImages.push({
                description: imageFile.name,
                small: image,
                medium: image,
                big: image
            });
        };
        reader.readAsDataURL(imageFile);
    }

    /**
     * Delete selected image from review & form-data arrays
     * @param event
     * @param index
     */
    deleteImage(event, index): void {
        this.removeFile(this.galleryImages[index].description);
        this.galleryImages.splice(index, 1);
    }

    initGalleryOptions() {
        this.galleryOptions = [
            {
                height: '90%',
                thumbnailMargin: 2,
                thumbnailsMargin: 2,
                thumbnailsOrder: NgxGalleryOrder.Row,
                thumbnailsColumns: 3,
                thumbnailsRows: 1,
                previewZoom: true,
                imageAnimation: NgxGalleryAnimation.Slide,
                thumbnailActions: [{icon: 'fa fa-trash', onClick: this.deleteImage.bind(this), titleText: 'Remove'}]
            }
            ,
            //  max-width 800
            {
                breakpoint: 800,
                width: '100%',
                height: '600px',
                imagePercent: 80,
                thumbnailsPercent: 20,
                thumbnailsMargin: 20,
                thumbnailMargin: 20
            },
            //  max-width 400
            {
                breakpoint: 400,
                preview: false
            }
        ];
    }

    /**
     * Prevent browser from open dropped file
     * @param ev
     */
    allowDrop(ev) {
        ev.preventDefault();
    }

    dateInDpOnClick() {
        this.dateInDp.toggle();
    }

    dateOutDpOnClick() {
        this.dateOutDp.toggle();
    }

    base64ToBlob(b64Data, contentType = '', sliceSize = 512) {
        b64Data = b64Data.replace(/\s/g, ''); //IE compatibility...
        let byteCharacters = atob(b64Data);
        let byteArrays = [];
        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            let slice = byteCharacters.slice(offset, offset + sliceSize);

            let byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            let byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        return new Blob(byteArrays, {type: contentType});
    }
}
