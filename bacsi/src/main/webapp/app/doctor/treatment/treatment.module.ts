import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CatunDentistAppSharedModule} from '../../shared/shared.module';
import {TREATMENT_ROUTE} from './treatment.route';
import {TreatmentListComponent} from './treatment-list/treatment-list.component';
import {HistoryListComponent} from './history-list/history-list.component';
import {HistoryDetailComponent} from './history-detail/history-detail.component';
import {TreatmentService} from './treatment.service';
import {DateService} from '../../shared/shared.date.service';
import {TreatmentResolvePagingParams} from './treatment.route';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';
import { NgxGalleryModule } from 'ngx-gallery';
import {CreateTreatmentComponent} from './create-treatment/create-treatment.component';
import {LoadingModule} from 'ngx-loading';
import {AvailabilitySettingService} from '../../entities/availability-setting/availability-setting.service';

@NgModule({
    imports: [
        RouterModule.forChild(TREATMENT_ROUTE),
        CatunDentistAppSharedModule,
        NgMultiSelectDropDownModule,
        NgxGalleryModule,
        CatunDentistAppSharedModule,
        NgMultiSelectDropDownModule,
        LoadingModule
    ],
    declarations: [
        CreateTreatmentComponent,
        TreatmentListComponent,
        HistoryListComponent,
        HistoryDetailComponent
    ],
    entryComponents: [],
    providers: [
        AvailabilitySettingService,
        TreatmentService,
        TreatmentResolvePagingParams,
        DateService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CatunDentistTreatmentModule {
}
