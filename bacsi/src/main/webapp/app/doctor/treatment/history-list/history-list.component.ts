import {Component, OnDestroy, OnInit} from '@angular/core';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {TranslateService} from '@ngx-translate/core';
import {TreatmentService} from '../treatment.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Treatment, TreatmentDetail} from '../../treatment-history/treatment-history.model';
import {Patient, TreatmentDetailResponse} from '../treatment.model';
import {NgbModal, NgbModalRef} from "@ng-bootstrap/ng-bootstrap";
import {ExpenditureRequest} from "../../../reception/expenditure/expenditure.model";
import {DateService} from "../../../shared/shared.date.service";
import {ExpenditureService} from "../../../reception/expenditure/expenditure.service";
import {Principal} from "../../../shared/auth/principal.service";

@Component({
    selector: 'jhi-confirm',
    templateUrl: './history-list.component.html',
    styleUrls: ['./history-list.component.css']
})
export class HistoryListComponent implements OnInit, OnDestroy {

    private subscription;
    selectedPatient: Patient;
    selectedTreatment: Treatment;

    treatmentId: any;
    histories: TreatmentDetail[];

    message;
    staticAlertClosed = false;
    staticAlertType = 'success';

    tempTreatmentDetail: TreatmentDetail;
    deleteModal: NgbModalRef;

    isDoctor: boolean;
    isReception: boolean;
    authorities: any;

    viewingTreatmentDetail: TreatmentDetailResponse;
    viewModal: NgbModalRef;

    paymentTreatmentDetail: TreatmentDetail;
    paymentModal: NgbModalRef;
    paymentAmount: number = 0;
    paymentInputEnabled: boolean = true;
    paymentType: string = 'partial';

    constructor(private activatedRoute: ActivatedRoute,
                private translateService: TranslateService,
                private modalService: NgbModal,
                private treatmentService: TreatmentService,
                private router: Router,
                private dateService: DateService,
                private expenditureService: ExpenditureService,
                private principal: Principal) {
        this.subscription = this.treatmentService.selectedPatient.subscribe((patient) => {
            if (!patient || !patient.id) {
                this.router.navigate(['/treatment']);
            }
            this.selectedPatient = patient;
        });
    }

    ngOnInit() {
        this.loadAll();
        this.getRole();
    }

    private getRole() {
        this.isDoctor = false;
        this.isReception = false;

        this.principal.identity(false).then((account) => {
            if (account !== null) {
                this.authorities = account.authorities;
                if (account.authorities.indexOf('ROLE_RECEPTIONIST') > -1) {
                    this.isReception = true;
                } else if (account.authorities.indexOf('ROLE_DOCTOR') > -1) {
                    this.isDoctor = true;
                }
            }
        });
    }

    loadAll() {
        this.treatmentId = this.activatedRoute.snapshot.paramMap.get('treatmentId');
        if (!this.treatmentId) {
            this.router.navigate(['/treatment']);
        }
        this.treatmentService.fetchTreatmentDetail(this.treatmentId).subscribe(
            (res: TreatmentDetail[]) => {
                this.onSuccess(res);
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    viewDetail(content: any, td: TreatmentDetail) {
        this.treatmentService.fetchTreatmentDetailResponse(td.treatmentDetailId).subscribe(
            (res: TreatmentDetailResponse) => {
                this.viewingTreatmentDetail = res;
                this.viewModal = this.modalService.open(content, {size: 'lg'});
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    closeViewTreatmentDetailModal() {
        this.viewModal.close();
        this.viewingTreatmentDetail = null;
    }

    createDetail() {
        this.router.navigate(['/treatment-detail/']);
    }

    deleteTreatmentDetail(content: any, td: TreatmentDetail) {
        this.tempTreatmentDetail = td;
        this.deleteModal = this.modalService.open(content);
    }

    confirmDeleteTreatmentDetail() {
        this.treatmentService.deleteTreatmentDetail(this.tempTreatmentDetail.treatmentDetailId).subscribe(
            (res) => {
                this.tempTreatmentDetail = null;
                this.deleteModal.close();
                this.loadAll();
            },
            (err) => {
                this.showALert(err.error.message, 'danger');
            }
        );
    }

    closeDeleteTreatmenntModal() {
        this.tempTreatmentDetail = null;
        this.deleteModal.close();
    }

    downloadPdf(fileLink: string) {
        this.treatmentService.downloadTreatmentDetail(fileLink)
            .subscribe(
                (response: HttpResponse<any>) => {
                    const blob = new Blob([response.body], {type: 'application/pdf'});
                    const url = window.URL.createObjectURL(blob);
                    window.open(url);
                },
                (e) => {
                    console.log(e.message);
                });
    }

    openPaymentModal(content: any, td: TreatmentDetail) {
        this.paymentTreatmentDetail = td;
        this.paymentModal = this.modalService.open(content, {size: 'lg'});
    }

    closePaymentModal() {
        this.paymentModal.close();
        this.paymentTreatmentDetail = null;
        this.paymentAmount = 0;
    }

    savePayment() {
        if (this.paymentType == 'full') {
            this.saveFullPayment();
        } else {
            this.savePartialPayment();
        }
    }

    pickPaymentType(type: string) {
        this.paymentType = type;
        if (type == 'full') {
            this.paymentInputEnabled = false;
            let realCost = this.paymentTreatmentDetail.realCost.split('.').join("");
            let paid = this.paymentTreatmentDetail.paymented.split('.').join("");
            this.paymentAmount = +realCost - +paid;
        } else {
            this.paymentInputEnabled = true;
        }
    }

    saveFullPayment() {
        this.treatmentService.changePaymentStatus(this.paymentTreatmentDetail.treatmentDetailId).subscribe(
            (response) => {
                this.closePaymentModal();
                this.loadAll();
                let message = this.translateService.instant('CatunDentistApp.treatment.message.collectMoneySuccess');
                this.showALert(message, 'success');
            },
            (error) => {
                this.onError(error);
            });
    }

    savePartialPayment() {
        let e: ExpenditureRequest = new ExpenditureRequest();
        e.treatmentDetailId = this.paymentTreatmentDetail.treatmentDetailId;
        e.name = this.paymentTreatmentDetail.services;
        e.value = this.paymentAmount + '';
        e.type = 'THU';
        e.createDate = this.dateService.joinDateElement(this.dateService.todayDatePickerObj(), true);
        this.expenditureService.saveExpenditure(e).subscribe(
            (response) => {
                if (response.status === 200) {
                    this.closePaymentModal();
                    this.loadAll();
                    let message = this.translateService.instant('CatunDentistApp.treatment.message.collectMoneySuccess');
                    this.showALert(message, 'success');
                }
            },
            (error) => {
                this.onError(error);
            });
    }

    /**
     * Show alert box
     * @param msg
     * @param type
     */
    showALert(msg: string, type: string) {
        this.message = msg;
        this.staticAlertType = type;
        this.staticAlertClosed = false;
        setTimeout(() => this.staticAlertClosed = true, 4000);
    }

    private onSuccess(data: TreatmentDetail[]) {
        this.histories = data;
    }

    private onError(error) {
        this.showALert(error.message, 'danger');
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
