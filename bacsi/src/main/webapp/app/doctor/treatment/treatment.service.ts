import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';
import {HistoryDetailRequest, HistoryDetailResponse, Patient, TreatmentDetailResponse} from './treatment.model';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Treatment, TreatmentDetail} from "../treatment-history/treatment-history.model";

@Injectable()
export class TreatmentService {
    private resourceUrl = SERVER_API_URL + '/doctor';

    private patient: BehaviorSubject<Patient> = new BehaviorSubject(new Patient());
    selectedPatient = this.patient.asObservable();

    private treatment: BehaviorSubject<Treatment> = new BehaviorSubject(new Treatment());
    selectedTreatment = this.treatment.asObservable();

    private treatmentDetail: BehaviorSubject<TreatmentDetail> = new BehaviorSubject(new TreatmentDetail());
    selectedTreatmentDetail = this.treatmentDetail.asObservable();

    constructor(private http: HttpClient) {
    }

    setPatient(patient: Patient) {
        this.patient.next(patient);
    }

    setTreatment(treatment: Treatment) {
        this.treatment.next(treatment);
    }

    setTreatmentDetail(treatmentDetail: TreatmentDetail) {
        this.treatmentDetail.next(treatmentDetail);
    }

    fetchTreatment(treatmentId: string): Observable<any> {
        const url = this.resourceUrl + '/treatment-history/booking/' + treatmentId;
        return this.http.get<Treatment>(url);
    }

    fetchPatient(patientId: string): Observable<any> {
        const url = this.resourceUrl + '/treatment-history/patient/' + patientId;
        return this.http.get<Patient>(url);
    }

    fetchTreatmentDetailNote(treatmentDetailId: string): Observable<any> {
        const url = this.resourceUrl + '/medical-history/' + treatmentDetailId;
        return this.http.get<HistoryDetailResponse>(url);
    }

    fetchTreatmentDetailResponse(treatmentDetailId: string): Observable<any> {
        const url = this.resourceUrl + '/treatment-detail-show/' + treatmentDetailId;
        return this.http.get<TreatmentDetailResponse>(url);
    }

    deleteTreatment(id: string): Observable<any> {
        const url = this.resourceUrl + '/treatment-history-delete/' + id;
        return this.http.get<HistoryDetailResponse>(url);
    }

    changeTreatmentStatus(request): Observable<any> {
        return this.http.post(
            this.resourceUrl + '/treatment-history/change-status', JSON.stringify(request), {
                headers: new HttpHeaders({'Content-Type': 'application/json'}),
                observe: 'response'
            });
    }

    downloadTreatmentDetail(link: string): Observable<any> {
        return this.http.get(this.resourceUrl + link, {
            responseType: 'blob',
            headers: new HttpHeaders({'Content-Type': 'application/json'}),
            observe: 'response'
        });
    }

    deleteTreatmentDetail(id: any) {
        const url = this.resourceUrl + '/medical-history-delete/' + id;
        return this.http.get<HistoryDetailResponse>(url);
    }

    addTreatment(treatment): Observable<any> {
        return this.http.post(
            this.resourceUrl + '/treatment/create', JSON.stringify(treatment), {
                headers: new HttpHeaders({'Content-Type': 'application/json'}),
                observe: 'response'
            });
    }

    fetchUnfinishedTreatments(request): Observable<any> {
        return this.http.post(
            this.resourceUrl + '/treatment-history', JSON.stringify(request), {
                headers: new HttpHeaders({'Content-Type': 'application/json'}),
                observe: 'response'
            });
    }

    changePaymentStatus(id): Observable<any> {
        const url = this.resourceUrl + '/treatment-detail-paymented/' + id;
        return this.http.get<any>(url);
    }

    fetchTreatmentDetail(id): Observable<TreatmentDetail[]> {
        const url = this.resourceUrl + '/treatment-detail/' + id;
        return this.http.get<TreatmentDetail[]>(url);
    }

    fetchTreatments(request): Observable<any> {
        return this.http.post(
            this.resourceUrl + '/treatment-history/all', JSON.stringify(request), {
                headers: new HttpHeaders({'Content-Type': 'application/json'}),
                observe: 'response'
            });
    }

    fetchPatients(request): Observable<any> {
        return this.http.post(
            this.resourceUrl + '/medical-history-patients', JSON.stringify(request), {
                headers: new HttpHeaders({'Content-Type': 'application/json'}),
                observe: 'response'
            });
    }

    fetchMedicalHistories(request): Observable<any> {
        return this.http.post(
            this.resourceUrl + '/medical-history/get', JSON.stringify(request), {
                headers: new HttpHeaders({'Content-Type': 'application/json'}),
                observe: 'response'
            });
    }

    fetchMedicalHistoryDetail(id): Observable<HistoryDetailResponse> {
        const url = this.resourceUrl + '/medical-history/' + id;
        return this.http.get<HistoryDetailResponse>(url);
    }

    saveHistoryDetail(request: HistoryDetailRequest) {
        const formData = new FormData();
        formData.append('id', request.id);
        formData.append('treatmentDetailId', request.treatmentDetailId);
        formData.append('userId', request.userId);
        formData.append('timeIn', request.timeIn);
        formData.append('timeOut', request.timeOut);
        formData.append('timeWorking', request.timeWorking);
        formData.append('noted', request.noted);
        // formData.append('tipName', request.tipName);
        // formData.append('totalCost', '' + request.totalCost);
        // formData.append('discount', '' + request.discount);
        // formData.append('realCost', '' + request.realCost);
        for (let i = 0; i < request.files.length; i++) {
            formData.append('files', request.files[i]);
        }
        return this.http.post(this.resourceUrl + '/medical-history/create', formData, {
            observe: 'response'
        });
    }

    deleteMedicalHistories(deleteId): Observable<any> {
        return this.http.get<HistoryDetailResponse>(this.resourceUrl + '/medical-history/delete/' + deleteId);
    }
}
