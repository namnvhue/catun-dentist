export class Patient {
    id?: string;
    name?: string;
    phone?: string;
    address?: string;
    email?: string;
    birthDay?: string;

}

export class UserResponse {
    name?: string;
    phone?: string;
}

export class Pageable {
    totalPage?: number;
    currentPage?: number;
    numberPages?: number[];
}

export class MedicalHistory {
    id?: string;
    tipName?: string;
    doctorName?: string;
    timeIn?: string;
    timeOut?: string;
    amount?: number;
    totalCost?: number;
    discount?: number;
    realCost?: number;
    description?: string;
}

export class HistoryDetailRequest {
    id? = '';
    treatmentDetailId?: string;
    // tipName?: string;
    userId?: string;
    timeIn?: string;
    timeOut?: string;
    timeWorking?: string;
    noted?: string;
    files: any[];
    // totalCost?: number;
    // discount?: number;
    // realCost?: number;
}

export class HistoryDetail {
    id?: string;
    treatmentDetailId?: string;
    userId?: string;
    tipName?: string;
    doctorName?: string;
    timeIn?: string;
    timeOut?: string;
    timeAmount?: string;
    description?: string;
    therapeuticCost?: number;
    discount?: number;
    realCost?: number;
}

export class HistoryDetailResponse {
    medicalHistory?: HistoryDetail;
    images?: string[];
}

export class TreatmentDetailResponse {
    treatmentDetailId?: string;
    totalCost?: string;
    dicount?: string;
    realCost?: string;
    noteFromDoctor?: string;
    noteFromPatient?: string;
    linkDownload?: string;
    createdBy?: string;
    createDate?: string;
    services?: Service[];
    paymented?: string;
    finishPaymented?: boolean;
}

export class Service {
    id?: string;
    relatedProducts?: string;
    name?: string;
    servicePrice?: string;
    toothNames?: string;
    amounts?: string;
}
