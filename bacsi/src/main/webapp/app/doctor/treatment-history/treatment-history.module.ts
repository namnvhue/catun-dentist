import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule} from '@angular/router';
import { CatunDentistAppSharedModule } from '../../shared/shared.module';
import {TREATMENT_HISTORY_ROUTE} from './treatment-history.route';
import {TreatmentHistoryComponent} from './treatment-history.component';
import {TreatmentHistoryService} from './treatment-history.service';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import {TreatmentHistoryResolvePagingParams} from './treatment-history.route';
import {TreatmentNewfeedsComponent} from './treatment-newfeeds/treatment-newfeeds.component';

@NgModule({
    imports: [
        RouterModule.forChild(TREATMENT_HISTORY_ROUTE),
        CatunDentistAppSharedModule,
        NgMultiSelectDropDownModule
    ],
    declarations: [
        TreatmentHistoryComponent,
        TreatmentNewfeedsComponent
    ],
    entryComponents: [
    ],
    providers: [
        TreatmentHistoryService,
        TreatmentHistoryResolvePagingParams
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CatunDentistTreatmentHistoryModule {}
