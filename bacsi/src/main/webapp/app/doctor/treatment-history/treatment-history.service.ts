import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Patient} from "../treatment/treatment.model";
import {Treatment, TreatmentDetail} from "./treatment-history.model";

@Injectable()
export class TreatmentHistoryService {
    private resourceUrl = SERVER_API_URL + '/doctor';

    private patient: BehaviorSubject<Patient> = new BehaviorSubject(new Patient());
    selectedPatient = this.patient.asObservable();

    private treatment: BehaviorSubject<Treatment> = new BehaviorSubject(new Treatment());
    selectedTreatment = this.treatment.asObservable();

    private treatmentDetail: BehaviorSubject<TreatmentDetail> = new BehaviorSubject(new TreatmentDetail());
    selectedTreatmentDetail = this.treatmentDetail.asObservable();

    constructor(private http: HttpClient) {
    }

    setPatient(patient: Patient) {
        this.patient.next(patient);
    }

    setTreatment(treatment: Treatment) {
        this.treatment.next(treatment);
    }

    setTreatmentDetail(treatmentDetail: TreatmentDetail) {
        this.treatmentDetail.next(treatmentDetail);
    }

    fetchTreatments(request): Observable<any> {
        return this.http.post(
            this.resourceUrl + '/treatment-history/all', JSON.stringify(request), {
                headers: new HttpHeaders({'Content-Type': 'application/json'}),
                observe: 'response'
            });
    }
}
