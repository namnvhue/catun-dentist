import {ActivatedRouteSnapshot, Resolve, Route, RouterStateSnapshot, Routes} from '@angular/router';
import {UserRouteAccessService} from '../../shared/auth/user-route-access-service';
import {TreatmentHistoryComponent} from './treatment-history.component';
import {Injectable} from '@angular/core';
import {JhiPaginationUtil} from 'ng-jhipster';
import {TreatmentNewfeedsComponent} from './treatment-newfeeds/treatment-newfeeds.component';


@Injectable()
export class TreatmentHistoryResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const TREATMENT_HISTORY_ROUTE: Routes = [
    {
        path: 'treatment-history',
        component: TreatmentHistoryComponent,
        resolve: {
            'pagingParams': TreatmentHistoryResolvePagingParams
        },
        data: {
            authorities: ['ROLE_ADMIN', 'ROLE_DOCTOR'],
            pageTitle: 'CatunDentistApp.treatment.titleTreatmentHistory'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'treatment-history/:id',
        component: TreatmentNewfeedsComponent,
        data: {
            authorities: ['ROLE_ADMIN', 'ROLE_DOCTOR'],
            pageTitle: 'CatunDentistApp.treatment.titleTreatmentHistory'
        },
        canActivate: [UserRouteAccessService]
    }
];
