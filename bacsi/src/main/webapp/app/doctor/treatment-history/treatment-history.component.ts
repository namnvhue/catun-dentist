import {Component, OnInit} from '@angular/core';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {TranslateService} from '@ngx-translate/core';
import {TreatmentHistoryService} from './treatment-history.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Pageable, Treatment, TreatmentResponse} from './treatment-history.model';
import {TreatmentService} from '../treatment/treatment.service';
import {Patient} from "../treatment/treatment.model";

@Component({
    selector: 'jhi-confirm',
    templateUrl: './treatment-history.component.html',
    styleUrls: ['./treatment-history.component.css']
})
export class TreatmentHistoryComponent implements OnInit {

    routeData: any;

    treatments: Treatment[];
    pageable: Pageable;

    predicate: any;
    reverse: any;
    page: any = 1;
    search: any = '';

    message;
    staticAlertClosed = false;
    staticAlertType = 'success';

    constructor(private activatedRoute: ActivatedRoute,
                private router: Router,
                private translateService: TranslateService,
                private treatmentHistoryService: TreatmentHistoryService,
                private treatmentService: TreatmentService) {
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
    }

    ngOnInit() {
        this.loadAll();
    }

    loadAll() {
        this.treatmentHistoryService.fetchTreatments({
            search: this.search,
            page: this.page,
            sort: this.reverse ? 'asc' : 'desc'
        }).subscribe(
            (res: HttpResponse<TreatmentResponse>) => {
                this.onSuccess(res.body);
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    selectTreatment(treatment: Treatment) {
        this.setSelectedValue(treatment);
        this.router.navigate(['/treatment-history/' + treatment.id]);
    }

    private setSelectedValue(treatment: Treatment) {
        this.treatmentHistoryService.setTreatment(treatment);
        let patient = new Patient();
        patient.id = treatment.patientId;
        patient.phone = treatment.phone;
        patient.email = treatment.email;
        patient.name = treatment.name;
        patient.birthDay = treatment.birthDay;
        this.treatmentHistoryService.setPatient(patient);
    }

    transition() {
        this.router.navigate(['/treatment-history'], {
            queryParams:
                {
                    search: this.search,
                    page: this.page,
                    sort: (this.reverse ? 'asc' : 'desc')
                }
        });
        this.loadAll();
    }

    sort() {
        return (this.reverse ? 'asc' : 'desc');
    }

    revertTreatment(t: Treatment) {
        this.treatmentService.changeTreatmentStatus({
            treatmentId: t.id,
            finishFlag: false
        }).subscribe(
            (res: HttpResponse<any>) => {
                this.router.navigate(['/treatment']);
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    private onSuccess(data: TreatmentResponse) {
        this.treatments = data.treatments;
        this.pageable = data.pageable;
    }

    private onError(error) {
        this.message = error.message;
        this.staticAlertType = 'danger';
        this.staticAlertClosed = false;
        setTimeout(() => this.staticAlertClosed = true, 4000);
    }
}
