import {HistoryDetailResponse} from "../treatment/treatment.model";

export class TreatmentResponse {
    treatments?: Treatment[];
    pageable?: Pageable;
}

export class Treatment {
    id?: string;
    patientId?: string;
    name?: string;
    birthDay?: string;
    phone?: string;
    email?: string;
    doctorName?: string;
    services?: string;
    treatmentTimeFrom?: string;
    treatmentTimeTo?: string;
    status?: string;
}

export class TreatmentDetail {
    treatmentDetailId?: string;
    totalCost?: string;
    dicount?: string;
    realCost?: string;
    noteFromDoctor?: string;
    noteFromPatient?: string;
    linkDownload?: string;
    createdBy?: string;
    createDate?: string;
    services?: string;
    paymented?: string;
    finishPaymented?: boolean;
}

export class Pageable {
    totalPage?: number;
    currentPage?: number;
    numberPages?: number[];
}

export class TreatmentNewFeeds {
    treatmentDetail?: TreatmentDetail;
    detailNote?: HistoryDetailResponse;
}



