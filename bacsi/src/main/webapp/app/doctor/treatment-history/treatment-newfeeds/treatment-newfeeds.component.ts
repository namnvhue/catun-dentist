import {Component, OnInit, ViewChild} from '@angular/core';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {TranslateService} from '@ngx-translate/core';
import {TreatmentService} from '../../treatment/treatment.service';
import {Router} from "@angular/router";
import {TreatmentHistoryService} from "../treatment-history.service";
import {HistoryDetailResponse, Patient} from "../../treatment/treatment.model";
import {Treatment, TreatmentDetail, TreatmentNewFeeds} from "../treatment-history.model";

@Component({
    selector: 'jhi-treatment-newfeeds',
    templateUrl: './treatment-newfeeds.component.html',
    styleUrls: ['./treatment-newfeeds.component.css']
})
export class TreatmentNewfeedsComponent implements OnInit {

    BASE_64_IMG_PREFIX = 'data:image/jpeg;base64,';
    private patientSub;
    private treatmentSub;
    selectedPatient: Patient;
    selectedTreatment: Treatment;

    treatmentDetails: TreatmentDetail[];
    newfeeds: TreatmentNewFeeds[];

    message;
    staticAlertClosed = false;
    staticAlertType = 'success';

    constructor(private translateService: TranslateService,
                private treatmentService: TreatmentService,
                private treatmentHistoryService: TreatmentHistoryService,
                private router: Router) {
        this.patientSub = this.treatmentHistoryService.selectedPatient.subscribe((patient) => {
            if (!patient.id) {
                this.router.navigate(['/treatment-history']);
            }
            this.selectedPatient = patient;
        });
        this.treatmentSub = this.treatmentHistoryService.selectedTreatment.subscribe((t) => {
            if (!t.id) {
                this.router.navigate(['/treatment-history']);
            }
            this.selectedTreatment = t;
        });
    }

    ngOnInit() {
        this.loadTreatmentDetails();
    }

    downloadPdf(fileLink: string) {
        this.treatmentService.downloadTreatmentDetail(fileLink)
            .subscribe(
                (response: HttpResponse<any>) => {
                    const blob = new Blob([response.body], {type: 'application/pdf'});
                    const url = window.URL.createObjectURL(blob);
                    window.open(url);
                },
                (e) => {
                    console.log(e.message);
                });
    }

    loadTreatmentDetails() {
        this.newfeeds = [];
        if (!this.selectedTreatment) {
            this.router.navigate(['/treatment-history']);
        }
        this.treatmentService.fetchTreatmentDetail(this.selectedTreatment.id).subscribe(
            (res: TreatmentDetail[]) => {
                this.treatmentDetails = res;
                this.mappingNewfeeds();
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    addNote(td: TreatmentDetail) {
        this.treatmentHistoryService.setTreatmentDetail(td);
        this.router.navigate(['/treatment-detail-note/new']);
    }

    editNote(id: any) {
        this.router.navigate(['treatment-detail-note/' + id]);
    }

    mappingNewfeeds() {
        this.treatmentDetails.forEach((td) => {
            this.getTreatmentDetailNote(td);
        });
    }

    getTreatmentDetailNote(td: TreatmentDetail) {
        let feed: TreatmentNewFeeds = new TreatmentNewFeeds();
        feed.treatmentDetail = td;
        this.treatmentService.fetchTreatmentDetailNote(td.treatmentDetailId).subscribe(
            (res: HistoryDetailResponse) => {
                feed.detailNote = res;
            },
            (res: HttpErrorResponse) => {
                feed.detailNote = null;
                this.onError(res.message)
            }
        );
        this.newfeeds.push(feed);
    }

    /**
     * Show alert box
     * @param msg
     * @param type
     */
    showALert(msg: string, type: string) {
        this.message = msg;
        this.staticAlertType = type;
        this.staticAlertClosed = false;
        setTimeout(() => this.staticAlertClosed = true, 10000);
    }

    private onError(error) {
        if (error.errorCode != '1031') {
            this.showALert(error.errorMessage, 'danger');
        }
    }
}
