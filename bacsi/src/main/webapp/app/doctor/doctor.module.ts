import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CatunDentistDetailTreatmentModule} from './detail-treatment/detail-treatment.module';
import {CatunDentistTreatmentModule} from './treatment/treatment.module';
import {CatunDentistTreatmentHistoryModule} from './treatment-history/treatment-history.module';

/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        CatunDentistDetailTreatmentModule,
        CatunDentistTreatmentModule,
        CatunDentistTreatmentHistoryModule
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CatunDentistAppDoctorModule {
}
