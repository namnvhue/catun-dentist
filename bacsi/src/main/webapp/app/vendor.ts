/* after changing this file run 'yarn run webpack:build' */
/* tslint:disable */
import '../content/css/vendor.css';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
// jhipster-needle-add-element-to-vendor - JHipster will add new menu items here
import '../app/creative-tim-libs/bootstrap-material-design.min.js';
import '../app/creative-tim-libs/perfect-scrollbar.jquery.min.js';
import '../app/creative-tim-libs/material-dashboard.min.js?v=2.1.0';
