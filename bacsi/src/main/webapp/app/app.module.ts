import './vendor.ts';

import {Injector, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {LocalStorageService, Ng2Webstorage, SessionStorageService} from 'ngx-webstorage';
import {JhiEventManager} from 'ng-jhipster';

import {AuthInterceptor} from './shared/blocks/interceptor/auth.interceptor';
import {AuthExpiredInterceptor} from './shared/blocks/interceptor/auth-expired.interceptor';
import {ErrorHandlerInterceptor} from './shared/blocks/interceptor/errorhandler.interceptor';
import {NotificationInterceptor} from './shared/blocks/interceptor/notification.interceptor';
import {CatunDentistAppSharedModule, UserRouteAccessService} from './shared';
import {CatunDentistAppAppRoutingModule} from './app-routing.module';
import {CatunDentistAppHomeModule} from './home/home.module';
import {CatunDentistAppAdminModule} from './admin/admin.module';
import {CatunDentistAppAccountModule} from './shared/account/account.module';
import {CatunDentistAppEntityModule} from './entities/entity.module';
import {CatunDentistAppReceptionModule} from './reception/reception.module';
import {CatunDentistAppDoctorModule} from './doctor/doctor.module';
import {PaginationConfig} from './shared/blocks/config/uib-pagination.config';

import {CatunDentistBookingModule} from './booking/booking.module';
import {CatunDentistConfirmModule} from './booking/confirm/confirm.module';

// jhipster-needle-angular-add-module-import JHipster will add new module here
import {
    ActiveMenuDirective,
    ErrorComponent,
    FooterComponent,
    JhiMainComponent,
    NavbarComponent,
    PageRibbonComponent,
    ProfileService
} from './layouts';

@NgModule({
    imports: [
        BrowserModule,
        CatunDentistAppAppRoutingModule,
        Ng2Webstorage.forRoot({prefix: 'jhi', separator: '-'}),
        CatunDentistAppSharedModule,
        CatunDentistAppHomeModule,
        CatunDentistAppAdminModule,
        CatunDentistAppAccountModule,
        CatunDentistAppEntityModule,
        CatunDentistAppReceptionModule,
        CatunDentistAppDoctorModule,
        CatunDentistBookingModule,
        CatunDentistConfirmModule

        // jhipster-needle-angular-add-module JHipster will add new module here
    ],
    declarations: [
        JhiMainComponent,
        NavbarComponent,
        ErrorComponent,
        PageRibbonComponent,
        ActiveMenuDirective,
        FooterComponent
    ],
    providers: [
        ProfileService,
        PaginationConfig,
        UserRouteAccessService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true,
            deps: [
                LocalStorageService,
                SessionStorageService
            ]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthExpiredInterceptor,
            multi: true,
            deps: [
                Injector
            ]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorHandlerInterceptor,
            multi: true,
            deps: [
                JhiEventManager
            ]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: NotificationInterceptor,
            multi: true,
            deps: [
                Injector
            ]
        }
    ],
    bootstrap: [JhiMainComponent]
})
export class CatunDentistAppModule {
}
