import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {ProdConfig} from './shared/blocks/config/prod.config';
import {CatunDentistAppModule} from './app.module';

ProdConfig();

if (module['hot']) {
    module['hot'].accept();
}

platformBrowserDynamic().bootstrapModule(CatunDentistAppModule)
.then((success) => console.log(`Application started`))
.catch((err) => console.error(err));
