import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager} from 'ng-jhipster';

import {ProductEntity} from './product-entity.model';
import {ProductEntityPopupService} from './product-entity-popup.service';
import {ProductEntityService} from './product-entity.service';

@Component({
    selector: 'jhi-product-entity-delete-dialog',
    templateUrl: './product-entity-delete-dialog.component.html'
})
export class ProductEntityDeleteDialogComponent {

    productEntity: ProductEntity;

    constructor(
        private productEntityService: ProductEntityService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.productEntityService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'productEntityListModification',
                content: 'Deleted an productEntity'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-product-entity-delete-popup',
    template: ''
})
export class ProductEntityDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private productEntityPopupService: ProductEntityPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.productEntityPopupService
            .open(ProductEntityDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
