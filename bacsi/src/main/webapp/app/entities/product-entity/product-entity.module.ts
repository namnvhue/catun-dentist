import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {CatunDentistAppSharedModule} from '../../shared/index';
import {
    ProductEntityComponent,
    ProductEntityDeleteDialogComponent,
    ProductEntityDeletePopupComponent,
    ProductEntityDetailComponent,
    ProductEntityDialogComponent,
    ProductEntityPopupComponent,
    productEntityPopupRoute,
    ProductEntityPopupService,
    productEntityRoute,
    ProductEntityService,
} from './';

const ENTITY_STATES = [
    ...productEntityRoute,
    ...productEntityPopupRoute,
];

@NgModule({
    imports: [
        CatunDentistAppSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ProductEntityComponent,
        ProductEntityDetailComponent,
        ProductEntityDialogComponent,
        ProductEntityDeleteDialogComponent,
        ProductEntityPopupComponent,
        ProductEntityDeletePopupComponent,
    ],
    entryComponents: [
        ProductEntityComponent,
        ProductEntityDialogComponent,
        ProductEntityPopupComponent,
        ProductEntityDeleteDialogComponent,
        ProductEntityDeletePopupComponent,
    ],
    providers: [
        ProductEntityService,
        ProductEntityPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CantunDentistProductEntityModule {
}
