import {Component, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {HttpResponse} from '@angular/common/http';
import {ProductEntity} from './product-entity.model';
import {ProductEntityService} from './product-entity.service';

@Injectable()
export class ProductEntityPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private productEntityService: ProductEntityService
    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.productEntityService.find(id)
                .subscribe((productEntityResponse: HttpResponse<ProductEntity>) => {
                    const productEntity: ProductEntity = productEntityResponse.body;
                    this.ngbModalRef = this.productEntityModalRef(component, productEntity);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.productEntityModalRef(component, new ProductEntity());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    productEntityModalRef(component: Component, productEntity: ProductEntity): NgbModalRef {
        const modalRef = this.modalService.open(component, {size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.productEntity = productEntity;
        modalRef.result.then((result) => {
            this.router.navigate([{outlets: {popup: null}}], {
                replaceUrl: true,
                queryParamsHandling: 'merge'
            });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{outlets: {popup: null}}], {
                replaceUrl: true,
                queryParamsHandling: 'merge'
            });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
