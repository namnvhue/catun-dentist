import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';

import {ProductEntity} from './product-entity.model';
import {createRequestOption} from '../../shared';

export type EntityResponseType = HttpResponse<ProductEntity>;

@Injectable()
export class ProductEntityService {

    private resourceUrl = SERVER_API_URL + 'admin/product-entities';

    constructor(private http: HttpClient) {
    }

    create(productEntity: ProductEntity): Observable<EntityResponseType> {
        const copy = this.convert(productEntity);
        return this.http.post<ProductEntity>(this.resourceUrl, copy, {observe: 'response'})
        .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(productEntity: ProductEntity): Observable<EntityResponseType> {
        const copy = this.convert(productEntity);
        return this.http.put<ProductEntity>(this.resourceUrl, copy, {observe: 'response'})
        .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ProductEntity>(`${this.resourceUrl}/${id}`, {observe: 'response'})
        .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ProductEntity[]>> {
        const options = createRequestOption(req);
        return this.http.get<ProductEntity[]>(this.resourceUrl, {
            params: options,
            observe: 'response'
        })
        .map((res: HttpResponse<ProductEntity[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ProductEntity = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ProductEntity[]>): HttpResponse<ProductEntity[]> {
        const jsonResponse: ProductEntity[] = res.body;
        const body: ProductEntity[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ProductEntity.
     */
    private convertItemFromServer(productEntity: ProductEntity): ProductEntity {
        const copy: ProductEntity = Object.assign({}, productEntity);
        return copy;
    }

    /**
     * Convert a ProductEntity to a JSON which can be sent to the server.
     */
    private convert(productEntity: ProductEntity): ProductEntity {
        const copy: ProductEntity = Object.assign({}, productEntity);
        return copy;
    }
}
