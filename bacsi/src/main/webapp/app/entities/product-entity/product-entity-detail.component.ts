import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpResponse} from '@angular/common/http';
import {Subscription} from 'rxjs/Subscription';
import {JhiEventManager} from 'ng-jhipster';

import {ProductEntity} from './product-entity.model';
import {ProductEntityService} from './product-entity.service';

@Component({
    selector: 'jhi-product-entity-detail',
    templateUrl: './product-entity-detail.component.html'
})
export class ProductEntityDetailComponent implements OnInit, OnDestroy {

    productEntity: ProductEntity;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private productEntityService: ProductEntityService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInProductEntities();
    }

    load(id) {
        this.productEntityService.find(id)
        .subscribe((productEntityResponse: HttpResponse<ProductEntity>) => {
            this.productEntity = productEntityResponse.body;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInProductEntities() {
        this.eventSubscriber = this.eventManager.subscribe(
            'productEntityListModification',
            (response) => this.load(this.productEntity.id)
        );
    }
}
