import {BaseEntity} from './../../shared';

export class ProductEntity implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public price?: number,
    ) {
    }
}
