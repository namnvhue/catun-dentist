import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';

import {ProductEntity} from './product-entity.model';
import {ProductEntityPopupService} from './product-entity-popup.service';
import {ProductEntityService} from './product-entity.service';
import {ServiceEntity, ServiceEntityService} from '../service-entity';

@Component({
    selector: 'jhi-product-entity-dialog',
    templateUrl: './product-entity-dialog.component.html'
})
export class ProductEntityDialogComponent implements OnInit {

    errorMessage: string;
    productEntity: ProductEntity;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private productEntityService: ProductEntityService,
        private serviceEntityService: ServiceEntityService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.productEntity.id !== undefined) {
            this.subscribeToSaveResponse(
                this.productEntityService.update(this.productEntity));
        } else {
            this.subscribeToSaveResponse(
                this.productEntityService.create(this.productEntity));
        }
    }

    trackServiceEntityById(index: number, item: ServiceEntity) {
        return item.id;
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ProductEntity>>) {
        result.subscribe((res: HttpResponse<ProductEntity>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError(res));
    }

    private onSaveSuccess(result: ProductEntity) {
        this.eventManager.broadcast({name: 'productEntityListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(res?: any) {
        this.isSaving = false;
        this.errorMessage = res.error.errorMessage;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-product-entity-popup',
    template: ''
})
export class ProductEntityPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private productEntityPopupService: ProductEntityPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.productEntityPopupService
                .open(ProductEntityDialogComponent as Component, params['id']);
            } else {
                this.productEntityPopupService
                .open(ProductEntityDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
