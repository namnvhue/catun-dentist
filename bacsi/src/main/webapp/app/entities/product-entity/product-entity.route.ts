import {Routes} from '@angular/router';

import {UserRouteAccessService} from '../../shared';
import {ProductEntityComponent} from './product-entity.component';
import {ProductEntityDetailComponent} from './product-entity-detail.component';
import {ProductEntityPopupComponent} from './product-entity-dialog.component';
import {ProductEntityDeletePopupComponent} from './product-entity-delete-dialog.component';

export const productEntityRoute: Routes = [
    {
        path: 'product-entity',
        component: ProductEntityComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'CatunDentistApp.productEntity.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'product-entity/:id',
        component: ProductEntityDetailComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'CatunDentistApp.productEntity.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const productEntityPopupRoute: Routes = [
    {
        path: 'product-entity-new',
        component: ProductEntityPopupComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'CatunDentistApp.productEntity.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'product-entity/:id/edit',
        component: ProductEntityPopupComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'CatunDentistApp.productEntity.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'product-entity/:id/delete',
        component: ProductEntityDeletePopupComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'CatunDentistApp.productEntity.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
