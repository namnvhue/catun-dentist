import {Route} from '@angular/router';
import {AvailabilitySettingComponent} from './availability-setting.component';
import {UserRouteAccessService} from '../../shared/auth/user-route-access-service';

export const AVAILABILITY_SETTING_ROUTE: Route = {
    path: 'availability-setting',
    component: AvailabilitySettingComponent,
    data: {
        authorities: ['ROLE_ADMIN'],
        pageTitle: 'CatunDentistApp.availabilitySettingEntity.title'
    },
    canActivate: [UserRouteAccessService]
};
