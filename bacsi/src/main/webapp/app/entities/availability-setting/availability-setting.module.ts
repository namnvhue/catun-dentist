import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CatunDentistAppSharedModule} from '../../shared/shared.module';
import {AVAILABILITY_SETTING_ROUTE} from './availability-setting.route';
import {AvailabilitySettingComponent} from './availability-setting.component';
import {AvailabilitySettingService} from './availability-setting.service';

@NgModule({
    imports: [
        RouterModule.forChild([AVAILABILITY_SETTING_ROUTE]),
        CatunDentistAppSharedModule
    ],
    declarations: [
        AvailabilitySettingComponent
    ],
    entryComponents: [],
    providers: [
        AvailabilitySettingService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CatunDentistAvailabilitySettingModule {
}
