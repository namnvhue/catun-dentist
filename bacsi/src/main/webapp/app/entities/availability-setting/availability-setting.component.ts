import {Component, OnInit, ViewChild} from '@angular/core';
import {AvailabilitySettingEntity, DateOffEntity, TIME_SLOTS} from './availability-setting.model';
import {AvailabilitySettingService} from './availability-setting.service';
import CheckAndConverUtils from '../../shared/utils/CheckAndConverUtils';
import {HttpResponse} from '@angular/common/http';
import {TranslateService} from '@ngx-translate/core';
import * as moment_ from 'moment';
import {DateService, DD_MM_YYYY_PATTERN} from '../../shared/shared.date.service';

const moment = moment_;
moment.locale('en');

const _utils = CheckAndConverUtils;
const today = new Date();

@Component({
    selector: 'jhi-confirm',
    templateUrl: './availability-setting.component.html',
    styleUrls: ['./availability-setting.component.css']
})
export class AvailabilitySettingComponent implements OnInit {
    settingDays = [];
    settings: AvailabilitySettingEntity;

    timeSlots = TIME_SLOTS;
    workTimeFrom: string;
    workTimeTo: string;
    holidayDpValFrom: any;
    holidayDpValTo: any;
    fromHoliday;
    toHoliday;
    holidayReason: string;

    holidaysTable = [];
    nameDateOff = [];
    @ViewChild('holidaysDpFrom')
    holidaysDpFrom: any;
    @ViewChild('holidaysDpTo')
    holidaysDpTo: any;
    minDate = {year: today.getFullYear(), month: today.getMonth() + 1, day: today.getDate()};
    pickHolidayType = 'one';

    message;
    staticAlertClosed = false;
    staticAlertType = 'success';

    weekdays: any;

    constructor(private dateService: DateService,
                private availabilitySettingService: AvailabilitySettingService,
                private translateService: TranslateService) {
    }

    ngOnInit() {

        this.getSettings(true);
        this.settingDays = this.dateService.getDates('en', 8);

        this.weekdays = [];
        for (let i = 0; i < 7; i++) {
            this.weekdays.push({
                date: moment(i, 'e').startOf('week').isoWeekday(i + 1).format('dddd'),
                isSelected: false
            });
        }
    }

    /**
     * Fetch saved-setting from server
     */
    getSettings(isInit) {
        this.availabilitySettingService.fetchSettingData().subscribe(
            (res) => {
                this.settings = res;
                this.modifySettingData(res, isInit);

            },
            (err) => {
                console.log(err.errorMessage); // TODO
            }
        );
    }

    /**
     * Select all selected weekdays in available days
     * @param {string} weekdate
     */
    chooseWeekDay(weekdate: string, isInit) {
        this.weekdays.forEach((d) => {
            if (d.date == weekdate) {
                d.isSelected = true;
                return;
            }
        });
        if (this.nameDateOff.find((wd) => wd == weekdate)) {
            if(!isInit) {
                this.removeWeekDay(weekdate);
            }
            return;
        }
        this.nameDateOff.push(weekdate);
        this.settingDays.forEach((week) => {
            week.forEach((d) => {
                if (d.weekday == weekdate) {
                    this.chooseDay(d.fullDateYear, true, 'dayoff', true);
                }
            });
        });
    }

    removeWeekDay(weekdate) {
        let i = this.nameDateOff.findIndex((wd) => wd == weekdate);
        if (i > -1) {
            this.nameDateOff.splice(i, 1);
            this.weekdays.forEach((d) => {
                if (d.date == weekdate) {
                    d.isSelected = false;
                    return;
                }
            });
            this.settingDays.forEach((week) => {
                week.forEach((d) => {
                    if (d.weekday == weekdate) {
                        this.chooseDay(d.fullDateYear, true, '', false);
                    }
                });
            });
        }
    }

    /**
     * Add a date to dateoffs list
     * @param date (dd/MM/yyyy)
     * @param isShown
     * @param reason
     */
    chooseDay(date, isShown, reason, isOverwrite) {

        if (reason === 'dayoff') {
            reason = this.translateService.instant('CatunDentistApp.availabilitySettingEntity.message.dayoff');
        }

        // if day is not available to select
        if (isShown == 'false') {
            return;
        }

        if (this.settings.dateOffs.find((d) => d.dateOff == date)) {
            if (!isOverwrite) {
                this.removeDateOff(date);
                this.toggleSelectDateFromSettingDays(date, reason, true);
            }
        } else {
            this.addDateOff(date, reason);
            this.toggleSelectDateFromSettingDays(date, reason, true);
        }
    }

    holidayDpFromOnClick() {
        this.holidaysDpFrom.toggle();
    }

    holidayDpToOnClick() {
        this.holidaysDpTo.toggle();
    }

    selectFromHoliday(fromHoliday) {
        this.fromHoliday = this.joinDateElement(fromHoliday, true);
    }

    selectToHoliday(toHoliday) {
        this.toHoliday = this.joinDateElement(toHoliday, true);
    }

    /**
     * Add selected holiday to dayoffs & holidays list
     */
    addHoliday() {

        if (this.pickHolidayType == 'one') {
            this.toggleSelectDateFromSettingDays(this.fromHoliday, this.holidayReason, false);
            this.checkHolidayExistAndAdd(this.fromHoliday);
        } else {

            let from = moment(this.fromHoliday, DD_MM_YYYY_PATTERN);
            let to = moment(this.toHoliday, DD_MM_YYYY_PATTERN);
            let day = from;

            while (day.isSameOrBefore(to)) {
                this.toggleSelectDateFromSettingDays(day.format(DD_MM_YYYY_PATTERN), this.holidayReason, false);
                this.checkHolidayExistAndAdd(day.format(DD_MM_YYYY_PATTERN));
                day = day.clone().add(1, 'd');
            }
        }
        this.staticAlertClosed = false;
        setTimeout(() => this.staticAlertClosed = true, 4000);
        this.message = this.translateService.instant('CatunDentistApp.availabilitySettingEntity.message.added');
    }

    checkHolidayExistAndAdd(date) {
        if (this.settings.dateOffs.find((d) => d.dateOff == date)) {
            return;
        }
        if (this.holidaysTable.find((hd) => hd.dateOff == date)) {
            return;
        }
        this.holidaysTable.push({
            'dateOff': date,
            'reason': this.holidayReason
        });
    }

    pickTypeOnSelect(type: string) {
        this.pickHolidayType = type;
        if (type === 'one') {
            this.holidayDpValTo = '';
        }
    }

    save() {
        this.settings.timeWorking = this.workTimeFrom + ',' + this.workTimeTo;
        this.settings.holidayOffs = this.holidaysTable;
        this.settings.nameDateOff = this.nameDateOff.join(',');
        this.availabilitySettingService.saveSetting(this.settings)
            .subscribe(
                (response: HttpResponse<any>) => {
                    if (response.body.errorCode == '201') {
                        this.message = this.translateService.instant('CatunDentistApp.availabilitySettingEntity.message.saved'),
                            this.staticAlertType = 'success';
                    }
                    this.staticAlertClosed = false;
                    setTimeout(() => this.staticAlertClosed = true, 4000);
                },
                () => {
                },
                () => {
                    this.getSettings(false);
                });
    }

    /**
     * Get date-value-string from object of datepicker
     * @param dateObj
     * @param isGetYear
     * @returns {string}
     */
    private joinDateElement(dateObj, isGetYear) {
        let stringDate = '';
        stringDate += _utils.isNumber(dateObj.day) ? _utils.padNumber(dateObj.day) : '';
        stringDate += _utils.isNumber(dateObj.month) ? '/' + _utils.padNumber(dateObj.month) : '';
        if (isGetYear) stringDate += '/' + dateObj.year;
        return stringDate;
    }

    /**
     * Server's response for working time: '08:00,09:30'
     * ==> Need to split to 2 values for binding with inputs
     * @param {AvailabilitySettingEntity} data
     */
    private modifySettingData(data, isInit) {
        if (data.timeWorking) {
            let vals = data.timeWorking.split(',');
            this.workTimeFrom = vals[0];
            this.workTimeTo = vals[1];
        }

        if (!data.numberPatientOneHour) {
            this.settings.numberPatientOneHour = '2';
        } else {
            this.settings.numberPatientOneHour = data.numberPatientOneHour;
        }

        // change selected status on init
        if (isInit) {
            this.settings.dateOffs.forEach((d) => {
                this.toggleSelectDateFromSettingDays(d.dateOff, d.reason, true);
            });
        }

        // select weekday on init
        if (data.nameDateOff) {
            this.nameDateOff = data.nameDateOff.split(',');
            this.nameDateOff.forEach((d) => {
                this.chooseWeekDay(d, true);
            });
        }

        // init holidays table
        if (data.holidayOffs) {
            this.holidaysTable = data.holidayOffs;
            this.holidaysTable.forEach((d) => {
                this.toggleSelectDateFromSettingDays(d.dateOff, d.reason, false);
            });
        }
    }

    /**
     * Remove selected date from dateOffs list of Settings
     * @param dateOff
     */
    private removeDateOff(dateStr) {
        this.settings.dateOffs = this.settings.dateOffs.filter((d) => {
            return d.dateOff != dateStr;
        });
    }

    /**
     * Add selected date to dateOffs list of Settings
     * @param dateStr
     */
    private addDateOff(dateStr, reason) {
        let dateOff: DateOffEntity = new DateOffEntity(dateStr, reason);
        this.settings.dateOffs.push(dateOff);
    }

    /**
     * Change selected-status for display-purpose
     * @param date
     */
    private toggleSelectDateFromSettingDays(date, reason, isToggle) {
        this.settingDays.forEach((week) => {
            week.forEach((d) => {
                if (d.fullDateYear == date) {
                    if (isToggle) {
                        d.isSelected = !d.isSelected;
                    } else {
                        d.isSelected = true;
                    }
                    d.reason = reason;
                    return;
                }
            });
        });
    }
}
