import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';
import {AvailabilitySettingEntity} from './availability-setting.model';
import * as moment_ from 'moment';
import {
    DD_MM_YYYY_HH_MM_PATTERN,
    DD_MM_YYYY_PATTERN,
    HH_MM
} from '../../shared/shared.date.service';

const moment = moment_;
moment.locale('en');

@Injectable()
export class AvailabilitySettingService {
    private resourceUrl = SERVER_API_URL + '/public/time-working';

    constructor(private http: HttpClient) {
    }

    fetchSettingData(): Observable<AvailabilitySettingEntity> {
        return this.http.get<AvailabilitySettingEntity>(this.resourceUrl);
    }

    saveSetting(settings): Observable<any> {
        console.log(settings);
        return this.http.post(
            SERVER_API_URL + 'admin/time-working', JSON.stringify(settings), {
                headers: new HttpHeaders({'Content-Type': 'application/json'}),
                observe: 'response'
            });
    }

    slitWorkTime(timeWorking: string) {
        let todayStr = moment().format(DD_MM_YYYY_PATTERN);
        let parts = timeWorking.split(',');
        let timeSlots = [];

        if (parts.length >= 2) {
            let start = moment(todayStr + ' ' + parts[0], DD_MM_YYYY_HH_MM_PATTERN);
            let end = moment(todayStr + ' ' + parts[1], DD_MM_YYYY_HH_MM_PATTERN);

            let d = start;
            let id = 1;
            while (d.isBefore(end)) {
                timeSlots.push({
                    id: id,
                    time: d.format(HH_MM)
                });
                d = d.clone().add(30, 'm');
                id++;
            }
        }

        return timeSlots;
    }
}
