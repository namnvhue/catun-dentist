import {BaseEntity} from './../../shared';

export class ServiceEntity implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public basePrice?: number,
        public description?: string,
        public relatedProducts?: BaseEntity[],
        public serviceCategory?: BaseEntity
    ) {
    }
}
