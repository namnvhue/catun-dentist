import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpResponse} from '@angular/common/http';
import {Subscription} from 'rxjs/Subscription';
import {JhiEventManager} from 'ng-jhipster';

import {ServiceEntity} from './service-entity.model';
import {ServiceEntityService} from './service-entity.service';

@Component({
    selector: 'jhi-service-entity-detail',
    templateUrl: './service-entity-detail.component.html'
})
export class ServiceEntityDetailComponent implements OnInit, OnDestroy {

    serviceEntity: ServiceEntity;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private serviceEntityService: ServiceEntityService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInServiceEntities();
    }

    load(id) {
        this.serviceEntityService.find(id)
        .subscribe((serviceEntityResponse: HttpResponse<ServiceEntity>) => {
            this.serviceEntity = serviceEntityResponse.body;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInServiceEntities() {
        this.eventSubscriber = this.eventManager.subscribe(
            'serviceEntityListModification',
            (response) => this.load(this.serviceEntity.id)
        );
    }
}
