import {Routes} from '@angular/router';

import {UserRouteAccessService} from '../../shared';
import {ServiceEntityComponent} from './service-entity.component';
import {ServiceEntityDetailComponent} from './service-entity-detail.component';
import {ServiceEntityPopupComponent} from './service-entity-dialog.component';
import {ServiceEntityDeletePopupComponent} from './service-entity-delete-dialog.component';

export const serviceEntityRoute: Routes = [
    {
        path: 'service-entity',
        component: ServiceEntityComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'CatunDentistApp.serviceEntity.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'service-entity/:id',
        component: ServiceEntityDetailComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'CatunDentistApp.serviceEntity.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const serviceEntityPopupRoute: Routes = [
    {
        path: 'service-entity-new',
        component: ServiceEntityPopupComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'CatunDentistApp.serviceEntity.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'service-entity/:id/edit',
        component: ServiceEntityPopupComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'CatunDentistApp.serviceEntity.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'service-entity/:id/delete',
        component: ServiceEntityDeletePopupComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'CatunDentistApp.serviceEntity.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
