import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';

import {ServiceEntity} from './service-entity.model';
import {createRequestOption} from '../../shared';
import {ServiceInitData} from './init-data.model';

export type EntityResponseType = HttpResponse<ServiceEntity>;

@Injectable()
export class ServiceEntityService {

    private resourceUrl = SERVER_API_URL + 'admin/service-entities';

    constructor(private http: HttpClient) {
    }

    loadInitData(): Observable<HttpResponse<ServiceInitData>> {
        return this.http.get<ServiceInitData>(this.resourceUrl + '/services', {observe: 'response'});
    }

    create(serviceEntity: ServiceEntity): Observable<EntityResponseType> {
        const copy = this.convertForCreate(serviceEntity);
        return this.http.post<ServiceEntity>(this.resourceUrl, copy, {observe: 'response'})
        .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(serviceEntity: ServiceEntity): Observable<EntityResponseType> {
        const copy = this.convertForCreate(serviceEntity);
        return this.http.put<ServiceEntity>(this.resourceUrl, copy, {observe: 'response'})
        .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ServiceEntity>(`${this.resourceUrl}/${id}`, {observe: 'response'})
        .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ServiceEntity[]>> {
        const options = createRequestOption(req);
        return this.http.get<ServiceEntity[]>(SERVER_API_URL + '/booking/services', {
            params: options,
            observe: 'response'
        })
        .map((res: HttpResponse<ServiceEntity[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ServiceEntity = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ServiceEntity[]>): HttpResponse<ServiceEntity[]> {
        const jsonResponse: ServiceEntity[] = res.body;
        const body: ServiceEntity[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ServiceEntity.
     */
    private convertItemFromServer(serviceEntity: ServiceEntity): ServiceEntity {
        const copy: ServiceEntity = Object.assign({}, serviceEntity);
        return copy;
    }

    /**
     * Convert a ServiceEntity to a JSON which can be sent to the server.
     */
    private convert(serviceEntity: ServiceEntity): ServiceEntity {
        const copy: ServiceEntity = Object.assign({}, serviceEntity);
        return copy;
    }

    /**
     * Convert a ServiceEntity to a JSON which can be sent to the server (Create Method).
     */
    private convertForCreate(obj: ServiceEntity) {
        const copy: ServiceSaveEntity = new ServiceSaveEntity();
        copy.id = obj.id;
        copy.name = obj.name;
        copy.basePrice = obj.basePrice;
        copy.relatedProducts = obj.relatedProducts.map((o) => o.id).join(',');
        copy.description = obj.description;
        copy.serviceCategoryId = obj.serviceCategory.id;
        return copy;
    }
}

export class ServiceSaveEntity {
    id?: number;
    name?: string;
    basePrice?: number;
    description?: string;
    relatedProducts?: string;
    serviceCategoryId?: string;
}
