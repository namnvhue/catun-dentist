import {Component, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {HttpResponse} from '@angular/common/http';
import {ServiceEntity} from './service-entity.model';
import {ServiceEntityService} from './service-entity.service';

@Injectable()
export class ServiceEntityPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private serviceEntityService: ServiceEntityService
    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.serviceEntityService.find(id)
                .subscribe((serviceEntityResponse: HttpResponse<ServiceEntity>) => {
                    const serviceEntity: ServiceEntity = serviceEntityResponse.body;
                    this.ngbModalRef = this.serviceEntityModalRef(component, serviceEntity);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.serviceEntityModalRef(component, new ServiceEntity());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    serviceEntityModalRef(component: Component, serviceEntity: ServiceEntity): NgbModalRef {
        const modalRef = this.modalService.open(component, {size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.serviceEntity = serviceEntity;
        modalRef.result.then((result) => {
            this.router.navigate([{outlets: {popup: null}}], {
                replaceUrl: true,
                queryParamsHandling: 'merge'
            });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{outlets: {popup: null}}], {
                replaceUrl: true,
                queryParamsHandling: 'merge'
            });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
