import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {CatunDentistAppSharedModule} from '../../shared/index';
import {
    ServiceEntityComponent,
    ServiceEntityDeleteDialogComponent,
    ServiceEntityDeletePopupComponent,
    ServiceEntityDetailComponent,
    ServiceEntityDialogComponent,
    ServiceEntityPopupComponent,
    serviceEntityPopupRoute,
    ServiceEntityPopupService,
    serviceEntityRoute,
    ServiceEntityService,
} from './';

const ENTITY_STATES = [
    ...serviceEntityRoute,
    ...serviceEntityPopupRoute,
];

@NgModule({
    imports: [
        CatunDentistAppSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ServiceEntityComponent,
        ServiceEntityDetailComponent,
        ServiceEntityDialogComponent,
        ServiceEntityDeleteDialogComponent,
        ServiceEntityPopupComponent,
        ServiceEntityDeletePopupComponent,
    ],
    entryComponents: [
        ServiceEntityComponent,
        ServiceEntityDialogComponent,
        ServiceEntityPopupComponent,
        ServiceEntityDeleteDialogComponent,
        ServiceEntityDeletePopupComponent,
    ],
    providers: [
        ServiceEntityService,
        ServiceEntityPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CantunDentistServiceEntityModule {
}
