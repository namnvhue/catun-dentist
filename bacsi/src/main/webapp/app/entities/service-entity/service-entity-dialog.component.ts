import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';

import {ServiceEntity} from './service-entity.model';
import {ServiceEntityPopupService} from './service-entity-popup.service';
import {ServiceEntityService} from './service-entity.service';
import {ServiceCategoryEntity, ServiceCategoryEntityService} from '../service-category-entity';

import {ServiceInitData} from './init-data.model';
import {ProductEntity} from '../product-entity/product-entity.model';

@Component({
    selector: 'jhi-service-entity-dialog',
    templateUrl: './service-entity-dialog.component.html'
})
export class ServiceEntityDialogComponent implements OnInit {

    errorMessage: string;
    serviceEntity: ServiceEntity;
    isSaving: boolean;

    servicecategoryentities: ServiceCategoryEntity[];

    initData: ServiceInitData = new ServiceInitData();

    servicePriceSuggest: number = 0;

    constructor(public activeModal: NgbActiveModal,
                private jhiAlertService: JhiAlertService,
                private serviceEntityService: ServiceEntityService,
                private serviceCategoryEntityService: ServiceCategoryEntityService,
                private eventManager: JhiEventManager) {
    }

    ngOnInit() {
        if (!this.serviceEntity.relatedProducts) {
            this.serviceEntity.relatedProducts = [];
        }
        this.loadInitData();
        this.isSaving = false;
        this.serviceCategoryEntityService.query()
            .subscribe((res: HttpResponse<ServiceCategoryEntity[]>) => {
                this.servicecategoryentities = res.body;
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    loadInitData() {
        this.serviceEntityService.loadInitData().subscribe(
            (res: HttpResponse<ServiceInitData>) => {
                this.initData = <ServiceInitData>res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.serviceEntity.id !== undefined) {
            this.subscribeToSaveResponse(
                this.serviceEntityService.update(this.serviceEntity));
        } else {
            this.subscribeToSaveResponse(
                this.serviceEntityService.create(this.serviceEntity));
        }
    }

    onProductSelect(e, product: ProductEntity) {
        if (e.target.checked) {
            this.serviceEntity.relatedProducts.push(product);
        } else {
            const index = this.trackRelatedProduct(product);
            if (index > -1) {
                this.serviceEntity.relatedProducts.splice(index, 1);
            }
        }
        this.calculateSuggestPrice();
    }

    private calculateSuggestPrice() {
        this.servicePriceSuggest = 0;
        this.serviceEntity.relatedProducts.forEach((p: any) => {
            this.servicePriceSuggest += +p.price;
        });
        this.servicePriceSuggest *= 4;
    }

    trackServiceCategoryEntityById(index: number, item: ServiceCategoryEntity) {
        return item.id;
    }

    trackRelatedProduct(product: ProductEntity) {
        for (let i = 0; i < this.serviceEntity.relatedProducts.length; i++) {
            if (this.serviceEntity.relatedProducts[i].id == product.id) {
                return i;
            }
        }
        return -1;
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ServiceEntity>>) {
        result.subscribe((res: HttpResponse<ServiceEntity>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError(res));
    }

    private onSaveSuccess(result: ServiceEntity) {
        this.eventManager.broadcast({name: 'serviceEntityListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(res?: any) {
        this.isSaving = false;
        this.errorMessage = res.error.errorMessage;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-service-entity-popup',
    template: ''
})
export class ServiceEntityPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(private route: ActivatedRoute,
                private serviceEntityPopupService: ServiceEntityPopupService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.serviceEntityPopupService
                    .open(ServiceEntityDialogComponent as Component, params['id']);
            } else {
                this.serviceEntityPopupService
                    .open(ServiceEntityDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
