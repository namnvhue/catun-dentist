import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager} from 'ng-jhipster';

import {ServiceEntity} from './service-entity.model';
import {ServiceEntityPopupService} from './service-entity-popup.service';
import {ServiceEntityService} from './service-entity.service';

@Component({
    selector: 'jhi-service-entity-delete-dialog',
    templateUrl: './service-entity-delete-dialog.component.html'
})
export class ServiceEntityDeleteDialogComponent {

    serviceEntity: ServiceEntity;

    constructor(
        private serviceEntityService: ServiceEntityService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.serviceEntityService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'serviceEntityListModification',
                content: 'Deleted an serviceEntity'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-service-entity-delete-popup',
    template: ''
})
export class ServiceEntityDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private serviceEntityPopupService: ServiceEntityPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.serviceEntityPopupService
            .open(ServiceEntityDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
