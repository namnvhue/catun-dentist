import {ProductEntity} from '../product-entity/product-entity.model';
import {ServiceCategoryEntity} from '../service-category-entity/service-category-entity.model';

export class ServiceInitData {
    products: ProductEntity[];
    categoryServices: ServiceCategoryEntity[];
}
