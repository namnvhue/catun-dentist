import {BaseEntity} from './../../shared';

export class ServiceCategoryEntity implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
    ) {
    }
}
