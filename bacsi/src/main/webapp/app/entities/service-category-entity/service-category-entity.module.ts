import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {CatunDentistAppSharedModule} from '../../shared/index';
import {
    ServiceCategoryEntityComponent,
    ServiceCategoryEntityDeleteDialogComponent,
    ServiceCategoryEntityDeletePopupComponent,
    ServiceCategoryEntityDetailComponent,
    ServiceCategoryEntityDialogComponent,
    ServiceCategoryEntityPopupComponent,
    serviceCategoryEntityPopupRoute,
    ServiceCategoryEntityPopupService,
    serviceCategoryEntityRoute,
    ServiceCategoryEntityService,
} from './';

const ENTITY_STATES = [
    ...serviceCategoryEntityRoute,
    ...serviceCategoryEntityPopupRoute,
];

@NgModule({
    imports: [
        CatunDentistAppSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ServiceCategoryEntityComponent,
        ServiceCategoryEntityDetailComponent,
        ServiceCategoryEntityDialogComponent,
        ServiceCategoryEntityDeleteDialogComponent,
        ServiceCategoryEntityPopupComponent,
        ServiceCategoryEntityDeletePopupComponent,
    ],
    entryComponents: [
        ServiceCategoryEntityComponent,
        ServiceCategoryEntityDialogComponent,
        ServiceCategoryEntityPopupComponent,
        ServiceCategoryEntityDeleteDialogComponent,
        ServiceCategoryEntityDeletePopupComponent,
    ],
    providers: [
        ServiceCategoryEntityService,
        ServiceCategoryEntityPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CantunDentistServiceCategoryEntityModule {
}
