import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager} from 'ng-jhipster';

import {ServiceCategoryEntity} from './service-category-entity.model';
import {ServiceCategoryEntityPopupService} from './service-category-entity-popup.service';
import {ServiceCategoryEntityService} from './service-category-entity.service';

@Component({
    selector: 'jhi-service-category-entity-delete-dialog',
    templateUrl: './service-category-entity-delete-dialog.component.html'
})
export class ServiceCategoryEntityDeleteDialogComponent {

    serviceCategoryEntity: ServiceCategoryEntity;

    constructor(
        private serviceCategoryEntityService: ServiceCategoryEntityService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.serviceCategoryEntityService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'serviceCategoryEntityListModification',
                content: 'Deleted an serviceCategoryEntity'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-service-category-entity-delete-popup',
    template: ''
})
export class ServiceCategoryEntityDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private serviceCategoryEntityPopupService: ServiceCategoryEntityPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.serviceCategoryEntityPopupService
            .open(ServiceCategoryEntityDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
