import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager} from 'ng-jhipster';

import {ServiceCategoryEntity} from './service-category-entity.model';
import {ServiceCategoryEntityPopupService} from './service-category-entity-popup.service';
import {ServiceCategoryEntityService} from './service-category-entity.service';

@Component({
    selector: 'jhi-service-category-entity-dialog',
    templateUrl: './service-category-entity-dialog.component.html'
})
export class ServiceCategoryEntityDialogComponent implements OnInit {

    errorMessage: string;
    serviceCategoryEntity: ServiceCategoryEntity;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private serviceCategoryEntityService: ServiceCategoryEntityService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.serviceCategoryEntity.id !== undefined) {
            this.subscribeToSaveResponse(
                this.serviceCategoryEntityService.update(this.serviceCategoryEntity));
        } else {
            this.subscribeToSaveResponse(
                this.serviceCategoryEntityService.create(this.serviceCategoryEntity));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ServiceCategoryEntity>>) {
        result.subscribe((res: HttpResponse<ServiceCategoryEntity>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError(res));
    }

    private onSaveSuccess(result: ServiceCategoryEntity) {
        this.eventManager.broadcast({name: 'serviceCategoryEntityListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(res?: any) {
        this.errorMessage = res.error.errorMessage;
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-service-category-entity-popup',
    template: ''
})
export class ServiceCategoryEntityPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private serviceCategoryEntityPopupService: ServiceCategoryEntityPopupService
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.serviceCategoryEntityPopupService
                .open(ServiceCategoryEntityDialogComponent as Component, params['id']);
            } else {
                this.serviceCategoryEntityPopupService
                .open(ServiceCategoryEntityDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
