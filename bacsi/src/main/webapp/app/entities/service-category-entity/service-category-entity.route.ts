import {Routes} from '@angular/router';

import {UserRouteAccessService} from '../../shared';
import {ServiceCategoryEntityComponent} from './service-category-entity.component';
import {ServiceCategoryEntityDetailComponent} from './service-category-entity-detail.component';
import {ServiceCategoryEntityPopupComponent} from './service-category-entity-dialog.component';
import {ServiceCategoryEntityDeletePopupComponent} from './service-category-entity-delete-dialog.component';

export const serviceCategoryEntityRoute: Routes = [
    {
        path: 'service-category-entity',
        component: ServiceCategoryEntityComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'CatunDentistApp.serviceCategoryEntity.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'service-category-entity/:id',
        component: ServiceCategoryEntityDetailComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'CatunDentistApp.serviceCategoryEntity.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const serviceCategoryEntityPopupRoute: Routes = [
    {
        path: 'service-category-entity-new',
        component: ServiceCategoryEntityPopupComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'CatunDentistApp.serviceCategoryEntity.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'service-category-entity/:id/edit',
        component: ServiceCategoryEntityPopupComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'CatunDentistApp.serviceCategoryEntity.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'service-category-entity/:id/delete',
        component: ServiceCategoryEntityDeletePopupComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'CatunDentistApp.serviceCategoryEntity.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
