import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpResponse} from '@angular/common/http';
import {Subscription} from 'rxjs/Subscription';
import {JhiEventManager} from 'ng-jhipster';

import {ServiceCategoryEntity} from './service-category-entity.model';
import {ServiceCategoryEntityService} from './service-category-entity.service';

@Component({
    selector: 'jhi-service-category-entity-detail',
    templateUrl: './service-category-entity-detail.component.html'
})
export class ServiceCategoryEntityDetailComponent implements OnInit, OnDestroy {

    serviceCategoryEntity: ServiceCategoryEntity;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private serviceCategoryEntityService: ServiceCategoryEntityService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInServiceCategoryEntities();
    }

    load(id) {
        this.serviceCategoryEntityService.find(id)
        .subscribe((serviceCategoryEntityResponse: HttpResponse<ServiceCategoryEntity>) => {
            this.serviceCategoryEntity = serviceCategoryEntityResponse.body;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInServiceCategoryEntities() {
        this.eventSubscriber = this.eventManager.subscribe(
            'serviceCategoryEntityListModification',
            (response) => this.load(this.serviceCategoryEntity.id)
        );
    }
}
