import {Component, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {HttpResponse} from '@angular/common/http';
import {ServiceCategoryEntity} from './service-category-entity.model';
import {ServiceCategoryEntityService} from './service-category-entity.service';

@Injectable()
export class ServiceCategoryEntityPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private serviceCategoryEntityService: ServiceCategoryEntityService
    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.serviceCategoryEntityService.find(id)
                .subscribe((serviceCategoryEntityResponse: HttpResponse<ServiceCategoryEntity>) => {
                    const serviceCategoryEntity: ServiceCategoryEntity = serviceCategoryEntityResponse.body;
                    this.ngbModalRef = this.serviceCategoryEntityModalRef(component, serviceCategoryEntity);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.serviceCategoryEntityModalRef(component, new ServiceCategoryEntity());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    serviceCategoryEntityModalRef(component: Component, serviceCategoryEntity: ServiceCategoryEntity): NgbModalRef {
        const modalRef = this.modalService.open(component, {size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.serviceCategoryEntity = serviceCategoryEntity;
        modalRef.result.then((result) => {
            this.router.navigate([{outlets: {popup: null}}], {
                replaceUrl: true,
                queryParamsHandling: 'merge'
            });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{outlets: {popup: null}}], {
                replaceUrl: true,
                queryParamsHandling: 'merge'
            });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
