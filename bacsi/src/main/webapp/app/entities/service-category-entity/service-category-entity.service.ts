import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';

import {ServiceCategoryEntity} from './service-category-entity.model';
import {createRequestOption} from '../../shared';

export type EntityResponseType = HttpResponse<ServiceCategoryEntity>;

@Injectable()
export class ServiceCategoryEntityService {

    private resourceUrl = SERVER_API_URL + 'admin/service-category-entities';

    constructor(private http: HttpClient) {
    }

    create(serviceCategoryEntity: ServiceCategoryEntity): Observable<EntityResponseType> {
        const copy = this.convert(serviceCategoryEntity);
        return this.http.post<ServiceCategoryEntity>(this.resourceUrl, copy, {observe: 'response'})
        .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(serviceCategoryEntity: ServiceCategoryEntity): Observable<EntityResponseType> {
        const copy = this.convert(serviceCategoryEntity);
        return this.http.put<ServiceCategoryEntity>(this.resourceUrl, copy, {observe: 'response'})
        .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ServiceCategoryEntity>(`${this.resourceUrl}/${id}`, {observe: 'response'})
        .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ServiceCategoryEntity[]>> {
        const options = createRequestOption(req);
        return this.http.get<ServiceCategoryEntity[]>(this.resourceUrl, {
            params: options,
            observe: 'response'
        })
        .map((res: HttpResponse<ServiceCategoryEntity[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ServiceCategoryEntity = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ServiceCategoryEntity[]>): HttpResponse<ServiceCategoryEntity[]> {
        const jsonResponse: ServiceCategoryEntity[] = res.body;
        const body: ServiceCategoryEntity[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ServiceCategoryEntity.
     */
    private convertItemFromServer(serviceCategoryEntity: ServiceCategoryEntity): ServiceCategoryEntity {
        const copy: ServiceCategoryEntity = Object.assign({}, serviceCategoryEntity);
        return copy;
    }

    /**
     * Convert a ServiceCategoryEntity to a JSON which can be sent to the server.
     */
    private convert(serviceCategoryEntity: ServiceCategoryEntity): ServiceCategoryEntity {
        const copy: ServiceCategoryEntity = Object.assign({}, serviceCategoryEntity);
        return copy;
    }
}
