export class MailTemplateMasterEntity {
    masterTemplate?: MailTemplateMasterDetailEntity[];
}

export class MailTemplateMasterDetailEntity {
    name?: string;
    contentEmail?: MailContentEntity;
    placeHolder?: MailTemplatePlaceHolderEntity[];
}

export class MailContentEntity {
    subject?: string;
    content?: string;
}

export class MailTemplatePlaceHolderEntity {
    keyword?: string;
    sample?: string;
}

export class MailTemplateEntity {
    masterTemplate?: string;
    subject?: string;
    content?: string;
}
