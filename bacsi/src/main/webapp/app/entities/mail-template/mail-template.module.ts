import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CatunDentistAppSharedModule} from '../../shared/shared.module';
import {MAIL_TEMPLATE_ROUTE} from './mail-template.route';
import {MailTemplateComponent} from './mail-template.component';
import {MailTemplateService} from './mail-template.service';

@NgModule({
    imports: [
        RouterModule.forChild([MAIL_TEMPLATE_ROUTE]),
        CatunDentistAppSharedModule
    ],
    declarations: [
        MailTemplateComponent
    ],
    entryComponents: [],
    providers: [
        MailTemplateService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CatunDentistMailTemplateModule {
}
