import {Route} from '@angular/router';
import {MailTemplateComponent} from './mail-template.component';
import {UserRouteAccessService} from '../../shared/auth/user-route-access-service';

export const MAIL_TEMPLATE_ROUTE: Route = {
    path: 'mail-template',
    component: MailTemplateComponent,
    data: {
        authorities: ['ROLE_ADMIN'],
        pageTitle: 'CatunDentistApp.mailTemplateEntity.title'
    },
    canActivate: [UserRouteAccessService]
};
