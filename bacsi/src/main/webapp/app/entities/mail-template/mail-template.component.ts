import {Component, OnInit} from '@angular/core';
import {MailTemplateService} from './mail-template.service';
import {
    MailTemplateEntity,
    MailTemplateMasterDetailEntity,
    MailTemplateMasterEntity
} from './mail-template.model';
import {HttpResponse} from '@angular/common/http';
import {Location} from '@angular/common';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'jhi-confirm',
    templateUrl: './mail-template.component.html',
    styleUrls: ['./mail-template.component.css']
})
export class MailTemplateComponent implements OnInit {

    mailTemplate: MailTemplateEntity = new MailTemplateEntity();
    mailTemplateMaster: MailTemplateMasterEntity;
    selectedMailTemplate: MailTemplateMasterDetailEntity;

    message;
    errorCode;
    staticAlertClosed = false;
    staticAlertType = 'danger';

    modal: NgbModalRef;
    modalTitle = '';
    modalMessage = '';
    modalConfirm = false;

    constructor(private mailTemplateService: MailTemplateService,
                private location: Location,
                private modalService: NgbModal,
                private translateService: TranslateService) {
    }

    ngOnInit() {
        this.getMasterInfo();
    }

    getMasterInfo() {
        this.mailTemplateService.fetchMailTemplateData().subscribe(
            (mailTemplateMaster) => {
                this.mailTemplateMaster = mailTemplateMaster;
            },
            (err) => {
                console.log(err.errorMessage); // TODO
            }
        );
    }

    trackTemplate(index: number, item: MailTemplateMasterDetailEntity) {
        return item.name;
    }

    save(modal) {
        // custom check content when using rich text editor
        if (!this.customCheckContent()) {
            this.openModal(
                modal,
                this.translateService.instant('CatunDentistApp.mailTemplateEntity.modal.errorTitle'),
                this.translateService.instant('CatunDentistApp.mailTemplateEntity.modal.errorContentNullMsg'),
                false);
        } else {
            this.mailTemplateService.saveMailTemplate(this.mailTemplate)
                .subscribe(
                    (response: HttpResponse<any>) => {
                        this.errorCode = response.body.errorCode;
                        this.message = this.translateService.instant('CatunDentistApp.mailTemplateEntity.message.saved');
                        if (this.errorCode == '201') {
                            this.staticAlertType = 'success';
                            this.mailTemplate = new MailTemplateEntity();
                        }
                        this.staticAlertClosed = false;
                        setTimeout(() => this.staticAlertClosed = true, 4000);
                    },
                    () => {
                    },
                    () => {
                        this.getMasterInfo();
                    });
        }
    }

    /**
     * Using rich text editor, after remove all content, there is still '<br>' left,
     * making 'content' variable not null but there is no real content
     * @returns {boolean}
     */
    customCheckContent() {
        if (!this.mailTemplate.content || this.mailTemplate.content === '<br>') {
            return false;
        }
        return true;
    }

    onSelectTemplate(value) {
        let selected = this.mailTemplateMaster.masterTemplate.find((item) => item.name == value);
        this.selectedMailTemplate = selected;
        this.mailTemplate.masterTemplate = selected.name;
        this.mailTemplate.subject = selected.contentEmail ? selected.contentEmail.subject : '';
        this.mailTemplate.content = selected.contentEmail ? selected.contentEmail.content : '';
    }

    openModal(modal, title, message, isConfirm) {
        this.modalConfirm = isConfirm;
        this.modalTitle = title;
        this.modalMessage = message;
        this.modal = this.modalService.open(modal, {size: 'sm'});
    }

    cancel(modal) {
        this.openModal(
            modal,
            this.translateService.instant('CatunDentistApp.mailTemplateEntity.modal.cancelTitle'),
            this.translateService.instant('CatunDentistApp.mailTemplateEntity.modal.cancelMsg'),
            true
        );
    }

    back() {
        this.modal.close();
        this.location.back();
    }
}
