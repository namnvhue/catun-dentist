import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';
import {MailTemplateMasterEntity} from './mail-template.model';

@Injectable()
export class MailTemplateService {
    private resourceUrl = SERVER_API_URL + '/admin/mail-template';

    constructor(private http: HttpClient) {
    }

    fetchMailTemplateData(): Observable<MailTemplateMasterEntity> {
        const url = this.resourceUrl + '/' + 'masters';
        return this.http.get<MailTemplateMasterEntity>(url);
    }

    saveMailTemplate(mailTemplate): Observable<any> {
        return this.http.post(
            this.resourceUrl, JSON.stringify(mailTemplate), {
                headers: new HttpHeaders({'Content-Type': 'application/json'}),
                observe: 'response'
            });
    }
}
