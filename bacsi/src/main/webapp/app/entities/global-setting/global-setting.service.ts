import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';

import {GlobalSetting, GroupInfo} from './global-setting.model';
import {createRequestOption} from '../../shared/index';

export type EntityResponseType = HttpResponse<GlobalSetting>;

@Injectable()
export class GlobalSettingService {

    private resourceUrl = SERVER_API_URL + 'admin/global-settings';

    constructor(private http: HttpClient) {
    }

    create(globalSetting: GlobalSetting): Observable<EntityResponseType> {
        const copy = this.convert(globalSetting);
        return this.http.post<GlobalSetting>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(globalSetting: GlobalSetting): Observable<EntityResponseType> {
        const copy = this.convert(globalSetting);
        return this.http.put<GlobalSetting>(this.resourceUrl, copy, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<GlobalSetting>(`${this.resourceUrl}/${id}`, {observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<GlobalSetting[]>> {
        const options = createRequestOption(req);
        return this.http.get<GlobalSetting[]>(this.resourceUrl, {
            params: options,
            observe: 'response'
        })
            .map((res: HttpResponse<GlobalSetting[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }

    fetchGroups(): Observable<GroupInfo[]> {
        return this.http.get<GroupInfo[]>(this.resourceUrl + '/info');
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: GlobalSetting = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<GlobalSetting[]>): HttpResponse<GlobalSetting[]> {
        const jsonResponse: GlobalSetting[] = res.body;
        const body: GlobalSetting[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to GlobalSetting.
     */
    private convertItemFromServer(globalSetting: GlobalSetting): GlobalSetting {
        const copy: GlobalSetting = Object.assign({}, globalSetting);
        return copy;
    }

    /**
     * Convert a GlobalSetting to a JSON which can be sent to the server.
     */
    private convert(globalSetting: GlobalSetting): GlobalSetting {
        const copy: GlobalSetting = Object.assign({}, globalSetting);
        return copy;
    }
}
