import {BaseEntity} from '../../shared/index';

export class GlobalSetting implements BaseEntity {
    constructor(
        public id?: number,
        public group?: string,
        public name?: string,
        public value?: string,
    ) {
    }
}

export class GroupInfo {
    groupName?: string;
    names?: string[];
}
