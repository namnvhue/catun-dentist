import {Component, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {HttpResponse} from '@angular/common/http';
import {GlobalSetting} from './global-setting.model';
import {GlobalSettingService} from './global-setting.service';

@Injectable()
export class GlobalSettingPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private globalSettingService: GlobalSettingService
    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.globalSettingService.find(id)
                .subscribe((globalSettingResponse: HttpResponse<GlobalSetting>) => {
                    const globalSetting: GlobalSetting = globalSettingResponse.body;
                    this.ngbModalRef = this.globalSettingModalRef(component, globalSetting);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.globalSettingModalRef(component, new GlobalSetting());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    globalSettingModalRef(component: Component, globalSetting: GlobalSetting): NgbModalRef {
        const modalRef = this.modalService.open(component, {size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.globalSetting = globalSetting;
        modalRef.result.then((result) => {
            this.router.navigate([{outlets: {popup: null}}], {
                replaceUrl: true,
                queryParamsHandling: 'merge'
            });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{outlets: {popup: null}}], {
                replaceUrl: true,
                queryParamsHandling: 'merge'
            });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
