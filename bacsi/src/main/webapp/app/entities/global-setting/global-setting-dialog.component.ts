import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager} from 'ng-jhipster';

import {GlobalSetting, GroupInfo} from './global-setting.model';
import {GlobalSettingPopupService} from './global-setting-popup.service';
import {GlobalSettingService} from './global-setting.service';

@Component({
    selector: 'jhi-global-setting-dialog',
    templateUrl: './global-setting-dialog.component.html'
})
export class GlobalSettingDialogComponent implements OnInit {

    globalSetting: GlobalSetting;
    isSaving: boolean;

    groups: GroupInfo[];
    selectedGroup: GroupInfo;

    message;
    staticAlertClosed = false;
    staticAlertType = 'success';

    constructor(public activeModal: NgbActiveModal,
                private globalSettingService: GlobalSettingService,
                private eventManager: JhiEventManager) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.getGroups();
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.selectedGroup) {
            this.globalSetting.group = this.selectedGroup.groupName;
        }
        if (this.globalSetting.id !== undefined) {
            this.subscribeToSaveResponse(
                this.globalSettingService.update(this.globalSetting));
        } else {
            this.subscribeToSaveResponse(
                this.globalSettingService.create(this.globalSetting));
        }
    }

    select(groupName: string) {
        if (this.groups) {
            this.groups.forEach((g) => {
                if (g.groupName == groupName) {
                    this.selectedGroup = g;
                    return;
                }
            });
        }
    }

    private getGroups() {
        this.globalSettingService.fetchGroups().subscribe(
            (res: GroupInfo[]) => {
                this.groups = res;
                if (this.globalSetting) {
                    this.select(this.globalSetting.group);
                }
            },
            (err) => {
                this.onError(err);
            }
        );
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<GlobalSetting>>) {
        result.subscribe((res: HttpResponse<GlobalSetting>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError(res));
    }

    private onSaveSuccess(result: GlobalSetting) {
        this.eventManager.broadcast({name: 'globalSettingListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(res: HttpErrorResponse) {
        this.isSaving = false;
        this.message = res.error.errorCode + ": " + res.error.errorMessage;
        this.staticAlertType = 'danger';
        this.staticAlertClosed = false;
        setTimeout(() => this.staticAlertClosed = true, 4000);
    }

    private onError(error) {
        this.message = error.message;
        this.staticAlertType = 'danger';
        this.staticAlertClosed = false;
        setTimeout(() => this.staticAlertClosed = true, 4000);
    }
}

@Component({
    selector: 'jhi-global-setting-popup',
    template: ''
})
export class GlobalSettingPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(private route: ActivatedRoute,
                private globalSettingPopupService: GlobalSettingPopupService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.globalSettingPopupService
                    .open(GlobalSettingDialogComponent as Component, params['id']);
            } else {
                this.globalSettingPopupService
                    .open(GlobalSettingDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
