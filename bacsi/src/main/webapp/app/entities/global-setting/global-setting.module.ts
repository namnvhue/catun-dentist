import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {CatunDentistAppSharedModule} from '../../shared/index';
import {
    GlobalSettingComponent,
    GlobalSettingDeleteDialogComponent,
    GlobalSettingDeletePopupComponent,
    GlobalSettingDetailComponent,
    GlobalSettingDialogComponent,
    GlobalSettingPopupComponent,
    globalSettingPopupRoute,
    GlobalSettingPopupService,
    GlobalSettingResolvePagingParams,
    globalSettingRoute,
    GlobalSettingService,
} from './index';

const ENTITY_STATES = [
    ...globalSettingRoute,
    ...globalSettingPopupRoute,
];

@NgModule({
    imports: [
        CatunDentistAppSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        GlobalSettingComponent,
        GlobalSettingDetailComponent,
        GlobalSettingDialogComponent,
        GlobalSettingDeleteDialogComponent,
        GlobalSettingPopupComponent,
        GlobalSettingDeletePopupComponent,
    ],
    entryComponents: [
        GlobalSettingComponent,
        GlobalSettingDialogComponent,
        GlobalSettingPopupComponent,
        GlobalSettingDeleteDialogComponent,
        GlobalSettingDeletePopupComponent,
    ],
    providers: [
        GlobalSettingService,
        GlobalSettingPopupService,
        GlobalSettingResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CantunDentistGlobalSettingModule {
}
