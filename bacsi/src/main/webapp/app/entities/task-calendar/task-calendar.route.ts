import {Route} from '@angular/router';
import {UserRouteAccessService} from '../../shared/auth/user-route-access-service';
import {TaskCalendarComponent} from './task-calendar.component';

export const TASK_CALENDAR_ROUTE: Route = {
    path: 'task-calendar',
    component: TaskCalendarComponent,
    data: {
        authorities: ['ROLE_ADMIN','ROLE_RECEPTIONIST','ROLE_DOCTOR'],
        pageTitle: 'CatunDentistApp.taskCalendar.title'
    },
    canActivate: [UserRouteAccessService]
};
