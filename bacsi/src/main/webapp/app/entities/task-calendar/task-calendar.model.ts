export class AppointmentTask {
    day?: string;
    appointments?: Appointment[];
}

export class Appointment {
    bookingId?: string;
    patientId?: string;
    patientName?: string;
    doctorId? : string;
    doctorName?: string;
    timeStart?: string;
    timeEnd?: string;
    services? :string;
    code?: string;
}

export class Doctor {
    doctorId?: string;
    doctorName?: string;
    numberPhone?: string;
    display: boolean = true;
}

export class Task {
    bookingId?: string;
    doctorId?: string;
    timeEnd?: string;
}
