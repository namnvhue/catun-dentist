import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';
import {AppointmentTask, Doctor, Task} from './task-calendar.model';

@Injectable()
export class TaskCalendarService {
    private resourceUrl = SERVER_API_URL + '/receptionist';

    constructor(private http: HttpClient) {
    }

    fetchAppointments(request): Observable<any> {
        const url = this.resourceUrl + '/assign-doctor';
        return this.http.post(
            this.resourceUrl + '/assign-doctor', JSON.stringify(request), {
                headers: new HttpHeaders({'Content-Type': 'application/json'}),
                observe: 'response'
            });
    }

    fetchDoctors(): Observable<Doctor[]> {
        const url = this.resourceUrl + '/doctors';
        return this.http.get<Doctor[]>(url);
    }

    assignDoctor(task: Task): Observable<any> {
        return this.http.post(
            this.resourceUrl + '/assign-doctor/create', JSON.stringify(task), {
                headers: new HttpHeaders({'Content-Type': 'application/json'}),
                observe: 'response'
            });
    }
}
