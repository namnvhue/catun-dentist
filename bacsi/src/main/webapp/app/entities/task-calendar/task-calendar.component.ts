import {Component, OnInit, ViewChild} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap/modal/modal.module';
import {JhiLanguageService} from 'ng-jhipster';
import {CalendarComponent} from 'ng-fullcalendar';
import {Options} from 'fullcalendar';
import {
    DateService,
    DD_MM_YYYY_HH_mm_ss_PATTERN,
    YYYY_MM_DD,
    YYYY_MM_DD_T_HH_mm_ss
} from '../../shared/shared.date.service';
import {TaskCalendarService} from './task-calendar.service';
import {Appointment, AppointmentTask, Doctor, Task} from './task-calendar.model';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import * as moment_ from 'moment';
import {Principal} from "../../shared/auth/principal.service";
import {Router} from "@angular/router";
import {Patient} from "../../doctor/treatment/treatment.model";
import {TreatmentService} from "../../doctor/treatment/treatment.service";
import {Treatment} from "../../doctor/treatment-history/treatment-history.model";

const moment = moment_;
const today = moment();

@Component({
    selector: 'jhi-confirm',
    templateUrl: './task-calendar.component.html',
    styleUrls: ['./task-calendar.component.css'],
})
export class TaskCalendarComponent implements OnInit {

    COLORS = [
        '#cecece', '#ffc3a0', '#5cf35d', '#088da5', '#40e0d0', '#ffbf44', '#b0e0e6', '#6cfb85', '#f7f6c7', '#a2bbef', '#b0fd03', '#85f131', '#f6cbbd', '#f6cbbd', '#68f1bc', '#eae06d', '#51fc6d', '#52dbd9', '#fa962c', '#fbfc62', '#bed1ec', '#70e86d', '#fde185', '#f58ed6', '#946efc', '#09ccfc', '#9ad8c2', '#d4a4be', '#1ce3af', '#7dd9dd', '#12f728', '#de9c5d', '#f76234', '#829ecf', '#b3a8ed', '#e6c197', '#fc5b46', '#2090e7', '#fc3297'
    ];

    calendarOptions: Options;
    @ViewChild(CalendarComponent) ucCalendar: CalendarComponent;
    events: any[];

    message;
    staticAlertClosed = false;
    staticAlertType = 'success';

    assignDoctorModal: NgbModalRef;

    tasks: AppointmentTask[];
    doctors: Doctor[];

    typeSelect: string = 'month';

    viewingDate: { year: number, month: number }; // Date being navigated on calendar
    viewingAppoinment: Appointment; // Appointment being clicked on calendar
    assignedDoctorId: string;

    filterEvents: any[] = [];
    filterAll: boolean = true;

    model: NgbDateStruct;
    date: { year: number, month: number };
    minDate = {year: today.year(), month: today.month() + 1, day: today.startOf('month').date()};
    maxDate = {year: today.year(), month: today.month() + 1, day: today.endOf('month').date()};

    isDoctor: boolean;
    isReception: boolean;
    authorities: any;

    @ViewChild('selectDate')
    public selectDateDp: any; // DOB date picker


    constructor(private taskCalendarService: TaskCalendarService,
                private languageService: JhiLanguageService,
                private dateService: DateService,
                private modalService: NgbModal,
                private calendar: NgbCalendar,
                private principal: Principal,
                private router: Router,
                private treatmentService: TreatmentService) {
    }

    ngOnInit() {
        this.selectToday();
        this.loadDoctors();
        this.getRole();
    }

    private getRole() {
        this.isDoctor = false;
        this.isReception = false;

        this.principal.identity(false).then((account) => {
            if (account !== null) {
                this.authorities = account.authorities;
                if (account.authorities.indexOf('ROLE_RECEPTIONIST') > -1) {
                    this.isReception = true;
                } else if (account.authorities.indexOf('ROLE_DOCTOR') > -1) {
                    this.isDoctor = true;
                }
            }
        });
    }

    private loadAppointments(isInit) {
        this.taskCalendarService.fetchAppointments({
            typeSelected: this.typeSelect,
            dateSelected: ''
        }).subscribe(
            (res: HttpResponse<AppointmentTask[]>) => {
                this.tasks = res.body;
                if (isInit) {
                    this.convertToEvents();
                    this.initCalendar();
                } else {
                    this.ucCalendar.fullCalendar('removeEventSource', this.events);
                    this.ucCalendar.fullCalendar('removeEventSource', this.filterEvents);
                    this.convertToEvents();
                    this.reloadCalendar(null);
                    this.doctors.forEach((d) => {
                        d.display = true
                    });
                    this.filterAll = true;
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    private loadDoctors() {
        this.taskCalendarService.fetchDoctors().subscribe(
            (res: Doctor[]) => {
                this.doctors = res;
                this.doctors.forEach((d) => {
                    d.display = true
                });
                this.loadAppointments(true);
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    private initCalendar() {
        this.calendarOptions = {
            locale: 'vi',
            editable: true,
            eventLimit: false,
            eventColor: '#aaf1bb',
            events: this.events,
            defaultView: 'agendaWeek',
            columnFormat: 'ddd D/M',
            allDaySlot: false,
            slotLabelFormat: 'HH:mm',
            eventStartEditable: false,
            customButtons: {
                pick: {
                    text: 'Chọn ngày',
                    click: function () {
                        document.getElementById('calendarBtn').click();
                    }
                }
            },
            header: {
                left: 'prev,today,next',
                center: '',
                right: 'agendaWeek,agendaDay,pick'
            },
            buttonText: {
                today: 'Hôm nay',
                week: 'Tuần',
                day: 'Ngày'
            }
        };
    }

    private reloadCalendar(filterEvents) {
        this.ucCalendar.fullCalendar('addEventSource', filterEvents ? filterEvents : this.events);
        this.ucCalendar.fullCalendar('refetchEvents');
    }

    private convertToEvents() {
        this.events = [];
        this.tasks.forEach((t) => {
            t.appointments.forEach((apm) => {
                let title = 'BN: ' + apm.patientName + '\n';
                title += apm.doctorName ? ('BS: ' + apm.doctorName) : 'NEW';
                title += apm.services ? '\nDV: ' + apm.services : '';
                let color = '#aaf1bb';
                if (apm.doctorId) {
                    // title += apm.patientName ? (' - ' + 'P: ' + apm.patientName) : '';
                    let idx = this.doctors.findIndex(d => d.doctorId == apm.doctorId);
                    if (idx > -1) {
                        color = this.COLORS[idx];
                    }
                }
                this.events.push({
                    title: title,
                    start: this.getCalendarTime(apm.timeStart),
                    end: this.getCalendarTime(apm.timeEnd),
                    apm: apm,
                    color: color
                });
            });
        });
    }

    filterDoctor(e, d: Doctor, all: boolean) {
        if (all) {
            this.ucCalendar.fullCalendar('removeEventSource', this.events);
            this.ucCalendar.fullCalendar('removeEventSource', this.filterEvents);
            this.filterAll = e.target.checked;
            this.doctors.forEach((d) => {
                d.display = e.target.checked;
            });
            if (e.target.checked) {
                this.reloadCalendar(null);
            }
        } else {
            this.filterAll = false;
            this.ucCalendar.fullCalendar('removeEventSource', this.events);
            this.ucCalendar.fullCalendar('removeEventSource', this.filterEvents);
            this.updateFilterDoctors(e, d);
            this.updateFilterEvents();
            this.reloadCalendar(this.filterEvents);
        }
    }

    viewAppointmentDetails() {
        if (this.viewingAppoinment) {
            if (this.isReception) {
                this.router.navigate(['/xac-nhan/' + this.viewingAppoinment.code]);
                this.resetSelectedEventValues();
            }
            if (this.isDoctor) {
                this.treatmentService.fetchTreatment(this.viewingAppoinment.bookingId).subscribe(
                    (res: Treatment) => {
                        this.treatmentService.setTreatment(res);
                    },
                    (res: HttpErrorResponse) => this.onError(res.message),
                    () => {
                        this.treatmentService.fetchPatient(this.viewingAppoinment.patientId).subscribe(
                            (res: Patient) => {
                                this.treatmentService.setPatient(res);
                            },
                            (res: HttpErrorResponse) => this.onError(res.message),
                            () => {
                                this.router.navigate(['/treatment/' + this.viewingAppoinment.patientId]);
                                this.resetSelectedEventValues();
                            }
                        );
                    }
                );
            }
        }
    }

    private updateFilterEvents() {
        this.filterEvents = [];
        this.events.forEach((e) => {
            this.doctors.forEach((d) => {
                if (e.apm.doctorId == d.doctorId && d.display) {
                    this.filterEvents.push(e);
                }
            });
        });
    }

    private updateFilterDoctors(e, d: Doctor) {
        d.display = e.target.checked;
    }

    assign() {
        let task = new Task();
        task.timeEnd = this.viewingAppoinment.timeEnd;
        task.bookingId = this.viewingAppoinment.bookingId;
        task.doctorId = this.assignedDoctorId;
        this.taskCalendarService.assignDoctor(task).subscribe(
            (res) => {
                // this.onSuccess('Assign done!');
                this.loadAppointments(false);
                this.reloadCalendar(this.filterEvents);
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.resetSelectedEventValues();
    }

    private resetSelectedEventValues() {
        this.assignDoctorModal.close();
        this.viewingAppoinment = null;
        this.assignedDoctorId = null;
    }

    selectDateDpOnClick() {
        this.selectDateDp.toggle();
    }

    navigateAgenda(event: any) {
        this.viewingDate = event;
        let viewDateStr = this.dateService.joinDateElementByFormat(this.viewingDate, true, YYYY_MM_DD);
        let startOfWk = this.dateService.getStartOfWeek(viewDateStr, YYYY_MM_DD);
        let startOfWkStr = this.dateService.getDateStrFromDate(startOfWk, YYYY_MM_DD);
        this.ucCalendar.fullCalendar('changeView', 'agendaWeek', startOfWkStr);
        this.ucCalendar.fullCalendar('gotoDate', viewDateStr);
        this.ucCalendar.fullCalendar('changeView', 'agendaDay');
        this.ucCalendar.fullCalendar('gotoDate', viewDateStr);
    }

    private selectToday() {
        this.model = this.calendar.getToday();
    }

    private getCalendarTime(dateStr: string) {
        if (dateStr) {
            return this.dateService.convertDateStringByFormat(
                DD_MM_YYYY_HH_mm_ss_PATTERN,
                YYYY_MM_DD_T_HH_mm_ss,
                dateStr);
        }
        return '';
    }

    private onSuccess(msg: string) {
        this.message = msg;
        this.staticAlertType = 'success';
        this.staticAlertClosed = false;
        setTimeout(() => this.staticAlertClosed = true, 4000);
    }

    private onError(error) {
        this.message = error.message;
        this.staticAlertType = 'danger';
        this.staticAlertClosed = false;
        setTimeout(() => this.staticAlertClosed = true, 4000);
    }

    closeModal() {
        this.resetSelectedEventValues();
    }

    eventClick(content: any, model: any) {
        model = {
            event: {
                id: model.event.id,
                start: model.event.start,
                end: model.event.end,
                title: model.event.title,
                apm: model.event.apm
            }
        };
        this.viewingAppoinment = model.event.apm;
        if (model.event.end) {
            this.viewingAppoinment.timeEnd = model.event.end.format(DD_MM_YYYY_HH_mm_ss_PATTERN);
        } else {
            this.viewingAppoinment.timeEnd = '';
        }
        if (this.viewingAppoinment.doctorId) {
            this.assignedDoctorId = this.viewingAppoinment.doctorId
        }
        this.assignDoctorModal = this.modalService.open(content);
    }

}
