import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CatunDentistAppSharedModule} from '../../shared/shared.module';
import {TASK_CALENDAR_ROUTE} from './task-calendar.route';
import {TaskCalendarComponent} from './task-calendar.component';
import {TaskCalendarService} from './task-calendar.service';
import {FullCalendarModule} from 'ng-fullcalendar';

@NgModule({
    imports: [
        RouterModule.forChild([TASK_CALENDAR_ROUTE]),
        CatunDentistAppSharedModule,
        FullCalendarModule
    ],
    declarations: [
        TaskCalendarComponent
    ],
    entryComponents: [],
    providers: [
        TaskCalendarService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CatunDentistTaskCalendarModule {
}
