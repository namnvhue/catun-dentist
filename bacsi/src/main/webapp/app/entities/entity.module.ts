import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';

import {CantunDentistGlobalSettingModule} from './global-setting/global-setting.module';
import {CantunDentistProductEntityModule} from './product-entity/product-entity.module';
import {CantunDentistServiceCategoryEntityModule} from './service-category-entity/service-category-entity.module';
import {CantunDentistServiceEntityModule} from './service-entity/service-entity.module';
import {CatunDentistMailTemplateModule} from './mail-template/mail-template.module';
import {CatunDentistAvailabilitySettingModule} from './availability-setting/availability-setting.module';
import {CatunDentistBackofficeBookingModule} from './backoffice-booking-entity/backoffice-booking.module';
import {CatunDentistTaskCalendarModule} from './task-calendar/task-calendar.module';

/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        CantunDentistGlobalSettingModule,
        CantunDentistProductEntityModule,
        CantunDentistServiceCategoryEntityModule,
        CantunDentistServiceEntityModule,
        CatunDentistMailTemplateModule,
        CatunDentistAvailabilitySettingModule,
        CatunDentistBackofficeBookingModule,
        CatunDentistTaskCalendarModule
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CatunDentistAppEntityModule {
}
