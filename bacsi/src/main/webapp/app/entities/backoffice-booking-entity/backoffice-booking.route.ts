import {Route} from '@angular/router';
import {BackofficeBookingComponent} from './backoffice-booking.component';
import {UserRouteAccessService} from '../../shared/auth/user-route-access-service';

export const BACKOFFICE_BOOKING_ROUTE: Route = {
    path: 'backoffice-booking',
    component: BackofficeBookingComponent,
    data: {
        authorities: ['ROLE_ADMIN', 'ROLE_DOCTOR', 'ROLE_RECEPTIONIST','ROLE_COUNTER'],
        pageTitle: 'CatunDentistApp.backofficeBookingEntity.title'
    },
    canActivate: [UserRouteAccessService]
};
