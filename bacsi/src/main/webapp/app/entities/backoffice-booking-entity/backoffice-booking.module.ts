import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CatunDentistAppSharedModule} from '../../shared/shared.module';
import {BACKOFFICE_BOOKING_ROUTE} from './backoffice-booking.route';
import {BackofficeBookingComponent} from './backoffice-booking.component';
import {BackofficeBookingService} from './backoffice-booking.service';
import {AvailabilitySettingService} from '../availability-setting/availability-setting.service';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';
import {LoadingModule} from 'ngx-loading';

@NgModule({
    imports: [
        RouterModule.forChild([BACKOFFICE_BOOKING_ROUTE]),
        CatunDentistAppSharedModule,
        NgMultiSelectDropDownModule,
        LoadingModule
    ],
    declarations: [
        BackofficeBookingComponent
    ],
    entryComponents: [],
    providers: [
        AvailabilitySettingService,
        BackofficeBookingService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CatunDentistBackofficeBookingModule {
}
