import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiLanguageService} from 'ng-jhipster';

import {ProfileService} from '../profiles/profile.service';
import {JhiLanguageHelper, LoginModalService, LoginService, Principal} from '../../shared';

import {VERSION} from '../../app.constants';
import {CheckNotificationResponse, Notification} from '../../shared/model/notification-entity';
import {NotificationService} from '../../shared/notification.service';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {IntervalObservable} from 'rxjs/observable/IntervalObservable';
import 'rxjs/add/operator/takeWhile';

@Component({
    selector: 'jhi-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: [
        'navbar.css'
    ]
})
export class NavbarComponent implements OnInit, OnDestroy {
    inProduction: boolean;
    isNavbarCollapsed: boolean;
    languages: any[];
    swaggerEnabled: boolean;
    modalRef: NgbModalRef;
    version: string;
    notStaff: boolean = true;
    receptionOrDoctor = false;
    counter: boolean = false;

    identity: any;
    showNoti: Notification[] = [];
    newNoti: Notification[] = [];
    oldNoti: Notification[] = [];
    newSize = 0;
    notiModal: NgbModalRef;
    alive = true;

    constructor(private loginService: LoginService,
                private languageService: JhiLanguageService,
                private languageHelper: JhiLanguageHelper,
                private principal: Principal,
                private loginModalService: LoginModalService,
                private profileService: ProfileService,
                private router: Router,
                private notificationService: NotificationService,
                private modalService: NgbModal) {
        this.version = VERSION ? 'v' + VERSION : '';
        this.isNavbarCollapsed = true;
    }

    ngOnInit() {
        this.languageHelper.getAll().then((languages) => {
            this.languages = languages;
        });

        this.profileService.getProfileInfo().then((profileInfo) => {
            this.inProduction = profileInfo.inProduction;
            this.swaggerEnabled = profileInfo.swaggerEnabled;
        });

        this.principal.getAuthenticationState().subscribe((identity) => {
            this.identity = identity;
            this.checkRole(identity);
            this.checkNotifications();
        });

    }

    ngOnDestroy() {
        this.alive = false;
    }

    checkNotifications() {
        if (this.identity) {
            this.getNotifications(this.identity);
            IntervalObservable.create(30000).takeWhile(() => this.alive).subscribe(() => {
                this.getNotifications(this.identity);
            });
        }
    }

    getNotifications(identity) {
        if (identity !== null && ((identity.authorities.indexOf('ROLE_DOCTOR') > -1)
                || (identity.authorities.indexOf('ROLE_RECEPTIONIST') > -1))) {
            this.notificationService.fetchAllNotifications().subscribe(
                (res: CheckNotificationResponse) => {
                    if (res) {
                        this.newNoti = Object.assign([], res.news);
                        this.newSize = this.newNoti.length;
                        this.oldNoti = res.olds;
                        this.showNoti = Object.assign(res.news.splice(0, 20));
                    }
                },
                (res: HttpErrorResponse) => console.log(res.message)
            );
        }
    }

    viewNotification(noti: Notification) {
        this.notificationService.setViewFlg(noti.notificationId).subscribe(
            (res: HttpResponse<any>) => {
                if (noti.linkConfirm) {
                    window.open(noti.linkConfirm, '_blank');
                }
                this.getNotifications(this.identity);
            },
            (res: HttpErrorResponse) => console.log(res.message)
        );
    }

    openNotiModal(modal: any) {
        this.notiModal = this.modalService.open(modal);
    }

    closeNotiModal() {
        this.notiModal.close();
    }

    readAllNewNoti() {
        this.notificationService.setViewFlg(0).subscribe(
            (res: HttpResponse<any>) => {
                this.getNotifications(this.identity);
            },
            (res: HttpErrorResponse) => console.log(res.message)
        );
    }

    checkRole(identity) {
        this.notStaff = false;
        this.receptionOrDoctor = false;
        this.counter = false;
        if (identity !== null) {
            if (identity.authorities.length == 1) {
                if (identity.authorities[0] == 'ROLE_USER') {
                    this.notStaff = true;
                    return;
                }
            }

            if ((identity.authorities.indexOf('ROLE_COUNTER') > -1)) {
                this.counter = true;
                return;
            }

            if ((identity.authorities.indexOf('ROLE_DOCTOR') > -1)
                || (identity.authorities.indexOf('ROLE_RECEPTIONIST') > -1)) {
                this.receptionOrDoctor = true;
                return;
            }

        } else {
            this.notStaff = true;
            this.receptionOrDoctor = true;
            return;
        }
    }

    changeLanguage(languageKey: string) {
        this.languageService.changeLanguage(languageKey);
    }

    collapseNavbar() {
        this.isNavbarCollapsed = true;
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }

    logout() {
        this.collapseNavbar();
        this.loginService.logout();
        this.router.navigate(['']);
    }

    toggleNavbar() {
        this.isNavbarCollapsed = !this.isNavbarCollapsed;
    }

    getImageUrl() {
        return this.isAuthenticated() ? this.principal.getImageUrl() : null;
    }
}
