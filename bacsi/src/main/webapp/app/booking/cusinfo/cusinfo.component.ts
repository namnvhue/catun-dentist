import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {BookingService} from '../booking.service';
import {BookingEntity} from '../../shared/model/booking-entity';
import {CusinfoEntity} from '../../shared/model/cusinfo-entity';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import * as moment_ from 'moment';
import {Principal} from '../../shared';

import {ServiceEntity} from '../../entities/service-entity/service-entity.model';
import {ServiceEntityService} from '../../entities/service-entity/service-entity.service';
import {HttpErrorResponse, HttpResponse} from "@angular/common/http";

const moment = moment_;
const today = moment();

@Component({
    selector: 'jhi-cusinfo',
    templateUrl: './cusinfo.component.html',
    styleUrls: ['./cusinfo.component.css']
})
export class CustomerInfoComponent implements OnInit, OnDestroy {

    errorMessage: string;

    selectedItems = [];
    dropdownSettings = {};
    serviceList;
    isDataAvailable = false;

    cusinfo: CusinfoEntity = new CusinfoEntity();
    booking: BookingEntity;

    public loading = false;
    @ViewChild('dob')
    dobDp: any; // DOB date picker
    minDate = {year: 1900, month: 1, day: 1};
    maxDate = {year: today.year(), month: today.month() + 1, day: today.date()};
    private subscription;

    constructor(private bookingService: BookingService,
                private location: Location,
                private router: Router,
                private principal: Principal,
                private serviceEntityService: ServiceEntityService) {
    }

    ngOnInit() {
        this.loadServices();
        this.bookingService.setCurrentStep(2);
        this.getLoginUserInfo();
        this.subscription = this.bookingService.currentBooking.subscribe(booking => this.booking = booking);

        if (!this.booking.dateVal || !this.booking.timeVal) {
            this.router.navigate(['dat-hen', 'chon-ngay']);
        }
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }


    /**
     * Service dropdown: Init method
     */
    initServiceSelect() {
        this.isDataAvailable = true;
        this.selectedItems = [];
        this.dropdownSettings = {
            singleSelection: false,
            idField: 'id',
            textField: 'name',
            selectAllText: 'Chọn hết',
            unSelectAllText: 'Bỏ chọn hết',
            itemsShowLimit: 3,
            allowSearchFilter: false
        };
    }

    /**
     * Service dropdown's methods
     */
    loadServices() {
        this.serviceList = [];
        this.serviceEntityService.query({
            page: 0,
            size: 9999,
            sort: ['id', 'asc']
        }).subscribe(
            (res: HttpResponse<ServiceEntity[]>) => this.onSuccess(res.body),
            (res: HttpErrorResponse) => this.onError(res.message),
            () => this.initServiceSelect() //  init service select after services-loading
        );
    }

    dobDpOnClick() {
        this.dobDp.toggle();
        this.dobDp.navigateTo({year: 1990, month: 1});
    }

    getLoginUserInfo() {
        this.principal.identity(false).then((account) => {
            if (account !== null) {
                this.compareInfo(account);
            }
        });
    }

    compareInfo(account) {
        if (!this.cusinfo.nameVal) {
            this.cusinfo.nameVal = account.lastName + ' ' + account.firstName;
        }
        if (!this.cusinfo.emailVal) {
            this.cusinfo.emailVal = account.email;
        }
    }

    book() {
        this.loading = true;
        this.bookingService.setCusinfo(this.cusinfo);
        this.bookingService.setServices(this.selectedItems);
        this.bookingService.setServices(this.selectedItems);

        this.bookingService.book(this.booking).subscribe(
            (response) => {
                if (response.status === 200) {
                    this.bookingService.resetBookingData();
                    this.router.navigate(['xac-nhan/' + response.body.code]);
                }
            },
            (error) => {
                if (error.status === 400) {
                    this.errorMessage = error.error.errorCode + ": " + error.error.errorMessage;
                    this.loading = false;
                } else {
                    this.errorMessage = error.errorMessage;
                }
            });
    }

    back() {
        this.location.back();
    }

    private onSuccess(data) {
        for (let i = 0; i < data.length; i++) {
            let service = {id: data[i].id, name: data[i].name};
            this.serviceList.push(service);
        }
    }

    private onError(error) {
        console.log(error); // TODO
    }
}
