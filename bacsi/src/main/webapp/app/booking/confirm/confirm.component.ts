import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ConfirmService} from './confirm.service';
import {BookingInfoEntity} from '../../shared/model/booking-info-entity';
import {Observable} from 'rxjs/Observable';

@Component({
    selector: 'jhi-confirm',
    templateUrl: './confirm.component.html',
    styleUrls: ['./confirm.component.css']
})
export class ConfirmComponent implements OnInit {

    bookingInfo: BookingInfoEntity;
    @ViewChild('gmap') gmapElement: any;
    map: google.maps.Map;
    mapLatitude;
    mapLongtitude;
    currentLocation;

    constructor(private confirmService: ConfirmService,
                private route: ActivatedRoute,
                private router: Router) {
    }

    ngOnInit() {
        this.retrieveCurrentPosition();
        this.render();
    }

    render(): void {
        const code = this.route.snapshot.paramMap.get('code');
        this.confirmService.fetchBookingInfo(code).subscribe(
            (bookingInfo) => {
                this.bookingInfo = bookingInfo;
            },
            (err) => {
                console.log(err.errorMessage); // TODO
                this.router.navigate(['/']);
            },
            () => {
                this.getGeoLocation(this.bookingInfo.address).subscribe(
                    (position) => {
                        this.mapLatitude = position.lat();
                        this.mapLongtitude = position.lng();
                    },
                    (err) => {
                        console.log(err.errorMessage); // TODO
                    },
                    () => {
                        this.initMap();
                    });
            }
        );
    }

    /**
     * Get current place latitutde & logntitude from browser
     */
    retrieveCurrentPosition() {
        navigator.geolocation.getCurrentPosition((position) => {
            this.currentLocation = position.coords;
        });
    }

    /**
     * Generate Google Map
     */
    initMap() {
        //  init map
        const mapProp = {
            center: new google.maps.LatLng(+this.mapLatitude, +this.mapLongtitude),
            zoom: 17,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            title: this.bookingInfo.address,
        };
        this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);

        //  add marker
        const marker = new google.maps.Marker({
            position: {lat: this.mapLatitude, lng: +this.mapLongtitude},
            map: this.map,
            title: this.bookingInfo.address
        });
        marker.setMap(this.map);
    }

    /**
     * Get latitude, longtitude from this.bookingInfo.address by Google Map API
     * @param {string} address
     * @returns {Observable<google.maps.LatLng>}
     */
    getGeoLocation(address: string): Observable<google.maps.LatLng> {

        const geocoder = new google.maps.Geocoder();
        return Observable.create((observer) => {
            geocoder.geocode({
                'address': address
            }, (results, status) => {
                if (status == google.maps.GeocoderStatus.OK) {
                    observer.next(results[0].geometry.location);
                    observer.complete();
                } else {
                    console.log('Error: ', results, ' & Status: ', status);
                    observer.error();
                }
            });
        });
    }

    /**
     * Open native map or google map for direction
     */
    mapsSelector() {
        window.open(this.getGoogleMapUrl());
    }

    getGoogleMapUrlPrefix() {
        return 'https://';
        // if  /* if we're on iOS, open in Apple Maps */
        // ((navigator.platform.indexOf('iPhone') != -1) ||
        //     (navigator.platform.indexOf('iPad') != -1) ||
        //     (navigator.platform.indexOf('iPod') != -1)) {
        //     return 'maps://';
        // }
        // else {/* else use Google */
        //     return 'https://';
        // }
    }

    getGoogleMapUrl() {
        let url = this.getGoogleMapUrlPrefix() + 'www.google.com/maps/dir/?api=1';
        url += '&origin=' + this.currentLocation.latitude + ',' + this.currentLocation.longitude;
        url += '&destination=' + this.mapLatitude + ',' + this.mapLongtitude;
        return url;
    }

    print(): void {
        window.print();
    }
}
