import {Route} from '@angular/router';
import {ConfirmComponent} from './confirm.component';

export const CONFIRM_ROUTE: Route = {
    path: 'xac-nhan/:code',
    component: ConfirmComponent,
    data: {
        pageTitle: 'CatunDentistApp.confirm.title'
    }
};
