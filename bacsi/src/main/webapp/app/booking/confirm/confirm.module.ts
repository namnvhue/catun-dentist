import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CatunDentistAppSharedModule} from '../../shared/shared.module';
import {CONFIRM_ROUTE} from './confirm.route';
import {ConfirmComponent} from './confirm.component';
import {ConfirmService} from './confirm.service';

@NgModule({
    imports: [
        RouterModule.forChild([CONFIRM_ROUTE]),
        CatunDentistAppSharedModule,
    ],
    declarations: [
        ConfirmComponent
    ],
    entryComponents: [],
    providers: [
        ConfirmService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CatunDentistConfirmModule {
}
