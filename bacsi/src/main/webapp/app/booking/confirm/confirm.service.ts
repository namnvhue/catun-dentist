import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {BookingInfoEntity} from '../../shared/model/booking-info-entity';
import {SERVER_API_URL} from '../../app.constants';

@Injectable()
export class ConfirmService {
    private resourceUrl = SERVER_API_URL + '/confirm';

    constructor(private _http: HttpClient) {
    }

    fetchBookingInfo(confirmCode: string): Observable<BookingInfoEntity> {

        const url = this.resourceUrl + '/' + confirmCode;

        const params: HttpParams = new HttpParams();
        params.set('code', confirmCode);

        return this._http.get<BookingInfoEntity>(url, {params});
    }
}
