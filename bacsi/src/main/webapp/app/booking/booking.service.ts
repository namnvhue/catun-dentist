import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {BookingEntity} from '../shared/model/booking-entity';
import {CusinfoEntity} from '../shared/model/cusinfo-entity';
import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {SERVER_API_URL} from '../app.constants';

@Injectable()
export class BookingService {
    private resourceUrl = SERVER_API_URL + '/booking/create';

    private booking: BehaviorSubject<BookingEntity> = new BehaviorSubject(new BookingEntity());
    currentBooking = this.booking.asObservable();
    private finSteps: BehaviorSubject<number> = new BehaviorSubject(0);
    currentSteps = this.finSteps.asObservable();

    constructor(private http: HttpClient) {
    }

    resetBookingData() {
        this.booking = new BehaviorSubject(new BookingEntity());
        this.currentBooking = this.booking.asObservable();
    }

    setDate(dateVal: string) {
        let temp = this.booking.value;
        temp.dateVal = dateVal;
        this.booking.next(temp);

        this.finSteps.next(1);
    }

    setTime(timeVal: string) {
        let temp = this.booking.value;
        temp.timeVal = timeVal;
        this.booking.next(temp);

        this.finSteps.next(2);
    }

    setCusinfo(cusinfo: CusinfoEntity) {
        let temp = this.booking.value;
        temp.nameVal = cusinfo.nameVal;
        temp.dobVal = cusinfo.getDobStr();
        temp.emailVal = cusinfo.emailVal;
        temp.phoneVal = cusinfo.phoneVal;
        temp.historyVal = cusinfo.historyVal;
        temp.currentVal = cusinfo.currentVal;
        temp.ortherRequest = cusinfo.ortherRequest;
        this.booking.next(temp);

        this.finSteps.next(3);
    }

    setCurrentStep(step: number) {
        this.finSteps.next(step);
    }

    setServices(services: any) {
        let temp = this.booking.value;
        temp.services = services;
        this.booking.next(temp);
    }

    book(bookingData): Observable<any> {
        return this.http.post(
            this.resourceUrl, JSON.stringify(bookingData), {
                headers: new HttpHeaders({'Content-Type': 'application/json'}),
                observe: 'response'
            });
    }
}
