import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CatunDentistAppSharedModule} from '../shared/shared.module';
import {PickdayComponent} from './pickday/pickday.component';
import {PicktimeComponent} from './picktime/picktime.component';
import {BookingComponent} from './booking.component';
import {CustomerInfoComponent} from './cusinfo/cusinfo.component';
import {BookingIndicatorComponent} from './indicator/indicator.component';
import {
    CustomDateParserFormatter,
    CustomDatepickerI18n,
    I18n,
    NgbdDatepickerI18n
} from '../shared/customized-datepicker/datepicker-i18n';
import {NgbDateParserFormatter, NgbDatepickerI18n} from '@ng-bootstrap/ng-bootstrap';
import {LoadingModule} from 'ngx-loading';
import {BOOKING_ROUTE} from './booking.route';
import {NgDatepickerModule} from 'ng2-datepicker';
import {MomentModule} from 'angular2-moment';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';

@NgModule({
    imports: [
        RouterModule.forChild([BOOKING_ROUTE]),
        CatunDentistAppSharedModule,
        NgDatepickerModule,
        LoadingModule,
        MomentModule,
        NgMultiSelectDropDownModule
    ],
    declarations: [
        BookingComponent,
        PickdayComponent,
        PicktimeComponent,
        CustomerInfoComponent,
        BookingIndicatorComponent,
        NgbdDatepickerI18n
    ],
    entryComponents: [],
    providers: [
        {provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n},
        I18n,
        {provide: NgbDateParserFormatter, useClass: CustomDateParserFormatter}
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CatunDentistBookingModule {
}
