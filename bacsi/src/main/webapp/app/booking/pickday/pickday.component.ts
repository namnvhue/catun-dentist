import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {BookingService} from '../booking.service';
import {BookingEntity} from '../../shared/model/booking-entity';
import {DateService, MMMM_PATTERN} from '../../shared/shared.date.service';
import {AvailabilitySettingService} from '../../entities/availability-setting/availability-setting.service';
import 'moment/locale/vi';
import * as moment_ from 'moment';
import {DateOffEntity} from '../../entities/availability-setting/availability-setting.model';

const moment = moment_;

@Component({
    selector: 'jhi-pickday',
    templateUrl: './pickday.component.html',
    styleUrls: ['./pickday.component.css']
})
export class PickdayComponent implements OnInit, OnDestroy {

    dates;
    booking: BookingEntity;
    currentMonth;
    dateOffs: DateOffEntity[];
    holidayOffs: DateOffEntity[];
    private subscription;

    constructor(private router: Router,
                private bookingService: BookingService,
                private dateService: DateService,
                private availabilitySettingService: AvailabilitySettingService) {
        this.dates = this.dateService.getDates('en', 4);
        moment.locale('en');
    }

    ngOnInit() {
        this.bookingService.setCurrentStep(0);
        this.currentMonth = moment().format(MMMM_PATTERN);
        //  Subscribe to current booking infomation
        this.subscription = this.bookingService.currentBooking.subscribe((booking) => this.booking = booking);

        this.getSettings();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    chooseDay(choosedDate, isShown, isDayoff) {
        if (isShown !== 'false' && isDayoff !== 'true') {
            this.bookingService.setDate(choosedDate);
            this.router.navigate(['dat-hen', 'chon-gio']);
        }
    }

    /**
     * Fetch saved-setting from server
     */
    getSettings() {
        this.availabilitySettingService.fetchSettingData().subscribe(
            (res) => {
                this.dateOffs = res.dateOffs;
                this.holidayOffs = res.holidayOffs;
                this.markDayOffs();
            },
            (err) => {
                console.log(err.errorMessage); // TODO
            }
        );
    }

    markDayOffs() {
        this.dateOffs.forEach((dayoff) => {
            this.dates.forEach((week) => {
                week.forEach((d) => {
                    if (d.fullDateYear == dayoff.dateOff) {
                        d.isSelected = true;
                        d.reason = dayoff.reason;
                    }
                });
            });
        });
        this.holidayOffs.forEach((dayoff) => {
            this.dates.forEach((week) => {
                week.forEach((d) => {
                    if (d.fullDateYear == dayoff.dateOff) {
                        d.isSelected = true;
                        d.reason = dayoff.reason;
                    }
                });
            });
        });
    }
}
