import {Route} from '@angular/router';
import {PickdayComponent} from './pickday/pickday.component';
import {PicktimeComponent} from './picktime/picktime.component';
import {BookingComponent} from './booking.component';
import {CustomerInfoComponent} from './cusinfo/cusinfo.component';

export const BOOKING_ROUTE: Route = {
    path: 'dat-hen',
    component: BookingComponent,
    children: [
        {
            path: '',
            redirectTo: 'chon-ngay',
            pathMatch: 'full'
        },
        {
            path: 'chon-ngay',
            component: PickdayComponent,
            data: {
                pageTitle: 'CatunDentistApp.booking.title'
            }
        },
        {
            path: 'chon-gio',
            component: PicktimeComponent,
            data: {
                pageTitle: 'CatunDentistApp.booking.title'
            }
        },
        {
            path: 'thong-tin-ca-nhan',
            component: CustomerInfoComponent,
            data: {
                pageTitle: 'CatunDentistApp.booking.title'
            }
        }
    ]
};
