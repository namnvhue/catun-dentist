import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {BookingService} from '../booking.service';
import {BookingEntity} from '../../shared/model/booking-entity';
import {AvailabilitySettingService} from '../../entities/availability-setting/availability-setting.service';
import 'moment/locale/vi';
import * as moment_ from 'moment';

const moment = moment_;
moment.locale('en');

@Component({
    selector: 'jhi-picktime',
    templateUrl: './picktime.component.html',
    styleUrls: ['./picktime.component.css']
})
export class PicktimeComponent implements OnInit, OnDestroy {

    timeSlots;
    booking: BookingEntity;
    timeWorking: string;

    private subscription;

    constructor(private router: Router,
                private bookingService: BookingService,
                private availabilitySettingService: AvailabilitySettingService) {
    }

    ngOnInit() {
        this.bookingService.setCurrentStep(1);
        this.subscription = this.bookingService.currentBooking.subscribe((booking) => this.booking = booking);
        if (!this.booking.dateVal) {
            this.router.navigate(['dat-hen', 'chon-ngay']);
        }

        this.getSettings();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    chooseTimeSlot(time) {
        if (time) {
            this.bookingService.setTime(time);
            this.router.navigate(['dat-hen', 'thong-tin-ca-nhan']);
        }
    }

    /**
     * Fetch saved-setting from server
     */
    getSettings() {
        this.availabilitySettingService.fetchSettingData().subscribe(
            (res) => {
                if (res.timeWorking) {
                    this.timeWorking = res.timeWorking;
                    this.timeSlots = this.availabilitySettingService.slitWorkTime(this.timeWorking);
                }
            },
            (err) => {
                console.log(err.errorMessage); // TODO
            }
        );
    }
}
