import {Component, OnDestroy, OnInit} from '@angular/core';
import {BookingService} from '../booking.service';
import {BookingEntity} from '../../shared/model/booking-entity';
import {Router} from '@angular/router';

@Component({
    selector: 'jhi-booking-indicator',
    templateUrl: 'indicator.component.html',
    styleUrls: ['indicator.component.css']
})
export class BookingIndicatorComponent implements OnInit, OnDestroy {

    booking: BookingEntity;
    finishedStep;
    private bookSubscribe;
    private stepSubscribe;

    constructor(private bookingService: BookingService,
                private router: Router) {

    }

    ngOnInit() {
        // Subscribe to current booking infomation
        this.bookSubscribe = this.bookingService.currentBooking.subscribe((booking) => this.booking = booking);
        this.stepSubscribe = this.bookingService.currentSteps.subscribe((finishedStep) => this.finishedStep = finishedStep);
    }

    ngOnDestroy() {
        this.bookSubscribe.unsubscribe();
        this.stepSubscribe.unsubscribe();
    }

    navigate(target, valid, stepNo) {
        if (valid) {
            this.bookingService.setCurrentStep(stepNo);
            this.router.navigate(['dat-hen', target]);
        }
    }

}
