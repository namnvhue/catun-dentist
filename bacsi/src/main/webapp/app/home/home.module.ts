import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {CatunDentistAppSharedModule} from '../shared/index';

import {HOME_ROUTE, HomeComponent} from './index';

@NgModule({
    imports: [
        CatunDentistAppSharedModule,
        RouterModule.forChild([HOME_ROUTE])
    ],
    declarations: [
        HomeComponent,
    ],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CatunDentistAppHomeModule {
}
