import {Route} from '@angular/router';

import {UserRouteAccessService} from '../../index';
import {SettingsComponent} from './settings.component';

export const settingsRoute: Route = {
    path: 'settings',
    component: SettingsComponent,
    data: {
        authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_DOCTOR', 'ROLE_RECEPTIONIST','ROLE_COUNTER'],
        pageTitle: 'global.menu.account.settings'
    },
    canActivate: [UserRouteAccessService]
};
