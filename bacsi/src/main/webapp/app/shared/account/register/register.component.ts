import {AfterViewInit, Component, ElementRef, OnInit, Renderer, ViewChild} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiLanguageService} from 'ng-jhipster';
import {TranslateService} from '@ngx-translate/core';

import {Register} from './register.service';
import {EMAIL_ALREADY_USED_TYPE, LOGIN_ALREADY_USED_TYPE, LoginModalService} from '../../index';
import {DateService} from '../../shared.date.service';

@Component({
    selector: 'jhi-register',
    templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit, AfterViewInit {

    confirmPassword: string;
    doNotMatch: string;
    error: string;
    errorEmailExists: string;
    errorUserExists: string;
    registerAccount: any;
    success: boolean;
    modalRef: NgbModalRef;
    channels: string[] = [
        'register.form.channelValues.internet',
        'register.form.channelValues.relative',
        'register.form.channelValues.address',
        'register.form.channelValues.facebook'
    ];

    @ViewChild('dob')
    dobDp: any; // DOB date picker
    minDate: any;
    maxDate: any;
    birthday: any;

    message;
    staticAlertClosed = false;
    staticAlertType = 'success';

    constructor(
        private languageService: JhiLanguageService,
        private loginModalService: LoginModalService,
        private registerService: Register,
        private elementRef: ElementRef,
        private renderer: Renderer,
        private translateService: TranslateService,
        private dateService: DateService
    ) {
    }

    ngOnInit() {
        const today = this.dateService.today();
        this.success = false;
        this.registerAccount = {};
        this.minDate = {year: 1900, month: 1, day: 1};
        this.maxDate = {year: today.year(), month: today.month() + 1, day: today.date()};
    }

    ngAfterViewInit() {
        this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#login'), 'focus', []);
    }

    getChannelValue(key: string) {
        return this.translateService.instant(key);
    }

    register() {
        if(this.birthday &&
            this.birthday.hasOwnProperty('day') &&
            this.birthday.hasOwnProperty('month') &&
            this.birthday.hasOwnProperty('year')) {
            this.registerAccount.birthday = this.dateService.joinDateElement(this.birthday, true);
        }
        if (this.registerAccount.password !== this.confirmPassword) {
            this.doNotMatch = 'ERROR';
        } else {
            this.doNotMatch = null;
            this.error = null;
            this.errorUserExists = null;
            this.errorEmailExists = null;
            this.languageService.getCurrent().then((key) => {
                this.registerAccount.langKey = key;
                this.registerService.save(this.registerAccount).subscribe(() => {
                    this.success = true;
                }, (response) => this.processError(response));
            });
        }
    }

    /**
     * Show alert box
     * @param msg
     * @param type
     */
    showALert(msg: string, type: string) {
        this.message = msg;
        this.staticAlertType = type;
        this.staticAlertClosed = false;
        setTimeout(() => this.staticAlertClosed = true, 4000);
    }

    openLogin() {
        this.modalRef = this.loginModalService.open();
    }

    dobDpOnClick() {
        this.dobDp.toggle();
        this.dobDp.navigateTo({year: 1990, month: 1});
    }

    private processError(response: HttpErrorResponse) {
        this.success = null;
        console.log(response);
        if (response.status !== 200) {
            this.showALert(response.error.errorMessage, 'danger');
        }
    }
}
