export class Notification {
    userId?: number;
    notificationId?: number;
    linkConfirm?: string;
    eventType?: string;
    content?: string;
    intervalTime?: string;
}

export class CheckNotificationResponse {
    news?: Notification[];
    olds?: Notification[];
}
