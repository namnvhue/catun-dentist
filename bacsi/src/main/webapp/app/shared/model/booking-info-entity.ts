export class BookingInfoEntity {
    code: string;
    qrCodeImageUrl: string;
    address: string;
    name: string;
    date: string;
    status: string;
    services: string[];
}
