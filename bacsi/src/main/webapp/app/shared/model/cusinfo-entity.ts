import CheckAndConverUtils from '../../shared/utils/CheckAndConverUtils';

const _utils = CheckAndConverUtils;

export class CusinfoEntity {
    nameVal = '';
    dobVal: { year: number, month: number, day: number };
    emailVal = '';
    phoneVal = '';
    historyVal = '';
    currentVal = '';
    ortherRequest = '';

    constructor() {
        this.dobVal = {year: 1990, month: -1, day: -1};
    }

    getDobStr() {
        return this.format(this.dobVal.day.toString())
            + '/'
            + this.format(this.dobVal.month.toString())
            + '/'
            + this.dobVal.year;
    }

    format(value: string) {
        return _utils.padNumber(_utils.toInteger(value));
    }
}
