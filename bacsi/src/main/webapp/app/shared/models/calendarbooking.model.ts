export class CalendarbookingModel {
    medicalHistoryDescrription: string;
    statusCurrent: string;
    appointmentDate: string;
    appointmentHourFrom: string;
    email: string;
}
