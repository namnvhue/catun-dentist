export interface QRCodeImageResponse {
    qrCodeImage: string;
    errorCode: string;
    errorMessage: string;
}
