import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../app.constants';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {CheckNotificationResponse} from './model/notification-entity';

@Injectable()
export class NotificationService {

    private resourceUrl = SERVER_API_URL + '/notification';

    constructor(private http: HttpClient) {
    }

    fetchNewNotifications(): Observable<CheckNotificationResponse> {
        const url = this.resourceUrl + '/20';
        return this.http.get<CheckNotificationResponse>(url);
    }

    fetchAllNotifications(): Observable<CheckNotificationResponse> {
        const url = this.resourceUrl + '/all';
        return this.http.get<CheckNotificationResponse>(url);
    }

    setViewFlg(notiId: any): Observable<any> {
        return this.http.get<any>(this.resourceUrl + '/viewed/' + notiId);
    }
}
