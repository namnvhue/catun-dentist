export default class CheckAndConverUtils {
    static padNumber(value: number) {
        if (this.isNumber(value)) {
            return `0${value}`.slice(-2);
        } else {
            return '';
        }
    }

    static isNumber(value: any): boolean {
        return !isNaN(this.toInteger(value));
    }

    static toInteger(value: any): number {
        return parseInt(`${value}`, 10);
    }
}
