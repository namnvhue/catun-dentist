import 'moment/locale/vi';
import {Injectable} from '@angular/core';
import * as moment_ from 'moment';

const moment = moment_;
import CheckAndConverUtils from './utils/CheckAndConverUtils';


const utils = CheckAndConverUtils;

export const DD_MM_PATTERN = 'DD/MM';
export const DD_MM_YYYY_PATTERN = 'DD/MM/YYYY';
export const DD_MM_YYYY_HH_MM_PATTERN = 'DD/MM/YYYY HH:mm';
export const DD_MM_YYYY_HH_mm_ss_PATTERN = 'DD/MM/YYYY HH:mm:ss';
export const MMMM_PATTERN = 'MMMM';
export const MM_PATTERN = 'MM';
export const DD_PATTERN = 'DD';
export const WEEKDAY_PATTERN = 'dddd';
export const HH_MM = 'HH:mm';
export const YYYY_MM_DD = 'YYYY-MM-DD';
export const DD_MM_YYYY = 'DD-MM-YYYY';
export const YYYY_MM_DD_T_HH_mm_ss = 'YYYY-MM-DD\THH:mm:ss';

@Injectable()
export class DateService {

    constructor() {
        this.setMomentLocale('vi');
    }

    /**
     * Calculate list of weeks
     * @returns {Array}
     */
    getDates(lang, weekLimit) {
        this.setMomentLocale(lang);

        const today = moment().subtract(1, 'days');
        const startDate = moment().startOf('isoWeek');
        let endDate = moment().endOf('isoWeek');

        const days = [];
        let week = [];
        let day = startDate;

        while (days.length < weekLimit) {
            // Add day to week
            week.push({
                weekday: moment(day).format(WEEKDAY_PATTERN),
                fullDate: moment(day).format(DD_MM_PATTERN),
                fullDateYear: moment(day).format(DD_MM_YYYY_PATTERN),
                date: moment(day).format(DD_PATTERN),
                isFirst: moment(day).format(DD_PATTERN) === '01',
                isShown: moment(day).isSameOrAfter(today),
                isSat: moment(day).weekday() === 5,
                isSun: moment(day).weekday() === 6,
                isSelected: false,
                reason: ''
            });

            // Reset to new week
            if (day.format(DD_MM_PATTERN) == endDate.format(DD_MM_PATTERN)) {
                days.push(week);
                week = [];
                endDate = endDate.add(1, 'days').endOf('isoWeek');
            }

            day = day.clone().add(1, 'd');
        }
        return days;
    }

    /**
     * Get date-value-string from object of datepicker
     * @param dateObj
     * @param isGetYear
     * @returns {string}
     */
    joinDateElement(dateObj, isGetYear) {
        let stringDate = '';
        stringDate += utils.isNumber(dateObj.day) ? utils.padNumber(dateObj.day) : '';
        stringDate += utils.isNumber(dateObj.month) ? '/' + utils.padNumber(dateObj.month) : '';
        if (isGetYear) {
            stringDate += '/' + dateObj.year;
        }
        return stringDate;
    }

    joinDateElementByFormat(dateObj, isGetYear, format) {
        let defaultPattern = isGetYear ? DD_MM_YYYY_PATTERN : DD_MM_PATTERN;
        let stringDate = '';
        stringDate += utils.isNumber(dateObj.day) ? utils.padNumber(dateObj.day) : '';
        stringDate += utils.isNumber(dateObj.month) ? '/' + utils.padNumber(dateObj.month) : '';
        if (isGetYear) {
            stringDate += '/' + dateObj.year;
        }

        let date = this.getDateFromString(stringDate, defaultPattern);
        let targetDate = this.getDateStrFromDate(date, format);
        return targetDate;
    }

    joinTimeElement(timeObj) {
        let stringTime = '';
        stringTime += utils.isNumber(timeObj.hour) ? utils.padNumber(timeObj.hour) : '';
        stringTime += ':';
        stringTime += utils.isNumber(timeObj.minute) ? utils.padNumber(timeObj.minute) : '';
        return stringTime;
    }

    getStartOfWeek(date: any, format) {
        let currentDate: any;
        if (typeof date == 'string') {
            currentDate = this.getDateFromString(date, format);
        }
        return moment(currentDate).startOf('week');
    }

    getEndOfWeek(date: any, format) {
        let currentDate: any;
        if (typeof date == 'string') {
            currentDate = this.getDateFromString(date, format);
        }
        return moment(currentDate).endOf('week');
    }

    getCurrentStartOfMonth() {
        return this.today().startOf('month');
    }

    getCurrentStartOfMonthDatePickerObj() {
        const date = this.getCurrentStartOfMonth().toDate();
        return {year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate()};
    }

    getDatePickerObject(dateStr: string, format: string) {
        const date = this.getDateFromString(dateStr, format);
        return {year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate()};
    }

    getMomentDateFromString(dateStr: string, format: string) {
        return moment(dateStr, format);
    }

    getDateFromString(dateStr: string, format: string) {
        return moment(dateStr, format).toDate();
    }

    getDateStrFromDate(date: any, format: string) {
        return moment(date).format(format);
    }

    convertDateStringByFormat(fromFormat: string, toFormat: string, dateStr: string) {
        const date = this.getDateFromString(dateStr, fromFormat);
        return moment(date).format(YYYY_MM_DD_T_HH_mm_ss);
    }

    getHourAndMinFromString(dateStr: string, format: string) {
        const date = this.getDateFromString(dateStr, format);
        return {hour: +date.getDate(), minute: +date.getMinutes()};
    }

    addDaysToToday(quantity: number) {
        return this.addDays(this.today(), quantity).toDate();
    }

    addDays(originDate: any, quantity: number) {
        return moment(originDate.add(quantity, 'd'));
    }

    today() {
        return moment();
    }

    todayDatePickerObj() {
        const date = this.today().toDate();
        return {year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate()};
    }

    private setMomentLocale(lang: string) {
        moment.locale(lang);
        moment.updateLocale(lang, {
            week: {
                dow: 1,
                doy: 4
            }
        })
    }
}
