import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {DatePipe} from '@angular/common';
import {BookingService} from '../booking/booking.service';
import {DateService} from './shared.date.service';
import {NotificationService} from './notification.service';

import {
    AccountService,
    AuthServerProvider,
    CatunDentistAppSharedCommonModule,
    CatunDentistAppSharedLibsModule,
    CSRFService,
    HasAnyAuthorityDirective,
    JhiLoginModalComponent,
    LoginModalService,
    LoginService,
    Principal,
    StateStorageService,
    UserService,
} from './';

@NgModule({
    imports: [
        CatunDentistAppSharedLibsModule,
        CatunDentistAppSharedCommonModule
    ],
    declarations: [
        JhiLoginModalComponent,
        HasAnyAuthorityDirective
    ],
    providers: [
        LoginService,
        LoginModalService,
        AccountService,
        StateStorageService,
        Principal,
        CSRFService,
        AuthServerProvider,
        UserService,
        DatePipe,
        BookingService,
        DateService,
        NotificationService
    ],
    entryComponents: [JhiLoginModalComponent],
    exports: [
        CatunDentistAppSharedCommonModule,
        JhiLoginModalComponent,
        HasAnyAuthorityDirective,
        DatePipe
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class CatunDentistAppSharedModule {
}
