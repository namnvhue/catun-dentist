import {Component, Injectable} from '@angular/core';
import {NgbDateParserFormatter, NgbDatepickerI18n, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import CheckAndConverUtils from '../utils/CheckAndConverUtils';

const _utils = CheckAndConverUtils;

const I18N_VALUES = {
    'vi': {
        weekdays: ['Hai', 'Ba', 'Tư', 'Năm', 'Sáu', 'Bảy', 'CN'],
        months: ['Th01', 'Th02', 'Th03', 'Th04', 'Th05', 'Th06', 'Th07', 'Th08', 'Th09', 'Th10', 'Th11', 'Th12'],
    }
    // other languages you would support
};

// Define a service holding the language. You probably already have one if your app is i18ned. Or you could also
// use the Angular LOCALE_ID value
@Injectable()
export class I18n {
    language = 'vi';
}

// Define custom service providing the months and weekdays translations
@Injectable()
export class CustomDatepickerI18n extends NgbDatepickerI18n {

    constructor(private _i18n: I18n) {
        super();
    }

    getWeekdayShortName(weekday: number): string {
        return I18N_VALUES[this._i18n.language].weekdays[weekday - 1];
    }

    getMonthShortName(month: number): string {
        return I18N_VALUES[this._i18n.language].months[month - 1];
    }

    getMonthFullName(month: number): string {
        return this.getMonthShortName(month);
    }

    getDayAriaLabel(date: NgbDateStruct): string {
        return `${date.day}-${date.month}-${date.year}`;
    }
}

@Injectable()
export class CustomDateParserFormatter extends NgbDateParserFormatter {
    parse(value: string): NgbDateStruct {
        if (value) {
            const dateParts = value.trim().split('/');
            if (dateParts.length === 1 && _utils.isNumber(dateParts[0])) {
                return {year: _utils.toInteger(dateParts[0]), month: null, day: null};
            } else if (dateParts.length === 2 && _utils.isNumber(dateParts[0]) && _utils.isNumber(dateParts[1])) {
                return {
                    year: _utils.toInteger(dateParts[1]),
                    month: _utils.toInteger(dateParts[0]),
                    day: null
                };
            } else if (dateParts.length === 3 && _utils.isNumber(dateParts[0]) && _utils.isNumber(dateParts[1]) && _utils.isNumber(dateParts[2])) {
                return {
                    year: _utils.toInteger(dateParts[2]),
                    month: _utils.toInteger(dateParts[1]),
                    day: _utils.toInteger(dateParts[0])
                };
            }
        }
        return null;
    }

    format(date: NgbDateStruct): string {
        let stringDate: string = '';
        if (date) {
            stringDate += _utils.isNumber(date.day) ? _utils.padNumber(date.day) + '/' : '';
            stringDate += _utils.isNumber(date.month) ? _utils.padNumber(date.month) + '/' : '';
            stringDate += date.year;
        }
        return stringDate;
    }

}

@Component({
    selector: 'ngbd-datepicker-i18n',
    templateUrl: './datepicker-i18n.html',
    providers: [I18n, {provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n}] // define custom NgbDatepickerI18n provider
})
export class NgbdDatepickerI18n {
    model;
}
