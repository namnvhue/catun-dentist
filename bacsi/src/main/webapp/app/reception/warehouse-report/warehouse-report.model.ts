export class WarehouseReport {
    wareHouses?: StockReport[];
    totalRestAmountMonthAgo?: string;
    totalInputAmount?: string;
    totalOutputAmount?: string;
    totalRestAmount?: string;
    fileName?: string;
}

export class StockReport {
    name?: string;
    restAmountMonthAgo?: string;
    inputAmount?: string;
    outputAmount?: string;
    restAmount?: string;
}
