import {Component, OnInit, ViewChild} from '@angular/core';
import {HttpResponse} from '@angular/common/http';
import {DateService, DD_MM_YYYY} from '../../shared/shared.date.service';
import {WarehouseReportService} from './warehouse-report.service';
import {Router} from '@angular/router';
import {WarehouseReport} from './warehouse-report.model';
import {SERVER_API_URL} from '../../app.constants';

@Component({
    selector: 'jhi-confirm',
    templateUrl: './warehouse-report.component.html',
    styleUrls: ['./warehouse-report.component.css']
})
export class WarehouseReportComponent implements OnInit {

    message;
    staticAlertClosed = false;
    staticAlertType = 'success';

    @ViewChild('fromDate')
    fromDateDp: any;
    fromDateVal: { year: number, month: number, day: number };

    @ViewChild('toDate')
    toDateDp: any;
    toDateVal: { year: number, month: number, day: number };

    report: WarehouseReport;

    constructor(private warehouseReportService: WarehouseReportService,
                private dateService: DateService,
                private router: Router) {
    }

    ngOnInit() {
        this.fromDateVal = this.dateService.getCurrentStartOfMonthDatePickerObj();
        this.toDateVal = this.dateService.todayDatePickerObj();
        this.fetchWarehouseReport();
    }

    fetchWarehouseReport() {
        let fromDateStr = this.dateService.joinDateElement(this.fromDateVal, true);
        let toDateStr = this.dateService.joinDateElement(this.toDateVal, true);
        this.warehouseReportService.fetchWarehouseReport({
            fromDate: fromDateStr,
            toDate: toDateStr
        }).subscribe(
            (response: HttpResponse<WarehouseReport>) => {
                this.report = response.body;
            },
            (error) => {
                this.onError(error);
            });
    }

    downloadCSV() {
        let fromDateStr = this.dateService.joinDateElementByFormat(this.fromDateVal, true, DD_MM_YYYY);
        let toDateStr = this.dateService.joinDateElementByFormat(this.toDateVal, true, DD_MM_YYYY);
        let reportUrl: string = SERVER_API_URL + '/excel/warehouse-report-download';
        reportUrl += '/' + fromDateStr;
        reportUrl += '/' + toDateStr;

        let a = document.createElement('a');
        a.href = reportUrl;
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    }

    // downloadFile(data: any, fileName: string) {
    //     let blob = new Blob([data.body], { type: 'text/csv;charset=UTF-8' });
    //     let url = window.URL.createObjectURL(blob);
    //
    //     if(navigator.msSaveOrOpenBlob) {
    //         navigator.msSaveBlob(blob, fileName);
    //     } else {
    //         let a = document.createElement('a');
    //         a.href = url;
    //         a.download = fileName;
    //         document.body.appendChild(a);
    //         a.click();
    //         document.body.removeChild(a);
    //     }
    //     window.URL.revokeObjectURL(url);
    // }

    /**
     * Show alert box
     * @param msg
     * @param type
     */
    showALert(msg: string, type: string) {
        this.message = msg;
        this.staticAlertType = type;
        this.staticAlertClosed = false;
        setTimeout(() => this.staticAlertClosed = true, 4000);
    }

    private onError(error) {
        this.showALert(error.error.errorMessage, 'danger');
    }
}
