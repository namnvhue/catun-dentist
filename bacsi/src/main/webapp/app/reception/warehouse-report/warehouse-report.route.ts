import {Route} from '@angular/router';
import {WarehouseReportComponent} from './warehouse-report.component';
import {UserRouteAccessService} from '../../shared/auth/user-route-access-service';

export const WAREHOUSE_REPORT_ROUTE: Route = {
    path: 'warehouse-report',
    component: WarehouseReportComponent,
    data: {
        authorities: ['ROLE_ADMIN', 'ROLE_RECEPTIONIST'],
        pageTitle: 'CatunDentistApp.warehouseReport.title'
    },
    canActivate: [UserRouteAccessService]
};
