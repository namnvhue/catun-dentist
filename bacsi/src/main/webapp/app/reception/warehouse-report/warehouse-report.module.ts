import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CatunDentistAppSharedModule} from '../../shared/shared.module';
import {WarehouseReportComponent} from './warehouse-report.component';
import {WarehouseReportService} from './warehouse-report.service';
import {WAREHOUSE_REPORT_ROUTE} from './warehouse-report.route';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
    imports: [
        RouterModule.forChild([WAREHOUSE_REPORT_ROUTE]),
        CatunDentistAppSharedModule,
        NgMultiSelectDropDownModule
    ],
    declarations: [
        WarehouseReportComponent
    ],
    entryComponents: [],
    providers: [
        WarehouseReportService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CatunDentistWareHouseReportModule {
}
