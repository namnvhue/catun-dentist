import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';

@Injectable()
export class WarehouseReportService {
    private resourceUrl = SERVER_API_URL + '/receptionist';

    constructor(private http: HttpClient) {
    }

    fetchWarehouseReport(request): Observable<any> {
        return this.http.post(
            this.resourceUrl + '/warehouse-report', JSON.stringify(request), {
                headers: new HttpHeaders({'Content-Type': 'application/json'}),
                observe: 'response'
            });
    }

    downloadReport(fileName: string): Observable<any> {
        return this.http.get(this.resourceUrl + '/warehouse-report-download/' + fileName, {
            responseType: 'blob',
            headers: new HttpHeaders({'Content-Type': 'application/json'}),
            observe: 'response'
        });
    }
}
