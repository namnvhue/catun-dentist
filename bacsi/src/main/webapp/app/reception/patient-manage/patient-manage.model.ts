export class Patient {
    id?: string;
    name?: string;
    phone?: string;
    address?: string;
    email?: string;
    birthDay?: string;
    firstTreatmentTime?: string;
    lastTreatmentTime?: string;
}

export class Pageable {
    totalPage?: number;
    currentPage?: number;
    numberPages?: number[];
}

export class PatientListResponse {
    patients?: Patient[];
    pageable?: Pageable;
}
