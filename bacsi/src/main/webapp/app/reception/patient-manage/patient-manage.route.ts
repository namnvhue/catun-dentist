import {Route} from '@angular/router';
import {PatientManageComponent} from './patient-manage.component';
import {UserRouteAccessService} from '../../shared/auth/user-route-access-service';
import {TreatmentResolvePagingParams} from "../../doctor/treatment/treatment.route";

export const PATIENT_MANAGE_ROUTE: Route = {
    path: 'patient-management',
    component: PatientManageComponent,
    resolve: {
        'pagingParams': TreatmentResolvePagingParams
    },
    data: {
        authorities: ['ROLE_ADMIN', 'ROLE_RECEPTIONIST'],
        pageTitle: 'CatunDentistApp.patientManage.title'
    },
    canActivate: [UserRouteAccessService]
};
