import {Component, OnInit} from '@angular/core';
import {PatientManageService} from './patient-manage.service';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Pageable, Patient, PatientListResponse} from "./patient-manage.model";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'jhi-confirm',
    templateUrl: './patient-manage.component.html',
    styleUrls: ['./patient-manage.component.css']
})
export class PatientManageComponent implements OnInit {

    routeData: any;

    patients: Patient[];
    pageable: Pageable;

    predicate: any;
    reverse: any;
    page: any = 1;
    search: any = '';

    message;
    staticAlertClosed = false;
    staticAlertType = 'success';

    constructor(private activatedRoute: ActivatedRoute,
                private router: Router,
                private patientManageService: PatientManageService) {
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
    }

    ngOnInit() {
        this.loadAll();
    }

    loadAll() {
        this.patientManageService.fetchPatients({
            search: this.search,
            page: this.page,
            sort: this.reverse ? 'asc' : 'desc'
        }).subscribe(
            (res: HttpResponse<PatientListResponse>) => {
                this.onSuccess(res.body);
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    transition() {
        this.router.navigate(['/patient-management'], {
            queryParams:
                {
                    search: this.search,
                    page: this.page,
                    sort: (this.reverse ? 'asc' : 'desc')
                }
        });
        this.loadAll();
    }

    sort() {
        return (this.reverse ? 'asc' : 'desc');
    }

    private onSuccess(data: PatientListResponse) {
        if(data) {
            this.patients = data.patients;
            this.pageable = data.pageable;
        }
    }

    private onError(error) {
        this.message = error.message;
        this.staticAlertType = 'danger';
        this.staticAlertClosed = false;
        setTimeout(() => this.staticAlertClosed = true, 4000);
    }
}
