import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CatunDentistAppSharedModule} from '../../shared/shared.module';
import {PATIENT_MANAGE_ROUTE} from './patient-manage.route';
import {PatientManageComponent} from './patient-manage.component';
import {PatientManageService} from './patient-manage.service';

@NgModule({
    imports: [
        RouterModule.forChild([PATIENT_MANAGE_ROUTE]),
        CatunDentistAppSharedModule
    ],
    declarations: [
        PatientManageComponent
    ],
    entryComponents: [],
    providers: [
        PatientManageService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CatunDentistPatientMangeModule {
}
