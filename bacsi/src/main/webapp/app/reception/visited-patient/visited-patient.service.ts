import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';
import {VisitedPatientModel} from './visited-patient.model';

@Injectable()
export class VisitedPatientService {
    private resourceUrl = SERVER_API_URL + '/receptionist';

    constructor(private http: HttpClient) {
    }

    fetchVisitedPatientData(): Observable<VisitedPatientModel[]> {
        return this.http.get<VisitedPatientModel[]>(this.resourceUrl + '/interval-time-treatmented');
    }

}
