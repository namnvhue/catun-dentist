import {Route} from '@angular/router';
import {VisitedPatientComponent} from './visited-patient.component';
import {UserRouteAccessService} from '../../shared/auth/user-route-access-service';

export const VISITED_PATIENT_ROUTE: Route = {
    path: 'visited-patient',
    component: VisitedPatientComponent,
    data: {
        authorities: ['ROLE_ADMIN', 'ROLE_RECEPTIONIST'],
        pageTitle: 'CatunDentistApp.visitedPatientDisplay.title'
    },
    canActivate: [UserRouteAccessService]
};
