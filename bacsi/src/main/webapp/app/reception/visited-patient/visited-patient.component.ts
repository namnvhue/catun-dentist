import {Component, OnInit} from '@angular/core';
import {VisitedPatientService} from './visited-patient.service';
import {VisitedPatientModel} from './visited-patient.model';

@Component({
    selector: 'jhi-confirm',
    templateUrl: './visited-patient.component.html',
    styleUrls: ['./visited-patient.component.css']
})
export class VisitedPatientComponent implements OnInit {

    patientList: VisitedPatientModel[];

    message;
    staticAlertClosed = false;
    staticAlertType = 'success';

    constructor(private visitedPatientService: VisitedPatientService) {
    }

    ngOnInit() {
        this.getVisitedPatients();
    }

    getVisitedPatients() {
        this.visitedPatientService.fetchVisitedPatientData().subscribe(
            (res: VisitedPatientModel[]) => {
                this.patientList = res;
            },
            (err) => {
                this.onError(err);
            }
        );
    }

    private onError(error) {
        this.message = error.message;
        this.staticAlertType = 'danger';
        this.staticAlertClosed = false;
        setTimeout(() => this.staticAlertClosed = true, 4000);
    }
}
