import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CatunDentistAppSharedModule} from '../../shared/shared.module';
import {VISITED_PATIENT_ROUTE} from './visited-patient.route';
import {VisitedPatientComponent} from './visited-patient.component';
import {VisitedPatientService} from './visited-patient.service';

@NgModule({
    imports: [
        RouterModule.forChild([VISITED_PATIENT_ROUTE]),
        CatunDentistAppSharedModule
    ],
    declarations: [
        VisitedPatientComponent
    ],
    entryComponents: [],
    providers: [
        VisitedPatientService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CatunDentistVisitedPatientModule {
}
