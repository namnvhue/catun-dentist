import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CatunDentistAppointmentDisplayModule} from './appointments';
import {CatunDentistComingBirhtdayModule} from './coming-birthday';
import {CatunDentistVisitedPatientModule} from './visited-patient';
import {CatunDentistTodayBookingModule} from './today-booking';
import {CatunDentistPatientMangeModule} from './patient-manage/patient-manage.module';
import {CatunDentistExpenditureModule} from './expenditure/expenditure.module';
import {CatunDentistSupplierModule} from './supplier/supplier.module';
import {CatunDentistWareHouseModule} from './warehouse';
import {CatunDentistWareHouseDetailModule} from './warehouse-detail';
import {CatunDentistWareHouseReportModule} from './warehouse-report';

/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        CatunDentistTodayBookingModule,
        CatunDentistAppointmentDisplayModule,
        CatunDentistComingBirhtdayModule,
        CatunDentistVisitedPatientModule,
        CatunDentistPatientMangeModule,
        CatunDentistExpenditureModule,
        CatunDentistSupplierModule,
        CatunDentistWareHouseModule,
        CatunDentistWareHouseDetailModule,
        CatunDentistWareHouseReportModule
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CatunDentistAppReceptionModule {
}
