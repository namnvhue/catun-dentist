import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';
import {Supplier} from "./supplier.model";

@Injectable()
export class SupplierService {
    private resourceUrl = SERVER_API_URL + '/receptionist';

    constructor(private http: HttpClient) {
    }

    fetchSupplier(): Observable<Supplier[]> {
        return this.http.get<Supplier[]>(this.resourceUrl + '/supplier-get');
    }

    createSupplier(request): Observable<any> {
        return this.http.post(
            this.resourceUrl + '/supplier-create', JSON.stringify(request), {
                headers: new HttpHeaders({'Content-Type': 'application/json'}),
                observe: 'response'
            });
    }

    editSupplier(request): Observable<any> {
        return this.http.post(
            this.resourceUrl + '/supplier-edit', JSON.stringify(request), {
                headers: new HttpHeaders({'Content-Type': 'application/json'}),
                observe: 'response'
            });
    }

    deleteSupplier(id: any) {
        return this.http.get<any>(this.resourceUrl + '/supplier-delete/' + id);
    }
}
