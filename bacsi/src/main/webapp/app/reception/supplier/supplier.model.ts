export class Supplier {
    id?: string;
    name?: string;
    address?: string;
    phone?: string;
    email?: string;
}
