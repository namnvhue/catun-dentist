import {Component, OnInit} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {Supplier} from "./supplier.model";
import {SupplierService} from "./supplier.service";

@Component({
    selector: 'jhi-confirm',
    templateUrl: './supplier.component.html',
    styleUrls: ['./supplier.component.css']
})
export class SupplierComponent implements OnInit {

    message;
    staticAlertClosed = false;
    staticAlertType = 'success';

    supplierList: Supplier[];

    createSupplier: Supplier;
    createModal: NgbModalRef;

    deleteSupplier: Supplier;
    deleteModal: NgbModalRef;

    editSupplier: Supplier = new Supplier();
    editModal: NgbModalRef;

    constructor(private modalService: NgbModal,
                private supplierService: SupplierService) {
    }

    ngOnInit() {
        this.fetchSuppliers();
    }

    fetchSuppliers() {
        this.supplierList = [];
        this.supplierService.fetchSupplier().subscribe(
            (res: Supplier[]) => {
                this.supplierList = res;
            },
            (res: HttpErrorResponse) => this.onError(res)
        );
    }

    ///CREATE////////////////////////////////
    /////////////////////////////////////////
    openCreateSupplierModal(content: any) {
        this.closeAllModal();
        this.createSupplier = new Supplier();
        this.createModal = this.modalService.open(content, {size: 'lg'});
    }

    confirmCreate() {
        this.supplierService.createSupplier(this.createSupplier).subscribe(
            (response) => {
                this.resetCreateModal();
                this.fetchSuppliers();
            },
            (error) => {
                this.onError(error);
            });
    }

    closeCreateModal() {
        this.createModal.close();
    }

    resetCreateModal() {
        this.createSupplier = new Supplier();
        this.closeCreateModal();
    }

    ///EDIT//////////////////////////////////
    /////////////////////////////////////////
    openEditSupplierModal(content: any, supplier: Supplier) {
        this.closeAllModal();
        this.editSupplier = Object.assign({}, supplier);
        this.editModal = this.modalService.open(content, {size: 'lg'});
    }

    confirmEdit() {
        this.supplierService.editSupplier(this.editSupplier).subscribe(
            (response) => {
                this.resetEditModal();
                this.fetchSuppliers();
            },
            (error) => {
                this.resetEditModal();
                this.onError(error);
            });
    }

    closeEditModal() {
        this.editModal.close();
    }

    resetEditModal() {
        this.editSupplier = null;
        this.closeEditModal();
    }

    ///DELETE//////////////////////////////////
    /////////////////////////////////////////
    openDeleteSupplierModal(content: any, supplier: Supplier) {
        this.closeAllModal();
        this.deleteSupplier = Object.assign({}, supplier);
        this.deleteModal = this.modalService.open(content, {size: 'lg'});
    }

    confirmDelete() {
        this.supplierService.deleteSupplier(this.deleteSupplier.id).subscribe(
            (response) => {
                this.resetDeleteModal();
            },
            (error) => {
                this.resetDeleteModal();
                this.onError(error);
            },
            () => {
                this.fetchSuppliers();
            });
    }

    closeDeleteModal() {
        this.deleteModal.close();
    }

    resetDeleteModal() {
        this.deleteSupplier = null;
        this.closeDeleteModal();
    }

    /**
     * Show alert box
     * @param msg
     * @param type
     */
    showALert(msg: string, type: string) {
        this.message = msg;
        this.staticAlertType = type;
        this.staticAlertClosed = false;
        setTimeout(() => this.staticAlertClosed = true, 4000);
    }

    private onError(error) {
        this.showALert(error.error.errorMessage, 'danger');
    }

    closeAllModal() {
        if (this.createModal) {
            this.createModal.close();
        }
        if (this.editModal) {
            this.editModal.close();
        }
        if (this.deleteModal) {
            this.deleteModal.close();
        }
    }
}
