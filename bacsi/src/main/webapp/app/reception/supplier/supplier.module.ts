import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CatunDentistAppSharedModule} from '../../shared/shared.module';
import {SupplierComponent} from './supplier.component';
import {SupplierService} from './supplier.service';
import {SUPPLIER_ROUTE} from './supplier.route';

@NgModule({
    imports: [
        RouterModule.forChild([SUPPLIER_ROUTE]),
        CatunDentistAppSharedModule
    ],
    declarations: [
        SupplierComponent
    ],
    entryComponents: [],
    providers: [
        SupplierService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CatunDentistSupplierModule {
}
