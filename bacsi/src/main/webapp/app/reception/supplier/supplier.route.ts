import {Route} from '@angular/router';
import {SupplierComponent} from './supplier.component';
import {UserRouteAccessService} from '../../shared/auth/user-route-access-service';

export const SUPPLIER_ROUTE: Route = {
    path: 'supplier',
    component: SupplierComponent,
    data: {
        authorities: ['ROLE_ADMIN', 'ROLE_RECEPTIONIST'],
        pageTitle: 'CatunDentistApp.supplier.title'
    },
    canActivate: [UserRouteAccessService]
};
