import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CatunDentistAppSharedModule} from '../../shared/shared.module';
import {TOMORROW_BOOKING_ROUTE} from './appointments.route';
import {AppointmentsComponent} from './appointments.component';
import {AppointmentsService} from './appointments.service';

@NgModule({
    imports: [
        RouterModule.forChild([TOMORROW_BOOKING_ROUTE]),
        CatunDentistAppSharedModule
    ],
    declarations: [
        AppointmentsComponent
    ],
    entryComponents: [],
    providers: [
        AppointmentsService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CatunDentistAppointmentDisplayModule {
}
