export class Appointment {
    bookingId?: number;
    code?: string;
    name?: string;
    numberPhone?: string;
    timeBooking?: string;
    active?: boolean = false;
}

export class EditRequest {
    bookingId?: number;
    name?: string;
    dateVal?: string;
    timeVal?: string;
}
