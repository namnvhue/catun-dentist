import {Route} from '@angular/router';
import {AppointmentsComponent} from './appointments.component';
import {UserRouteAccessService} from '../../shared/auth/user-route-access-service';

export const TOMORROW_BOOKING_ROUTE: Route = {
    path: 'coming-appointments',
    component: AppointmentsComponent,
    data: {
        authorities: ['ROLE_ADMIN', 'ROLE_RECEPTIONIST','ROLE_DOCTOR'],
        pageTitle: 'CatunDentistApp.appointmentDisplay.title'
    },
    canActivate: [UserRouteAccessService]
};
