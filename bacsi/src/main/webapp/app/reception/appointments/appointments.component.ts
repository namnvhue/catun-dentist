import {Component, OnInit, ViewChild} from '@angular/core';
import {Appointment, EditRequest} from './appointments.model';
import {AppointmentsService} from './appointments.service';
import {HttpResponse} from '@angular/common/http';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import * as moment_ from 'moment';
import {AvailabilitySettingService} from '../../entities/availability-setting/availability-setting.service';
import {DateService, DD_MM_YYYY_PATTERN} from '../../shared/shared.date.service';

const moment = moment_;
const today = moment();

@Component({
    selector: 'jhi-confirm',
    templateUrl: './appointments.component.html',
    styleUrls: ['./appointments.component.css']
})
export class AppointmentsComponent implements OnInit {

    bookingList: Appointment[];
    numberDate: number = 3;

    editBooking: Appointment;
    editRequest: EditRequest;
    editBookingModal: NgbModalRef;

    timeSlots: any[] = [];

    @ViewChild('dateVal')
    dateValDp: any; // dateVal date picker
    editDateVal: any;
    minDate = {year: today.year(), month: today.month() + 1, day: today.date()};
    // maxDate = {year: today.year(), month: today.month() + 1, day: today.date()};

    message;
    staticAlertClosed = false;
    staticAlertType = 'success';

    constructor(private appointmentsService: AppointmentsService,
                private modalService: NgbModal,
                private availabilitySettingService: AvailabilitySettingService,
                private dateService: DateService) {
    }

    ngOnInit() {
        this.getTodayBooking();
        this.getSettings();
    }

    getTodayBooking() {
        this.appointmentsService.fetchTomorrowBookingData({numberDate: this.numberDate}).subscribe(
            (res: HttpResponse<Appointment[]>) => {
                this.bookingList = res.body;
            },
            (err) => {
                this.onError(err);
            }
        );
    }

    reload() {
        this.appointmentsService.fetchTomorrowBookingData({numberDate: this.numberDate}).subscribe(
            (res: HttpResponse<Appointment[]>) => {
                this.bookingList = res.body;
            },
            (err) => {
                this.onError(err);
            }
        );
    }

    edit(booking: Appointment, content: any) {
        this.editBooking = booking;

        this.editRequest = new EditRequest();
        this.editRequest.bookingId = this.editBooking.bookingId;
        this.editRequest.name = this.editBooking.name;
        this.editRequest.timeVal = this.editBooking.timeBooking ? this.editBooking.timeBooking.split(/(\s+)/)[0] : '';
        this.editRequest.dateVal = this.editBooking.timeBooking ? this.editBooking.timeBooking.split(/(\s+)/)[2] : '';
        this.editDateVal = this.dateService.getDatePickerObject(this.editRequest.dateVal, DD_MM_YYYY_PATTERN);

        this.editBookingModal = this.modalService.open(content);
    }

    submitEdit() {
        this.editRequest.dateVal = this.dateService.joinDateElement(this.editDateVal, false);
        console.log(this.editRequest);
        this.appointmentsService.editBookingData(this.editRequest).subscribe(
            (res) => {
                this.closeEdit();
                this.getTodayBooking();
            },
            (err) => {
                console.log(err);
            }
        );
    }

    closeEdit() {
        this.editBookingModal.close();
        this.editRequest = new EditRequest;
        this.editBooking = null;
        this.editDateVal = null;
    }

    getSettings() {
        this.availabilitySettingService.fetchSettingData().subscribe(
            (res) => {
                if (res.timeWorking) {
                    this.timeSlots = this.availabilitySettingService.slitWorkTime(res.timeWorking);
                }
            },
            (err) => {
                console.log(err.errorMessage);
            }
        );
    }

    private onError(error) {
        this.message = error.message;
        this.staticAlertType = 'danger';
        this.staticAlertClosed = false;
        setTimeout(() => this.staticAlertClosed = true, 4000);
    }
}
