import {Route} from '@angular/router';
import {ComingBirthdayComponent} from './coming-birthday.component';
import {UserRouteAccessService} from '../../shared/auth/user-route-access-service';

export const BIRTHDAY_TODAY_ROUTE: Route = {
    path: 'coming-birthday',
    component: ComingBirthdayComponent,
    data: {
        authorities: ['ROLE_ADMIN', 'ROLE_RECEPTIONIST'],
        pageTitle: 'CatunDentistApp.comingBirthdayDisplay.title'
    },
    canActivate: [UserRouteAccessService]
};
