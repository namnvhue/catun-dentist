import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';
import {BirthdayTodayModel} from './coming-birthday.model';

@Injectable()
export class ComingBirthdayService {
    private resourceUrl = SERVER_API_URL + '/receptionist';

    constructor(private http: HttpClient) {
    }

    fetchBirthdayData(request): Observable<any> {
        return this.http.post(
            this.resourceUrl + '/coming-birthday', JSON.stringify(request), {
                headers: new HttpHeaders({'Content-Type': 'application/json'}),
                observe: 'response'
            });
    }

}
