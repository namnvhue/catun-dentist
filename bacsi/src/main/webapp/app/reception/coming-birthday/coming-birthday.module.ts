import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CatunDentistAppSharedModule} from '../../shared/shared.module';
import {ComingBirthdayComponent} from './coming-birthday.component';
import {ComingBirthdayService} from './coming-birthday.service';
import {BIRTHDAY_TODAY_ROUTE} from './coming-birthday.route';

@NgModule({
    imports: [
        RouterModule.forChild([BIRTHDAY_TODAY_ROUTE]),
        CatunDentistAppSharedModule
    ],
    declarations: [
        ComingBirthdayComponent
    ],
    entryComponents: [],
    providers: [
        ComingBirthdayService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CatunDentistComingBirhtdayModule {
}
