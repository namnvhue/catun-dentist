import {Component, OnInit} from '@angular/core';
import {ComingBirthdayService} from './coming-birthday.service';
import {BirthdayTodayModel} from './coming-birthday.model';
import {HttpResponse} from "@angular/common/http";
import {Appointment} from "../appointments/appointments.model";

@Component({
    selector: 'jhi-confirm',
    templateUrl: './coming-birthday.component.html',
    styleUrls: ['./coming-birthday.component.css']
})
export class ComingBirthdayComponent implements OnInit {

    patientList: BirthdayTodayModel[];

    message;
    staticAlertClosed = false;
    staticAlertType = 'success';

    numberDate: number = 0;

    constructor(private birthdayTodayService: ComingBirthdayService) {
    }

    ngOnInit() {
        this.getBirthday();
    }

    getBirthday() {
        this.birthdayTodayService.fetchBirthdayData({numberDate: this.numberDate}).subscribe(
            (res: HttpResponse<BirthdayTodayModel[]>) => {
                this.patientList = res.body;
            },
            (err) => {
                this.onError(err);
            }
        );
    }

    reload() {
        this.getBirthday();
    }

    private onError(error) {
        this.message = error.message;
        this.staticAlertType = 'danger';
        this.staticAlertClosed = false;
        setTimeout(() => this.staticAlertClosed = true, 4000);
    }
}
