import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CatunDentistAppSharedModule} from '../../shared/shared.module';
import {ExpenditureComponent} from './expenditure.component';
import {ExpenditureService} from './expenditure.service';
import {EXPENDITURE_ROUTE} from './expenditure.route';

@NgModule({
    imports: [
        RouterModule.forChild([EXPENDITURE_ROUTE]),
        CatunDentistAppSharedModule
    ],
    declarations: [
        ExpenditureComponent
    ],
    entryComponents: [],
    providers: [
        ExpenditureService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CatunDentistExpenditureModule {
}
