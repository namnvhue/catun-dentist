import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';

@Injectable()
export class ExpenditureService {
    private resourceUrl = SERVER_API_URL + '/receptionist';

    constructor(private http: HttpClient) {
    }

    saveExpenditure(request): Observable<any> {
        return this.http.post(
            this.resourceUrl + '/manage-expenditure', JSON.stringify(request), {
                headers: new HttpHeaders({'Content-Type': 'application/json'}),
                observe: 'response'
            });
    }

    fetchExpenditure(request): Observable<any> {
        return this.http.post(
            this.resourceUrl + '/manage-expenditure-show', JSON.stringify(request), {
                headers: new HttpHeaders({'Content-Type': 'application/json'}),
                observe: 'response'
            });
    }

    delete(id: any) {
        return this.http.get<any>(this.resourceUrl + '/manage-expenditure-delete/' + id);
    }
}
