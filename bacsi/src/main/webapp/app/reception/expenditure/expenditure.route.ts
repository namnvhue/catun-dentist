import {Route} from '@angular/router';
import {ExpenditureComponent} from './expenditure.component';
import {UserRouteAccessService} from '../../shared/auth/user-route-access-service';

export const EXPENDITURE_ROUTE: Route = {
    path: 'expenditure',
    component: ExpenditureComponent,
    data: {
        authorities: ['ROLE_ADMIN', 'ROLE_RECEPTIONIST'],
        pageTitle: 'CatunDentistApp.expenditure.title'
    },
    canActivate: [UserRouteAccessService]
};
