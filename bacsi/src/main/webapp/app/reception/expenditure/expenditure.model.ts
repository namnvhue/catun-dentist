export class ExpenditureRequest {
    treatmentDetailId?: string;
    type?: string;
    name?: string;
    value?: string;
    createDate?: string;
}

export class ExpenditureResponse {
    expenditures?: Expenditure[];
    totalCollect?: string;
    totalExpenditure?: string;
}

export class Expenditure {
    id?: string;
    type?: string;
    name?: string;
    value?: string;
    createDateExpenditure?: string;
    createById?: string;
    createByName?: string;
}
