import {Component, OnInit, ViewChild} from '@angular/core';
import {ExpenditureService} from './expenditure.service';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Expenditure, ExpenditureRequest, ExpenditureResponse} from './expenditure.model';
import {DateService, DD_MM_YYYY_PATTERN} from '../../shared/shared.date.service';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'jhi-confirm',
    templateUrl: './expenditure.component.html',
    styleUrls: ['./expenditure.component.css']
})
export class ExpenditureComponent implements OnInit {

    message;
    staticAlertClosed = false;
    staticAlertType = 'success';

    formDisplay: boolean = false;
    newExpenditure: ExpenditureRequest;
    dateVal: { year: number, month: number, day: number };

    @ViewChild('date')
    dateDp: any;

    fromDateVal: { year: number, month: number, day: number };
    @ViewChild('fromDate')
    fromDateDp: any;

    toDateVal: { year: number, month: number, day: number };
    @ViewChild('toDate')
    toDateDp: any;

    expenditureList: Expenditure[];
    expenditureRes: ExpenditureResponse;

    tempExpenditure: Expenditure;
    deleteModal: NgbModalRef;

    saveModal: NgbModalRef;

    constructor(private expenditureService: ExpenditureService,
                private dateService: DateService,
                private modalService: NgbModal) {
    }

    ngOnInit() {
        this.dateVal = this.dateService.todayDatePickerObj();
        this.newExpenditure = new ExpenditureRequest();
        this.initExpenditureData();
    }

    saveExpenditure(content: any) {
        this.newExpenditure.createDate = this.dateService.joinDateElement(this.dateVal, true);
        this.saveModal = this.modalService.open(content);
    }

    confirmSave() {
        this.expenditureService.saveExpenditure(this.newExpenditure).subscribe(
            (response) => {
                if (response.status === 200) {
                    this.saveModal.close();
                    this.resetNewExpenditure();
                    this.search();
                }
            },
            (error) => {
                this.onError(error);
            });
    }

    closeSaveModal() {
        this.saveModal.close();
    }

    search() {
        let fromStr = this.dateService.joinDateElement(this.fromDateVal, true);
        let toStr = this.dateService.joinDateElement(this.toDateVal, true);
        this.expenditureService.fetchExpenditure({
            fromDate: fromStr,
            toDate: toStr
        }).subscribe(
            (res: HttpResponse<ExpenditureResponse>) => {
                this.expenditureRes = res.body;
                this.expenditureList = res.body.expenditures;
            },
            (res: HttpErrorResponse) => this.onError(res)
        );
    }

    resetNewExpenditure() {
        this.formDisplay = false;
        this.newExpenditure = new ExpenditureRequest();
        this.dateVal = this.dateService.todayDatePickerObj();
    }

    initExpenditureData() {
        this.initDateRange();
        this.search();
    }

    initDateRange() {
        let from = this.dateService.getCurrentStartOfMonth();
        let fromStr = this.dateService.getDateStrFromDate(from, DD_MM_YYYY_PATTERN);
        this.fromDateVal = this.dateService.getDatePickerObject(fromStr, DD_MM_YYYY_PATTERN);
        this.toDateVal = this.dateService.todayDatePickerObj();
    }

    deleteExpenditure(content: any, e: Expenditure) {
        this.tempExpenditure = e;
        this.deleteModal = this.modalService.open(content);
    }

    confirmDelete() {
        this.expenditureService.delete(this.tempExpenditure.id).subscribe(
            (res) => {
                this.tempExpenditure = null;
                this.deleteModal.close();
                this.search();
            },
            (err) => {
                this.onError(err);
            }
        );
    }

    closeDeleteModal() {
        this.tempExpenditure = null;
        this.deleteModal.close();
    }

    dateDpOnClick() {
        this.dateDp.toggle();
    }

    fromDateDpOnClick() {
        this.fromDateDp.toggle();
    }

    toDateDpOnClick() {
        this.toDateDp.toggle();
    }

    /**
     * Show alert box
     * @param msg
     * @param type
     */
    showALert(msg: string, type: string) {
        this.message = msg;
        this.staticAlertType = type;
        this.staticAlertClosed = false;
        setTimeout(() => this.staticAlertClosed = true, 4000);
    }

    private onError(error) {
        this.showALert(error.error.errorMessage, 'danger');
    }
}
