export class Stock {
    id?: string;
    code?: string;
    name?: string;
    userId?: string;
    userName?: string;
    supplierId?: string;
    supplierName?: string;
    dateInput?: string;
    amount?: string;
    amountUsed?: string;
    price?: string;
    amountExpiryDate?: string;
    expiryDate?: string;
}

export class StockIO {
    warehouseId?: string;
    warehouseName?: string;
    type?: string;
    name?: string;
    amountIOput?: string;
    timeIOput?: string;
}
