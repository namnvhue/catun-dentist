import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CatunDentistAppSharedModule} from '../../shared/shared.module';
import {WarehouseComponent} from './warehouse.component';
import {WarehouseService} from './warehouse.service';
import {WAREHOUSE_ROUTE} from './warehouse.route';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
    imports: [
        RouterModule.forChild([WAREHOUSE_ROUTE]),
        CatunDentistAppSharedModule,
        NgMultiSelectDropDownModule
    ],
    declarations: [
        WarehouseComponent
    ],
    entryComponents: [],
    providers: [
        WarehouseService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CatunDentistWareHouseModule {
}
