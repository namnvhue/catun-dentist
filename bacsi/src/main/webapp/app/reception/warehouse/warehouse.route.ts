import {Route} from '@angular/router';
import {WarehouseComponent} from './warehouse.component';
import {UserRouteAccessService} from '../../shared/auth/user-route-access-service';

export const WAREHOUSE_ROUTE: Route = {
    path: 'warehouse',
    component: WarehouseComponent,
    data: {
        authorities: ['ROLE_ADMIN', 'ROLE_RECEPTIONIST'],
        pageTitle: 'CatunDentistApp.warehouse.title'
    },
    canActivate: [UserRouteAccessService]
};
