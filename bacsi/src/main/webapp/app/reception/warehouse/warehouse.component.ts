import {Component, OnInit, ViewChild} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {Stock, StockIO} from './warehouse.model';
import {WarehouseService} from './warehouse.service';
import {Supplier} from '../supplier/supplier.model';
import {SupplierService} from '../supplier';
import {DateService} from '../../shared/shared.date.service';
import {Router} from "@angular/router";

@Component({
    selector: 'jhi-confirm',
    templateUrl: './warehouse.component.html',
    styleUrls: ['./warehouse.component.css']
})
export class WarehouseComponent implements OnInit {

    message;
    staticAlertClosed = false;
    staticAlertType = 'success';

    createMessage;
    createAlertClosed = false;
    createAlertType = 'success';

    stockList: Stock[];

    isSupplierAvailable: boolean = false;
    supplierDropdownSettings = {};
    supplierDropdownList = [];

    createStock: Stock;
    createModal: NgbModalRef;

    @ViewChild('createStockInputDate')
    createStockInputDateDp: any;
    createStockInputDateVal: { year: number, month: number, day: number };

    @ViewChild('createStockExpiryDate')
    createStockExpiryDateDp: any;
    createStockExpiryDateVal: { year: number, month: number, day: number };

    deleteStock: Stock;
    deleteModal: NgbModalRef;

    addStockIO: StockIO;
    addStockIOModal: NgbModalRef;
    @ViewChild('stockIODate')
    stockIODateDp: any;
    stockIODateVal: { year: number, month: number, day: number };

    constructor(private modalService: NgbModal,
                private warehouseService: WarehouseService,
                private supplierService: SupplierService,
                private dateService: DateService,
                private router: Router) {
    }

    ngOnInit() {
        this.createStockInputDateVal = this.dateService.todayDatePickerObj();
        this.createStockExpiryDateVal = this.dateService.todayDatePickerObj();
        this.stockIODateVal = this.dateService.todayDatePickerObj();
        this.fetchStocks();
        this.fetchSuppliers();
    }

    fetchStocks() {
        this.stockList = [];
        this.warehouseService.fetchStock().subscribe(
            (res: Stock[]) => {
                this.stockList = res;
            },
            (res: HttpErrorResponse) => this.onError(res)
        );
    }

    fetchSuppliers() {
        this.supplierDropdownList = [];
        this.supplierService.fetchSupplier().subscribe(
            (res: Supplier[]) => {
                res.forEach((s) => {
                    this.supplierDropdownList.push({
                        'id': s.id,
                        'name': s.name
                    });
                });
            },
            (res: HttpErrorResponse) => this.onError(res),
            () => this.initSupplierSelect()
        );
    }

    initSupplierSelect() {
        this.isSupplierAvailable = true;
        this.supplierDropdownSettings = {
            singleSelection: true,
            idField: 'id',
            textField: 'name',
            selectAllText: 'Chọn hết',
            unSelectAllText: 'Bỏ chọn hết',
            itemsShowLimit: 1,
            allowSearchFilter: true
        };
    }

    chooseSupplierOnCreate(event) {
        if (this.createStock) {
            this.createStock.supplierId = event.id;
        }
    }

    ///ADD STOCK IO///////////////////////////
    /////////////////////////////////////////
    openAddStockIOModal(content: any, stock: Stock, type: string) {
        this.closeAllModal();
        this.addStockIO = new StockIO();
        this.addStockIO.warehouseId = stock.id;
        this.addStockIO.warehouseName = stock.name;
        this.addStockIO.type = type;
        this.addStockIOModal = this.modalService.open(content, {size: 'lg'});
    }

    confirmAddStockIO() {
        this.addStockIO.timeIOput = this.dateService.joinDateElement(this.stockIODateVal, true);
        this.warehouseService.addStockIO(this.addStockIO).subscribe(
            (response) => {
                this.resetAddStockIOModal();
            },
            (error) => {
                this.showCreateALert(error.error.errorMessage, 'danger');
            });
    }

    closeAddStockIOModal() {
        this.addStockIOModal.close();
    }

    resetAddStockIOModal() {
        this.addStockIO = new StockIO();
        this.closeAddStockIOModal();
    }

    ///CREATE////////////////////////////////
    /////////////////////////////////////////
    openCreateStockModal(content: any) {
        this.closeAllModal();
        this.createStock = new Stock();
        this.createModal = this.modalService.open(content, {size: 'lg'});
    }

    confirmCreate() {
        this.createStock.dateInput = this.dateService.joinDateElement(this.createStockInputDateVal, true);
        this.createStock.expiryDate = this.dateService.joinDateElement(this.createStockExpiryDateVal, true);
        this.warehouseService.createStock(this.createStock).subscribe(
            (response) => {
                this.resetCreateModal();
                this.fetchStocks();
            },
            (error) => {
                this.showCreateALert(error.error.errorMessage, 'danger');
            });
    }

    closeCreateModal() {
        this.createModal.close();
    }

    resetCreateModal() {
        this.createStock = new Stock();
        this.closeCreateModal();
    }

    ///DELETE//////////////////////////////////
    /////////////////////////////////////////
    openDeleteStockModal(content: any, supplier: Stock) {
        this.closeAllModal();
        this.deleteStock = Object.assign({}, supplier);
        this.deleteModal = this.modalService.open(content, {size: 'lg'});
    }

    confirmDelete() {
        this.warehouseService.deleteStock(this.deleteStock.id).subscribe(
            (response) => {
                this.resetDeleteModal();
            },
            (error) => {
                this.resetDeleteModal();
                this.onError(error);
            },
            () => {
                this.fetchStocks();
            });
    }

    closeDeleteModal() {
        this.deleteModal.close();
    }

    resetDeleteModal() {
        this.deleteStock = null;
        this.closeDeleteModal();
    }

    viewStockIODetails(stock: Stock) {
        this.router.navigate(['warehouse-detail', stock.id]);
    }

    /**
     * Show alert box
     * @param msg
     * @param type
     */
    showALert(msg: string, type: string) {
        this.message = msg;
        this.staticAlertType = type;
        this.staticAlertClosed = false;
        setTimeout(() => this.staticAlertClosed = true, 4000);
    }

    /**
     * Show alert box
     * @param msg
     * @param type
     */
    showCreateALert(msg: string, type: string) {
        this.createMessage = msg;
        this.createAlertType = type;
        this.createAlertClosed = false;
        setTimeout(() => this.createAlertClosed = true, 4000);
    }

    private onError(error) {
        this.showALert(error.error.errorMessage, 'danger');
    }

    closeAllModal() {
        if (this.createModal) {
            this.createModal.close();
        }
        if (this.deleteModal) {
            this.deleteModal.close();
        }
    }

}
