import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';
import {Stock} from "./warehouse.model";

@Injectable()
export class WarehouseService {
    private resourceUrl = SERVER_API_URL + '/receptionist';

    constructor(private http: HttpClient) {
    }

    fetchSingleStock(id): Observable<Stock> {
        return this.http.get<Stock>(this.resourceUrl + '/warehouse-get/' + id);
    }

    fetchStock(): Observable<Stock[]> {
        return this.http.get<Stock[]>(this.resourceUrl + '/warehouse-get');
    }

    createStock(request): Observable<any> {
        return this.http.post(
            this.resourceUrl + '/warehouse-create', JSON.stringify(request), {
                headers: new HttpHeaders({'Content-Type': 'application/json'}),
                observe: 'response'
            });
    }

    deleteStock(id: any) {
        return this.http.get<any>(this.resourceUrl + '/warehouse-delete/' + id);
    }

    addStockIO(request): Observable<any> {
        return this.http.post(
            this.resourceUrl + '/warehousedetail-create', JSON.stringify(request), {
                headers: new HttpHeaders({'Content-Type': 'application/json'}),
                observe: 'response'
            });
    }

}
