import {Component, OnInit} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {DateService} from '../../shared/shared.date.service';
import {StockDetail} from './warehouse-detail.model';
import {WarehouseDetailService} from './warehouse-detail.service';
import {Stock} from '../warehouse/warehouse.model';
import {ActivatedRoute, Router} from '@angular/router';
import {WarehouseService} from "../warehouse";

@Component({
    selector: 'jhi-confirm',
    templateUrl: './warehouse-detail.component.html',
    styleUrls: ['./warehouse-detail.component.css']
})
export class WarehouseDetailComponent implements OnInit {

    message;
    staticAlertClosed = false;
    staticAlertType = 'success';

    stockId: string;
    selectedStock: Stock;
    stockDetailList: StockDetail[];

    deleteStockDetail: StockDetail;
    deleteModal: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute,
                private modalService: NgbModal,
                private warehouseDetailService: WarehouseDetailService,
                private warehouseService: WarehouseService,
                private dateService: DateService,
                private router: Router) {
    }

    ngOnInit() {
        this.stockId = this.activatedRoute.snapshot.paramMap.get('stockId');
        this.fetchSingleStock();
    }

    fetchSingleStock() {
        this.warehouseService.fetchSingleStock(this.stockId).subscribe(
            (res: Stock) => {
                this.selectedStock = res;
                this.fetchStockDetails();
            },
            (res: HttpErrorResponse) => {
                this.router.navigate(['warehouse']);
            }
        );
    }

    fetchStockDetails() {
        if (this.selectedStock) {
            this.stockDetailList = [];
            this.warehouseDetailService.fetchStockDetail(this.stockId).subscribe(
                (res: StockDetail[]) => {
                    this.stockDetailList = res;
                },
                (res: HttpErrorResponse) => this.onError(res)
            );
        }
    }

    ///DELETE//////////////////////////////////
    /////////////////////////////////////////
    openDeleteStockDetailModal(content: any, stockDetail: StockDetail) {
        this.closeAllModal();
        this.deleteStockDetail = Object.assign({}, stockDetail);
        this.deleteModal = this.modalService.open(content, {size: 'lg'});
    }

    confirmDelete() {
        this.warehouseDetailService.deleteStockDetail(this.deleteStockDetail.id).subscribe(
            (response) => {
                this.resetDeleteModal();
            },
            (error) => {
                this.resetDeleteModal();
                this.onError(error);
            },
            () => {
                this.fetchStockDetails();
            });
    }

    closeDeleteModal() {
        this.deleteModal.close();
    }

    resetDeleteModal() {
        this.deleteStockDetail = null;
        this.closeDeleteModal();
    }

    /**
     * Show alert box
     * @param msg
     * @param type
     */
    showALert(msg: string, type: string) {
        this.message = msg;
        this.staticAlertType = type;
        this.staticAlertClosed = false;
        setTimeout(() => this.staticAlertClosed = true, 4000);
    }

    private onError(error) {
        this.showALert(error.error.errorMessage, 'danger');
    }

    closeAllModal() {
        if (this.deleteModal) {
            this.deleteModal.close();
        }
    }

}
