export class StockDetail {
    id?: string;
    wareHouseId?: string;
    userId?: string;
    userName?: string;
    type?: string;
    timeIOput?: string;
    amountIOput?: string;
    amount?: string;
}
