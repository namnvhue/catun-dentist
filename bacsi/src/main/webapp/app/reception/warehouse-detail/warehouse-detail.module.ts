import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CatunDentistAppSharedModule} from '../../shared/shared.module';
import {WarehouseDetailComponent} from './warehouse-detail.component';
import {WarehouseDetailService} from './warehouse-detail.service';
import {WAREHOUSE_DETAIL_ROUTE} from './warehouse-detail.route';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
    imports: [
        RouterModule.forChild([WAREHOUSE_DETAIL_ROUTE]),
        CatunDentistAppSharedModule,
        NgMultiSelectDropDownModule
    ],
    declarations: [
        WarehouseDetailComponent
    ],
    entryComponents: [],
    providers: [
        WarehouseDetailService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CatunDentistWareHouseDetailModule {
}
