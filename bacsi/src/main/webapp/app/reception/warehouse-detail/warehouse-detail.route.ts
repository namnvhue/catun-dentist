import {Route} from '@angular/router';
import {WarehouseDetailComponent} from './warehouse-detail.component';
import {UserRouteAccessService} from '../../shared/auth/user-route-access-service';

export const WAREHOUSE_DETAIL_ROUTE: Route = {
    path: 'warehouse-detail/:stockId',
    component: WarehouseDetailComponent,
    data: {
        authorities: ['ROLE_ADMIN', 'ROLE_RECEPTIONIST'],
        pageTitle: 'CatunDentistApp.warehouseDetail.title'
    },
    canActivate: [UserRouteAccessService]
};
