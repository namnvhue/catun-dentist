import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_API_URL} from '../../app.constants';
import {StockDetail} from './warehouse-detail.model';

@Injectable()
export class WarehouseDetailService {
    private resourceUrl = SERVER_API_URL + '/receptionist';

    constructor(private http: HttpClient) {
    }

    fetchStockDetail(stockId: string): Observable<StockDetail[]> {
        return this.http.get<StockDetail[]>(this.resourceUrl + '/warehousedetail-get/' + stockId);
    }

    deleteStockDetail(id: any) {
        return this.http.get<any>(this.resourceUrl + '/warehousedetail-delete/' + id);
    }

}
