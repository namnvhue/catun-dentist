import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CatunDentistAppSharedModule} from '../../shared/shared.module';
import {TODAY_BOOKING_ROUTE} from './today-booking.route';
import {TodayBookingComponent} from './today-booking.component';

@NgModule({
    imports: [
        RouterModule.forChild([TODAY_BOOKING_ROUTE]),
        CatunDentistAppSharedModule
    ],
    declarations: [
        TodayBookingComponent
    ],
    entryComponents: [],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CatunDentistTodayBookingModule {
}
