import {Route} from '@angular/router';
import {TodayBookingComponent} from './today-booking.component';
import {UserRouteAccessService} from '../../shared/auth/user-route-access-service';

export const TODAY_BOOKING_ROUTE: Route = {
    path: 'today-appointments',
    component: TodayBookingComponent,
    data: {
        authorities: ['ROLE_ADMIN', 'ROLE_RECEPTIONIST','ROLE_COUNTER'],
        pageTitle: 'CatunDentistApp.todayBookingDisplay.title'
    },
    canActivate: [UserRouteAccessService]
};
