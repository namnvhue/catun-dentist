import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Appointment} from './today-booking.model';
import {AppointmentsService} from "../appointments/appointments.service";
import {HttpResponse} from "@angular/common/http";

@Component({
    selector: 'jhi-confirm',
    templateUrl: './today-booking.component.html',
    styleUrls: ['./today-booking.component.css']
})
export class TodayBookingComponent implements OnInit {

    bookingList: Appointment[];
    current: Appointment;

    @ViewChild("scrollHere") scrollHere: ElementRef;

    constructor(private appointmentsService: AppointmentsService) {
    }

    ngOnInit() {
        this.getTodayBooking();
    }

    getTodayBooking() {
        this.appointmentsService.fetchTodayBookingData({numberDate: 0}).subscribe(
            (res: HttpResponse<Appointment[]>) => {
                this.bookingList = res.body;
            },
            (err) => {
                console.log(err);
            }
        );
    }

    select(tb: Appointment) {
        if (this.bookingList) {
            this.bookingList.forEach((b) => {
                b.active = false;
            });

            tb.active = true;
            this.current = tb;
        }
    }

    next(event) {
        this.nextAppointment(event);
        document.querySelector('.bookingActive').scrollIntoView({ block: 'start',  behavior: 'smooth' });
    }

    nextAppointment(event) {
        if (event.keyCode == 13) {
            if (this.bookingList) {

                // If not select yet
                if (!this.current) {
                    this.bookingList[0].active = true;
                    this.current = this.bookingList[0];
                    return;
                }

                // Find active booking & change all active status to false
                let id;
                for (let i = 0; i < this.bookingList.length; i++) {
                    if (this.bookingList[i].code == this.current.code) {
                        id = i;
                    }
                    this.bookingList[i].active = ((id + 1) >= this.bookingList.length);
                }
                //  Select next booking
                if ((id + 1) < this.bookingList.length) {
                    this.bookingList[id + 1].active = true;
                    this.current = this.bookingList[id + 1]
                }
            }
        }

    }
}
