export class Appointment {
    code?: string;
    name?: string;
    numberPhone?: string;
    timeBooking?: string;
    active?: boolean = false;
}
