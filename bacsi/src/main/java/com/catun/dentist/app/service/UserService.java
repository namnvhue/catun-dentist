package com.catun.dentist.app.service;

import com.catun.dentist.app.entity.UserEntity;
import com.catun.dentist.app.exception.EmailAlreadyUsedException;
import com.catun.dentist.app.exception.InvalidPasswordException;
import com.catun.dentist.app.exception.LoginAlreadyUsedException;
import com.catun.dentist.app.service.dto.UserDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface UserService {

    Optional<UserEntity> activateRegistration(String key);
    Optional<UserEntity> completePasswordReset(String newPassword, String key);
    Optional<UserEntity> requestPasswordReset(String mail);
    UserEntity registerUser(UserDTO userDTO, String password) throws InvalidPasswordException, LoginAlreadyUsedException, EmailAlreadyUsedException;
    UserEntity createUser(UserDTO userDTO);
    void updateUser(String firstName, String lastName, String email, String langKey, String imageUrl);
    Optional<UserDTO> updateUser(UserDTO userDTO);
    void deleteUser(String login);
    void changePassword(String password);
    Page<UserDTO> getAllManagedUsers(Pageable pageable);
    Optional<UserEntity> getUserWithAuthoritiesByLogin(String login);
    Optional<UserEntity> getUserWithAuthorities(Long id);
    Optional<UserEntity> getUserWithAuthorities();
    void removeNotActivatedUsers();
    List<String> getAuthorities();
}
