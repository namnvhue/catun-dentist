package com.catun.dentist.app.repository;

import com.catun.dentist.app.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Spring Data JPA repository for the ProductEntity entity.
 */
public interface ProductRepository extends JpaRepository<ProductEntity, Long> {

    ProductEntity findByName(String name);
}
