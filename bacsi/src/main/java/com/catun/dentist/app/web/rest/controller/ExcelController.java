package com.catun.dentist.app.web.rest.controller;

import com.catun.dentist.app.service.WareHouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;


@RestController
@RequestMapping("/excel")
public class ExcelController {

    @Autowired
    private WareHouseService wareHouseService;

    @GetMapping
    @RequestMapping(value = "/warehouse-report-download/{fromDate}/{toDate}")
    public void excel(HttpServletResponse response, @PathVariable("fromDate") String fromDate,@PathVariable("toDate") String toDate) throws Exception {
        wareHouseService.downloadExcel(response,fromDate,toDate);
    }
}
