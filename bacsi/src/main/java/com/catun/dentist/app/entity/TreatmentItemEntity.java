package com.catun.dentist.app.entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * A TreatmentItemEntity.
 */
@Entity
@Table(name = "treatment_item")
public class TreatmentItemEntity extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @ManyToOne
    @JoinColumn(name = "treatment_detail_id")
    public TreatmentDetailEntity treatmentDetailEntity;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "service_id")
    private String serviceId;

    @Column(name = "related_products")
    private String relatedProducts;

    @Column(name = "service_price")
    private float servicePrice;

    @Column(name = "tooth_names")
    private String toothNames;

    @Column(name = "amounts")
    private int amounts;


    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TreatmentDetailEntity getTreatmentDetailEntity() {
        return treatmentDetailEntity;
    }

    public void setTreatmentDetailEntity(TreatmentDetailEntity treatmentDetailEntity) {
        this.treatmentDetailEntity = treatmentDetailEntity;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getRelatedProducts() {
        return relatedProducts;
    }

    public void setRelatedProducts(String relatedProducts) {
        this.relatedProducts = relatedProducts;
    }

    public float getServicePrice() {
        return servicePrice;
    }

    public void setServicePrice(float servicePrice) {
        this.servicePrice = servicePrice;
    }

    public String getToothNames() {
        return toothNames;
    }

    public void setToothNames(String toothNames) {
        this.toothNames = toothNames;
    }

    public int getAmounts() {
        return amounts;
    }

    public void setAmounts(int amounts) {
        this.amounts = amounts;
    }


    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TreatmentItemEntity treatmentItemEntity = (TreatmentItemEntity) o;
        if (treatmentItemEntity.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), treatmentItemEntity.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TreatmentItemEntity{" +
            "id=" + getId() +
            ", serviceId='" + getServiceId() + "'" +
            ", relatedProducts='" + getRelatedProducts() + "'" +
            ", treatmentDetail='" + getTreatmentDetailEntity() + "'" +
            "}";
    }
}
