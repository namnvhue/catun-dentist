package com.catun.dentist.app.web.rest.response;

public class ReportWareHouseDetailResponse {

    private String name;
    private int restAmountMonthAgo;
    private int inputAmount;
    private int outputAmount;
    private int restAmount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRestAmountMonthAgo() {
        return restAmountMonthAgo;
    }

    public void setRestAmountMonthAgo(int restAmountMonthAgo) {
        this.restAmountMonthAgo = restAmountMonthAgo;
    }

    public int getInputAmount() {
        return inputAmount;
    }

    public void setInputAmount(int inputAmount) {
        this.inputAmount = inputAmount;
    }

    public int getOutputAmount() {
        return outputAmount;
    }

    public void setOutputAmount(int outputAmount) {
        this.outputAmount = outputAmount;
    }

    public int getRestAmount() {
        return restAmount;
    }

    public void setRestAmount(int restAmount) {
        this.restAmount = restAmount;
    }
}
