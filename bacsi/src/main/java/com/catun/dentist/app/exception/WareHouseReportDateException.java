package com.catun.dentist.app.exception;

public class WareHouseReportDateException extends Exception{

    public WareHouseReportDateException(String message) {
        super(message);
    }
}
