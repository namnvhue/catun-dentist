package com.catun.dentist.app.service.impl;

import com.catun.dentist.app.entity.ProductEntity;
import com.catun.dentist.app.entity.ServiceEntity;
import com.catun.dentist.app.exception.ProductEntityExistException;
import com.catun.dentist.app.repository.ProductRepository;
import com.catun.dentist.app.repository.ServiceRepository;
import com.catun.dentist.app.service.ProductService;
import com.catun.dentist.app.web.rest.request.ProductRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    private final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);

    private final ProductRepository productRepository;
    private final ServiceRepository serviceRepository;

    public ProductServiceImpl(ProductRepository productRepository, ServiceRepository serviceRepository) {
        this.productRepository = productRepository;
        this.serviceRepository = serviceRepository;
    }

    @Override
    public ProductEntity createServiceCategory(ProductRequest productRequest)
        throws ProductEntityExistException {
        ProductEntity productEntityExist = productRepository
            .findByName(productRequest.getName());
        if (productEntityExist != null) {
            log.error("Product entity exist in system, please input name different");
            throw new ProductEntityExistException(
                "Product entity exist in system, please input name different");
        }
        ProductEntity productEntity = convertProductRequest(productRequest);
        return productRepository.save(productEntity);
    }

    @Override
    public ProductEntity updateServiceCategory(ProductRequest productRequest) {
        ProductEntity productEntity = productRepository
            .findOne(Long.valueOf(productRequest.getId()));
        productEntity.setName(productRequest.getName());
        productEntity.setPrice(productRequest.getPrice());
        return productRepository.save(productEntity);
    }

    @Override
    public Page<ProductEntity> findAll(Pageable pageable) {
        return productRepository.findAll(pageable);
    }

    @Override
    public ProductEntity getDetailServiceCategory(long id) {
        return productRepository.findOne(id);
    }

    @Override
    public void deleteServiceCategory(long id) {
        log.info("Start delete service category with id {}",id);
        productRepository.delete(id);
        List<ServiceEntity> services = serviceRepository.findByRelatedProductsContaining(String.valueOf(id));
        for(ServiceEntity serviceEntity : services){
            String productId = String.valueOf(id);
            String newRelatedProducts = serviceEntity.getRelatedProducts().replace(productId+",","").replace(","+productId,"").replace(productId,"");
            serviceEntity.setRelatedProducts(newRelatedProducts);
            serviceRepository.save(serviceEntity);
        }
        log.info("Start delete service category Success");
    }

    private ProductEntity convertProductRequest(ProductRequest productRequest) {
        ProductEntity productEntity = new ProductEntity();
        productEntity.setName(productRequest.getName());
        productEntity.setPrice(productRequest.getPrice());
        return productEntity;
    }
}
