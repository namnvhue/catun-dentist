package com.catun.dentist.app.web.rest.controller;

import com.catun.dentist.app.exception.*;
import com.catun.dentist.app.service.WareHouseDetailService;
import com.catun.dentist.app.web.rest.request.WareHouseDetailRequest;
import com.catun.dentist.app.web.rest.response.DetailCommonResponse;
import com.catun.dentist.app.web.rest.response.WareHouseDetailResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("/receptionist")
public class WareHouseDetailController {

    @Autowired
    private WareHouseDetailService wareHouseDetailService;

    @GetMapping
    @RequestMapping("/warehousedetail-get/{warehouseId}")
    public ResponseEntity<List<WareHouseDetailResponse>> getWareHouseDetailByWareHouseId(@PathVariable("warehouseId") long wareHouseId) throws WareHouseEnumException {
        List<WareHouseDetailResponse> wareHouseDetails = wareHouseDetailService.getWareHouseDetail(wareHouseId);
        return new ResponseEntity<>(wareHouseDetails, HttpStatus.OK) ;
    }

    @PostMapping
    @RequestMapping("/warehousedetail-create")
    public ResponseEntity<DetailCommonResponse> createWareHouseDetailByWareHouseId(@Valid @RequestBody WareHouseDetailRequest request) throws UserNotFoundException, WareHouseNotFoundException, ParseException, WareHouseAmountInvalidException {
        wareHouseDetailService.createWareHouseDetail(request);
        return new ResponseEntity<>(new DetailCommonResponse("200","Success"), HttpStatus.OK) ;
    }

    @DeleteMapping
    @RequestMapping("/warehousedetail-delete/{id}")
    public ResponseEntity<DetailCommonResponse> editWareHouse(@PathVariable("id") long id) throws WareHouseDetailNotFoundException, WareHouseEnumException {
        wareHouseDetailService.deleteWareHouseDetail(id);
        return new ResponseEntity<>(new DetailCommonResponse("200","Success"), HttpStatus.OK) ;
    }
}
