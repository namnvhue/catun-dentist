package com.catun.dentist.app.web.rest.response;

import java.util.List;

public class ExpenditureResponse {

    private List<ListExpenditureResponse> expenditures;
    private String totalCollect;
    private String totalExpenditure;

    public List<ListExpenditureResponse> getExpenditures() {
        return expenditures;
    }

    public void setExpenditures(List<ListExpenditureResponse> expenditures) {
        this.expenditures = expenditures;
    }

    public String getTotalCollect() {
        return totalCollect;
    }

    public void setTotalCollect(String totalCollect) {
        this.totalCollect = totalCollect;
    }

    public String getTotalExpenditure() {
        return totalExpenditure;
    }

    public void setTotalExpenditure(String totalExpenditure) {
        this.totalExpenditure = totalExpenditure;
    }
}
