package com.catun.dentist.app.service.impl;

import com.catun.dentist.app.entity.ServiceCategoryEntity;
import com.catun.dentist.app.entity.ServiceEntity;
import com.catun.dentist.app.exception.ServiceCategoryEntityExistException;
import com.catun.dentist.app.repository.ServiceCategoryRepository;
import com.catun.dentist.app.repository.ServiceRepository;
import com.catun.dentist.app.service.ServiceCategoryService;
import com.catun.dentist.app.web.rest.request.ServiceCategoryRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceCategoryServiceImpl implements ServiceCategoryService {

    private final Logger log = LoggerFactory.getLogger(ServiceCategoryServiceImpl.class);

    private ServiceRepository serviceRepository;
    private final ServiceCategoryRepository serviceCategoryRepository;

    public ServiceCategoryServiceImpl(
        ServiceCategoryRepository serviceCategoryRepository,
        ServiceRepository serviceRepository) {
        this.serviceCategoryRepository = serviceCategoryRepository;
        this.serviceRepository = serviceRepository;
    }

    @Override
    public ServiceCategoryEntity createServiceCategory(
        ServiceCategoryRequest serviceCategoryRequest) throws ServiceCategoryEntityExistException {
        ServiceCategoryEntity serviceCategoryEntityExist = serviceCategoryRepository
            .findByName(serviceCategoryRequest.getName());
        if (serviceCategoryEntityExist != null) {
            log.error("CategoryService exist in system,please input nawm different");
            throw new ServiceCategoryEntityExistException(
                "CategoryService exist in system,please input nawm different");
        }
        ServiceCategoryEntity serviceCategoryEntity = convertServiceCategoryRequest(
            serviceCategoryRequest);
        return serviceCategoryRepository.save(serviceCategoryEntity);
    }

    @Override
    public ServiceCategoryEntity updateServiceCategory(
        ServiceCategoryRequest serviceCategoryRequest) {
        ServiceCategoryEntity serviceCategoryEntity = serviceCategoryRepository
            .findOne(Long.valueOf(serviceCategoryRequest.getId()));
        serviceCategoryEntity.setName(serviceCategoryRequest.getName());
        serviceCategoryEntity.setDescription(serviceCategoryRequest.getDescription());
        return serviceCategoryRepository.save(serviceCategoryEntity);
    }

    @Override
    public Page<ServiceCategoryEntity> findAll(Pageable pageable) {
        return serviceCategoryRepository.findAll(pageable);
    }

    @Override
    public ServiceCategoryEntity getDetailServiceCategory(long id) {
        return serviceCategoryRepository.findOne(id);
    }

    @Override
    public void deleteServiceCategory(long id) {
        log.info("Start delete service category with id {}",id);
        List<ServiceEntity> services = serviceRepository.findAllByServiceCategoryId(id);
        if(!services.isEmpty()){
            log.info("Number service delete size {}",services.size());
            for(ServiceEntity service :services){
                serviceRepository.delete(service);
            }
        }
        serviceCategoryRepository.delete(id);
        log.info("End delete service category Success");
    }

    private ServiceCategoryEntity convertServiceCategoryRequest(
        ServiceCategoryRequest serviceCategoryRequest) {
        ServiceCategoryEntity serviceCategoryEntity = new ServiceCategoryEntity();
        serviceCategoryEntity.setName(serviceCategoryRequest.getName());
        serviceCategoryEntity.description(serviceCategoryRequest.getDescription());
        return serviceCategoryEntity;
    }
}
