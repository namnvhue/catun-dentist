package com.catun.dentist.app.web.rest.controller;

import com.catun.dentist.app.entity.ServiceEntity;
import com.catun.dentist.app.exception.BookingDateOffException;
import com.catun.dentist.app.exception.BookingHourOffException;
import com.catun.dentist.app.exception.EncodeBase64ImageException;
import com.catun.dentist.app.exception.GlobalSettingNotFoundException;
import com.catun.dentist.app.exception.NumberPatientOneHourException;
import com.catun.dentist.app.exception.ParseDateException;
import com.catun.dentist.app.exception.ProductEntityNotFoundException;
import com.catun.dentist.app.exception.SendGridEmailException;
import com.catun.dentist.app.exception.ServiceEntityNotFoundException;
import com.catun.dentist.app.exception.SqlException;
import com.catun.dentist.app.service.CalendarbookingService;
import com.catun.dentist.app.service.Services;
import com.catun.dentist.app.service.util.DomainUtil;
import com.catun.dentist.app.web.rest.request.CalendarbookingRequest;
import com.catun.dentist.app.web.rest.response.CalendarbookingResponse;
import com.catun.dentist.app.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import com.google.zxing.WriterException;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/booking")
public class CalendarbookingController {

    private final Logger log = LoggerFactory.getLogger(ServiceController.class);
    private CalendarbookingService calendarbookingService;
    private Services services;

    public CalendarbookingController(CalendarbookingService calendarbookingService,
        Services services) {
        this.services = services;
        this.calendarbookingService = calendarbookingService;
    }

    @PostMapping
    @RequestMapping("/create")
    public ResponseEntity<CalendarbookingResponse> booking(
        @RequestBody @Valid CalendarbookingRequest calendarbookingRequest,
        HttpServletRequest httpServletRequest)
        throws SqlException, IOException, WriterException, EncodeBase64ImageException, JSONException, ParseException, SendGridEmailException, ParseDateException, ServiceEntityNotFoundException, BookingDateOffException, BookingHourOffException, NumberPatientOneHourException, GlobalSettingNotFoundException {
        String uri = DomainUtil.getDomain(httpServletRequest);
        CalendarbookingResponse calendarbookingResponse = calendarbookingService
            .createCalendarbooking(calendarbookingRequest, uri);
        return new ResponseEntity<>(calendarbookingResponse, HttpStatus.OK);
    }

    @GetMapping("/services")
    @Timed
    public ResponseEntity<List<ServiceEntity>> getAllServiceEntities(Pageable pageable)
        throws ProductEntityNotFoundException {
        log.debug("REST request to get a page of ServiceEntities");
        Page<ServiceEntity> page = services.findAll(pageable);
        HttpHeaders headers = PaginationUtil
            .generatePaginationHttpHeaders(page, "/booking/services");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
