package com.catun.dentist.app.exception;

public class TreatmentDetailPdfException extends Exception{

    public TreatmentDetailPdfException(String message) {
        super(message);
    }
}
