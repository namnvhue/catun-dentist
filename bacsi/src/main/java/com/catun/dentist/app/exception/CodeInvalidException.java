package com.catun.dentist.app.exception;

public class CodeInvalidException extends Exception {

    public CodeInvalidException(String message) {
        super(message);
    }
}
