package com.catun.dentist.app.service;

import com.catun.dentist.app.entity.ServiceEntity;
import com.catun.dentist.app.exception.ProductEntityNotFoundException;
import com.catun.dentist.app.exception.ServiceCategoryNotFoundException;
import com.catun.dentist.app.exception.ServiceEntityExistException;
import com.catun.dentist.app.exception.ServiceEntityNotFoundException;
import com.catun.dentist.app.web.rest.request.ServiceRequest;
import com.catun.dentist.app.web.rest.response.DetailServiceResponse;
import com.catun.dentist.app.web.rest.response.ServicesResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface Services {

    ServicesResponse getServices() throws ServiceCategoryNotFoundException;

    void createService(ServiceRequest serviceRequest)
        throws ServiceCategoryNotFoundException, ServiceEntityExistException, ProductEntityNotFoundException;

    void updateService(ServiceRequest serviceRequest)
        throws ServiceEntityNotFoundException, ServiceCategoryNotFoundException;

    Page<ServiceEntity> findAll(Pageable pageable) throws ProductEntityNotFoundException;

    DetailServiceResponse getDetail(long id)
        throws ServiceEntityNotFoundException, ServiceCategoryNotFoundException, ProductEntityNotFoundException;

    void delete(long id) throws ServiceEntityNotFoundException;
}
