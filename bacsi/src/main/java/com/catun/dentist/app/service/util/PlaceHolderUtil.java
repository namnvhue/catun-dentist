package com.catun.dentist.app.service.util;

import com.catun.dentist.app.convert.PlaceHolderConvert;

public class PlaceHolderUtil {

    private String subject;
    private String content;

    public static PlaceHolderUtil placeHolder(PlaceHolderConvert placeHolderConvert) {
        String room = placeHolderConvert.getRoom();
        String doctor = placeHolderConvert.getDocter();
        String receptionist = placeHolderConvert.getReceptionist();
        String booking = placeHolderConvert.getBooking();
        String patient = placeHolderConvert.getPatient();
        String bookingStatus = placeHolderConvert.getBookingStatus();
        String newline = System.getProperty("line.separator");
        String newsubject = placeHolderConvert.getSubject().replace("[[PHONG_KHAM]]", room)
            .replace("[[BAC_SI]]", doctor).replace("[[LE_TAN]]", receptionist)
            .replace("[[CUOC_HEN]]", booking).replace("[[BENH_NHAN]]", patient)
            .replace("[[TINH_TRANG_CUOC_HEN]]", bookingStatus).replace("[[NEW_LINE]]", newline);
        String newContent = placeHolderConvert.getContent().replace("[[PHONG_KHAM]]", room)
            .replace("[[BAC_SI]]", doctor).replace("[[LE_TAN]]", receptionist)
            .replace("[[CUOC_HEN]]", booking).replace("[[BENH_NHAN]]", patient)
            .replace("[[TINH_TRANG_CUOC_HEN]]", bookingStatus).replace("[[NEW_LINE]]", newline);
        PlaceHolderUtil placeHolderUtil = new PlaceHolderUtil();
        placeHolderUtil.setSubject(newsubject);
        placeHolderUtil.setContent(newContent);
        return placeHolderUtil;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
