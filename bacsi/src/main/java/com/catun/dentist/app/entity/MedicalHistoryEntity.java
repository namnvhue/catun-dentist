package com.catun.dentist.app.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * A MedicalHistoryEntity.
 */
@Entity
@Table(name = "medical_history")
public class MedicalHistoryEntity extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "treatmentdetail_id")
    private String treatmentDetailId;

    @Column(name = "user_id")
    private long userId;

    @Column(name = "doctor_name")
    private String doctorName;

    @Column(name = "time_in")
    private String timeIn;

    @Column(name = "time_out")
    private String timeOut;

    @Column(name = "time_amount")
    private Integer timeAmount;

    @Column(name = "description")
    @Lob
    private String description;

    @Column(name = "time_treatment")
    private Date timeTreatment;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTreatmentDetailId() {
        return treatmentDetailId;
    }

    public void setTreatmentDetailId(String treatmentDetailId) {
        this.treatmentDetailId = treatmentDetailId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(String timeIn) {
        this.timeIn = timeIn;
    }

    public String getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }

    public Integer getTimeAmount() {
        return timeAmount;
    }

    public void setTimeAmount(Integer timeAmount) {
        this.timeAmount = timeAmount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getTimeTreatment() {
        return timeTreatment;
    }

    public void setTimeTreatment(Date timeTreatment) {
        this.timeTreatment = timeTreatment;
    }


// jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MedicalHistoryEntity medicalHistoryEntity = (MedicalHistoryEntity) o;
        if (medicalHistoryEntity.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), medicalHistoryEntity.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MedicalHistoryEntity{" +
            "id=" + getId() +
            ", doctorName='" + getDoctorName() + "'" +
            ", timeIn='" + getTimeIn() + "'" +
            ", timeOut='" + getTimeOut() + "'" +
            ", timeAmount=" + getTimeAmount() +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
