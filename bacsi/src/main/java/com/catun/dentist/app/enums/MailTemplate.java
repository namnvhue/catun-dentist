package com.catun.dentist.app.enums;

public enum MailTemplate {
    BOOKING,
    TAKE_CARE_CUSTOMER_SERVICE,
    TREATMENT
}
