package com.catun.dentist.app.service;

import com.catun.dentist.app.entity.TreatmentDetailEntity;
import com.catun.dentist.app.exception.ConvertMoneyFortmatException;
import com.catun.dentist.app.exception.GlobalSettingNotFoundException;
import com.catun.dentist.app.exception.ServiceCategoryNotFoundException;
import com.catun.dentist.app.exception.TreatmentDetailNotFoundException;
import com.catun.dentist.app.exception.TreatmentDetailPdfException;
import com.catun.dentist.app.exception.TreatmentPriceException;
import com.catun.dentist.app.exception.UserNotFoundException;
import com.catun.dentist.app.web.rest.request.TreatmentDetailRequest;
import com.catun.dentist.app.web.rest.response.TreatmentDetailResponse;
import com.catun.dentist.app.web.rest.response.TreatmentDetailServiceResponse;
import com.catun.dentist.app.web.rest.response.TreatmentDetailShowResponse;
import com.itextpdf.text.DocumentException;
import org.springframework.core.io.Resource;

import javax.management.ServiceNotFoundException;
import java.io.IOException;
import java.util.List;

public interface TreatmentDetailService {

    TreatmentDetailServiceResponse getTreatmentDetailService() throws UserNotFoundException, ServiceCategoryNotFoundException, ServiceNotFoundException;
    void createTreatmentDetail(TreatmentDetailRequest treatmentDetailRequest) throws UserNotFoundException, IOException, DocumentException, TreatmentDetailPdfException, GlobalSettingNotFoundException, TreatmentPriceException, ConvertMoneyFortmatException;
    List<TreatmentDetailResponse> getTreatmentDetail(String id) throws TreatmentDetailNotFoundException, ConvertMoneyFortmatException;

    Resource downloadTreatmentDetail(String patientId,String fileName) throws TreatmentDetailPdfException;

    TreatmentDetailShowResponse getTreatmentDetailTail(long treatmentDetailId) throws TreatmentDetailNotFoundException, ConvertMoneyFortmatException;

    void paymentTreatmentDetail(long treatmentDetailId) throws TreatmentDetailNotFoundException;
}
