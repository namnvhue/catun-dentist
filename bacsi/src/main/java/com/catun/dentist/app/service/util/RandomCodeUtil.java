package com.catun.dentist.app.service.util;

import java.util.Random;

public class RandomCodeUtil {

    private static final String PARTTERN_RAMDOM_CODE = "abcdefghijklmnopqrstuvxyz1234567890";

    public static String ramdomCode(int numberCharacter) {
        StringBuilder stringBuilder = new StringBuilder();
        Random ramdom = new Random();
        while (stringBuilder.length() < numberCharacter) {
            int index = (int) (ramdom.nextFloat() * PARTTERN_RAMDOM_CODE.length());
            stringBuilder.append(PARTTERN_RAMDOM_CODE.charAt(index));
        }
        return stringBuilder.toString();
    }
}
