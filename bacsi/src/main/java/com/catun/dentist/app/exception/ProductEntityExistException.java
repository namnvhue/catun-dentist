package com.catun.dentist.app.exception;

public class ProductEntityExistException extends Exception {

    public ProductEntityExistException(String message) {
        super(message);
    }
}
