package com.catun.dentist.app.web.rest.controller;

import com.catun.dentist.app.exception.AppointmentNotFoundException;
import com.catun.dentist.app.exception.ConvertMoneyFortmatException;
import com.catun.dentist.app.exception.DoctorNotFoundException;
import com.catun.dentist.app.exception.ExpenditureEnumException;
import com.catun.dentist.app.exception.ExpenditureNotFoundException;
import com.catun.dentist.app.exception.InvalidParameterException;
import com.catun.dentist.app.exception.NumberPatientOneHourException;
import com.catun.dentist.app.exception.ParameterGetAssignDoctorInvalidException;
import com.catun.dentist.app.exception.ParseDateException;
import com.catun.dentist.app.exception.PaymentInvalidMoneyException;
import com.catun.dentist.app.exception.UserNotFoundException;
import com.catun.dentist.app.service.CalendarbookingService;
import com.catun.dentist.app.service.DisplayService;
import com.catun.dentist.app.web.rest.request.AssignDoctorRequest;
import com.catun.dentist.app.web.rest.request.BirthdayToDayRequest;
import com.catun.dentist.app.web.rest.request.ConfirmBeforeRequest;
import com.catun.dentist.app.web.rest.request.EditCalendarBookingRequest;
import com.catun.dentist.app.web.rest.request.ExpenditureGetRequest;
import com.catun.dentist.app.web.rest.request.ExpenditureRequest;
import com.catun.dentist.app.web.rest.request.GetAssignDoctorRequest;
import com.catun.dentist.app.web.rest.request.ManagePatientRequest;
import com.catun.dentist.app.web.rest.response.AssignDoctorResponse;
import com.catun.dentist.app.web.rest.response.BirthdayToDayResponse;
import com.catun.dentist.app.web.rest.response.BookingTodayResponse;
import com.catun.dentist.app.web.rest.response.DetailCommonResponse;
import com.catun.dentist.app.web.rest.response.DoctorInfoResponse;
import com.catun.dentist.app.web.rest.response.ExpenditureResponse;
import com.catun.dentist.app.web.rest.response.ManagePatientsResponse;
import com.catun.dentist.app.web.rest.response.PatientIntervalTimeTreatmentResponse;
import com.codahale.metrics.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("/receptionist")
public class DisplayController {

    @Autowired
    private DisplayService displayService;

    @GetMapping
    @RequestMapping("/confirm-today")
    public ResponseEntity<List<BookingTodayResponse>> getConfirmToday() throws InvalidParameterException {
        ConfirmBeforeRequest confirmBeforeRequest = new ConfirmBeforeRequest();
        confirmBeforeRequest.setNumberDate(1);
        List<BookingTodayResponse> bookingTodayResponses = displayService.getConfirmToday(confirmBeforeRequest);
        return new ResponseEntity<>(bookingTodayResponses, HttpStatus.OK) ;
    }

    @PostMapping
    @RequestMapping("/confirm-before")
    public ResponseEntity<List<BookingTodayResponse>> getConfirmBefore(@RequestBody @Valid ConfirmBeforeRequest confirmBeforeRequest) throws InvalidParameterException {
        List<BookingTodayResponse> bookingTodayResponses = displayService.getConfirmBefore(confirmBeforeRequest);
        return new ResponseEntity<>(bookingTodayResponses, HttpStatus.OK) ;
    }

    @PostMapping
    @RequestMapping("/edit-booking")
    public ResponseEntity<Void> editBooking(@RequestBody @Valid EditCalendarBookingRequest request) throws InvalidParameterException, AppointmentNotFoundException, ParseDateException, NumberPatientOneHourException {
        displayService.editBooking(request);
        return new ResponseEntity<>(HttpStatus.OK) ;
    }

    @GetMapping
    @RequestMapping("/interval-time-treatmented")
    public ResponseEntity<List<PatientIntervalTimeTreatmentResponse>> getPatientWithIntervalTimeTreatmented() {
        List<PatientIntervalTimeTreatmentResponse> patientIntervalTimeTreatmentResponses = displayService.getPatientWithIntervalTimeTreatmented();
        return new ResponseEntity<>(patientIntervalTimeTreatmentResponses, HttpStatus.OK) ;
    }

    @PostMapping
    @RequestMapping("/coming-birthday")
    public ResponseEntity<List<BirthdayToDayResponse>> getBirthdayNextTwoMonths(@RequestBody @Valid BirthdayToDayRequest birthdayToDayRequest) {
        List<BirthdayToDayResponse> patientIntervalTimeTreatmentResponses = displayService.getBirthday(birthdayToDayRequest);
        return new ResponseEntity<>(patientIntervalTimeTreatmentResponses, HttpStatus.OK) ;
    }

    @GetMapping
    @RequestMapping("/doctors")
    public ResponseEntity<List<DoctorInfoResponse>> getDoctors() throws AppointmentNotFoundException {
        List<DoctorInfoResponse> doctors = displayService.getDoctors();
        return new ResponseEntity<>(doctors, HttpStatus.OK) ;
    }

    @PostMapping
    @RequestMapping("/assign-doctor")
    public ResponseEntity<List<AssignDoctorResponse>> getAssignDoctor(@RequestBody GetAssignDoctorRequest getAssignDoctorRequest) throws AppointmentNotFoundException, ParseDateException, ParameterGetAssignDoctorInvalidException {
        List<AssignDoctorResponse> assignAppointments = displayService.getAssignDoctorAppointments(getAssignDoctorRequest);
        return new ResponseEntity<>(assignAppointments, HttpStatus.OK) ;
    }

    @PostMapping
    @RequestMapping("/assign-doctor/create")
    public ResponseEntity<DetailCommonResponse> assignDoctor(@Valid @RequestBody AssignDoctorRequest assignDoctorRequest) throws AppointmentNotFoundException, ParseDateException, DoctorNotFoundException {
        displayService.assignDoctorAppointments(assignDoctorRequest);
        DetailCommonResponse  detailCommonResponse = new DetailCommonResponse("200","Assign Success");
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.OK) ;
    }

    @PostMapping("/manage-patient")
    @Timed
    public ResponseEntity<ManagePatientsResponse> getPatients(@RequestBody @Valid ManagePatientRequest managePatientRequest) {
        ManagePatientsResponse managePatientsResponse = displayService.getPatientPaging(managePatientRequest);
        return new ResponseEntity<>(managePatientsResponse, HttpStatus.OK);
    }

    @PostMapping("/manage-expenditure")
    @Timed
    public ResponseEntity<DetailCommonResponse> createExpenditure(@RequestBody @Valid ExpenditureRequest expenditureRequest) throws UserNotFoundException, ParseException, ExpenditureEnumException, PaymentInvalidMoneyException {
        displayService.createExpenditure(expenditureRequest);
        return new ResponseEntity<>(new DetailCommonResponse("200","Success"), HttpStatus.OK);
    }

    @PostMapping("/manage-expenditure-show")
    @Timed
    public ResponseEntity<ExpenditureResponse> getExpenditure(@RequestBody @Valid ExpenditureGetRequest expenditureGetRequest) throws ParseException, ExpenditureEnumException, ExpenditureNotFoundException, ConvertMoneyFortmatException {
        ExpenditureResponse response = displayService.getExpenditure(expenditureGetRequest);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/manage-expenditure-delete/{id}")
    @Timed
    public ResponseEntity<DetailCommonResponse> deleteExpenditure(@PathVariable("id") long id){
        displayService.deleteExpenditure(id);
        return new ResponseEntity<>(new DetailCommonResponse("200","Success"), HttpStatus.OK);
    }
}
