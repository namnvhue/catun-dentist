package com.catun.dentist.app.exception;

public class ParameterGetAssignDoctorInvalidException extends Exception{

    public ParameterGetAssignDoctorInvalidException(String message) {
        super(message);
    }

}
