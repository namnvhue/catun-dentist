package com.catun.dentist.app.web.rest.controller;

import com.catun.dentist.app.entity.TreatmentDetailEntity;
import com.catun.dentist.app.exception.ConvertMoneyFortmatException;
import com.catun.dentist.app.exception.GlobalSettingNotFoundException;
import com.catun.dentist.app.exception.ServiceCategoryNotFoundException;
import com.catun.dentist.app.exception.TreatmentDetailNotFoundException;
import com.catun.dentist.app.exception.TreatmentDetailPdfException;
import com.catun.dentist.app.exception.TreatmentPriceException;
import com.catun.dentist.app.exception.UserNotFoundException;
import com.catun.dentist.app.service.TreatmentDetailService;
import javax.management.ServiceNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.catun.dentist.app.web.rest.request.TreatmentDetailRequest;
import com.catun.dentist.app.web.rest.response.DetailCommonResponse;
import com.catun.dentist.app.web.rest.response.TreatmentDetailResponse;
import com.catun.dentist.app.web.rest.response.TreatmentDetailServiceResponse;
import com.catun.dentist.app.web.rest.response.TreatmentDetailShowResponse;
import com.itextpdf.text.DocumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/doctor")
public class TreatmentDetailController {

    private final Logger log = LoggerFactory.getLogger(TreatmentDetailController.class);

    @Autowired
    private TreatmentDetailService treatmentDetailService;

    @GetMapping
    @RequestMapping("/treatment-detail-services")
    public ResponseEntity<TreatmentDetailServiceResponse> getTreatmentDetailServices() throws UserNotFoundException, ServiceNotFoundException, ServiceCategoryNotFoundException {
        TreatmentDetailServiceResponse treatmentDetailServiceResponse = treatmentDetailService.getTreatmentDetailService();
        return new ResponseEntity<>(treatmentDetailServiceResponse, HttpStatus.OK);
    }

    @GetMapping
    @RequestMapping("/treatment-detail/{treatmentId}")
    public ResponseEntity<List<TreatmentDetailResponse>> getTreatmentDetail(@PathVariable("treatmentId") String treatmentId) throws TreatmentDetailNotFoundException, ConvertMoneyFortmatException {
        List<TreatmentDetailResponse> responses = treatmentDetailService.getTreatmentDetail(treatmentId);
        return new ResponseEntity<>(responses, HttpStatus.OK);
    }

    @PostMapping
    @RequestMapping("/treatment-detail")
    public ResponseEntity<DetailCommonResponse> createTreatmentDetail(HttpServletRequest httpServletRequest, @Valid @RequestBody TreatmentDetailRequest treatmentDetailRequest) throws UserNotFoundException, IOException, DocumentException, TreatmentDetailPdfException, GlobalSettingNotFoundException, TreatmentPriceException, ConvertMoneyFortmatException {
        treatmentDetailService.createTreatmentDetail(treatmentDetailRequest);
        return new ResponseEntity<>(new DetailCommonResponse("200","Success"), HttpStatus.OK);
    }

    @GetMapping
    @RequestMapping("/treatment-detail-show/{treatmentDetailId}")
    public ResponseEntity<TreatmentDetailShowResponse> getTreatmentDetail(@PathVariable("treatmentDetailId") long treatmentDetailId) throws ConvertMoneyFortmatException, TreatmentDetailNotFoundException {
        TreatmentDetailShowResponse response =  treatmentDetailService.getTreatmentDetailTail(treatmentDetailId);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping
    @RequestMapping("/treatment-detail-paymented/{treatmentDetailId}")
    public ResponseEntity<DetailCommonResponse> paymentTreatmentDetail(@PathVariable("treatmentDetailId") long treatmentDetailId) throws TreatmentDetailNotFoundException {
        treatmentDetailService.paymentTreatmentDetail(treatmentDetailId);
        return new ResponseEntity<>(new DetailCommonResponse("200","Success"), HttpStatus.OK);
    }

    @GetMapping
    @RequestMapping("/treatment-detail/{patientId}/{fileName}")
    public ResponseEntity<Resource> createTreatmentDetail(HttpServletRequest httpServletRequest,@PathVariable("patientId") String patientId,@PathVariable("fileName") String fileName) throws IOException, TreatmentDetailPdfException{
        Resource resource = treatmentDetailService.downloadTreatmentDetail(patientId,fileName);
        String contentType = httpServletRequest.getServletContext()
            .getMimeType(resource.getFile().getAbsolutePath());
        return ResponseEntity.ok()
            .contentType(MediaType.parseMediaType(contentType))
            .header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + resource.getFilename() + "\"")
            .body(resource);
    }


}
