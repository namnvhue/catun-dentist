package com.catun.dentist.app.exception;

public class WareHouseDetailNotFoundException extends Exception {

    public WareHouseDetailNotFoundException(String message) {
        super(message);
    }
}
