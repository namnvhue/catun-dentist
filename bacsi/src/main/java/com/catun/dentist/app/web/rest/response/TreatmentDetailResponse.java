package com.catun.dentist.app.web.rest.response;

public class TreatmentDetailResponse {

    private String treatmentDetailId;
    private String totalCost;
    private float dicount;
    private String realCost;
    private String noteFromDoctor;
    private String noteFromPatient;
    private String createdBy;
    private String createDate;
    private String linkDownload;
    private String services;
    private String paymented;
    private boolean finishPaymented;

    public String getTreatmentDetailId() {
        return treatmentDetailId;
    }

    public void setTreatmentDetailId(String treatmentDetailId) {
        this.treatmentDetailId = treatmentDetailId;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }

    public float getDicount() {
        return dicount;
    }

    public void setDicount(float dicount) {
        this.dicount = dicount;
    }

    public String getRealCost() {
        return realCost;
    }

    public void setRealCost(String realCost) {
        this.realCost = realCost;
    }

    public String getNoteFromDoctor() {
        return noteFromDoctor;
    }

    public void setNoteFromDoctor(String noteFromDoctor) {
        this.noteFromDoctor = noteFromDoctor;
    }

    public String getNoteFromPatient() {
        return noteFromPatient;
    }

    public void setNoteFromPatient(String noteFromPatient) {
        this.noteFromPatient = noteFromPatient;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getLinkDownload() {
        return linkDownload;
    }

    public void setLinkDownload(String linkDownload) {
        this.linkDownload = linkDownload;
    }

    public String getServices() {
        return services;
    }

    public void setServices(String services) {
        this.services = services;
    }

    public String getPaymented() {
        return paymented;
    }

    public void setPaymented(String paymented) {
        this.paymented = paymented;
    }

    public boolean isFinishPaymented() {
        return finishPaymented;
    }

    public void setFinishPaymented(boolean finishPaymented) {
        this.finishPaymented = finishPaymented;
    }
}
