package com.catun.dentist.app.web.rest.controller;

import com.catun.dentist.app.entity.ProductEntity;
import com.catun.dentist.app.exception.BadRequestAlertException;
import com.catun.dentist.app.exception.ProductEntityExistException;
import com.catun.dentist.app.service.ProductService;
import com.catun.dentist.app.web.rest.request.ProductRequest;
import com.catun.dentist.app.web.rest.util.HeaderUtil;
import com.catun.dentist.app.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin")
public class ProductController {

    private static final String ENTITY_NAME = "productEntity";
    private final Logger log = LoggerFactory.getLogger(ProductController.class);
    private ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping("/product-entities")
    @Timed
    public ResponseEntity<ProductEntity> createProductEntity(
        @RequestBody ProductRequest productRequest)
        throws URISyntaxException, ProductEntityExistException {
        log.debug("REST request to save ProductEntity : {}", productRequest);
        if (productRequest.getId() != null) {
            throw new BadRequestAlertException("A new productEntity cannot already have an ID",
                ENTITY_NAME, "idexists");
        }

        ProductEntity result = productService.createServiceCategory(productRequest);
        return ResponseEntity.created(new URI("/api/product-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PutMapping("/product-entities")
    @Timed
    public ResponseEntity<ProductEntity> updateProductEntity(
        @RequestBody ProductRequest productRequest)
        throws URISyntaxException, ProductEntityExistException {
        log.debug("REST request to update ProductEntity : {}", productRequest);
        if (productRequest.getId() == null) {
            return createProductEntity(productRequest);
        }
        ProductEntity result = productService.updateServiceCategory(productRequest);
        return ResponseEntity.ok()
            .headers(
                HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, productRequest.getId().toString()))
            .body(result);
    }

    @GetMapping("/product-entities")
    @Timed
    public ResponseEntity<List<ProductEntity>> getAllProductEntities(Pageable pageable) {
        log.debug("REST request to get a page of ProductEntities");
        Page<ProductEntity> page = productService.findAll(pageable);
        HttpHeaders headers = PaginationUtil
            .generatePaginationHttpHeaders(page, "/api/product-entities");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/product-entities/{id}")
    @Timed
    public ResponseEntity<ProductEntity> getProductEntity(@PathVariable Long id) {
        log.debug("REST request to get ProductEntity : {}", id);
        ProductEntity productEntity = productService.getDetailServiceCategory(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(productEntity));
    }

    @DeleteMapping("/product-entities/{id}")
    @Timed
    public ResponseEntity<Void> deleteProductEntity(@PathVariable Long id) {
        log.debug("REST request to delete ProductEntity : {}", id);
        productService.deleteServiceCategory(id);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
