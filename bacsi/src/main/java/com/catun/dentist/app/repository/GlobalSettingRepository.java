package com.catun.dentist.app.repository;

import com.catun.dentist.app.domain.GlobalSetting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


/**
 * Spring Data JPA repository for the GlobalSetting entity.
 */
@SuppressWarnings("unused")
public interface GlobalSettingRepository extends JpaRepository<GlobalSetting, Long>,
    JpaSpecificationExecutor<GlobalSetting> {

    GlobalSetting findByGroupAndName(String group,String name);
}
