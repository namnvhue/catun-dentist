package com.catun.dentist.app.exception;

public class ExcelFileException extends Exception{

    public ExcelFileException(String message) {
        super(message);
    }
}
