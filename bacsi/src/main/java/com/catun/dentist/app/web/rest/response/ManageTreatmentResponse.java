package com.catun.dentist.app.web.rest.response;

import java.util.List;

public class ManageTreatmentResponse {

    private List<PatiensDetailResponse> patients;

    private PagingResponse pageable;


    public List<PatiensDetailResponse> getPatients() {
        return patients;
    }

    public void setPatients(List<PatiensDetailResponse> patients) {
        this.patients = patients;
    }

    public PagingResponse getPageable() {
        return pageable;
    }

    public void setPageable(PagingResponse pageable) {
        this.pageable = pageable;
    }
}
