package com.catun.dentist.app.web.rest.response;

public class ListExpenditureResponse {

    private long id;
    private String type;
    private String name;
    private String value;
    private String createDateExpenditure;
    private long createById;
    private String createByName;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCreateDateExpenditure() {
        return createDateExpenditure;
    }

    public void setCreateDateExpenditure(String createDateExpenditure) {
        this.createDateExpenditure = createDateExpenditure;
    }

    public long getCreateById() {
        return createById;
    }

    public void setCreateById(long createById) {
        this.createById = createById;
    }

    public String getCreateByName() {
        return createByName;
    }

    public void setCreateByName(String createByName) {
        this.createByName = createByName;
    }
}
