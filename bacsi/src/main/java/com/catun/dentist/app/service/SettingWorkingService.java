package com.catun.dentist.app.service;

import com.catun.dentist.app.exception.DateOffInvalidException;
import com.catun.dentist.app.exception.GlobalSettingNotFoundException;
import com.catun.dentist.app.web.rest.request.SettingTimeWorkingRequest;
import com.catun.dentist.app.web.rest.response.SettingTimeWorkingResponse;

public interface SettingWorkingService {

    void settingTimeWorking(SettingTimeWorkingRequest settingTimeWorkingRequest)
        throws DateOffInvalidException;

    SettingTimeWorkingResponse getTimeWorking() throws GlobalSettingNotFoundException;
}
