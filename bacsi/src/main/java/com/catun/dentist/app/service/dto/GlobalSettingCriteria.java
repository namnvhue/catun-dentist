package com.catun.dentist.app.service.dto;

import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import java.io.Serializable;


/**
 * Criteria class for the GlobalSetting entity. This class is used in GlobalSettingResource to
 * receive all the possible filtering options from the Http GET request parameters. For example the
 * following could be a valid requests:
 * <code> /global-settings?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used,
 * we need to use fix type specific filters.
 */
public class GlobalSettingCriteria implements Serializable {

    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter group;

    private StringFilter name;

    private StringFilter value;

    public GlobalSettingCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getGroup() {
        return group;
    }

    public void setGroup(StringFilter group) {
        this.group = group;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getValue() {
        return value;
    }

    public void setValue(StringFilter value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "GlobalSettingCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (group != null ? "group=" + group + ", " : "") +
            (name != null ? "name=" + name + ", " : "") +
            (value != null ? "value=" + value + ", " : "") +
            "}";
    }

}
