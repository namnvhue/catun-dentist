package com.catun.dentist.app.web.rest.request;

import java.util.List;
import javax.validation.constraints.NotNull;

public class SettingTimeWorkingRequest {

    private List<DateOffRequest> dateOffs;

    private List<DateOffRequest> holidayOffs;

    private String nameDateOff;

    @NotNull
    private String timeWorking;

    @NotNull
    private String numberPatientOneHour;

    public List<DateOffRequest> getDateOffs() {
        return dateOffs;
    }

    public void setDateOffs(List<DateOffRequest> dateOffs) {
        this.dateOffs = dateOffs;
    }

    public String getTimeWorking() {
        return timeWorking;
    }

    public void setTimeWorking(String timeWorking) {
        this.timeWorking = timeWorking;
    }

    public String getNumberPatientOneHour() {
        return numberPatientOneHour;
    }

    public void setNumberPatientOneHour(String numberPatientOneHour) {
        this.numberPatientOneHour = numberPatientOneHour;
    }

    public List<DateOffRequest> getHolidayOffs() {
        return holidayOffs;
    }

    public void setHolidayOffs(List<DateOffRequest> holidayOffs) {
        this.holidayOffs = holidayOffs;
    }

    public String getNameDateOff() {
        return nameDateOff;
    }

    public void setNameDateOff(String nameDateOff) {
        this.nameDateOff = nameDateOff;
    }
}
