package com.catun.dentist.app.service;

import com.catun.dentist.app.exception.PlaceHolderSettingNotFoundException;
import com.catun.dentist.app.web.rest.request.MailTemplateRequest;
import com.catun.dentist.app.web.rest.response.MasterMailTemplateResponse;

public interface MailTeamplateService {

    void createMailTemplate(MailTemplateRequest mailTemplateRequest);

    MasterMailTemplateResponse getMasterMailTemplate() throws PlaceHolderSettingNotFoundException;
}
