package com.catun.dentist.app.exception;

public class WareHouseEnumException extends Exception{

    public WareHouseEnumException(String message) {
        super(message);
    }
}
