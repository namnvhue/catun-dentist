package com.catun.dentist.app.repository;

import com.catun.dentist.app.entity.UserEntity;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Spring Data JPA repository for the User entity.
 */
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    String USERS_BY_LOGIN_CACHE = "usersByLogin";

    String USERS_BY_EMAIL_CACHE = "usersByEmail";

    Optional<UserEntity> findOneByActivationKey(String activationKey);

    List<UserEntity> findAllByActivatedIsFalseAndCreatedDateBefore(Instant dateTime);

    Optional<UserEntity> findOneByResetKey(String resetKey);

    Optional<UserEntity> findOneByEmailIgnoreCase(String email);

    Optional<UserEntity> findOneByLogin(String login);

    @EntityGraph(attributePaths = "authorities")
    Optional<UserEntity> findOneWithAuthoritiesById(Long id);

    @EntityGraph(attributePaths = "authorities")
    Optional<UserEntity> findOneWithAuthoritiesByLogin(String login);

    @EntityGraph(attributePaths = "authorities")
    Optional<UserEntity> findOneWithAuthoritiesByEmail(String email);

    Page<UserEntity> findAllByLoginNot(Pageable pageable, String login);

    List<UserEntity> findAllByCreatedBy(String anonymousUser);

    @EntityGraph(attributePaths = "authorities")
    Page<UserEntity> findAllByAuthoritiesName(Pageable pageable,String name);

    List<UserEntity> findAllByAuthoritiesName(String name);

    UserEntity findByAuthoritiesNameAndId(String name,long id);

    @EntityGraph(attributePaths = "authorities")
    Page<UserEntity> findAllByAuthoritiesNameAndFullNameContainingOrPhoneContainingOrAddressContainingOrEmailContainingOrBirthdayContaining(Pageable pageable, String name, String fullName,String phone,String address,String email,String birthDate);

    List<UserEntity> findAllByBirthdayContainingOrBirthdayContainingOrderByFullNameAsc(String startDay,String endDate);


    @EntityGraph(attributePaths = "authorities")
    List<UserEntity> findAllByAuthoritiesNameOrAuthoritiesNameAndIdNot(String reception,String doctor,long userId);

    @Query(value = "SELECT full_name FROM user WHERE id = ?1", nativeQuery = true)
    public List<Object[]> findNameById(long id);
}
