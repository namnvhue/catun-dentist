package com.catun.dentist.app.repository;

import com.catun.dentist.app.entity.DateOffEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DateOffRepository extends JpaRepository<DateOffEntity, Long> {

    DateOffEntity findByDateOff(String dateOff);
}
