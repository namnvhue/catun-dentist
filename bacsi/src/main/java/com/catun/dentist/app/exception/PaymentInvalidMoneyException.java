package com.catun.dentist.app.exception;

public class PaymentInvalidMoneyException extends Exception{

    public PaymentInvalidMoneyException(String message) {
        super(message);
    }

}
