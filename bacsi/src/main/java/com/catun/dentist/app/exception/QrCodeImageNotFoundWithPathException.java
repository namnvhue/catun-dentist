package com.catun.dentist.app.exception;

public class QrCodeImageNotFoundWithPathException extends Exception {

    public QrCodeImageNotFoundWithPathException(String message) {
        super(message);
    }
}
