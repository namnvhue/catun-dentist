package com.catun.dentist.app.web.rest.controller;

import com.catun.dentist.app.exception.DoctorNotFoundException;
import com.catun.dentist.app.exception.GlobalSettingNotFoundException;
import com.catun.dentist.app.exception.MedicalHistoryNotFoundException;
import com.catun.dentist.app.exception.MedicalHistoryParamInvalidException;
import com.catun.dentist.app.exception.MedicalHistoryUploadImagesException;
import com.catun.dentist.app.exception.ParseDateException;
import com.catun.dentist.app.exception.SendGridEmailException;
import com.catun.dentist.app.exception.ServiceEntityNotFoundException;
import com.catun.dentist.app.exception.TreatmentExistException;
import com.catun.dentist.app.exception.TreatmentNotFoundException;
import com.catun.dentist.app.exception.UserNotFoundException;
import com.catun.dentist.app.service.MedicalHistoryService;
import com.catun.dentist.app.web.rest.request.CalendarbookingRequest;
import com.catun.dentist.app.web.rest.request.MedicalHistoryAddNewRequest;
import com.catun.dentist.app.web.rest.request.MedicalHistoryPatientRequest;
import com.catun.dentist.app.web.rest.response.DetailCommonResponse;
import com.catun.dentist.app.web.rest.response.MedicalHistoryDetailResponse;
import com.catun.dentist.app.web.rest.response.PatiensDetailResponse;
import com.catun.dentist.app.web.rest.response.TreatmentHistoryResponse;
import com.catun.dentist.app.web.rest.response.TreatmentResponse;
import com.catun.dentist.app.web.rest.response.TreatmentStatusRequest;
import com.codahale.metrics.annotation.Timed;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import javax.validation.Valid;
import java.text.ParseException;

@RestController
@RequestMapping("/doctor")
public class TreatmentHistoryController {

    private final Logger log = LoggerFactory.getLogger(TreatmentHistoryController.class);

    @Autowired
    private MedicalHistoryService medicalHistoryService;

    @PostMapping("/treatment/create")
    @Timed
    public ResponseEntity<DetailCommonResponse> createTreatment(@RequestBody @Valid CalendarbookingRequest treatmentRequest) throws SendGridEmailException, ParseException, ParseDateException, TreatmentExistException, ServiceEntityNotFoundException {
        log.debug("REST request to get medical hisoty patients");
        medicalHistoryService.createTreatment(treatmentRequest);
        return new ResponseEntity<>(new DetailCommonResponse("200","Success"), HttpStatus.OK);
    }

    @PostMapping("/treatment-history")
    @Timed
    public ResponseEntity<TreatmentHistoryResponse> getTreatmentNotFinish(@RequestBody @Valid MedicalHistoryPatientRequest medicalHistoryPatientRequest) {
        log.debug("REST request to get medical hisoty patients");
        TreatmentHistoryResponse treatmentHistoryResponse = medicalHistoryService.getTreatmentPaging(medicalHistoryPatientRequest,false);
        return new ResponseEntity<>(treatmentHistoryResponse, HttpStatus.OK);
    }

    @GetMapping("/treatment-history/booking/{id}")
    @Timed
    public ResponseEntity<TreatmentResponse> getTreatmentByBookingId(@PathVariable("id") long id) throws TreatmentNotFoundException {
        log.debug("REST request to get medical hisoty patients");
        TreatmentResponse treatmentResponse = medicalHistoryService.getTreatmentByBookingId(id);
        return new ResponseEntity<>(treatmentResponse, HttpStatus.OK);
    }

    @GetMapping("/treatment-history/patient/{id}")
    @Timed
    public ResponseEntity<PatiensDetailResponse> getPatientInfo(@PathVariable("id") long id) throws UserNotFoundException {
        log.debug("REST request to get medical hisoty patients");
        PatiensDetailResponse patientInfo = medicalHistoryService.getPatientInfo(id);
        return new ResponseEntity<>(patientInfo, HttpStatus.OK);
    }


    @PostMapping("/treatment-history/all")
    @Timed
    public ResponseEntity<TreatmentHistoryResponse> getTreatmentAll(@RequestBody @Valid MedicalHistoryPatientRequest medicalHistoryPatientRequest) {
        log.debug("REST request to get medical hisoty patients");
        TreatmentHistoryResponse treatmentHistoryResponse = medicalHistoryService.getTreatmentPaging(medicalHistoryPatientRequest,true);
        return new ResponseEntity<>(treatmentHistoryResponse, HttpStatus.OK);
    }

    @PostMapping("/treatment-history/change-status")
    @Timed
    public ResponseEntity<DetailCommonResponse> changeTreatmentStatus(@RequestBody @Valid TreatmentStatusRequest treatmentStatusRequest) throws TreatmentNotFoundException {
        log.debug("REST request to get medical hisoty patients");
        medicalHistoryService.changeTreatmentStatus(treatmentStatusRequest);
        DetailCommonResponse  detailCommonResponse = new DetailCommonResponse("200","Success");
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.OK);
    }

    @PostMapping("/medical-history/create")
    @Timed
    public ResponseEntity<DetailCommonResponse> createMedicalHistoryEntities(
        @RequestParam("treatmentDetailId") String treatmentDetailId,
        @RequestParam("userId") long userId,
        @RequestParam("timeIn") String timeIn,
        @RequestParam("timeOut") String timeOut,
        @RequestParam("timeWorking") int timeWorking,
        @RequestParam("noted") String noted,
        @RequestParam("files") MultipartFile[] files) throws UserNotFoundException, MedicalHistoryUploadImagesException, MedicalHistoryParamInvalidException, ParseDateException, ParseException, SendGridEmailException, GlobalSettingNotFoundException, DoctorNotFoundException {
        log.debug("REST request to get a page of MedicalHistoryEntities");
        if(StringUtils.isEmpty(treatmentDetailId) ||StringUtils.isEmpty(timeIn) || StringUtils.isEmpty(timeOut)){
            log.error("Create medicalHistory failed with parameters treatmentId {},timeIn {} , timeOut {}",treatmentDetailId,timeIn,timeOut);
            throw new MedicalHistoryParamInvalidException("Parametter invalid");
        }
        MedicalHistoryAddNewRequest request = new MedicalHistoryAddNewRequest();
        request.setTreatmentDetailId(treatmentDetailId);
        request.setUserId(userId);
        request.setTimeIn(timeIn);
        request.setTimeOut(timeOut);
        request.setTimeWorking(timeWorking);
        request.setNoted(noted);
        request.setFiles(files);
        medicalHistoryService.createMedicalHistory(request);
        return new ResponseEntity<>(new DetailCommonResponse("200","Success"), HttpStatus.OK);
    }

    @GetMapping
    @RequestMapping("/medical-history/{treatmentDetailId}")
    public ResponseEntity<MedicalHistoryDetailResponse> getResources(@PathVariable String treatmentDetailId) throws MedicalHistoryNotFoundException, ParseDateException {
        MedicalHistoryDetailResponse medicalHistoryDetailResponse = medicalHistoryService.getMedicalHistoryDetail(treatmentDetailId);
        return new ResponseEntity<>(medicalHistoryDetailResponse,HttpStatus.OK);
    }

    @GetMapping
    @RequestMapping("/medical-history-delete/{treatmentDetailId}")
    public ResponseEntity<DetailCommonResponse> deleteMedicalHistory(@PathVariable String treatmentDetailId) throws ParseException, MedicalHistoryNotFoundException {
        medicalHistoryService.deleteMedicalHistory(treatmentDetailId);
        DetailCommonResponse  detailCommonResponse = new DetailCommonResponse("200","Remove Success");
        return new ResponseEntity<>(detailCommonResponse,HttpStatus.OK);
    }

    @GetMapping
    @RequestMapping("/treatment-history-delete/{treatmentId}")
    public ResponseEntity<DetailCommonResponse> deleteTreatment(@PathVariable long treatmentId) throws ParseException, MedicalHistoryNotFoundException, TreatmentNotFoundException {
        medicalHistoryService.deleteTreatment(treatmentId);
        DetailCommonResponse  detailCommonResponse = new DetailCommonResponse("200","Remove Success");
        return new ResponseEntity<>(detailCommonResponse,HttpStatus.OK);
    }
}
