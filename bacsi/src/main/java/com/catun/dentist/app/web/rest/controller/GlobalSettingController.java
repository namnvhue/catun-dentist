package com.catun.dentist.app.web.rest.controller;

import com.catun.dentist.app.exception.BadRequestAlertException;
import com.catun.dentist.app.exception.GlobalSettingExistException;
import com.catun.dentist.app.service.GlobalSettingQueryService;
import com.catun.dentist.app.service.GlobalSettingService;
import com.catun.dentist.app.service.dto.GlobalSettingCriteria;
import com.catun.dentist.app.service.dto.GlobalSettingDTO;
import com.catun.dentist.app.web.rest.response.GlobalSettingGroupResponse;
import com.catun.dentist.app.web.rest.util.HeaderUtil;
import com.catun.dentist.app.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing GlobalSetting.
 */
@RestController
@RequestMapping("/admin")
public class GlobalSettingController {

    private static final String ENTITY_NAME = "globalSetting";
    private final Logger log = LoggerFactory.getLogger(GlobalSettingController.class);
    private final GlobalSettingService globalSettingService;

    private final GlobalSettingQueryService globalSettingQueryServiceImpl;

    public GlobalSettingController(GlobalSettingService globalSettingService,
                                   GlobalSettingQueryService globalSettingQueryServiceImpl) {
        this.globalSettingService = globalSettingService;
        this.globalSettingQueryServiceImpl = globalSettingQueryServiceImpl;
    }

    /**
     * POST  /global-settings : Create a new globalSetting.
     *
     * @param globalSettingDTO the globalSettingDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new globalSettingDTO, or
     * with status 400 (Bad Request) if the globalSetting has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/global-settings")
    @Timed
    public ResponseEntity<GlobalSettingDTO> createGlobalSetting(
        @Valid @RequestBody GlobalSettingDTO globalSettingDTO) throws URISyntaxException, GlobalSettingExistException {
        log.debug("REST request to save GlobalSetting : {}", globalSettingDTO);
        if (globalSettingDTO.getId() != null) {
            throw new BadRequestAlertException("A new globalSetting cannot already have an ID",
                ENTITY_NAME, "idexists");
        }
        GlobalSettingDTO result = globalSettingService.save(globalSettingDTO);
        return ResponseEntity.created(new URI("/api/global-settings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /global-settings : Updates an existing globalSetting.
     *
     * @param globalSettingDTO the globalSettingDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated globalSettingDTO, or
     * with status 400 (Bad Request) if the globalSettingDTO is not valid, or with status 500
     * (Internal Server Error) if the globalSettingDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/global-settings")
    @Timed
    public ResponseEntity<GlobalSettingDTO> updateGlobalSetting(
        @Valid @RequestBody GlobalSettingDTO globalSettingDTO) throws URISyntaxException, GlobalSettingExistException {
        log.debug("REST request to update GlobalSetting : {}", globalSettingDTO);
        if (globalSettingDTO.getId() == null) {
            return createGlobalSetting(globalSettingDTO);
        }
        GlobalSettingDTO result = globalSettingService.update(globalSettingDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil
                .createEntityUpdateAlert(ENTITY_NAME, globalSettingDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /global-settings : get all the globalSettings.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of globalSettings in body
     */
    @GetMapping("/global-settings")
    @Timed
    public ResponseEntity<List<GlobalSettingDTO>> getAllGlobalSettings(
        GlobalSettingCriteria criteria, Pageable pageable) {
        log.debug("REST request to get GlobalSettings by criteria: {}", criteria);
        Page<GlobalSettingDTO> page = globalSettingQueryServiceImpl.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil
            .generatePaginationHttpHeaders(page, "/api/global-settings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /global-settings/:id : get the "id" globalSetting.
     *
     * @param id the id of the globalSettingDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the globalSettingDTO, or with
     * status 404 (Not Found)
     */
    @GetMapping("/global-settings/{id}")
    @Timed
    public ResponseEntity<GlobalSettingDTO> getGlobalSetting(@PathVariable Long id) {
        log.debug("REST request to get GlobalSetting : {}", id);
        GlobalSettingDTO globalSettingDTO = globalSettingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(globalSettingDTO));
    }

    /**
     * DELETE  /global-settings/:id : delete the "id" globalSetting.
     *
     * @param id the id of the globalSettingDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/global-settings/{id}")
    @Timed
    public ResponseEntity<Void> deleteGlobalSetting(@PathVariable Long id) {
        log.debug("REST request to delete GlobalSetting : {}", id);
        globalSettingService.delete(id);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/global-settings/info")
    @Timed
    public ResponseEntity<List<GlobalSettingGroupResponse>> getAllGlobalSettingGroups() {
        log.debug("REST request to get Groups info ");
        List<GlobalSettingGroupResponse> response = globalSettingService.getGlobalSettingInfo();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
