package com.catun.dentist.app.exception;

public class AppointmentNotFoundException extends Exception{
    public AppointmentNotFoundException(String message){
        super(message);
    }
}
