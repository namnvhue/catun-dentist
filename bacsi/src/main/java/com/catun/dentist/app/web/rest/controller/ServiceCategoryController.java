package com.catun.dentist.app.web.rest.controller;

import com.catun.dentist.app.entity.ServiceCategoryEntity;
import com.catun.dentist.app.exception.BadRequestAlertException;
import com.catun.dentist.app.exception.ServiceCategoryEntityExistException;
import com.catun.dentist.app.service.ServiceCategoryService;
import com.catun.dentist.app.web.rest.request.ServiceCategoryRequest;
import com.catun.dentist.app.web.rest.util.HeaderUtil;
import com.catun.dentist.app.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/admin")
public class ServiceCategoryController {

    private static final String ENTITY_NAME = "serviceCategoryEntity";
    private final Logger log = LoggerFactory.getLogger(ServiceCategoryController.class);
    private ServiceCategoryService serviceCategoryService;

    public ServiceCategoryController(ServiceCategoryService serviceCategoryService) {
        this.serviceCategoryService = serviceCategoryService;
    }


    @PostMapping("/service-category-entities")
    @Timed
    public ResponseEntity<ServiceCategoryEntity> createServiceCategoryEntity(
        @Valid @RequestBody ServiceCategoryRequest serviceCategoryRequest)
        throws URISyntaxException, ServiceCategoryEntityExistException {
        log.debug("REST request to save ServiceCategoryEntity : {}", serviceCategoryRequest);
        if (serviceCategoryRequest.getId() != null) {
            throw new BadRequestAlertException(
                "A new serviceCategoryEntity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ServiceCategoryEntity result = serviceCategoryService
            .createServiceCategory(serviceCategoryRequest);
        return ResponseEntity.created(new URI("/api/service-category-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }


    @PutMapping("/service-category-entities")
    @Timed
    public ResponseEntity<ServiceCategoryEntity> updateServiceCategoryEntity(
        @Valid @RequestBody ServiceCategoryRequest serviceCategoryRequest)
        throws URISyntaxException, ServiceCategoryEntityExistException {
        log.debug("REST request to update ServiceCategoryEntity : {}", serviceCategoryRequest);
        if (serviceCategoryRequest.getId() == null) {
            return createServiceCategoryEntity(serviceCategoryRequest);
        }
        ServiceCategoryEntity result = serviceCategoryService
            .updateServiceCategory(serviceCategoryRequest);
        return ResponseEntity.ok()
            .headers(HeaderUtil
                .createEntityUpdateAlert(ENTITY_NAME, serviceCategoryRequest.getId().toString()))
            .body(result);
    }


    @GetMapping("/service-category-entities")
    @Timed
    public ResponseEntity<List<ServiceCategoryEntity>> getAllServiceCategoryEntities(
        Pageable pageable) {
        log.debug("REST request to get a page of ServiceCategoryEntities");
        Page<ServiceCategoryEntity> page = serviceCategoryService.findAll(pageable);
        HttpHeaders headers = PaginationUtil
            .generatePaginationHttpHeaders(page, "/api/service-category-entities");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @GetMapping("/service-category-entities/{id}")
    @Timed
    public ResponseEntity<ServiceCategoryEntity> getServiceCategoryEntity(@PathVariable Long id) {
        log.debug("REST request to get ServiceCategoryEntity : {}", id);
        ServiceCategoryEntity serviceCategoryEntity = serviceCategoryService
            .getDetailServiceCategory(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(serviceCategoryEntity));
    }

    @DeleteMapping("/service-category-entities/{id}")
    @Timed
    public ResponseEntity<Void> deleteServiceCategoryEntity(@PathVariable Long id) {
        log.debug("REST request to delete ServiceCategoryEntity : {}", id);
        serviceCategoryService.deleteServiceCategory(id);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
