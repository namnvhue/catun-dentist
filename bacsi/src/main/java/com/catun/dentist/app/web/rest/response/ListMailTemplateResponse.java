package com.catun.dentist.app.web.rest.response;

import java.util.List;

public class ListMailTemplateResponse {

    List<PlaceHolderResponse> placeHolder;
    private String name;
    private ContentEmailResponse contentEmail;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PlaceHolderResponse> getPlaceHolder() {
        return placeHolder;
    }

    public void setPlaceHolder(List<PlaceHolderResponse> placeHolder) {
        this.placeHolder = placeHolder;
    }

    public ContentEmailResponse getContentEmail() {
        return contentEmail;
    }

    public void setContentEmail(ContentEmailResponse contentEmail) {
        this.contentEmail = contentEmail;
    }
}
