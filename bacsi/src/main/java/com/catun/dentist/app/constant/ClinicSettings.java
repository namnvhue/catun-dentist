package com.catun.dentist.app.constant;

import java.util.Arrays;
import java.util.List;

public class ClinicSettings {
    public static List<String> groups = Arrays.asList("Phòng Khám","Repeat Đặt Tái Khám");
    public static List<String> nameSettingClinics = Arrays.asList("Tên Phòng Khám","Địa Chỉ Phòng Khám","Số Bệnh Nhân Khám Trong 1h");
    public static List<String> nameSettingRepeats = Arrays.asList("days","weeks","months","years");

    public static String FORTMAT_DATE_TYPE = "HH:mm dd/MM/yyyy";
}
