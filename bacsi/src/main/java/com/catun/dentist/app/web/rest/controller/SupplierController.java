package com.catun.dentist.app.web.rest.controller;

import com.catun.dentist.app.entity.SupplierEntity;
import com.catun.dentist.app.exception.InvalidParameterException;
import com.catun.dentist.app.exception.SupplierNotFoundException;
import com.catun.dentist.app.service.SupplierService;
import com.catun.dentist.app.web.rest.request.SupplierRequest;
import com.catun.dentist.app.web.rest.response.DetailCommonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/receptionist")
public class SupplierController {

    @Autowired
    private SupplierService supplierService;

    @GetMapping
    @RequestMapping("/supplier-get")
    public ResponseEntity<List<SupplierEntity>> getSupplier() throws SupplierNotFoundException {
        List<SupplierEntity> suppliers = supplierService.getSupplier();
        return new ResponseEntity<>(suppliers, HttpStatus.OK) ;
    }

    @PostMapping
    @RequestMapping("/supplier-create")
    public ResponseEntity<DetailCommonResponse> createSupplier(@Valid @RequestBody SupplierRequest request){
        supplierService.createSupplier(request);
        return new ResponseEntity<>(new DetailCommonResponse("200","Success"), HttpStatus.OK) ;
    }

    @PutMapping
    @RequestMapping("/supplier-edit")
    public ResponseEntity<DetailCommonResponse> editSupplier(@Valid @RequestBody SupplierRequest request) throws SupplierNotFoundException {
        supplierService.editSupplier(request);
        return new ResponseEntity<>(new DetailCommonResponse("200","Success"), HttpStatus.OK) ;
    }

    @DeleteMapping
    @RequestMapping("/supplier-delete/{id}")
    public ResponseEntity<DetailCommonResponse> deleteSupplier(@PathVariable("id") long id){
        supplierService.deleteSupplier(id);
        return new ResponseEntity<>(new DetailCommonResponse("200","Success"), HttpStatus.OK) ;
    }
}
