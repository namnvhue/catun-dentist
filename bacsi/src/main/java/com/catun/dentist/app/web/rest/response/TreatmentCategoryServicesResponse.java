package com.catun.dentist.app.web.rest.response;

import java.util.List;

public class TreatmentCategoryServicesResponse {

    private String id;
    private String name;
    private List<TreatmentServiceResponse> services;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TreatmentServiceResponse> getServices() {
        return services;
    }

    public void setServices(List<TreatmentServiceResponse> services) {
        this.services = services;
    }
}
