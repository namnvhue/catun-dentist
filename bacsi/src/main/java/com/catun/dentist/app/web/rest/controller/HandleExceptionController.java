package com.catun.dentist.app.web.rest.controller;

import com.catun.dentist.app.entity.UserEntity;
import com.catun.dentist.app.exception.*;
import com.catun.dentist.app.repository.UserRepository;
import com.catun.dentist.app.security.SecurityUtils;
import com.catun.dentist.app.service.util.GetMessageUtil;
import com.catun.dentist.app.web.rest.response.DetailCommonResponse;
import com.google.zxing.WriterException;
import java.io.IOException;
import java.text.ParseException;
import java.util.Optional;
import org.json.JSONException;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class HandleExceptionController extends ResponseEntityExceptionHandler {

    private MessageSource messageSource;
    private UserRepository userRepository;

    public HandleExceptionController(MessageSource messageSource, UserRepository userRepository) {
        this.messageSource = messageSource;
        this.userRepository = userRepository;
    }

    private String getLangkey() {
        Optional<UserEntity> userEntityOptional = SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin);
        if (userEntityOptional.isPresent()) {
            return userEntityOptional.get().getLangKey();
        }
        return "vi";
    }

    @ExceptionHandler(SqlException.class)
    public ResponseEntity<DetailCommonResponse> handleSqlException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil.getMessage(messageSource, "error.code.sql", langKey);
        String messageCode = GetMessageUtil.getMessage(messageSource, "error.message.sql", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IOException.class)
    public ResponseEntity<DetailCommonResponse> handleIOException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.ioexception", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.ioexception", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(WriterException.class)
    public ResponseEntity<DetailCommonResponse> handleWriterException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.writerexception", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.writerexception", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EncodeBase64ImageException.class)
    public ResponseEntity<DetailCommonResponse> handleEncodeBase64ImageException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.encodebase64", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.encodebase64", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(JSONException.class)
    public ResponseEntity<DetailCommonResponse> handleJSONException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.jsonexception", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.jsonexception", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ParseException.class)
    public ResponseEntity<DetailCommonResponse> handleParseException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.parseexception", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.parseexception", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(SendGridEmailException.class)
    public ResponseEntity<DetailCommonResponse> handleSendGridEmailException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.sendgridmail", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.sendgridmail", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InterruptedException.class)
    public ResponseEntity<DetailCommonResponse> handleInterruptedException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.interrupted", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.interrupted", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(CodeInvalidException.class)
    public ResponseEntity<DetailCommonResponse> handleCalendarbookingEntityException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.confirmcode", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.confirmcode", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AppointmentDateException.class)
    public ResponseEntity<DetailCommonResponse> handleAppointmentDateException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.appointmentdate", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.appointmentdate", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ParseDateException.class)
    public ResponseEntity<DetailCommonResponse> handleParseDateException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.parsedate", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.parsedate", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ServiceCategoryNotFoundException.class)
    public ResponseEntity<DetailCommonResponse> handleServiceCategoryNotFoundException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.servicecategorynotfound", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.servicecategorynotfound", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ServiceEntityExistException.class)
    public ResponseEntity<DetailCommonResponse> handleServiceEntityExistException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.serviceentityexist", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.serviceentityexist", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ProductEntityExistException.class)
    public ResponseEntity<DetailCommonResponse> handleProductEntityExistException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.productentityexist", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.productentityexist", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ServiceCategoryEntityExistException.class)
    public ResponseEntity<DetailCommonResponse> handleServiceCategoryEntityExistException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.servicecategoryentityexist", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.servicecategoryentityexist", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ProductEntityNotFoundException.class)
    public ResponseEntity<DetailCommonResponse> handleProductEntityNotFoundException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.productentitynotfound", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.productentitynotfound", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ServiceEntityNotFoundException.class)
    public ResponseEntity<DetailCommonResponse> handleServiceEntityNotFoundException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.serviceentitynotfound", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.serviceentitynotfound", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DoctorNotFoundException.class)
    public ResponseEntity<DetailCommonResponse> handleDoctorNotFoundException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.doctornotfound", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.doctornotfound", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<DetailCommonResponse> handleUserNotFoundException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.usernotfound", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.usernotfound", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(TreatmentDetailNotFoundException.class)
    public ResponseEntity<DetailCommonResponse> handleTreatmentDetailNotFoundException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.treatmentdetailnotfound", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.treatmentdetailnotfound", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(BookingCodeNotFoundException.class)
    public ResponseEntity<DetailCommonResponse> handleBookingCodeNotFoundException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.bookingcodenotfound", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.bookingcodenotfound", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(QrCodeImageNotFoundWithPathException.class)
    public ResponseEntity<DetailCommonResponse> handleQrCodeImageNotFoundWithPath() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.qrcodeimagenotfound", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.qrcodeimagenotfound", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(PlaceHolderSettingNotFoundException.class)
    public ResponseEntity<DetailCommonResponse> handlePlaceHolderSettingNotFoundException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.placeholdersettingnotfound", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.placeholdersettingnotfound", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DateOffInvalidException.class)
    public ResponseEntity<DetailCommonResponse> handleDateOffInvalidException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.dateoffinvalid", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.dateoffinvalid", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(BookingDateOffException.class)
    public ResponseEntity<DetailCommonResponse> handleBookingDateOffException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.bookingdateoff", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.bookingdateoff", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(BookingHourOffException.class)
    public ResponseEntity<DetailCommonResponse> handleBookingHourOffException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.bookinghouroff", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.bookinghouroff", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NumberPatientOneHourException.class)
    public ResponseEntity<DetailCommonResponse> handleNumberPatientOneHourException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.numberpatientdonehour", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.numberpatientdonehour", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(TreatmentDetailPdfException.class)
    public ResponseEntity<DetailCommonResponse> handleTreatmentDetailPdfException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.treatmentdetail", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.treatmentdetail", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MedicalHistoryUploadImagesException.class)
    public ResponseEntity<DetailCommonResponse> handleMedicalHistoryUploadImagesException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.medicalhistory.uploadimages", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.medicalhistory.uploadimages", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MedicalHistoryParamInvalidException.class)
    public ResponseEntity<DetailCommonResponse> handleMedicalHistoryParamInvalidException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.medicalhistory.invalidparam", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.medicalhistory.invalidparam", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MedicalHistoryNotFoundException.class)
    public ResponseEntity<DetailCommonResponse> handleMedicalHistoryNotFoundException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.medicalhistory.notfound", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.medicalhistory.notfound", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(LoginAlreadyUsedException.class)
    public ResponseEntity<DetailCommonResponse> handleLoginAlreadyUsedException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.login.already", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.login.already", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InvalidPasswordException.class)
    public ResponseEntity<DetailCommonResponse> handleInvalidPasswordException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.invalid.password", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.invalid.password", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EmailAlreadyUsedException.class)
    public ResponseEntity<DetailCommonResponse> handleEmailAlreadyUsedException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.email.already", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.email.already", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(GlobalSettingNotFoundException.class)
    public ResponseEntity<DetailCommonResponse> handleGlobalSettingNotFoundException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.globalsetting.notfound", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.globalsetting.notfound", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AppointmentNotFoundException.class)
    public ResponseEntity<DetailCommonResponse> handleAppointmentNotFoundException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.appointment.notfound", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.appointment.notfound", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ParameterGetAssignDoctorInvalidException.class)
    public ResponseEntity<DetailCommonResponse> handleParameterGetAssignDoctorInvalidException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.parameter.assigndoctor.invalid", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.parameter.assigndoctor.invalid", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(TreatmentPriceException.class)
    public ResponseEntity<DetailCommonResponse> handleTreatmentPriceException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.parameter.treatment.invalid", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.parameter.treatment.invalid", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConvertMoneyFortmatException.class)
    public ResponseEntity<DetailCommonResponse> handleConvertMoneyFortmatException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.convert.money.failed", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.convert.money.failed", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(TreatmentNotFoundException.class)
    public ResponseEntity<DetailCommonResponse> handleTreatmentNotFoundException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.treatment.not.found", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.treatment.not.found", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(TreatmentExistException.class)
    public ResponseEntity<DetailCommonResponse> handleTreatmentExistException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.treatment.exist", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.treatment.exist", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InvalidParameterException.class)
    public ResponseEntity<DetailCommonResponse> handleInvalidParameterException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.parameter.invalid", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.parameter.invalid", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(GlobalSettingExistException.class)
    public ResponseEntity<DetailCommonResponse> handleGlobalSettingExistException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.globalsetting.exist", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.globalsetting.exist", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ExpenditureEnumException.class)
    public ResponseEntity<DetailCommonResponse> handleExpenditureEnumException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.expenditure.notfound", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.expenditure.notfound", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ExpenditureNotFoundException.class)
    public ResponseEntity<DetailCommonResponse> handleExpenditureNotFoundException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.expendituredata.notfound", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.expendituredata.notfound", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(PaymentInvalidMoneyException.class)
    public ResponseEntity<DetailCommonResponse> handlePaymentInvalidMoneyException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.paymentmoney.invalid", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.paymentmoney.invalid", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(SupplierNotFoundException.class)
    public ResponseEntity<DetailCommonResponse> handleSupplierNotFoundException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.supplier.notfound", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.supplier.notfound", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(WareHouseNotFoundException.class)
    public ResponseEntity<DetailCommonResponse> handleWareHouseNotFoundException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.warehouse.notfound", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.warehouse.notfound", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(WareHouseEnumException.class)
    public ResponseEntity<DetailCommonResponse> handleWareHouseEnumException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.warehouseenum.notfound", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.warehouseenum.notfound", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(WareHouseAmountInvalidException.class)
    public ResponseEntity<DetailCommonResponse> handleWareHouseAmountInvalidException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.warehouseamount.invalid", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.warehouseamount.invalid", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(CsvException.class)
    public ResponseEntity<DetailCommonResponse> handleCsvException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.csv.failed", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.csv.failed", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(WaHouseExistException.class)
    public ResponseEntity<DetailCommonResponse> handleWaHouseExistException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.warehousecode.exist", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.warehousecode.exist", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(WareHouseReportDateException.class)
    public ResponseEntity<DetailCommonResponse> handleWareHouseReportDateException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.warehousereportdate.invalid", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.warehousereportdate.invalid", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ExcelFileException.class)
    public ResponseEntity<DetailCommonResponse> handleExcelFileException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.fileexcel.failed", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.fileexcel.failed", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotificationTypeException.class)
    public ResponseEntity<DetailCommonResponse> handleNotificationTypeException() {
        String langKey = getLangkey();
        String errorCode = GetMessageUtil
            .getMessage(messageSource, "error.code.notificationtypeenum.failed", langKey);
        String messageCode = GetMessageUtil
            .getMessage(messageSource, "error.message.notificationtypeenum.failed", langKey);
        DetailCommonResponse detailCommonResponse = new DetailCommonResponse(errorCode,
            messageCode);
        return new ResponseEntity<>(detailCommonResponse, HttpStatus.BAD_REQUEST);
    }

}
