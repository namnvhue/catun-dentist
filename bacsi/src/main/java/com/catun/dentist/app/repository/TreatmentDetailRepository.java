package com.catun.dentist.app.repository;

import com.catun.dentist.app.entity.TreatmentDetailEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Spring Data JPA repository for the TreatmentDetailEntity entity.
 */
public interface TreatmentDetailRepository extends
    JpaRepository<TreatmentDetailEntity, Long> {

    void deleteAllByPatientId(String userId);

    List<TreatmentDetailEntity> findAllByTreatmentId(long treatmentId);

}
