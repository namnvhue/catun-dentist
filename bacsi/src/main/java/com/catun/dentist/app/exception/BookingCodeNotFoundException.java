package com.catun.dentist.app.exception;

public class BookingCodeNotFoundException extends Exception {

    public BookingCodeNotFoundException(String message) {
        super(message);
    }
}
