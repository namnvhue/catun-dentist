package com.catun.dentist.app.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "booking_code")
public class BookingCodeEntity extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    @Column(name = "code", length = 50)
    private String code;

    @Column(name = "name_qr_code_image")
    private String nameQrCodeImage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNameQrCodeImage() {
        return nameQrCodeImage;
    }

    public void setNameQrCodeImage(String nameQrCodeImage) {
        this.nameQrCodeImage = nameQrCodeImage;
    }
}
