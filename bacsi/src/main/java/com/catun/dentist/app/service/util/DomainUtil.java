package com.catun.dentist.app.service.util;

import javax.servlet.http.HttpServletRequest;

public class DomainUtil {

    public static String getDomain(HttpServletRequest request) {
        return request.getScheme() + "://" + request.getServerName() + ":" + request
            .getServerPort();
    }
}
