package com.catun.dentist.app.web.rest.request;

import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

public class CalendarbookingRequest {

    @NotNull
    @Size(min = 0)
    private String nameVal;

    @NotNull
    @NotEmpty
    private String dobVal;

    @Email
    private String emailVal;

    @NotNull
    @NotEmpty
    @Size(min = 9, max = 14)
    private String phoneVal;

    private List<BookingServiceRequest> services;

    private String ortherRequest;

    private String historyVal;

    private String currentVal;

    @NotNull
    @NotEmpty
    private String dateVal;

    @NotNull
    @NotEmpty
    private String timeVal;

    public String getNameVal() {
        return nameVal;
    }

    public void setNameVal(String nameVal) {
        this.nameVal = nameVal;
    }

    public String getDobVal() {
        return dobVal;
    }

    public void setDobVal(String dobVal) {
        this.dobVal = dobVal;
    }

    public String getEmailVal() {
        return emailVal;
    }

    public void setEmailVal(String emailVal) {
        this.emailVal = emailVal;
    }

    public String getPhoneVal() {
        return phoneVal;
    }

    public void setPhoneVal(String phoneVal) {
        this.phoneVal = phoneVal;
    }

    public String getHistoryVal() {
        return historyVal;
    }

    public void setHistoryVal(String historyVal) {
        this.historyVal = historyVal;
    }

    public String getCurrentVal() {
        return currentVal;
    }

    public void setCurrentVal(String currentVal) {
        this.currentVal = currentVal;
    }

    public String getDateVal() {
        return dateVal;
    }

    public void setDateVal(String dateVal) {
        this.dateVal = dateVal;
    }

    public String getTimeVal() {
        return timeVal;
    }

    public void setTimeVal(String timeVal) {
        this.timeVal = timeVal;
    }

    public List<BookingServiceRequest> getServices() {
        return services;
    }

    public void setServices(List<BookingServiceRequest> services) {
        this.services = services;
    }

    public String getOrtherRequest() {
        return ortherRequest;
    }

    public void setOrtherRequest(String ortherRequest) {
        this.ortherRequest = ortherRequest;
    }
}
