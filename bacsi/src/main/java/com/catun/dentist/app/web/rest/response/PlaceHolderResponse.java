package com.catun.dentist.app.web.rest.response;

public class PlaceHolderResponse {

    private String keyword;
    private String sample;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getSample() {
        return sample;
    }

    public void setSample(String sample) {
        this.sample = sample;
    }
}
