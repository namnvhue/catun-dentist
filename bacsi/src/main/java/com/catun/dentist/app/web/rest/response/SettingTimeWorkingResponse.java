package com.catun.dentist.app.web.rest.response;

import com.catun.dentist.app.web.rest.request.DateOffRequest;
import java.util.List;

public class SettingTimeWorkingResponse {

    private List<DateOffRequest> dateOffs;
    private List<DateOffRequest> holidayOffs;
    private String nameDateOff;
    private String timeWorking;
    private String numberPatientOneHour;
    private List<Integer> numberPatientSelected;

    public List<DateOffRequest> getDateOffs() {
        return dateOffs;
    }

    public void setDateOffs(List<DateOffRequest> dateOffs) {
        this.dateOffs = dateOffs;
    }

    public String getTimeWorking() {
        return timeWorking;
    }

    public void setTimeWorking(String timeWorking) {
        this.timeWorking = timeWorking;
    }

    public List<DateOffRequest> getHolidayOffs() {
        return holidayOffs;
    }

    public void setHolidayOffs(List<DateOffRequest> holidayOffs) {
        this.holidayOffs = holidayOffs;
    }

    public String getNameDateOff() {
        return nameDateOff;
    }

    public void setNameDateOff(String nameDateOff) {
        this.nameDateOff = nameDateOff;
    }

    public String getNumberPatientOneHour() {
        return numberPatientOneHour;
    }

    public void setNumberPatientOneHour(String numberPatientOneHour) {
        this.numberPatientOneHour = numberPatientOneHour;
    }

    public List<Integer> getNumberPatientSelected() {
        return numberPatientSelected;
    }

    public void setNumberPatientSelected(List<Integer> numberPatientSelected) {
        this.numberPatientSelected = numberPatientSelected;
    }
}


