package com.catun.dentist.app.exception;

public class TreatmentPriceException extends Exception{

    public TreatmentPriceException(String message) {
        super(message);
    }
}
