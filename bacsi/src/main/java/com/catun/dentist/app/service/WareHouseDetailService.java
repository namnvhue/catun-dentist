package com.catun.dentist.app.service;

import com.catun.dentist.app.exception.*;
import com.catun.dentist.app.web.rest.request.WareHouseDetailRequest;
import com.catun.dentist.app.web.rest.response.WareHouseDetailResponse;

import java.text.ParseException;
import java.util.List;

public interface WareHouseDetailService {

    List<WareHouseDetailResponse> getWareHouseDetail(long wareHouseId) throws WareHouseEnumException;

    void createWareHouseDetail(WareHouseDetailRequest request) throws UserNotFoundException, WareHouseNotFoundException, ParseException, WareHouseAmountInvalidException;

    void deleteWareHouseDetail(long wareHouseDetailId) throws WareHouseDetailNotFoundException, WareHouseEnumException;
}
