package com.catun.dentist.app.web.rest.request;

import javax.validation.constraints.NotNull;

public class MedicalHistoryPatientRequest {

    private String search;
    private int page;

    @NotNull
    private String sort;

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
}
