package com.catun.dentist.app.web.rest.controller;

import com.catun.dentist.app.exception.NotificationTypeException;
import com.catun.dentist.app.service.PushNotificationService;
import com.catun.dentist.app.web.rest.response.NotificationResponse;
import com.codahale.metrics.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/notification")
public class PushNotificationController {

    @Autowired
    private PushNotificationService pushNotificationService;

    @GetMapping("/{notificationNumber}")
    @Timed
    public ResponseEntity<NotificationResponse> getAllPushNotification(@PathVariable("notificationNumber") String notificationNumber) throws NotificationTypeException {
        NotificationResponse responses = pushNotificationService.getPushNotification(notificationNumber);
        return new ResponseEntity<>(responses,HttpStatus.OK);
    }

    @GetMapping("/viewed/{notificationId}")
    @Timed
    public ResponseEntity<Void> viewed(@PathVariable("notificationId") long notificationId) throws NotificationTypeException {
        pushNotificationService.viewed(notificationId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
