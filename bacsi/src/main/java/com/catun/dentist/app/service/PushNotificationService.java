package com.catun.dentist.app.service;

import com.catun.dentist.app.enums.NotificationType;
import com.catun.dentist.app.exception.NotificationTypeException;
import com.catun.dentist.app.web.rest.response.NotificationResponse;

public interface PushNotificationService {

    void pushNofitication(long userId, NotificationType notificationType,String content,String linkConfirm);

    NotificationResponse getPushNotification(String notificationNumber) throws NotificationTypeException;

    void viewed(long notificationId);
}
