package com.catun.dentist.app.exception;

public class LoginAlreadyUsedException extends Exception {

    public LoginAlreadyUsedException(String message) {
        super(message);
    }
}
