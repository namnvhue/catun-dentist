package com.catun.dentist.app.exception;

public class AppointmentDateException extends Exception {

    public AppointmentDateException(String message) {
        super(message);
    }
}
