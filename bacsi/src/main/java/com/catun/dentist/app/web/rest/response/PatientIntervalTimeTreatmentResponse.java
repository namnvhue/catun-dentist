package com.catun.dentist.app.web.rest.response;

public class PatientIntervalTimeTreatmentResponse {

    private String name;
    private String phone;
    private String intervalTimeTreatment;
    private String email;
    private String time;
    private String typeTime;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIntervalTimeTreatment() {
        return intervalTimeTreatment;
    }

    public void setIntervalTimeTreatment(String intervalTimeTreatment) {
        this.intervalTimeTreatment = intervalTimeTreatment;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTypeTime() {
        return typeTime;
    }

    public void setTypeTime(String typeTime) {
        this.typeTime = typeTime;
    }
}
