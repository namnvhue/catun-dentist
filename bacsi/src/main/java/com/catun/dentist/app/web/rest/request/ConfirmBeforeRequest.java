package com.catun.dentist.app.web.rest.request;

import org.springframework.format.annotation.NumberFormat;

public class ConfirmBeforeRequest {

    @NumberFormat
    private int numberDate;

    public int getNumberDate() {
        return numberDate;
    }

    public void setNumberDate(int numberDate) {
        this.numberDate = numberDate;
    }
}
