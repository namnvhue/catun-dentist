package com.catun.dentist.app.service;

import com.catun.dentist.app.exception.AppointmentDateException;
import com.catun.dentist.app.exception.BookingCodeNotFoundException;
import com.catun.dentist.app.exception.BookingDateOffException;
import com.catun.dentist.app.exception.BookingHourOffException;
import com.catun.dentist.app.exception.CodeInvalidException;
import com.catun.dentist.app.exception.EncodeBase64ImageException;
import com.catun.dentist.app.exception.GlobalSettingNotFoundException;
import com.catun.dentist.app.exception.NumberPatientOneHourException;
import com.catun.dentist.app.exception.ParseDateException;
import com.catun.dentist.app.exception.QrCodeImageNotFoundWithPathException;
import com.catun.dentist.app.exception.SendGridEmailException;
import com.catun.dentist.app.exception.ServiceEntityNotFoundException;
import com.catun.dentist.app.exception.SqlException;
import com.catun.dentist.app.web.rest.request.CalendarbookingRequest;
import com.catun.dentist.app.web.rest.response.CalendarbookingResponse;
import com.catun.dentist.app.web.rest.response.ConfirmResponse;
import com.google.zxing.WriterException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.ParseException;
import org.json.JSONException;
import org.springframework.core.io.Resource;

public interface CalendarbookingService {

    CalendarbookingResponse createCalendarbooking(CalendarbookingRequest calendarbookingRequest,
        String uri)
        throws SqlException, IOException, WriterException, EncodeBase64ImageException, JSONException,
        ParseException, SendGridEmailException, ParseDateException, ServiceEntityNotFoundException, BookingDateOffException, BookingHourOffException, NumberPatientOneHourException, GlobalSettingNotFoundException;

    ConfirmResponse getConfirm(String code, String uri)
        throws CodeInvalidException, AppointmentDateException, ServiceEntityNotFoundException, GlobalSettingNotFoundException;

    Resource getQrCodeImage(String code)
        throws CodeInvalidException, AppointmentDateException, MalformedURLException, BookingCodeNotFoundException, QrCodeImageNotFoundWithPathException;
}

