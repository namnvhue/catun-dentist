package com.catun.dentist.app.service.impl;

import com.catun.dentist.app.constant.ClinicSettings;
import com.catun.dentist.app.domain.GlobalSetting;
import com.catun.dentist.app.exception.GlobalSettingExistException;
import com.catun.dentist.app.exception.GlobalSettingNotFoundException;
import com.catun.dentist.app.repository.GlobalSettingRepository;
import com.catun.dentist.app.service.GlobalSettingService;
import com.catun.dentist.app.service.dto.GlobalSettingDTO;
import com.catun.dentist.app.service.mapper.GlobalSettingMapper;
import com.catun.dentist.app.web.rest.response.GlobalSettingGroupResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * Service Implementation for managing GlobalSetting.
 */
@Service
@Transactional
public class GlobalSettingServiceImpl implements GlobalSettingService {

    private final Logger log = LoggerFactory.getLogger(GlobalSettingServiceImpl.class);

    private final GlobalSettingRepository globalSettingRepository;

    private final GlobalSettingMapper globalSettingMapper;

    public GlobalSettingServiceImpl(GlobalSettingRepository globalSettingRepository,
        GlobalSettingMapper globalSettingMapper) {
        this.globalSettingRepository = globalSettingRepository;
        this.globalSettingMapper = globalSettingMapper;
    }

    /**
     * Save a globalSetting.
     *
     * @param globalSettingDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public GlobalSettingDTO save(GlobalSettingDTO globalSettingDTO) throws GlobalSettingExistException {
        String group = globalSettingDTO.getGroup();
        String name = globalSettingDTO.getName();
        log.debug("Request to save GlobalSetting with globalGroup: {} and name {} ", group,name);
        GlobalSetting checkGlobal = globalSettingRepository.findByGroupAndName(group,name);
        if(checkGlobal != null){
            log.error("Global Seting is exist ,can't create new");
            throw new GlobalSettingExistException("Global Seting is exist");
        }
        GlobalSetting globalSetting = globalSettingMapper.toEntity(globalSettingDTO);
        globalSetting = globalSettingRepository.save(globalSetting);
        return globalSettingMapper.toDto(globalSetting);
    }

    @Override
    public GlobalSettingDTO update(GlobalSettingDTO globalSettingDTO){
        GlobalSetting globalSetting = globalSettingMapper.toEntity(globalSettingDTO);
        globalSetting = globalSettingRepository.save(globalSetting);
        return globalSettingMapper.toDto(globalSetting);
    }

    /**
     * Get all the globalSettings.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<GlobalSettingDTO> findAll(Pageable pageable) {
        log.debug("Request to get all GlobalSettings");
        return globalSettingRepository.findAll(pageable)
            .map(globalSettingMapper::toDto);
    }

    /**
     * Get one globalSetting by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public GlobalSettingDTO findOne(Long id) {
        log.debug("Request to get GlobalSetting : {}", id);
        GlobalSetting globalSetting = globalSettingRepository.findOne(id);
        return globalSettingMapper.toDto(globalSetting);
    }

    /**
     * Delete the globalSetting by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete GlobalSetting : {}", id);
        globalSettingRepository.delete(id);
    }

    @Override
    public List<GlobalSettingGroupResponse> getGlobalSettingInfo() {
        List<GlobalSettingGroupResponse> responses = new ArrayList<>();
        GlobalSettingGroupResponse settingGroupPK = new GlobalSettingGroupResponse();
        settingGroupPK.setGroupName(ClinicSettings.groups.get(0));
        settingGroupPK.setNames(ClinicSettings.nameSettingClinics);
        GlobalSettingGroupResponse settingGroupRepeatAppointment = new GlobalSettingGroupResponse();
        settingGroupRepeatAppointment.setGroupName(ClinicSettings.groups.get(1));
        settingGroupRepeatAppointment.setNames(ClinicSettings.nameSettingRepeats);
        responses.add(settingGroupPK);
        responses.add(settingGroupRepeatAppointment);
        return responses;
    }
}
