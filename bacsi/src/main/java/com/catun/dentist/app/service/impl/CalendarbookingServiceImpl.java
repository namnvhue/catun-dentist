package com.catun.dentist.app.service.impl;

import com.catun.dentist.app.constant.ClinicSettings;
import com.catun.dentist.app.convert.PlaceHolderConvert;
import com.catun.dentist.app.domain.GlobalSetting;
import com.catun.dentist.app.entity.AuthorityEntity;
import com.catun.dentist.app.entity.BookingCodeEntity;
import com.catun.dentist.app.entity.CalendarbookingEntity;
import com.catun.dentist.app.entity.DateOffEntity;
import com.catun.dentist.app.entity.GroupEntity;
import com.catun.dentist.app.entity.MailTemplateEntity;
import com.catun.dentist.app.entity.NotificationEntity;
import com.catun.dentist.app.entity.NotificationUserEntity;
import com.catun.dentist.app.entity.ServiceEntity;
import com.catun.dentist.app.entity.TreatmentEntity;
import com.catun.dentist.app.entity.UserEntity;
import com.catun.dentist.app.entity.WorkingRuleEntity;
import com.catun.dentist.app.enums.MailTemplate;
import com.catun.dentist.app.enums.NotificationType;
import com.catun.dentist.app.exception.AppointmentDateException;
import com.catun.dentist.app.exception.BookingCodeNotFoundException;
import com.catun.dentist.app.exception.BookingDateOffException;
import com.catun.dentist.app.exception.BookingHourOffException;
import com.catun.dentist.app.exception.CodeInvalidException;
import com.catun.dentist.app.exception.GlobalSettingNotFoundException;
import com.catun.dentist.app.exception.NumberPatientOneHourException;
import com.catun.dentist.app.exception.ParseDateException;
import com.catun.dentist.app.exception.QrCodeImageNotFoundWithPathException;
import com.catun.dentist.app.exception.SendGridEmailException;
import com.catun.dentist.app.exception.ServiceEntityNotFoundException;
import com.catun.dentist.app.repository.AuthorityRepository;
import com.catun.dentist.app.repository.BookingCodeRepository;
import com.catun.dentist.app.repository.CalendarbookingRepository;
import com.catun.dentist.app.repository.DateOffRepository;
import com.catun.dentist.app.repository.GlobalSettingRepository;
import com.catun.dentist.app.repository.GroupRepository;
import com.catun.dentist.app.repository.MailTemplateRepository;
import com.catun.dentist.app.repository.NotificationRepository;
import com.catun.dentist.app.repository.NotificationUserRepository;
import com.catun.dentist.app.repository.ServiceRepository;
import com.catun.dentist.app.repository.TreatmentRepository;
import com.catun.dentist.app.repository.UserRepository;
import com.catun.dentist.app.repository.WorkingRuleRepository;
import com.catun.dentist.app.security.AuthoritiesConstants;
import com.catun.dentist.app.service.CalendarbookingService;
import com.catun.dentist.app.service.PushNotificationService;
import com.catun.dentist.app.service.SendGridEmailService;
import com.catun.dentist.app.service.util.GenerateQRCodeUtil;
import com.catun.dentist.app.service.util.PlaceHolderUtil;
import com.catun.dentist.app.service.util.RandomCodeUtil;
import com.catun.dentist.app.web.rest.request.BookingServiceRequest;
import com.catun.dentist.app.web.rest.request.CalendarbookingRequest;
import com.catun.dentist.app.web.rest.response.CalendarbookingResponse;
import com.catun.dentist.app.web.rest.response.ConfirmResponse;
import com.catun.dentist.app.web.rest.util.ParseDateUtil;
import com.google.zxing.WriterException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.regex.Pattern;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class CalendarbookingServiceImpl implements CalendarbookingService {

    private static final String CONFIRMATION_FOLDER = "/confirmations/";
    private final Logger log = LoggerFactory.getLogger(CalendarbookingServiceImpl.class);
    private CalendarbookingRepository calendarbookingRepository;
    private UserRepository userRepository;
    private BookingCodeRepository bookingCodeRepository;
    private SendGridEmailService sendGridEmailService;
    private AuthorityRepository authorityRepository;
    private PasswordEncoder passwordEncoder;
    private GroupRepository groupRepository;
    private ServiceRepository serviceRepository;
    private MailTemplateRepository mailTemplateRepository;
    private DateOffRepository dateOffRepository;
    private WorkingRuleRepository workingRuleRepository;
    private GlobalSettingRepository globalSettingRepository;
    private TreatmentRepository treatmentRepository;
    private PushNotificationService pushNotificationService;
    @Value("${catun.directory.path.server}")
    private String directoryPathServer;
    @Value("${catun.sendgrid.format.newuser.subject}")
    private String newuserSubjectEmail;
    @Value("${catun.sendgrid.format.newuser.content}")
    private String newuserContentEmail;

    public CalendarbookingServiceImpl(CalendarbookingRepository calendarbookingRepository,
        UserRepository userRepository, BookingCodeRepository bookingCodeRepository,
        SendGridEmailService sendGridEmailService, AuthorityRepository authorityRepository,
        PasswordEncoder passwordEncoder, GroupRepository groupRepository,
        ServiceRepository serviceRepository,
        MailTemplateRepository mailTemplateRepository, DateOffRepository dateOffRepository,
        WorkingRuleRepository workingRuleRepository,
        GlobalSettingRepository globalSettingRepository,
        TreatmentRepository treatmentRepository,
        PushNotificationService pushNotificationService) {
        this.calendarbookingRepository = calendarbookingRepository;
        this.userRepository = userRepository;
        this.bookingCodeRepository = bookingCodeRepository;
        this.sendGridEmailService = sendGridEmailService;
        this.authorityRepository = authorityRepository;
        this.passwordEncoder = passwordEncoder;
        this.groupRepository = groupRepository;
        this.serviceRepository = serviceRepository;
        this.mailTemplateRepository = mailTemplateRepository;
        this.dateOffRepository = dateOffRepository;
        this.workingRuleRepository = workingRuleRepository;
        this.globalSettingRepository = globalSettingRepository;
        this.treatmentRepository = treatmentRepository;
        this.pushNotificationService = pushNotificationService;
    }

    private void checkDateTimeOff(CalendarbookingRequest calendarbookingRequest)
        throws BookingDateOffException, BookingHourOffException {
        log.info("Start check date time off when calendarbooking");
        String year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
        String dateOff = calendarbookingRequest.getDateVal() + "/" + year;
        DateOffEntity dateOffEntity = dateOffRepository.findByDateOff(dateOff);
        if (dateOffEntity != null) {
            log.error("Can't booking , because {} is DateOff", dateOff);
            throw new BookingDateOffException("Can't booking");
        }
        String hourBooking = calendarbookingRequest.getTimeVal();
        List<WorkingRuleEntity> workingRuleEntities = workingRuleRepository.findAll();
        if (!workingRuleEntities.isEmpty()) {
            WorkingRuleEntity workingRuleEntity = workingRuleEntities.get(0);
            String workingHourFrom = workingRuleEntity.getTimeWorkingFrom();
            String workingHourTo = workingRuleEntity.getTimeWorkingTo();
            int hour = Integer.parseInt(hourBooking.replace(":", ""));
            int hourFrom = Integer.parseInt(workingHourFrom.replace(":", ""));
            int hourTo = Integer.parseInt(workingHourTo.replace(":", ""));
            if (hour < hourFrom || hour > hourTo) {
                log.error("Can't booking , because this time is not working", hour);
                throw new BookingHourOffException("Can't booking,because this time is not working");
            }
        }
        log.info("End check date time off when calendarbooking");
    }

    @Override
    public CalendarbookingResponse createCalendarbooking(
        CalendarbookingRequest calendarbookingRequest, String uri)
        throws IOException, WriterException, JSONException,
        ParseException, SendGridEmailException, ParseDateException, ServiceEntityNotFoundException, BookingDateOffException, BookingHourOffException, NumberPatientOneHourException, GlobalSettingNotFoundException {
        checkDateTimeOff(calendarbookingRequest);
        log.info("Start create calendarbooking");
        Optional<UserEntity> optionalUser = userRepository
            .findOneByLogin(calendarbookingRequest.getPhoneVal());
        UserEntity user;
        String code = RandomCodeUtil.ramdomCode(5);
        if (optionalUser.isPresent()) {
            user = optionalUser.get();
        } else {
            optionalUser = userRepository
                .findOneByEmailIgnoreCase(calendarbookingRequest.getEmailVal());
            if (!optionalUser.isPresent()) {
                user = registerNewUser(calendarbookingRequest, code);
            } else {
                user = optionalUser.get();
            }
        }
        String nameQrCodeImage = RandomCodeUtil.ramdomCode(25);
        String appointmentDateJson = calendarbookingRequest.getDateVal();
        String appointmentHourJson = calendarbookingRequest.getTimeVal();
        Date appointmentDate = ParseDateUtil
            .parseJsonToDate(appointmentDateJson, appointmentHourJson);
        String userId = String.valueOf(user.getId());

        checkNumberBookingSameTime(appointmentDate);
        String urlConfirm = uri + "/#/xac-nhan/" + code;
        saveRequest(calendarbookingRequest, code, appointmentDate, user,urlConfirm);
        BookingCodeEntity bookingCodeEntity = createBookingCodeEntity(code, nameQrCodeImage);
        bookingCodeRepository.save(bookingCodeEntity);
        DateFormat df = new SimpleDateFormat("dd_MM_yyyy");
        String appointmentDateString = df.format(appointmentDate);
        generateQRCodeImage(appointmentHourJson, calendarbookingRequest.getNameVal(), userId, code,
            nameQrCodeImage, appointmentDateString);
        String email = calendarbookingRequest.getEmailVal();
        sendMail(email, urlConfirm, appointmentDate, user);
        CalendarbookingResponse calendarbookingResponse = new CalendarbookingResponse();
        calendarbookingResponse.setCode(code);
        log.info("End create calendarbooking");
        pushNotificationService.pushNofitication(Long.parseLong(userId), NotificationType.REGISTER,"Bạn có 1 cuộc đặt hẹn mới !",urlConfirm);
        return calendarbookingResponse;
    }

    private void checkNumberBookingSameTime(Date appointmentDate)
        throws NumberPatientOneHourException {
        log.info("Start check number booking same time appointmentDate {}",appointmentDate);
        List<WorkingRuleEntity> workingRuleEntitys = workingRuleRepository.findAll();
        if (!workingRuleEntitys.isEmpty()) {
            List<CalendarbookingEntity> calendarbookingEntities = calendarbookingRepository
                .findAllByAppointmentTimeFrom(appointmentDate);
            int numberPatientOneHour = Integer
                .parseInt(workingRuleEntitys.get(0).getNumberPatientOneHour());
            if (calendarbookingEntities.size() >= numberPatientOneHour) {
                log.error("Can't booking at time ,because have {} booked", numberPatientOneHour);
                throw new NumberPatientOneHourException(
                    "Can't booking at time ,because have number patient is exceeding");
            }
        }
    }

    private void sendMail(String email, String urlConfirm, Date appointmentDate, UserEntity user)
        throws SendGridEmailException, GlobalSettingNotFoundException {
        log.info("Start sendMail email {} , urlConfirm {} , appointmentDate {} , userId {}",email,urlConfirm,appointmentDate,user.getId());
        MailTemplateEntity mailTemplateEntity = mailTemplateRepository
            .findByMasterTemplate(MailTemplate.BOOKING.toString());
        if (mailTemplateEntity == null) {
            CompletableFuture.runAsync(() ->{
                try {
                    sendGridEmailService.sendGridEmail("Phòng Khám Nha Khoa", email,
                        "Click vào link sau để nhận phiếu đặt hẹn :" + urlConfirm);
                } catch (SendGridEmailException e) {
                    log.error("SendEmail link confirm QRcodeImages failed email {} , urlConfirm {} ",email,urlConfirm);
                }
            });

        } else {
            String address = addressClinic(ClinicSettings.groups.get(0),ClinicSettings.nameSettingClinics.get(1));
            PlaceHolderConvert placeHolderConvert = new PlaceHolderConvert(address,
                "", "", appointmentDate.toString(), user.getLogin(), "",
                mailTemplateEntity.getSubject(), mailTemplateEntity.getContent());
            PlaceHolderUtil placeHolderUtil = PlaceHolderUtil.placeHolder(placeHolderConvert);
            if (checkEmail(email)) {
                String subject = placeHolderUtil.getSubject();
                String newContent = placeHolderUtil.getContent()
                    + " , Vui lòng Click vào link sau để nhận phiếu xác nhận :" + urlConfirm;

                CompletableFuture.runAsync(() ->{
                    try {
                        sendGridEmailService.sendGridEmail(subject, email, newContent);
                    } catch (SendGridEmailException e) {
                        log.error("SendEmail calendarbooking with mailTeamplate subject subject {} , email {}",subject,email);
                    }
                });
                String emailUser = user.getEmail();
                if (!emailUser.equals(email)) {
                    CompletableFuture.runAsync(() ->{
                        try {
                            sendGridEmailService.sendGridEmail(subject, emailUser, newContent);
                        } catch (SendGridEmailException e) {
                            log.error("SendEmail calendarbooking with mailTeamplate subject subject {} , email {}",subject,email);
                        }
                    });

                }
            }
        }
        log.info("End sendMail");
    }

    private boolean checkEmail(String email) {
        if (email == null || email.equals("")) {
            return false;
        }
        return true;
    }

    private UserEntity registerNewUser(CalendarbookingRequest calendarbookingRequest,
        String defaultPassword) throws SendGridEmailException, ParseException {
        log.info("Calendarbooking register new user");
        UserEntity newUser = new UserEntity();
        AuthorityEntity authorityEntity = authorityRepository.findOne(AuthoritiesConstants.USER);
        Set<AuthorityEntity> authorities = new HashSet<>();
        String encryptedPassword = passwordEncoder.encode(defaultPassword);
        String numberPhone = calendarbookingRequest.getPhoneVal();
        newUser.setLogin(numberPhone);
        newUser.setPassword(encryptedPassword);
        newUser.setFullName(calendarbookingRequest.getNameVal());
        String email = calendarbookingRequest.getEmailVal();
        if (checkEmail(email)) {
            newUser.setEmail(email);
        }
        newUser.setPhone(numberPhone);
        newUser.setLangKey("vi");
        newUser.setBirthday(calendarbookingRequest.getDobVal());
        newUser.setActivated(true);
        authorities.add(authorityEntity);
        newUser.setAuthorities(authorities);
        GroupEntity groupEntity = groupRepository.findAll().get(0);
        newUser.setGroupEntity(groupEntity);
        userRepository.save(newUser);
        log.info("Created Information for User: {}", newUser);
        if (checkEmail(email)) {
            log.info("SendEmail when calendarbooking register new user");
            CompletableFuture.runAsync(() ->{
                try {
                    sendGridEmailService.sendGridEmail(newuserSubjectEmail, email,
                        newuserContentEmail + numberPhone + " - Password :" + defaultPassword);
                } catch (SendGridEmailException e) {
                    log.error("Send mail failed from email {} to newUserSubjectEmail {}",email,newuserContentEmail);

                }
            });

        }
        log.info("End sendEmail when calendarbooking register new user");
        return newUser;
    }

    private void saveRequest(CalendarbookingRequest calendarbookingRequest, String randomCode, Date appointmentDate,
        UserEntity user,String urlConfirm)
        throws  ServiceEntityNotFoundException {
        String fullName = calendarbookingRequest.getNameVal();
        String birthDay = calendarbookingRequest.getDobVal();
        log.info("Start convert and save  data request to calendarbooking with userId {}",user.getId());
        CalendarbookingEntity calendarbookingEntity = new CalendarbookingEntity();
        calendarbookingEntity.setFullName(fullName);
        calendarbookingEntity.setBirthDay(birthDay);
        calendarbookingEntity.setUserEntity(user);
        calendarbookingEntity.setMedicalHistoryDescription(calendarbookingRequest.getHistoryVal());
        calendarbookingEntity.setStatusCurrent(calendarbookingRequest.getCurrentVal());
        calendarbookingEntity.setAppointmentTimeFrom(appointmentDate);
        String emailRequest = calendarbookingRequest.getEmailVal();
        calendarbookingEntity.setEmail(emailRequest);
        calendarbookingEntity.setCode(randomCode);
        String userId = String.valueOf(user.getId());
        calendarbookingEntity.setBookingBy(userId);
        String numberPhoneRequest = calendarbookingRequest.getPhoneVal();
        if (numberPhoneRequest.equals(user.getLogin()) && emailRequest != null && emailRequest
            .equals(user.getEmail())) {
            calendarbookingEntity.setBookingFor(userId);
        } else {
            calendarbookingEntity.setBookingFor(numberPhoneRequest);
        }
        calendarbookingEntity.setCreatedBy(user.getFirstName());
        calendarbookingEntity.setLastModifiedBy(user.getFirstName());
        calendarbookingEntity.setOrtherRequest(calendarbookingRequest.getOrtherRequest());
        String relatedServices = convertRequestToSerivces(calendarbookingRequest.getServices());
        calendarbookingEntity
            .setRelatedServices(relatedServices);
        calendarbookingEntity.setUrlConfirm(urlConfirm);
        calendarbookingRepository.save(calendarbookingEntity);
        log.info("End convert and save  data request to calendarbooking");

        log.info("Start convert and save data request to Treatment");
        TreatmentEntity treatment = treatmentRepository.findByPatientId(userId);
        if(treatment == null){
            treatment = new TreatmentEntity();
            treatment.setBookingId(calendarbookingEntity.getId());
            treatment.setPatientId(userId);
            treatment.setFullName(fullName);
            treatment.setBirthday(birthDay);
            treatment.setPhone(numberPhoneRequest);
            treatment.setEmail(emailRequest);
            treatment.setRelatedServices(relatedServices);
            treatment.setTreatmentTimeFrom(appointmentDate);
            treatment.setFinished(false);
            treatmentRepository.save(treatment);
        }
        log.info("End convert and save  data request to Treatment");
    }

    private String convertRequestToSerivces(List<BookingServiceRequest> bookingServiceRequests)
        throws ServiceEntityNotFoundException {
        log.info("Start convert calendarbooking services with size {}",bookingServiceRequests.size());
        StringBuilder relatedServices = new StringBuilder();
        for (BookingServiceRequest bookingServiceRequest : bookingServiceRequests) {
            ServiceEntity serviceEntity = serviceRepository
                .findOne(Long.valueOf(bookingServiceRequest.getId()));
            if (serviceEntity == null) {
                log.error("ServiceEntity not found when booking");
                throw new ServiceEntityNotFoundException("ServiceEntity not found when booking");
            }
            relatedServices.append(serviceEntity.getId());
            relatedServices.append(":");
            relatedServices.append(serviceEntity.getName());
            relatedServices.append(",");
        }
        log.info("End convert calendarbooking services");
        return relatedServices.toString();
    }

    private void generateQRCodeImage(String appointmentHourJson, String fullName, String userId,
        String code, String nameQrCodeImage, String appointmentDateString)
        throws IOException, WriterException, JSONException, ParseDateException, GlobalSettingNotFoundException {
        log.info("Start generate QRCodeImages with appointmentHour {} ,fullName {} , userId {} , code {} , nameQrCodeImage {} , appointmentDate {}",appointmentHourJson,fullName,userId,code,nameQrCodeImage,appointmentDateString);
        String appointmentHourString = ParseDateUtil.getStringHourFromJson(appointmentHourJson);
        String textQRCode = generateStringTextQRCode(code, fullName, appointmentDateString,
            appointmentHourString);
        String directory = generateDirectoryFolder(String.valueOf(userId), appointmentDateString);
        new File(directory).mkdirs();
        GenerateQRCodeUtil
            .generateQRCodeImage(textQRCode, 350, 350, directory + nameQrCodeImage + ".png");
        log.info("End generate QRCodeImages");
    }

    private String generateStringTextQRCode(String code, String userName, String appointmentDate,
        String appointmentHourFrom) throws GlobalSettingNotFoundException {
        String address = addressClinic(ClinicSettings.groups.get(0),ClinicSettings.nameSettingClinics.get(1));
        log.info("Start generate QRCode fortmat with code {} , userName {}",code,userName);
        String lineSeparator = System.getProperty("line.separator");
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("- Code : " + code);
        stringBuilder.append(lineSeparator);
        stringBuilder.append("- Phong Kham : " + deAccent(address));
        stringBuilder.append(lineSeparator);
        stringBuilder.append("- Benh Nhan : " + deAccent(userName));
        stringBuilder.append(lineSeparator);
        stringBuilder.append("- Ngay Gio : " + appointmentHourFrom + " " + appointmentDate);
        stringBuilder.append(lineSeparator);
        stringBuilder.append("- Tinh Trang Dat : Xac Nhan");
        log.info("End generate QRCode fortmat");
        return stringBuilder.toString();
    }


    private String deAccent(String str) {
        log.info("Start convert vietnamese to english QRCodeImages");
        String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(nfdNormalizedString).replaceAll("").replaceAll("Đ", "D");
    }

    private String generateDirectoryFolder(String userId, String appointmentDateString) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(directoryPathServer);
        stringBuilder.append(userId);
        stringBuilder.append(CONFIRMATION_FOLDER);
        stringBuilder.append(appointmentDateString + "/");
        return stringBuilder.toString();
    }

    private BookingCodeEntity createBookingCodeEntity(String code, String nameQrCodeImage) {
        BookingCodeEntity bookingCodeEntity = new BookingCodeEntity();
        bookingCodeEntity.setCode(code);
        bookingCodeEntity.setNameQrCodeImage(nameQrCodeImage);
        return bookingCodeEntity;
    }


    @Override
    public ConfirmResponse getConfirm(String code, String uri)
        throws CodeInvalidException, ServiceEntityNotFoundException, GlobalSettingNotFoundException {
        log.info("Start get confirm with code {} , uri {}",code,uri);
        CalendarbookingEntity calendarbookingEntity = calendarbookingRepository.findOneByCode(code);
        if (calendarbookingEntity == null) {
            log.error("Get CalendarbookingEntity from database null with code :" + code);
            throw new CodeInvalidException(
                "Get CalendarbookingEntity from database null with code ");
        }
        String appointmentdate = calendarbookingEntity.getAppointmentTimeFrom().toString();
        ConfirmResponse confirmResponse = new ConfirmResponse();
        confirmResponse.setCode(calendarbookingEntity.getCode());
        confirmResponse.setDate(appointmentdate);
        confirmResponse.setName(calendarbookingEntity.getFullName());
        String address = addressClinic(ClinicSettings.groups.get(0),ClinicSettings.nameSettingClinics.get(1));
        confirmResponse.setAddress(address);
        String qrCodeImagesUrl =
            "/confirm/images/" + code + ".png";
        log.debug("Confirmation code URL: {}", qrCodeImagesUrl);
        confirmResponse.setQrCodeImageUrl(qrCodeImagesUrl);
        confirmResponse.setStatus("Xác Nhận");
        String services = calendarbookingEntity.getRelatedServices();
        if (services != null && services != "") {
            confirmResponse.setServices(getServices(services));
        } else {
            confirmResponse.setServices(Arrays.asList(calendarbookingEntity.getOrtherRequest()));
        }
        log.info("End get confirm");
        return confirmResponse;
    }

    @Override
    public Resource getQrCodeImage(String code)
        throws CodeInvalidException, AppointmentDateException, BookingCodeNotFoundException, QrCodeImageNotFoundWithPathException {
        log.info("Start get QRcodeImages code {} ",code);
        CalendarbookingEntity calendarbookingEntity = calendarbookingRepository.findOneByCode(code);
        if (calendarbookingEntity == null) {
            log.error("Get CalendarbookingEntity from database null with code :" + code);
            throw new CodeInvalidException(
                "Get CalendarbookingEntity from database null with code ");
        }
        String appointmentdate = calendarbookingEntity.getAppointmentTimeFrom().toString();
        BookingCodeEntity bookingCodeEntity = bookingCodeRepository.findOneByCode(code);
        if (bookingCodeEntity == null) {
            log.error("BookingCodeEntity not found");
            throw new BookingCodeNotFoundException("BookingCodeEntity not found");
        }
        String qrCodeImagesUrl = directoryPathServer + calendarbookingEntity.getBookingBy()
            + CONFIRMATION_FOLDER + getAppointmentDateString(appointmentdate) + "/"
            + bookingCodeEntity.getNameQrCodeImage() + ".png";
        try {
            Path path = Paths.get(qrCodeImagesUrl);
            Resource resource = new UrlResource(path.toUri());
            log.info("End get QRcodeImages");
            return resource;
        } catch (Exception e) {
            log.error("Can't get QrCodeImages with path : " + e.getMessage());
            throw new QrCodeImageNotFoundWithPathException("Can't get QrCodeImages with path : ");
        }
    }

    private List<String> getServices(String services) throws ServiceEntityNotFoundException {
        try {
            log.info("Start get services with String service {}",services);
            List<String> strings = new ArrayList<>();
            String[] listServices = services.split(",");
            for (String service : listServices) {
                String[] nameService = service.split(":");
                strings.add(nameService[1]);
            }
            return strings;
        } catch (Exception e) {
            log.error("Service wrong :" + e.getMessage());
            throw new ServiceEntityNotFoundException("Service wrong");
        }
    }

    private String getAppointmentDateString(String appointmentDate)
        throws AppointmentDateException {
        try {
            log.info("Start get appointmentDate with String appointmentDate {}",appointmentDate);
            String[] partAppointmentDates = appointmentDate.split(" ");
            partAppointmentDates = partAppointmentDates[0].split("-");
            return partAppointmentDates[2] + "_" + partAppointmentDates[1] + "_"
                + partAppointmentDates[0];
        } catch (Exception e) {
            log.error("AppointmentDate invalid");
            throw new AppointmentDateException("AppointmentDate invalid");
        }
    }

    private String addressClinic(String group,String name) throws GlobalSettingNotFoundException {
        log.info("Start get globalsetting with group {} , name {}",group,name);
    GlobalSetting globalSetting = globalSettingRepository.findByGroupAndName(group,name);
    if(globalSetting == null){
        log.error("Get GlobalSetting not found with group {},name {}",group,name);
        throw new GlobalSettingNotFoundException("Get GlobalSetting not found");
    }
    return globalSetting.getValue();
    }
}
