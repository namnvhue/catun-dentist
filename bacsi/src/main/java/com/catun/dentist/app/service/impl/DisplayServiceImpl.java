package com.catun.dentist.app.service.impl;

import com.catun.dentist.app.constant.ClinicSettings;
import com.catun.dentist.app.entity.CalendarbookingEntity;
import com.catun.dentist.app.entity.ExpenditureEntity;
import com.catun.dentist.app.entity.TreatmentDetailEntity;
import com.catun.dentist.app.entity.TreatmentEntity;
import com.catun.dentist.app.entity.UserEntity;
import com.catun.dentist.app.entity.WorkingRuleEntity;
import com.catun.dentist.app.enums.Expenditure;
import com.catun.dentist.app.enums.NotificationType;
import com.catun.dentist.app.enums.TypeSelected;
import com.catun.dentist.app.exception.AppointmentNotFoundException;
import com.catun.dentist.app.exception.ConvertMoneyFortmatException;
import com.catun.dentist.app.exception.DoctorNotFoundException;
import com.catun.dentist.app.exception.ExpenditureEnumException;
import com.catun.dentist.app.exception.ExpenditureNotFoundException;
import com.catun.dentist.app.exception.InvalidParameterException;
import com.catun.dentist.app.exception.NumberPatientOneHourException;
import com.catun.dentist.app.exception.ParameterGetAssignDoctorInvalidException;
import com.catun.dentist.app.exception.ParseDateException;
import com.catun.dentist.app.exception.PaymentInvalidMoneyException;
import com.catun.dentist.app.exception.UserNotFoundException;
import com.catun.dentist.app.repository.CalendarbookingRepository;
import com.catun.dentist.app.repository.ExpenditureRepository;
import com.catun.dentist.app.repository.TreatmentDetailRepository;
import com.catun.dentist.app.repository.TreatmentRepository;
import com.catun.dentist.app.repository.UserRepository;
import com.catun.dentist.app.repository.WorkingRuleRepository;
import com.catun.dentist.app.security.AuthoritiesConstants;
import com.catun.dentist.app.security.SecurityUtils;
import com.catun.dentist.app.service.DisplayService;
import com.catun.dentist.app.service.PushNotificationService;
import com.catun.dentist.app.service.util.NumberToMoneyCharactersUtil;
import com.catun.dentist.app.web.rest.request.AssignDoctorRequest;
import com.catun.dentist.app.web.rest.request.BirthdayToDayRequest;
import com.catun.dentist.app.web.rest.request.ConfirmBeforeRequest;
import com.catun.dentist.app.web.rest.request.EditCalendarBookingRequest;
import com.catun.dentist.app.web.rest.request.ExpenditureGetRequest;
import com.catun.dentist.app.web.rest.request.ExpenditureRequest;
import com.catun.dentist.app.web.rest.request.GetAssignDoctorRequest;
import com.catun.dentist.app.web.rest.request.ManagePatientRequest;
import com.catun.dentist.app.web.rest.response.AssignDoctorDetailResponse;
import com.catun.dentist.app.web.rest.response.AssignDoctorResponse;
import com.catun.dentist.app.web.rest.response.BirthdayToDayResponse;
import com.catun.dentist.app.web.rest.response.BookingTodayResponse;
import com.catun.dentist.app.web.rest.response.DoctorInfoResponse;
import com.catun.dentist.app.web.rest.response.ExpenditureResponse;
import com.catun.dentist.app.web.rest.response.ListExpenditureResponse;
import com.catun.dentist.app.web.rest.response.ManagePatientsResponse;
import com.catun.dentist.app.web.rest.response.PagingResponse;
import com.catun.dentist.app.web.rest.response.PatiensDetailResponse;
import com.catun.dentist.app.web.rest.response.PatientIntervalTimeTreatmentResponse;
import com.catun.dentist.app.web.rest.util.ParseDateUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

@Service
public class DisplayServiceImpl implements DisplayService {

    private final Logger log = LoggerFactory.getLogger(DisplayServiceImpl.class);
    private final int NUMBER_RECORD_IN_PAGES = 20;
    private CalendarbookingRepository calendarbookingRepository;
    private UserRepository userRepository;
    private TreatmentRepository treatmentRepository;
    private ExpenditureRepository expenditureRepository;
    private TreatmentDetailRepository treatmentDetailRepository;
    private WorkingRuleRepository workingRuleRepository;
    private PushNotificationService pushNotificationService;

    public DisplayServiceImpl(CalendarbookingRepository calendarbookingRepository,
                              UserRepository userRepository,
                              TreatmentRepository treatmentRepository,
                              ExpenditureRepository expenditureRepository,
                              TreatmentDetailRepository treatmentDetailRepository,
                              WorkingRuleRepository workingRuleRepository,
                              PushNotificationService pushNotificationService){
        this.calendarbookingRepository = calendarbookingRepository;
        this.userRepository = userRepository;
        this.treatmentRepository = treatmentRepository;
        this.expenditureRepository = expenditureRepository;
        this.treatmentDetailRepository = treatmentDetailRepository;
        this.workingRuleRepository = workingRuleRepository;
        this.pushNotificationService = pushNotificationService;
    }

    @Override
    public List<BookingTodayResponse> getConfirmBefore(ConfirmBeforeRequest confirmBeforeRequest) throws InvalidParameterException {
        int numberDate = confirmBeforeRequest.getNumberDate();
        if(numberDate == 0){
            log.error("Parameter get booking then intervaltime wrong , can't not get booking today with Reception");
            throw new InvalidParameterException("Parameter get booking then intervaltime wrong");
        }
        log.info("Start get confirm booking before numberDate {}",numberDate);
        Date confirmBeforeOneDateTo = getDateWithIntervalDayStart(0);
        Date confirmBeforeOneDateEnd = getDateWithIntervalDayEnd(numberDate);
        return getBookingByDate(confirmBeforeOneDateTo,confirmBeforeOneDateEnd,numberDate);
    }

    @Override
    public List<BookingTodayResponse> getConfirmToday(ConfirmBeforeRequest confirmBeforeRequest){
        int numberDate = confirmBeforeRequest.getNumberDate();
        log.info("Start get confirm booking before numberDate {}",numberDate);
        Date confirmBeforeOneDateTo = getDateWithIntervalDayStart(0);
        Date confirmBeforeOneDateEnd = getDateWithIntervalDayEnd(0);
        return getBookingByDate(confirmBeforeOneDateTo,confirmBeforeOneDateEnd,numberDate);
    }

    @Override
    public List<PatientIntervalTimeTreatmentResponse> getPatientWithIntervalTimeTreatmented() {
        log.info("Start get patient with interval time treatmented ago 1,3 day , 1,3,6 month ,every year");
        Date oneDayTo = getDateWithIntervalDayStart(-1);
        Date oneDayEnd = getDateWithIntervalDayEnd(-1);
        Date threeDayTo = getDateWithIntervalDayStart(-3);
        Date threeDayEnd = getDateWithIntervalDayEnd(-3);
        Date oneMonthTo = getDateWithIntervalMonthStart(-1);
        Date oneMonthEnd = getDateWithIntervalMonthEnd(-1);
        Date threeMonthTo = getDateWithIntervalMonthStart(-3);
        Date threeMonthEnd = getDateWithIntervalMonthEnd(-3);
        Date sixMonthTo = getDateWithIntervalMonthStart(-6);
        Date sixMonthEnd = getDateWithIntervalMonthEnd(-6);
        Date everyYearTo = getDateWithIntervalYearStart(-1);
        Date everyYearEnd = getDateWithIntervalYearEnd(-1);
        List<TreatmentEntity> treatmentEntities
            = treatmentRepository.findAllByFinishedTrueAndTreatmentTimeToBetweenOrTreatmentTimeToBetweenOrTreatmentTimeToBetweenOrTreatmentTimeToBetweenOrTreatmentTimeToBetweenOrTreatmentTimeToBetweenOrderByTreatmentTimeToDesc(
            oneDayTo,oneDayEnd,
            threeDayTo,threeDayEnd,
            oneMonthTo,oneMonthEnd,
            threeMonthTo,threeMonthEnd,
            sixMonthTo,sixMonthEnd,
            everyYearTo,everyYearEnd);
        log.info("Number patient treatment then interval time : {}",treatmentEntities.size());
        List<PatientIntervalTimeTreatmentResponse> patients = new ArrayList<>();
        for(TreatmentEntity treatmentEntity : treatmentEntities){
            PatientIntervalTimeTreatmentResponse patientIntervalTime = new PatientIntervalTimeTreatmentResponse();

            Date treatmentTimeTo = treatmentEntity.getTreatmentTimeTo();
            String timeFortmat =
                ParseDateUtil.fortmatDate(treatmentTimeTo, ClinicSettings.FORTMAT_DATE_TYPE);
            patientIntervalTime.setIntervalTimeTreatment(timeFortmat);
            long userId = Long.parseLong(treatmentEntity.getPatientId());
            log.info("Get info user with userId {}",userId);
            UserEntity patient = userRepository.findOne(Long.valueOf(userId));
            if(patient != null){
                patientIntervalTime.setName(patient.getFullName());
                patientIntervalTime.setPhone(patient.getPhone());
                patientIntervalTime.setEmail(patient.getEmail());
            }
            Date treatmentDate = treatmentEntity.getTreatmentTimeTo();
            if(treatmentDate.after(oneDayTo) && treatmentDate.before(oneDayEnd)){
                patientIntervalTime.setTime("1");
                patientIntervalTime.setTypeTime("date");
            }
            if(treatmentDate.after(threeDayTo) && treatmentDate.before(threeDayEnd)){
                patientIntervalTime.setTime("3");
                patientIntervalTime.setTypeTime("date");
            }
            if(treatmentDate.after(oneMonthTo) && treatmentDate.before(oneMonthEnd)){
                patientIntervalTime.setTime("1");
                patientIntervalTime.setTypeTime("month");
            }
            if(treatmentDate.after(threeMonthTo) && treatmentDate.before(threeMonthEnd)){
                patientIntervalTime.setTime("3");
                patientIntervalTime.setTypeTime("month");
            }
            if(treatmentDate.after(sixMonthTo) && treatmentDate.before(sixMonthEnd)){
                patientIntervalTime.setTime("6");
                patientIntervalTime.setTypeTime("month");
            }
            if(treatmentDate.after(everyYearTo) && treatmentDate.before(everyYearEnd)){
                patientIntervalTime.setTime("");
                patientIntervalTime.setTypeTime("year");
            }
            patients.add(patientIntervalTime);
        }
        return patients;
    }

    @Override
    public List<BirthdayToDayResponse> getBirthday(BirthdayToDayRequest birthdayToDayRequest) {
        log.info("Start get birthday is next 2 months");
        List<UserEntity> patients = getBirthDate(birthdayToDayRequest.getNumberDate());
        List<BirthdayToDayResponse> birthdayToDayResponses = new ArrayList<>();
        for(UserEntity patient :patients){
            BirthdayToDayResponse birthdayToDayResponse = new BirthdayToDayResponse();
            birthdayToDayResponse.setName(patient.getFullName());
            birthdayToDayResponse.setEmail(patient.getEmail());
            birthdayToDayResponse.setPhone(patient.getPhone());
            birthdayToDayResponse.setBirthDate(patient.getBirthday());
            birthdayToDayResponses.add(birthdayToDayResponse);
        }
        return birthdayToDayResponses;
    }

    @Override
    public List<AssignDoctorResponse> getAssignDoctorAppointments(GetAssignDoctorRequest getAssignDoctorRequest) throws AppointmentNotFoundException, ParseDateException, ParameterGetAssignDoctorInvalidException {
        log.info("Start get Assign doctor for appointments");
        Pair<Date,Date> currentMonths = getDateRangerOfCurrentMonth(getAssignDoctorRequest);
        Date beginMonths = currentMonths.getLeft();
        Date endMonths = currentMonths.getRight();
        List<CalendarbookingEntity> bookingInMonths = calendarbookingRepository.findAllByAppointmentTimeFromBetweenOrderByAppointmentTimeFromAsc(beginMonths,endMonths);
        if(bookingInMonths.isEmpty()){
            log.error("Calendar booking not found with current months ,please booking before assign");
            throw new AppointmentNotFoundException("Calendar booking not found");
        }
        List<AssignDoctorResponse> assignDoctorResponses = new ArrayList<>();
        do{
            Date endDate = endEveryDate(beginMonths);
            AssignDoctorResponse assignDoctorResponse = new AssignDoctorResponse();
            assignDoctorResponse.setDay(fortmatDayResponse(beginMonths));
            List<AssignDoctorDetailResponse> assignDoctorDetailResponses = new ArrayList<>();
            Iterator<CalendarbookingEntity> CalendarbookingEntitys =  bookingInMonths.iterator();
            while(CalendarbookingEntitys.hasNext()){
                CalendarbookingEntity booking = CalendarbookingEntitys.next();
                Date appointmentDateTime = booking.getAppointmentTimeFrom();
                if(appointmentDateTime.after(beginMonths) && appointmentDateTime.before(endDate)){
                    AssignDoctorDetailResponse assignDoctorDetailResponse = addAssignDoctorDetailResponse(booking);
                    assignDoctorDetailResponses.add(assignDoctorDetailResponse);
                    CalendarbookingEntitys.remove();
                }
            }
            assignDoctorResponse.setAppointments(assignDoctorDetailResponses);
            beginMonths = beginEveryDate(beginMonths);
            assignDoctorResponses.add(assignDoctorResponse);
        }while (beginMonths.before(endMonths));
        log.info("End get Assign doctor for appointments Success");
        return assignDoctorResponses;
    }

    @Override
    public void assignDoctorAppointments(AssignDoctorRequest assignDoctorRequest) throws AppointmentNotFoundException, ParseDateException, DoctorNotFoundException {
        long bookingId = assignDoctorRequest.getBookingId();
        log.info("Start assignDoctor for appointment with bookingId {}",bookingId);
        CalendarbookingEntity calendarbookingEntity = calendarbookingRepository.findById(bookingId);
        if(calendarbookingEntity == null){
            log.error("Appointment to assign not found with id {}",bookingId);
            throw new AppointmentNotFoundException("Appointment to assign not found");
        }
        String doctorId = assignDoctorRequest.getDoctorId();
        UserEntity doctor = userRepository.findByAuthoritiesNameAndId(AuthoritiesConstants.DOCTOR, Long.parseLong(doctorId));
        if(doctor == null){
            log.error("Doctor not found with id {}",doctorId);
            throw new DoctorNotFoundException("Doctor not found");
        }
        try {
            calendarbookingEntity.setDoctorId(doctorId);
            String docterName = doctor.getFullName();
            calendarbookingEntity.setDoctorName(docterName);
            String timeEnd = assignDoctorRequest.getTimeEnd();
            if(!StringUtils.isEmpty(timeEnd)){
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                Date appointmentTimeTo = simpleDateFormat.parse(timeEnd);;
                calendarbookingEntity.setAppointmentTimeTo(appointmentTimeTo);
            }
            calendarbookingRepository.save(calendarbookingEntity);

            TreatmentEntity treatment = treatmentRepository.findByBookingId(calendarbookingEntity.getId());
            treatment.setDoctorId(doctorId);
            treatment.setDoctorName(docterName);
            treatmentRepository.save(treatment);
        } catch (ParseException e) {
            log.error("Parse Appointment Date to failed :"+e.getMessage());
            throw new ParseDateException("Parse Appointment Date");
        }
        log.info("Assign doctor id {} for appointmentId {} success",doctorId,bookingId);
    }

    @Override
    public List<DoctorInfoResponse> getDoctors() {
        log.info("Start get all doctors");
        List<UserEntity> doctors = userRepository.findAllByAuthoritiesName(AuthoritiesConstants.DOCTOR);
        if(doctors.isEmpty()){
            log.error("Can't get any doctors ,please add new doctor in system");
        }
        log.info("Number doctors is {}",doctors.size());
        List<DoctorInfoResponse> doctorInfoResponses = new ArrayList<>();
        for(UserEntity doctor : doctors){
            DoctorInfoResponse doctorInfoResponse = new DoctorInfoResponse();
            doctorInfoResponse.setDoctorId(String.valueOf(doctor.getId()));
            doctorInfoResponse.setDoctorName(doctor.getFullName());
            doctorInfoResponse.setNumberPhone(doctor.getPhone());
            doctorInfoResponses.add(doctorInfoResponse);
        }
        return doctorInfoResponses;
    }

    @Override
    public ManagePatientsResponse getPatientPaging(ManagePatientRequest managePatientRequest) {

        log.info("Start get PatientsPaging");
        int page = managePatientRequest.getPage();
        Sort.Direction direction = getSortCondition(managePatientRequest.getSort());
        Pageable pageable  = new PageRequest(page - 1,NUMBER_RECORD_IN_PAGES, direction,"fullName");
        Page<UserEntity> userEntitysPage = userRepository.findAllByAuthoritiesName(pageable,AuthoritiesConstants.USER);
        String searchCondition = managePatientRequest.getSearch();
        if(searchCondition != null && !searchCondition.equals("")){
            userEntitysPage = userRepository.findAllByAuthoritiesNameAndFullNameContainingOrPhoneContainingOrAddressContainingOrEmailContainingOrBirthdayContaining(pageable,AuthoritiesConstants.USER,searchCondition,searchCondition,searchCondition,searchCondition,searchCondition);
        }
        List<UserEntity> userEntities = userEntitysPage.getContent();
        log.info("Number patient finded is {}",userEntities.size());
        List<PatiensDetailResponse> patients = setInfoPatients(userEntities);
        PagingResponse pagingResponse = createPageResponse(page,userEntitysPage.getTotalPages());
        ManagePatientsResponse managePatientsResponse = new ManagePatientsResponse();
        managePatientsResponse.setPatients(patients);
        managePatientsResponse.setPageable(pagingResponse);
        log.info("Finish get PatientsPaging");
        return managePatientsResponse;
    }

    @Override
    public void createExpenditure(ExpenditureRequest expenditureRequest) throws UserNotFoundException, ParseException, ExpenditureEnumException, PaymentInvalidMoneyException {
        UserEntity reception = SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).get();
        if(reception == null){
            log.error("User not found");
            throw new UserNotFoundException("User not found");
        }
        long receptionId = reception.getId();
        String fullName = reception.getFullName();
        log.info("Start create Expenditure with receptionId {} , receptionName ",receptionId,fullName);
        ExpenditureEntity expenditureEntity = new ExpenditureEntity();
        int type = Expenditure.fromName(expenditureRequest.getType()).getValue();
        expenditureEntity.setType(type);
        expenditureEntity.setName(expenditureRequest.getName());
        String value = expenditureRequest.getValue().replace(".","");
        float fValue = Float.parseFloat(value);
        expenditureEntity.setValue(fValue);
        expenditureEntity.setCreateById(receptionId);
        expenditureEntity.setCreateByName(fullName);
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse(expenditureRequest.getCreateDate());
        expenditureEntity.setCreateDateExpenditure(date);
        expenditureRepository.save(expenditureEntity);
        long treatmentDetailId = expenditureRequest.getTreatmentDetailId();
        TreatmentDetailEntity treatmentDetail = treatmentDetailRepository.findOne(treatmentDetailId);
        if(treatmentDetail != null){
            float valuePaymented = treatmentDetail.getPaymented();
            float newPaymented = fValue + valuePaymented;
            if(newPaymented < treatmentDetail.getRealCost()){
                treatmentDetail.setPaymented(newPaymented);
                treatmentDetailRepository.save(treatmentDetail);
            }else if(newPaymented == treatmentDetail.getRealCost()){
                treatmentDetail.setPaymented(newPaymented);
                treatmentDetail.setFinishPaymented(true);
                treatmentDetailRepository.save(treatmentDetail);
            }else{
                log.error("Can't Payment with amount money big than realCost");
                throw new PaymentInvalidMoneyException("Can't Payment with amount money big than realCost");
            }
        }
        log.info("End create Expenditure Success");
    }

    @Override
    public ExpenditureResponse getExpenditure(ExpenditureGetRequest request) throws ExpenditureNotFoundException, ParseException, ExpenditureEnumException, ConvertMoneyFortmatException {
        Date fromDate = new SimpleDateFormat("dd/MM/yyyy").parse(request.getFromDate());
        Date toDate = new SimpleDateFormat("dd/MM/yyyy").parse(request.getToDate());
        log.info("Start get Expenditure with fromDate {} , toDate {}",fromDate,toDate);
        List<ExpenditureEntity> expenditures = expenditureRepository.findAllByCreateDateExpenditureBetweenOrderByCreateDateExpenditureAsc(fromDate,toDate);
        if(expenditures.isEmpty()){
            log.error("Get Expenditure data empty");
            throw new ExpenditureNotFoundException("Get Expenditure data empty");
        }
        ExpenditureResponse response = new ExpenditureResponse();
        float totalCollect = 0;
        float totalExpenditure = 0;
        List<ListExpenditureResponse> expenditureResponses = new ArrayList<>();
        for(ExpenditureEntity expenditure : expenditures){
            ListExpenditureResponse expenditureResponse = new ListExpenditureResponse();
            Expenditure expen = Expenditure.fromValue(expenditure.getType());
            String type = expen.getName();
            float value = expenditure.getValue();
            if(expen.getValue() == Expenditure.THU.getValue()){
                totalCollect += value;
            }else if(expen.getValue() == Expenditure.CHI.getValue()){
                totalExpenditure += value;
            }
            expenditureResponse.setId(expenditure.getId());
            expenditureResponse.setType(type);
            expenditureResponse.setName(expenditure.getName());
            String valueString = NumberToMoneyCharactersUtil.convertMoneyFortmat(value);
            expenditureResponse.setValue(valueString);
            String createDateFormat = ParseDateUtil.fortmatDate(expenditure.getCreateDateExpenditure(),"dd/MM/yyyy");
            expenditureResponse.setCreateDateExpenditure(createDateFormat);
            expenditureResponse.setCreateById(expenditure.getCreateById());
            expenditureResponse.setCreateByName(expenditure.getCreateByName());
            expenditureResponses.add(expenditureResponse);
        }
        response.setExpenditures(expenditureResponses);
        String totalCollectString = NumberToMoneyCharactersUtil.convertMoneyFortmat(totalCollect);
        String totalExpenditureString = NumberToMoneyCharactersUtil.convertMoneyFortmat(totalExpenditure);
        response.setTotalCollect(totalCollectString);
        response.setTotalExpenditure(totalExpenditureString);
        return response;
    }

    @Override
    public void deleteExpenditure(long id) {
        log.info("Start delete expenditure with id {}",id);
        expenditureRepository.delete(id);
        log.info("Delete expenditure Success");
    }

    @Override
    public void editBooking(EditCalendarBookingRequest request) throws AppointmentNotFoundException, ParseDateException, NumberPatientOneHourException {
        long bookingId = request.getBookingId();
        log.info("Start edit booking with bookingId {}",bookingId);
        CalendarbookingEntity calendarbooking = calendarbookingRepository.findById(bookingId);
        if(calendarbooking == null){
            log.error("CalendarBooking not found with bookingId {}",bookingId);
            throw new AppointmentNotFoundException("Calendarbooking not found");
        }
        Date appointmentDate = ParseDateUtil
            .parseJsonToDate(request.getDateVal(), request.getTimeVal());
        checkNumberBookingSameTime(appointmentDate);
        calendarbooking.setFullName(request.getName());
        calendarbooking.setAppointmentTimeFrom(appointmentDate);
        calendarbookingRepository.save(calendarbooking);
        log.info("Edit booking Success");
        UserEntity reception = SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).get();
        if(reception != null){
            pushNotificationService.pushNofitication(reception.getId(), NotificationType.EDIT,reception.getFullName() + " thay đỗi cuộc hẹn , vui lòng kiểm tra lại !",calendarbooking.getUrlConfirm());
        }
    }

    private void checkNumberBookingSameTime(Date appointmentDate)
        throws NumberPatientOneHourException {
        log.info("Start check number booking same time appointmentDate {}",appointmentDate);
        List<WorkingRuleEntity> workingRuleEntitys = workingRuleRepository.findAll();
        if (!workingRuleEntitys.isEmpty()) {
            List<CalendarbookingEntity> calendarbookingEntities = calendarbookingRepository
                .findAllByAppointmentTimeFrom(appointmentDate);
            int numberPatientOneHour = Integer
                .parseInt(workingRuleEntitys.get(0).getNumberPatientOneHour());
            if (calendarbookingEntities.size() >= numberPatientOneHour) {
                log.error("Can't booking at time ,because have {} booked", numberPatientOneHour);
                throw new NumberPatientOneHourException(
                    "Can't booking at time ,because have number patient is exceeding");
            }
        }
    }

    private Sort.Direction getSortCondition(String sort){
        if(sort.equals("asc")){
            return Sort.Direction.ASC;
        }
        return Sort.Direction.DESC;
    }

    private List<PatiensDetailResponse> setInfoPatients(List<UserEntity> userEntitys){
        log.info("Set patient info with number patient {}",userEntitys.size());
        List<PatiensDetailResponse> patients = new ArrayList<>();
        for(UserEntity userEntity : userEntitys){
            long userId = userEntity.getId();
            TreatmentEntity treatment = treatmentRepository.findByPatientId(String.valueOf(userId));
            if(treatment != null){
                PatiensDetailResponse patient = new PatiensDetailResponse();
                patient.setId(String.valueOf(userEntity.getId()));
                patient.setName(userEntity.getFullName());
                patient.setAddress(userEntity.getAddress());
                patient.setBirthDay(userEntity.getBirthday());
                patient.setEmail(userEntity.getEmail());
                patient.setPhone(userEntity.getPhone());
                if(treatment.getTreatmentTimeFrom() != null){
                    String dateFrom = ParseDateUtil.fortmatDate(treatment.getTreatmentTimeFrom(),"dd/MM/yyyy HH:mm:ss");
                    patient.setFirstTreatmentTime(dateFrom);
                }
                if(treatment.getTreatmentTimeTo() != null){
                    String dateTo = ParseDateUtil.fortmatDate(treatment.getTreatmentTimeTo(),"dd/MM/yyyy HH:mm:ss");
                    patient.setLastTreatmentTime(dateTo);
                }else{
                    patient.setLastTreatmentTime("Đang Điều Trị");
                }
                patients.add(patient);
            }
        }
        return patients;
    }

    private PagingResponse createPageResponse(int page,int totalPages){
        String[] numberPages = getNumberPages(totalPages,page);
        PagingResponse pagingResponse = new PagingResponse();
        pagingResponse.setCurrentPage(page);
        pagingResponse.setTotalPage(totalPages);
        pagingResponse.setNumberPages(numberPages);
        return pagingResponse;
    }

    private String[] getNumberPages(int totalPage,int currentPage){
        List<Integer> integers = Arrays.asList(-2,-1,0,1,2);
        List<String> numberPages = new ArrayList<>();
        for(int a :integers){
            int numberPage = currentPage + a;
            if(numberPage > 0 && numberPage <= totalPage){
                numberPages.add(String.valueOf(numberPage));
            }
        }
        return numberPages.toArray(new String[0]);
    }

    private List<BookingTodayResponse> getBookingByDate(Date toDate,Date endDate,int numberDate){
        List<CalendarbookingEntity> bookings = calendarbookingRepository.findAllByAppointmentTimeFromBetweenOrderByAppointmentTimeFromAsc(toDate,endDate);
        log.info("Number patient Booking is {}",bookings.size());
        List<BookingTodayResponse> bookingTodayResponses = new ArrayList<>();
        for(CalendarbookingEntity booking : bookings){
            BookingTodayResponse bookingTodayResponse = new BookingTodayResponse();
            bookingTodayResponse.setBookingId(booking.getId());
            bookingTodayResponse.setCode(booking.getCode());
            bookingTodayResponse.setName(booking.getFullName());
            UserEntity patient = userRepository.findOne(booking.getUserEntity().getId());
            bookingTodayResponse.setNumberPhone(patient.getPhone());
            String timeBooking = getTimeBooking(numberDate,booking.getAppointmentTimeFrom());
            bookingTodayResponse.setTimeBooking(timeBooking);
            bookingTodayResponse.setServices(booking.getRelatedServices());
            bookingTodayResponse.setDoctorName(booking.getDoctorName());
            bookingTodayResponses.add(bookingTodayResponse);
        }
        log.info("End get Booking today");
        return bookingTodayResponses;
    }

    private List<UserEntity> getBirthDate(int numberDate){
        Date toDate = new Date();
        if(numberDate == 0){
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM");
            String birthDay = simpleDateFormat.format(toDate);
            return userRepository.findAllByBirthdayContainingOrBirthdayContainingOrderByFullNameAsc(birthDay,birthDay);
        }else{
            int currentMonth = toDate.getMonth() + 1;
            int nextMonth = currentMonth + 1;
            String startDay = getMonthBirthDay(currentMonth);
            String endDay = getMonthBirthDay(nextMonth);
            return userRepository.findAllByBirthdayContainingOrBirthdayContainingOrderByFullNameAsc(startDay,endDay);
        }
    }

    private String getMonthBirthDay(int month){
        if(month < 10){
            return "/0"+month+"/";
        }else{
            return "/"+month+"/";
        }
    }

    private String getTimeBooking(int numberDate,Date appointmentTimeFrom){
        if(numberDate == 0){
            return getTimeBookingToDay(appointmentTimeFrom);
        }else{
            return ParseDateUtil.fortmatDate(appointmentTimeFrom,ClinicSettings.FORTMAT_DATE_TYPE);
        }
    }

    private String getTimeBookingToDay(Date date){
        int hour = date.getHours();
        int minute = date.getMinutes();
        String hourBooking = String.valueOf(hour);
        String minuteBooking = String.valueOf(minute);
        if(hour < 10){
            hourBooking = "0"+hour;
        }
        if(minute < 10){
            minuteBooking = "0"+minute;
        }
        return hourBooking + ":" + minuteBooking;
    }


    private Date getDateWithIntervalDayStart(int interValDate){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH,interValDate);
        calendar.set(Calendar.HOUR_OF_DAY,0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);
        return calendar.getTime();
    }

    private Date getDateWithIntervalDayEnd(int interValDate){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH,interValDate);
        calendar.set(Calendar.HOUR_OF_DAY,23);
        calendar.set(Calendar.MINUTE,59);
        calendar.set(Calendar.SECOND,59);
        return calendar.getTime();
    }

    private Date getDateWithIntervalMonthStart(int interValMonth){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH,interValMonth);
        calendar.set(Calendar.HOUR_OF_DAY,0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);
        return calendar.getTime();
    }

    private Date getDateWithIntervalMonthEnd(int interValMonth){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH,interValMonth);
        calendar.set(Calendar.HOUR_OF_DAY,23);
        calendar.set(Calendar.MINUTE,59);
        calendar.set(Calendar.SECOND,59);
        return calendar.getTime();
    }

    private Date getDateWithIntervalYearStart(int interValYear){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR,interValYear);
        calendar.set(Calendar.HOUR_OF_DAY,0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);
        return calendar.getTime();
    }

    private Date getDateWithIntervalYearEnd(int interValYear){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR,interValYear);
        calendar.set(Calendar.HOUR_OF_DAY,23);
        calendar.set(Calendar.MINUTE,59);
        calendar.set(Calendar.SECOND,59);
        return calendar.getTime();
    }

    private Pair<Date,Date> getDateRangerOfCurrentMonth(GetAssignDoctorRequest getAssignDoctorRequest) throws ParseDateException, ParameterGetAssignDoctorInvalidException {
        String dateSelected = getAssignDoctorRequest.getDateSelected();
        String typeSelected = getAssignDoctorRequest.getTypeSelected();
        if(!StringUtils.isEmpty(dateSelected) && !StringUtils.isEmpty(typeSelected)){
            log.error("Parameters for get Asssign doctor invalid");
            throw new ParameterGetAssignDoctorInvalidException("Parameters Invalid");
        }
        if(StringUtils.isEmpty(dateSelected)){
            if(StringUtils.isEmpty(typeSelected)){
                log.error("Parameters for get Asssign doctor invalid");
                throw new ParameterGetAssignDoctorInvalidException("Parameters Invalid");
            }
            Date today = new Date();
            Calendar calendarBegin = getCurrentTime(today);
            Calendar calendarEnd = getCurrentTime(today);
            if(typeSelected.equals(TypeSelected.month.toString())){
                calendarBegin.set(Calendar.DAY_OF_MONTH,calendarBegin.getActualMinimum(Calendar.DAY_OF_MONTH));
                calendarEnd.set(Calendar.DAY_OF_MONTH,calendarBegin.getActualMaximum(Calendar.DAY_OF_MONTH));
            }else if(typeSelected.equals(TypeSelected.week.toString())){
                calendarBegin.set(Calendar.DAY_OF_WEEK,calendarBegin.getActualMinimum(Calendar.DAY_OF_WEEK));
                calendarEnd.set(Calendar.DAY_OF_WEEK,calendarBegin.getActualMaximum(Calendar.DAY_OF_WEEK));
                calendarBegin.add(Calendar.DATE,1);
                calendarEnd.add(Calendar.DATE,1);
            }
            setTimeToBegingDate(calendarBegin);
            setTimeToEndofDay(calendarEnd);
            return Pair.of(calendarBegin.getTime(),calendarEnd.getTime());
        }else{
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date date = simpleDateFormat.parse(dateSelected);
                Calendar calendarBegin = getCurrentTime(date);
                Calendar calendarEnd = getCurrentTime(date);
                setTimeToBegingDate(calendarBegin);
                setTimeToEndofDay(calendarEnd);
                return Pair.of(calendarBegin.getTime(),calendarEnd.getTime());
            } catch (ParseException e) {
                log.error("DateSelected invalid fortmat :"+e.getMessage());
                throw new ParseDateException("Parse DateSelected faileds");
            }
        }
    }

    private Calendar getCurrentTime(Date today){
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(today);
        return calendar;
    }

    private void setTimeToBegingDate(Calendar calendar){
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    private void setTimeToEndofDay(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
    }


    private Date endEveryDate(Date beginMonths){
        Calendar endEveryDate = Calendar.getInstance();
        endEveryDate.setTime(beginMonths);
        endEveryDate.set(Calendar.HOUR,23);
        endEveryDate.set(Calendar.MINUTE,59);
        endEveryDate.set(Calendar.SECOND,59);
        return endEveryDate.getTime();
    }

    private String fortmatDayResponse(Date beginMonths){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return simpleDateFormat.format(beginMonths);
    }

    private String fortmatDayAppointmentResponse(Date appointmentDate){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return simpleDateFormat.format(appointmentDate);
    }

    private AssignDoctorDetailResponse addAssignDoctorDetailResponse(CalendarbookingEntity booking){
        UserEntity patient = booking.getUserEntity();
        AssignDoctorDetailResponse assignDoctorDetailResponse = new AssignDoctorDetailResponse();
        assignDoctorDetailResponse.setBookingId(booking.getId());
        if(patient != null){
            assignDoctorDetailResponse.setPatientId(String.valueOf(patient.getId()));
        }
        assignDoctorDetailResponse.setPatientName(booking.getFullName());
        assignDoctorDetailResponse.setDoctorId(booking.getDoctorId());
        assignDoctorDetailResponse.setDoctorName(booking.getDoctorName());
        String appointmentDateFrom = fortmatDayAppointmentResponse(booking.getAppointmentTimeFrom());
        assignDoctorDetailResponse.setTimeStart(appointmentDateFrom);
        if(booking.getAppointmentTimeTo() != null){
            String appointmentDateTo = fortmatDayAppointmentResponse(booking.getAppointmentTimeTo());
            assignDoctorDetailResponse.setTimeEnd(appointmentDateTo);
        }
        assignDoctorDetailResponse.setServices(booking.getRelatedServices());
        assignDoctorDetailResponse.setCode(booking.getCode());
        return assignDoctorDetailResponse;
    }

    private Date beginEveryDate(Date beginMonths){
        Calendar beginEveryDate = Calendar.getInstance();
        beginEveryDate.setTime(beginMonths);
        beginEveryDate.add(Calendar.DATE,1);
        beginEveryDate.set(Calendar.HOUR,0);
        beginEveryDate.set(Calendar.MINUTE,0);
        beginEveryDate.set(Calendar.SECOND,0);
        return beginEveryDate.getTime();
    }
}
