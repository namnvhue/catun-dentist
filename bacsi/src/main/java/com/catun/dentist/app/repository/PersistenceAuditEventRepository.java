package com.catun.dentist.app.repository;

import com.catun.dentist.app.entity.PersistentAuditEventEntity;
import java.time.Instant;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the PersistentAuditEvent entity.
 */
public interface PersistenceAuditEventRepository extends
    JpaRepository<PersistentAuditEventEntity, Long> {

    List<PersistentAuditEventEntity> findByPrincipal(String principal);

    List<PersistentAuditEventEntity> findByAuditEventDateAfter(Instant after);

    List<PersistentAuditEventEntity> findByPrincipalAndAuditEventDateAfter(String principal,
        Instant after);

    List<PersistentAuditEventEntity> findByPrincipalAndAuditEventDateAfterAndAuditEventType(
        String principle, Instant after, String type);

    Page<PersistentAuditEventEntity> findAllByAuditEventDateBetween(Instant fromDate,
        Instant toDate, Pageable pageable);
}
