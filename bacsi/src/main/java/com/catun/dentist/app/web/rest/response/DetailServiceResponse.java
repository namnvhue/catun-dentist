package com.catun.dentist.app.web.rest.response;

import java.util.List;

public class DetailServiceResponse {

    private String id;
    private String name;
    private float basePrice;
    private String description;
    private List<ProductResponse> relatedProducts;
    private ServiceResponse serviceCategory;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(float basePrice) {
        this.basePrice = basePrice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ProductResponse> getRelatedProducts() {
        return relatedProducts;
    }

    public void setRelatedProducts(List<ProductResponse> relatedProducts) {
        this.relatedProducts = relatedProducts;
    }

    public ServiceResponse getServiceCategory() {
        return serviceCategory;
    }

    public void setServiceCategory(ServiceResponse serviceCategory) {
        this.serviceCategory = serviceCategory;
    }
}
