package com.catun.dentist.app.exception;

public class InvalidParameterException extends Exception{
    public InvalidParameterException(String message) {
        super(message);
    }
}
