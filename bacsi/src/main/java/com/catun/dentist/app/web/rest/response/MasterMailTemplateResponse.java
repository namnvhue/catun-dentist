package com.catun.dentist.app.web.rest.response;


import java.util.List;

public class MasterMailTemplateResponse {

    private List<ListMailTemplateResponse> masterTemplate;


    public List<ListMailTemplateResponse> getMasterTemplate() {
        return masterTemplate;
    }

    public void setMasterTemplate(List<ListMailTemplateResponse> masterTemplate) {
        this.masterTemplate = masterTemplate;
    }
}
