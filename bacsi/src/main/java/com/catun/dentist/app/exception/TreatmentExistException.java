package com.catun.dentist.app.exception;

public class TreatmentExistException extends Exception{

    public TreatmentExistException(String message) {
        super(message);
    }
}
