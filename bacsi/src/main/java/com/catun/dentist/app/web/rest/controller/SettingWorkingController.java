package com.catun.dentist.app.web.rest.controller;

import com.catun.dentist.app.exception.DateOffInvalidException;
import com.catun.dentist.app.exception.GlobalSettingNotFoundException;
import com.catun.dentist.app.service.SettingWorkingService;
import com.catun.dentist.app.web.rest.request.SettingTimeWorkingRequest;
import com.catun.dentist.app.web.rest.response.DetailCommonResponse;
import com.catun.dentist.app.web.rest.response.SettingTimeWorkingResponse;
import com.codahale.metrics.annotation.Timed;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping()
public class SettingWorkingController {

    private final Logger log = LoggerFactory.getLogger(MailTemplateController.class);
    private SettingWorkingService settingWorkingService;

    public SettingWorkingController(SettingWorkingService settingWorkingService) {
        this.settingWorkingService = settingWorkingService;
    }


    @PostMapping("/admin/time-working")
    @Timed
    public ResponseEntity<DetailCommonResponse> settingTimeWorking(
        @Valid @RequestBody SettingTimeWorkingRequest settingTimeWorkingRequest)
        throws DateOffInvalidException {
        log.debug("REST request to save MailTemplate : {}", settingTimeWorkingRequest);
        settingWorkingService.settingTimeWorking(settingTimeWorkingRequest);
        return new ResponseEntity<>(new DetailCommonResponse("201", "Success"), HttpStatus.CREATED);
    }

    @GetMapping("/public/time-working")
    @Timed
    public ResponseEntity<SettingTimeWorkingResponse> getTimeWorking() throws GlobalSettingNotFoundException {
        log.debug("REST request to get time working : ");
        SettingTimeWorkingResponse settingTimeWorkingResponse = settingWorkingService
            .getTimeWorking();
        return new ResponseEntity<>(settingTimeWorkingResponse, HttpStatus.OK);
    }
}
