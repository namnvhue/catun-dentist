package com.catun.dentist.app.web.rest.util;

import com.catun.dentist.app.exception.ExcelFileException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.List;

public class ExportExcelUtils {

    private static final Logger log = LoggerFactory.getLogger(ExportExcelUtils.class);

    public static void exportExcel(HttpServletResponse response, String fileName, ExcelData data) throws Exception {
        // 告诉浏览器用什么软件可以打开此文件
        response.setHeader("content-Type", "application/vnd.ms-excel");
        // 下载文件的默认名称
        response.setHeader("Content-Disposition", "attachment;filename="+ URLEncoder.encode(fileName, "utf-8"));
        exportExcel(data, response.getOutputStream());
    }

    public static void exportExcel(ExcelData data, OutputStream out) throws Exception {
        try
        {
            log.info("Start ExportExcel ....................");
            HSSFWorkbook wb = new HSSFWorkbook();
            CreationHelper createHelper = wb.getCreationHelper();
            // Create a Sheet
            Sheet sheet = wb.createSheet("WareHouse");
            sheet.setDefaultColumnWidth(30);
            int colIndex = 0;
            Row dataRowHeader = sheet.createRow(0);
            Cell cellHdOne = dataRowHeader.createCell(0);
            cellHdOne.setCellValue("Tên Sản Phẩm");
            Cell cellHdTwo = dataRowHeader.createCell(1);
            cellHdTwo.setCellValue("Tháng Trước Còn Lại");
            Cell cellHdThree = dataRowHeader.createCell(2);
            cellHdThree.setCellValue("Nhập Vào");
            Cell cellHdFour = dataRowHeader.createCell(3);
            cellHdFour.setCellValue("Xuất Ra");
            Cell cellHdFive = dataRowHeader.createCell(4);
            cellHdFive.setCellValue("Còn Lại");
            int rowIndex = 1;
            List<List<Object>> rows = data.getRows();
            for (List<Object> rowData : rows) {
                Row dataRow = sheet.createRow(rowIndex);
                colIndex = 0;
                for (Object cellData : rowData) {
                    Cell cell = dataRow.createCell(colIndex);
                    if (cellData != null) {
                        cell.setCellValue(cellData.toString());
                    } else {
                        cell.setCellValue("");
                    }
                    colIndex++;
                }
                rowIndex++;
            }
            wb.write(out);
            log.info("ExportExcel ....................Success");
        }
        catch (Exception e)
        {
            log.error("Create files Excel failed {}",e.getMessage());
            throw new ExcelFileException("");
        }
    }
}
