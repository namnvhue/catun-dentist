package com.catun.dentist.app.exception;

public class TreatmentNotFoundException extends Exception{

    public TreatmentNotFoundException(String message) {
        super(message);
    }
}
