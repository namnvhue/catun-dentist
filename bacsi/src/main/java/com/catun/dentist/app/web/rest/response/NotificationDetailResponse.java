package com.catun.dentist.app.web.rest.response;

public class NotificationDetailResponse {

    private long userId;
    private long notificationId;
    private String linkConfirm;
    private String eventType;
    private String content;
    private String intervalTime;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(long notificationId) {
        this.notificationId = notificationId;
    }

    public String getLinkConfirm() {
        return linkConfirm;
    }

    public void setLinkConfirm(String linkConfirm) {
        this.linkConfirm = linkConfirm;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getIntervalTime() {
        return intervalTime;
    }

    public void setIntervalTime(String intervalTime) {
        this.intervalTime = intervalTime;
    }
}
