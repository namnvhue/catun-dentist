package com.catun.dentist.app.repository;

import com.catun.dentist.app.entity.WareHouseReportEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WareHouseReportRepository extends JpaRepository<WareHouseReportEntity, Long> {
}
