package com.catun.dentist.app.service.util;

import com.catun.dentist.app.exception.EncodeBase64ImageException;
import java.util.Base64;

public class EncodeBase64Util {

    public static String encodeBase64Image(byte[] image) throws EncodeBase64ImageException {
        try {
            String base64Image = Base64.getEncoder().encodeToString(image);
            return base64Image;
        } catch (Exception e) {
            throw new EncodeBase64ImageException("Encodebase64 failed");
        }
    }
}
