package com.catun.dentist.app.web.rest.response;

public class PagingResponse {

    private int totalPage;
    private int currentPage;
    private String[] numberPages;

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public String[] getNumberPages() {
        return numberPages;
    }

    public void setNumberPages(String[] numberPages) {
        this.numberPages = numberPages;
    }
}
