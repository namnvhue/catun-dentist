package com.catun.dentist.app.web.rest.request;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

public class MailTemplateRequest {

    @NotNull
    @NotEmpty
    private String masterTemplate;

    @NotNull
    @NotEmpty
    private String subject;

    @NotNull
    @NotEmpty
    private String content;

    public String getMasterTemplate() {
        return masterTemplate;
    }

    public void setMasterTemplate(String masterTemplate) {
        this.masterTemplate = masterTemplate;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
