package com.catun.dentist.app.constant;

public class CsvConstants {

    public static final String CSV_DELIMITER = ",";
    public static final String CSV_EXT = ".csv";
    public static final char FILE_DELIMITER = ',';
    public static final String FILE_EXTN = ".xls";

    public static final String[] ware_house = {
        "Tên Sản Phẩm",
        "Tháng Trước Còn Lại",
        "Nhập Vào",
        "Xuất Ra",
        "Còn Lại"
    };
}
