package com.catun.dentist.app.service;

import com.catun.dentist.app.service.dto.GlobalSettingCriteria;
import com.catun.dentist.app.service.dto.GlobalSettingDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface GlobalSettingQueryService {
    List<GlobalSettingDTO> findByCriteria(GlobalSettingCriteria criteria);
    Page<GlobalSettingDTO> findByCriteria(GlobalSettingCriteria criteria, Pageable page);

}
