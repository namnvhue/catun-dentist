package com.catun.dentist.app.exception;

public class ServiceEntityExistException extends Exception {

    public ServiceEntityExistException(String message) {
        super(message);
    }
}
