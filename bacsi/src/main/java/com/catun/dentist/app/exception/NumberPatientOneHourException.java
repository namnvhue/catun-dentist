package com.catun.dentist.app.exception;

public class NumberPatientOneHourException extends Exception {

    public NumberPatientOneHourException(String message) {
        super(message);
    }
}
