package com.catun.dentist.app.repository;

import com.catun.dentist.app.entity.TreatmentEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface TreatmentRepository extends
    JpaRepository<TreatmentEntity, Long> {

    TreatmentEntity findByBookingId(long bookingId);

    Page<TreatmentEntity> findAllByFinishedFalse(Pageable pageable);

    Page<TreatmentEntity> findAllByFinishedFalseAndFullNameContainingOrPhoneContainingOrEmailContainingOrBirthdayContaining(Pageable pageable,String fullName,String phone,String email,String birthDay);

    Page<TreatmentEntity> findAllByFullNameContainingOrPhoneContainingOrEmailContainingOrBirthdayContaining(Pageable pageable,String fullName,String phone,String email,String birthDay);

    TreatmentEntity findByPatientId(String treatmentId);

    List<TreatmentEntity> findAllByFinishedTrueAndTreatmentTimeToBetweenOrTreatmentTimeToBetweenOrTreatmentTimeToBetweenOrTreatmentTimeToBetweenOrTreatmentTimeToBetweenOrTreatmentTimeToBetweenOrderByTreatmentTimeToDesc(Date oneDayStart, Date oneDayEnd, Date threeDayStart, Date threeDayEnd, Date oneMonthStart, Date oneMonthEnd, Date threeMonthStart, Date threeMonthEnd, Date sixMonthStart, Date sixMonthEnd, Date oneYearStart, Date oneYearEnd);
}
