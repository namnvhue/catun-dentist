package com.catun.dentist.app.web.rest.controller;

import com.catun.dentist.app.exception.AppointmentDateException;
import com.catun.dentist.app.exception.BookingCodeNotFoundException;
import com.catun.dentist.app.exception.CodeInvalidException;
import com.catun.dentist.app.exception.GlobalSettingNotFoundException;
import com.catun.dentist.app.exception.QrCodeImageNotFoundWithPathException;
import com.catun.dentist.app.exception.ServiceEntityNotFoundException;
import com.catun.dentist.app.service.CalendarbookingService;
import com.catun.dentist.app.service.util.DomainUtil;
import com.catun.dentist.app.web.rest.response.ConfirmResponse;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConfirmController {

    private final Logger log = LoggerFactory.getLogger(ConfirmController.class);
    @Autowired
    private CalendarbookingService calendarbookingService;

    @GetMapping
    @RequestMapping("/confirm/{code}")
    public ConfirmResponse getConfirm(@PathVariable("code") String code,
        HttpServletRequest httpServletRequest)
        throws CodeInvalidException, AppointmentDateException, ServiceEntityNotFoundException, GlobalSettingNotFoundException {
        String uri = DomainUtil.getDomain(httpServletRequest);
        return calendarbookingService.getConfirm(code, uri);
    }

    @GetMapping
    @RequestMapping("/confirm/images/{code}")
    public ResponseEntity<Resource> getResources(@PathVariable("code") String code,
        HttpServletRequest httpServletRequest)
        throws CodeInvalidException, AppointmentDateException, BookingCodeNotFoundException, QrCodeImageNotFoundWithPathException, IOException {
        log.debug("Request for qr code image: {}", code);
        String qrCode = code.replace(".png", "");

        Resource resource = calendarbookingService.getQrCodeImage(qrCode);
        String contentType = httpServletRequest.getServletContext()
            .getMimeType(resource.getFile().getAbsolutePath());
        return ResponseEntity.ok()
            .contentType(MediaType.parseMediaType(contentType))
            .header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + resource.getFilename() + "\"")
            .body(resource);
    }

}
