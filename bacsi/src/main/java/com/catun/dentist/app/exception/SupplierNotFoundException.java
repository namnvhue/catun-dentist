package com.catun.dentist.app.exception;

public class SupplierNotFoundException extends Exception {

    public SupplierNotFoundException(String message) {
        super(message);
    }
}
