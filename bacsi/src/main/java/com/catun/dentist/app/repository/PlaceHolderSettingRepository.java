package com.catun.dentist.app.repository;

import com.catun.dentist.app.entity.PlaceHolderSettingEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlaceHolderSettingRepository extends
    JpaRepository<PlaceHolderSettingEntity, Long> {

}
