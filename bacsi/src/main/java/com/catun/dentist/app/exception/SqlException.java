package com.catun.dentist.app.exception;

public class SqlException extends Exception {

    public SqlException(String message) {
        super(message);
    }
}
