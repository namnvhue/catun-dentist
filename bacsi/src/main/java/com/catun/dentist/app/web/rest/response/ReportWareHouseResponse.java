package com.catun.dentist.app.web.rest.response;

import java.util.List;

public class ReportWareHouseResponse {

    private List<ReportWareHouseDetailResponse> wareHouses;
    private int totalRestAmountMonthAgo;
    private int totalInputAmount;
    private int totalOutputAmount;
    private int totalRestAmount;

    public List<ReportWareHouseDetailResponse> getWareHouses() {
        return wareHouses;
    }

    public void setWareHouses(List<ReportWareHouseDetailResponse> wareHouses) {
        this.wareHouses = wareHouses;
    }


    public int getTotalRestAmountMonthAgo() {
        return totalRestAmountMonthAgo;
    }

    public void setTotalRestAmountMonthAgo(int totalRestAmountMonthAgo) {
        this.totalRestAmountMonthAgo = totalRestAmountMonthAgo;
    }

    public int getTotalInputAmount() {
        return totalInputAmount;
    }

    public void setTotalInputAmount(int totalInputAmount) {
        this.totalInputAmount = totalInputAmount;
    }

    public int getTotalOutputAmount() {
        return totalOutputAmount;
    }

    public void setTotalOutputAmount(int totalOutputAmount) {
        this.totalOutputAmount = totalOutputAmount;
    }

    public int getTotalRestAmount() {
        return totalRestAmount;
    }

    public void setTotalRestAmount(int totalRestAmount) {
        this.totalRestAmount = totalRestAmount;
    }
}
