package com.catun.dentist.app.exception;

public class BookingDateOffException extends Exception {

    public BookingDateOffException(String message) {
        super(message);
    }
}
