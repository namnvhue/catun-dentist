package com.catun.dentist.app.service.impl;

import com.catun.dentist.app.constant.ClinicSettings;
import com.catun.dentist.app.domain.GlobalSetting;
import com.catun.dentist.app.entity.ServiceCategoryEntity;
import com.catun.dentist.app.entity.ServiceEntity;
import com.catun.dentist.app.entity.TreatmentDetailEntity;
import com.catun.dentist.app.entity.TreatmentItemEntity;
import com.catun.dentist.app.entity.UserEntity;
import com.catun.dentist.app.exception.ConvertMoneyFortmatException;
import com.catun.dentist.app.exception.GlobalSettingNotFoundException;
import com.catun.dentist.app.exception.ServiceCategoryNotFoundException;
import com.catun.dentist.app.exception.TreatmentDetailNotFoundException;
import com.catun.dentist.app.exception.TreatmentDetailPdfException;
import com.catun.dentist.app.exception.TreatmentPriceException;
import com.catun.dentist.app.exception.UserNotFoundException;
import com.catun.dentist.app.repository.GlobalSettingRepository;
import com.catun.dentist.app.repository.ServiceCategoryRepository;
import com.catun.dentist.app.repository.ServiceRepository;
import com.catun.dentist.app.repository.TreatmentDetailRepository;
import com.catun.dentist.app.repository.TreatmentItemRepository;
import com.catun.dentist.app.repository.UserRepository;
import com.catun.dentist.app.security.AuthoritiesConstants;
import com.catun.dentist.app.security.SecurityUtils;
import com.catun.dentist.app.service.TreatmentDetailService;
import com.catun.dentist.app.service.util.NumberToMoneyCharactersUtil;
import com.catun.dentist.app.web.rest.request.TreatmentDetailRequest;
import com.catun.dentist.app.web.rest.request.TreatmentServicesRequest;
import com.catun.dentist.app.web.rest.response.PatientInforResponse;
import com.catun.dentist.app.web.rest.response.TreatmentCategoryServicesResponse;
import com.catun.dentist.app.web.rest.response.TreatmentDetailResponse;
import com.catun.dentist.app.web.rest.response.TreatmentDetailServiceResponse;
import com.catun.dentist.app.web.rest.response.TreatmentDetailServicesResponse;
import com.catun.dentist.app.web.rest.response.TreatmentDetailShowResponse;
import com.catun.dentist.app.web.rest.response.TreatmentServiceResponse;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.management.ServiceNotFoundException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class TreatmentDetailServiceImpl implements TreatmentDetailService {

    private final Logger log = LoggerFactory.getLogger(TreatmentDetailServiceImpl.class);
    private UserRepository userRepository;
    private ServiceCategoryRepository serviceCategoryRepository;
    private ServiceRepository serviceRepository;
    private TreatmentDetailRepository treatmentDetailRepository;
    private GlobalSettingRepository globalSettingRepository;
    private TreatmentItemRepository treatmentItemRepository;

    @Value("${catun.directory.pathtreatment.server}")
    private String directoryPathTreatmentServer;

    @Value("${catun.pdf.deploy.production}")
    private String production;

    public TreatmentDetailServiceImpl(UserRepository userRepository, ServiceCategoryRepository serviceCategoryRepository, ServiceRepository serviceRepository, TreatmentDetailRepository treatmentDetailRepository,
                                      GlobalSettingRepository globalSettingRepository,TreatmentItemRepository treatmentItemRepository){
        this.userRepository = userRepository;
        this.serviceCategoryRepository = serviceCategoryRepository;
        this.serviceRepository = serviceRepository;
        this.treatmentDetailRepository = treatmentDetailRepository;
        this.globalSettingRepository = globalSettingRepository;
        this.treatmentItemRepository = treatmentItemRepository;
    }



    @Override
    public TreatmentDetailServiceResponse getTreatmentDetailService() throws ServiceCategoryNotFoundException, ServiceNotFoundException {
        TreatmentDetailServiceResponse treatmentDetailServiceResponse  = new TreatmentDetailServiceResponse();
        List<String> toothNames = Arrays.asList("11-18","21-28","31-38","41","42","43","44","45","46","47","48","Hàm trên","Hàm dưới","2 hàm");
        List<Integer> amounts = Arrays.asList(1,2,3,4,5);
        List<TreatmentCategoryServicesResponse> categoryServices =  getCategoryServices();
        treatmentDetailServiceResponse.setCategoryServices(categoryServices);
        treatmentDetailServiceResponse.setToothNames(toothNames);
        treatmentDetailServiceResponse.setAmounts(amounts);
        return treatmentDetailServiceResponse;
    }

    @Override
    @Transactional
    public void createTreatmentDetail(TreatmentDetailRequest treatmentDetailRequest) throws UserNotFoundException, IOException, DocumentException, TreatmentDetailPdfException, GlobalSettingNotFoundException, TreatmentPriceException, ConvertMoneyFortmatException {
        checkRequest(treatmentDetailRequest);
        log.info("Start create treatment detail PDF");
        UserEntity doctor = SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).get();
        long patientId = Long.parseLong(treatmentDetailRequest.getPatientId());
        UserEntity patient = userRepository.findOne(patientId);
        if(patient == null){
            String errorMessage = "User Not Found with userId "+patientId;
            log.error(errorMessage);
            throw new UserNotFoundException(errorMessage);
        }
        String fileName = createTreatmentPDF(patient,treatmentDetailRequest,doctor);
        StringBuilder stringBuilder = new StringBuilder("/treatment-detail");
        stringBuilder.append("/").append(patientId).append("/").append(fileName);
        saveTreatmentDetail(treatmentDetailRequest,doctor.getId(),patient.getId(),stringBuilder.toString(),doctor.getFullName());
    }

    @Override
    public List<TreatmentDetailResponse> getTreatmentDetail(String treatmentId) throws TreatmentDetailNotFoundException, ConvertMoneyFortmatException {
        log.info("Start get TreatmentDetail with treatmentId {}",treatmentId);
        List<TreatmentDetailEntity> treatmentDetails = treatmentDetailRepository.findAllByTreatmentId(Long.parseLong(treatmentId));
        if(treatmentDetails.isEmpty()){
            log.info("TreatmentDetail not exist with treatmentId {}",treatmentId);
        }
        List<TreatmentDetailResponse> responses = new ArrayList<>();
        for(TreatmentDetailEntity treatmentDetail : treatmentDetails){
            TreatmentDetailResponse response = new TreatmentDetailResponse();
            response.setTreatmentDetailId(String.valueOf(treatmentDetail.getId()));
            String totalCost = NumberToMoneyCharactersUtil.convertMoneyFortmat(treatmentDetail.getTotalCost());
            response.setTotalCost(totalCost);
            response.setDicount(treatmentDetail.getDiscount());
            String realCost = NumberToMoneyCharactersUtil.convertMoneyFortmat(treatmentDetail.getRealCost());
            response.setRealCost(realCost);
            response.setNoteFromDoctor(treatmentDetail.getNotedFromDoctor());
            response.setNoteFromPatient(treatmentDetail.getNotedFromPatient());
            response.setCreatedBy(treatmentDetail.getCreatedBy());
            List<Object[]> objects  = userRepository.findNameById(Long.parseLong(treatmentDetail.getDoctorId()));
            if(!objects.isEmpty()){
                response.setCreatedBy(String.valueOf(objects.get(0)));
            }
            Date date = Date.from(treatmentDetail.getCreatedDate());
            String dateFormat = new SimpleDateFormat("dd/MM/yyyy").format(date);
            response.setCreateDate(dateFormat);
            List<TreatmentItemEntity> items = treatmentDetail.getTreatmentItem();
            if(!items.isEmpty()){
                StringBuilder services = new StringBuilder();
                for(TreatmentItemEntity item :items){
                    ServiceEntity serviceEntity = serviceRepository.findOne(Long.valueOf(item.getServiceId()));
                    services.append(serviceEntity.getName()).append(",");
                }
                response.setServices(services.toString());
            }
            response.setLinkDownload(treatmentDetail.getLinkDownloadFile());
            String realPaymented = NumberToMoneyCharactersUtil.convertMoneyFortmat(treatmentDetail.getPaymented());
            response.setPaymented(realPaymented);
            response.setFinishPaymented(treatmentDetail.isFinishPaymented());
            responses.add(response);
        }
        return responses;
    }

    @Override
    public Resource downloadTreatmentDetail(String patientId, String fileName) throws TreatmentDetailPdfException {
        return downloadFilePdf(patientId,fileName);
    }

    @Override
    public TreatmentDetailShowResponse getTreatmentDetailTail(long treatmentDetailId) throws TreatmentDetailNotFoundException, ConvertMoneyFortmatException {
        log.info("Start get TreatmentDetail with treatmentDetailId {}",treatmentDetailId);
        TreatmentDetailEntity treatmentDetail =  treatmentDetailRepository.findOne(treatmentDetailId);
        if(treatmentDetail == null){
            log.error("Treatmentdetail not found");
            throw new TreatmentDetailNotFoundException("TreatmentDetail not found");
        }
        TreatmentDetailShowResponse response = new TreatmentDetailShowResponse();
        response.setTreatmentDetailId(String.valueOf(treatmentDetail.getId()));
        String totalCost = NumberToMoneyCharactersUtil.convertMoneyFortmat(treatmentDetail.getTotalCost());
        response.setTotalCost(totalCost);
        response.setDicount(treatmentDetail.getDiscount());
        String realCost = NumberToMoneyCharactersUtil.convertMoneyFortmat(treatmentDetail.getRealCost());
        response.setRealCost(realCost);
        response.setNoteFromDoctor(treatmentDetail.getNotedFromDoctor());
        response.setNoteFromPatient(treatmentDetail.getNotedFromPatient());
        response.setCreatedBy(treatmentDetail.getCreatedBy());
        Date date = Date.from(treatmentDetail.getCreatedDate());
        String dateFormat = new SimpleDateFormat("dd/MM/yyyy").format(date);
        response.setCreateDate(dateFormat);
        List<TreatmentItemEntity> items = treatmentDetail.getTreatmentItem();
        List<TreatmentDetailServicesResponse> services = new ArrayList<>();
        log.info("Number services in treatmentdetail is {}",items.size());
        if(!items.isEmpty()){
            for(TreatmentItemEntity item :items){
                TreatmentDetailServicesResponse service = new TreatmentDetailServicesResponse();
                service.setId(item.getId());
                service.setAmounts(item.getAmounts());
                service.setRelatedProducts(item.getRelatedProducts());
                service.setToothNames(item.getToothNames());
                service.setServicePrice(item.getServicePrice());
                long serviceId = Long.parseLong(item.getServiceId());
                ServiceEntity serviceEntity = serviceRepository.findOne(serviceId);
                if(serviceEntity != null){
                    service.setName(serviceEntity.getName());
                }
                services.add(service);
            }
        }
        response.setServices(services);
        log.info("End get TreatmentDetail Success");
        return response;
    }

    @Override
    public void paymentTreatmentDetail(long treatmentDetailId) throws TreatmentDetailNotFoundException {
        log.info("Start Update Paymented for treatmentdetailId {}",treatmentDetailId);
        TreatmentDetailEntity treatmentDetail = treatmentDetailRepository.findOne(treatmentDetailId);
        if(treatmentDetail == null){
            log.error("TreatmentDetail not found with treatmentDetailId {}",treatmentDetailId);
            throw new TreatmentDetailNotFoundException("TreatmentDetail not found");
        }
        treatmentDetail.setPaymented(treatmentDetail.getRealCost());
        treatmentDetail.setFinishPaymented(true);
        treatmentDetailRepository.save(treatmentDetail);
        log.info("End Update Paymented Success");
    }

    private void checkRequest(TreatmentDetailRequest treatmentDetailRequest) throws TreatmentPriceException {
        List<TreatmentServicesRequest> services = treatmentDetailRequest.getServices();
        float sum = 0;
        for(TreatmentServicesRequest service : services){
            float totalService = service.getPrice() * service.getAmounts();
            sum += totalService;
        }
        float totalPrice = treatmentDetailRequest.getTotalPrice();
        if(sum != totalPrice){
            log.error("Parameter of request wrong ,Total all price service not equals totalPrice");
            throw new TreatmentPriceException("Parameter of request wrong ");
        }
        float finalPrice = totalPrice - (totalPrice * treatmentDetailRequest.getDiscount())/100;
        if(finalPrice != treatmentDetailRequest.getFinalPrice()){
            log.error("Parameter of request wrong ,finalPrice not exactly");
            throw new TreatmentPriceException("Parameter of request wrong ");
        }
    }


    private List<PatientInforResponse> getPatientInfo() throws UserNotFoundException {
        List<UserEntity> userEntities = userRepository.findAllByAuthoritiesName(AuthoritiesConstants.USER);
        if(userEntities.isEmpty()){
            String errorMessage = "Can't find any User";
            log.error(errorMessage);
            throw new UserNotFoundException(errorMessage);
        }
        List<PatientInforResponse> patientInforResponses = new ArrayList<>();
        for(UserEntity userEntity : userEntities){
            PatientInforResponse patientInforResponse = new PatientInforResponse();
            patientInforResponse.setName(userEntity.getFullName());
            patientInforResponse.setNumberPhone(userEntity.getPhone());
            patientInforResponses.add(patientInforResponse);
        }
        return patientInforResponses;
    }

    private List<TreatmentCategoryServicesResponse> getCategoryServices() throws ServiceCategoryNotFoundException, ServiceNotFoundException {
        List<ServiceCategoryEntity> serviceCategories = serviceCategoryRepository.findAll();
        if(serviceCategories.isEmpty()){
            String errorMessage = "CategoryServices is empty";
            log.error(errorMessage);
            throw new ServiceCategoryNotFoundException(errorMessage);
        }
        List<TreatmentCategoryServicesResponse> categoryServices = new ArrayList<>();
        for(ServiceCategoryEntity serviceCategoryEntity : serviceCategories){
            TreatmentCategoryServicesResponse treatmentCategoryServicesResponse =  new TreatmentCategoryServicesResponse();
            treatmentCategoryServicesResponse.setId(String.valueOf(serviceCategoryEntity.getId()));
            treatmentCategoryServicesResponse.setName(serviceCategoryEntity.getName());
            List<TreatmentServiceResponse> services = getServices(serviceCategoryEntity.getId());
            treatmentCategoryServicesResponse.setServices(services);
            categoryServices.add(treatmentCategoryServicesResponse);
        }
        return categoryServices;
    }

    private List<TreatmentServiceResponse> getServices(long categoryServiceId){
        log.info("Start get Service with categoryServiceId {}",categoryServiceId);
        List<ServiceEntity> serviceEntities = serviceRepository.findAllByServiceCategoryId(categoryServiceId);
        List<TreatmentServiceResponse> services = new ArrayList<>();
        for(ServiceEntity serviceEntity : serviceEntities){
            TreatmentServiceResponse service = new TreatmentServiceResponse();
            service.setId(String.valueOf(serviceEntity.getId()));
            service.setName(serviceEntity.getName());
            service.setPrice(serviceEntity.getBasePrice());
            services.add(service);
        }
        return services;
    }

    private void saveTreatmentDetail(TreatmentDetailRequest treatmentDetailRequest,long doctorId,long patientId,String linkFile,String doctorName){
        log.info("Start save treatment detail with doctorId {} , patientId {} , linkFile {}",doctorId,patientId,linkFile);
        List<TreatmentServicesRequest> treatmentServices = treatmentDetailRequest.getServices();
        List<TreatmentItemEntity> treatmentItemEntities = new ArrayList<>();
        long treatmentId = Long.parseLong(treatmentDetailRequest.getTreatmentId());
        TreatmentDetailEntity treatmentDetailEntity = new TreatmentDetailEntity();
        for(TreatmentServicesRequest treatmentServicesRequest : treatmentServices){
            TreatmentItemEntity treatmentItemEntity = new TreatmentItemEntity();
            treatmentItemEntity.setServiceId(treatmentServicesRequest.getId());
            treatmentItemEntity.setServicePrice(treatmentServicesRequest.getPrice());
            String toothNames = String.join(",",treatmentServicesRequest.getToothNames());
            int amounts = treatmentServicesRequest.getAmounts();
            treatmentItemEntity.setToothNames(toothNames);
            treatmentItemEntity.setAmounts(amounts);
            treatmentItemEntity.setTreatmentDetailEntity(treatmentDetailEntity);
            treatmentItemEntities.add(treatmentItemEntity);
        }
        treatmentDetailEntity.setTreatmentId(treatmentId);
        treatmentDetailEntity.setDoctorId(String.valueOf(doctorId));
        treatmentDetailEntity.setPatientId(String.valueOf(patientId));
        treatmentDetailEntity.setTreatmentId(Long.parseLong(treatmentDetailRequest.getTreatmentId()));
        treatmentDetailEntity.setTreatmentItem(treatmentItemEntities);
        treatmentDetailEntity.setTotalCost(treatmentDetailRequest.getTotalPrice());
        treatmentDetailEntity.setDiscount(treatmentDetailRequest.getDiscount());
        treatmentDetailEntity.setRealCost(treatmentDetailRequest.getFinalPrice());
        treatmentDetailEntity.setNotedFromDoctor(treatmentDetailRequest.getNotedFromDoctor());
        treatmentDetailEntity.setNotedFromPatient(treatmentDetailRequest.getNotedFromPatient());
        treatmentDetailEntity.setLinkDownloadFile(linkFile);
        treatmentDetailEntity.setFinishPaymented(false);
        treatmentDetailEntity.setPaymented(0);
        if(!org.springframework.util.StringUtils.isEmpty(doctorName)){
            treatmentDetailEntity.setCreatedBy(doctorName);
        }
        treatmentDetailRepository.save(treatmentDetailEntity);
        log.info("Save treatment detail success");
    }

    private Font setFont(BaseFont baseFont,int fontSize,int style){
        Font font = new Font(baseFont,fontSize);
        font.setStyle(style);
        return font;
    }

    private String getPath(String production,String path){
        if(production.equals("true")){
            return "./treatment"+path;
        }else{
            return "src/main/resources"+path;
        }
    }

    private String createTreatmentPDF(UserEntity patient,TreatmentDetailRequest treatmentDetailRequest,UserEntity doctor) throws IOException, DocumentException,  GlobalSettingNotFoundException, ConvertMoneyFortmatException {
        log.info("Start create files PDF");
        String patientId = String.valueOf(patient.getId());
        String directoryPath = generateDirectoryFolder(patientId);
        new File(directoryPath).mkdirs();
        Date toDate = new Date();
        String simpleDateFormat = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss").format(toDate);
        Document document = new Document(PageSize.A4, 20, 20, 20, 20);
        PdfWriter.getInstance(document,new FileOutputStream(directoryPath+"/"+simpleDateFormat+".pdf"));
        document.open();
        String fontPath = getPath(production,"/fonts/Roboto-Light.ttf");
        log.info("Font Path : {} ",fontPath);
        File fontFile = new File(fontPath);
        BaseFont bf = BaseFont.createFont(fontFile.getAbsolutePath(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        Font fontHeader = setFont(bf,15,Font.BOLD);
        Font fontBold = setFont(bf,9,Font.BOLD);
        Font fontSubHeader = new Font(bf,11);
        Font fontNormal = new Font(bf,9);
        Font fontNormalItalic = setFont(bf,9,Font.ITALIC);;

        String clinicGroup = ClinicSettings.groups.get(0);
        List<String> nameClinics = ClinicSettings.nameSettingClinics;
        String nameClinic = clinicInfo(clinicGroup,nameClinics.get(0));
        Paragraph lineZero = new Paragraph("Phòng khám : ",fontSubHeader);
        lineZero.add(nameClinic);
        lineZero.setAlignment(Element.ALIGN_RIGHT);
        Paragraph lineOne = new Paragraph("Địa chỉ : ",fontSubHeader);
        String address = clinicInfo(clinicGroup,nameClinics.get(1));
        lineOne.add(address);
        lineOne.setAlignment(Element.ALIGN_RIGHT);
        document.add(lineZero);
        document.add(Chunk.NEWLINE);
        document.add(lineOne);
        document.add(Chunk.NEWLINE);
        String logoPath = getPath(production,"/logo.jpeg");
        Image logo = Image.getInstance(logoPath);
        logo.scaleAbsolute(50f, 50f);
        logo.setAbsolutePosition(30f,760f);
        document.add(logo);

        Paragraph title = new Paragraph("BẢNG DỰ TOÁN CHI PHÍ ĐIỀU TRỊ",fontHeader);
        title.setAlignment(Element.ALIGN_CENTER);

        String profileDate = new SimpleDateFormat("dd/MM/yyyy").format(toDate);
        Paragraph lineTwo = new Paragraph("Ngày lập hồ sơ : ",fontSubHeader);
        lineTwo.add(profileDate);
        lineTwo.add("                  Bác Sĩ : "+doctor.getFullName());
        Paragraph lineThree = new Paragraph("Họ và tên : ",fontSubHeader);
        lineThree.setFont(fontBold);
        lineThree.add(patient.getFullName());
        Paragraph lineFour = new Paragraph("Số điện thoại : ",fontSubHeader);
        lineFour.add(patient.getPhone());
        Paragraph lineFive = new Paragraph("Địa chỉ : ",fontSubHeader);
        lineFive.add(patient.getAddress());
        PdfPTable pdfPTable = new PdfPTable(7);
        float[] columnWidths = {2f, 7f, 1f, 6f,4f,1f,4f};
        pdfPTable.setWidths(columnWidths);
        addTableHeader(pdfPTable,fontBold);
        addRows(pdfPTable,fontNormal,treatmentDetailRequest);


        String totalPriceFortmat = NumberToMoneyCharactersUtil.convertMoneyFortmat(treatmentDetailRequest.getTotalPrice());
        float finalPrice = treatmentDetailRequest.getFinalPrice();
        String finalPriceFortmat = NumberToMoneyCharactersUtil.convertMoneyFortmat(finalPrice);
        Paragraph line6 = new Paragraph("Tổng Chi Phí :       ",fontBold);
        line6.setIndentationLeft(370);
        line6.add(totalPriceFortmat);
        Paragraph line7 = new Paragraph("Giảm Giá :            ",fontBold);
        line7.setIndentationLeft(380);
        line7.add(String.valueOf(treatmentDetailRequest.getDiscount()) + "  (%)");
        Paragraph line8 = new Paragraph("Thành Tiền :          ",fontBold);
        line8.setIndentationLeft(370);
        line8.add(finalPriceFortmat);

        String finalPriceString = getFinalPriceString(finalPrice);

        String moneyVND = NumberToMoneyCharactersUtil.readMoney(finalPriceString);
        Paragraph line9 = new Paragraph("Thành tiền bằng chữ : ",fontBold);
        line9.setFont(fontNormalItalic);
        line9.add(moneyVND);


        Paragraph line10 = new Paragraph("Ghi chú : ",fontBold);
        line10.setFont(fontNormalItalic);
        String notedFromDoctor = treatmentDetailRequest.getNotedFromDoctor();
        if(notedFromDoctor != null){
            line10.add(String.valueOf(notedFromDoctor));
        }
        Paragraph line11 = new Paragraph("Vấn đề toàn thân : ",fontBold);
        line11.setFont(fontNormalItalic);
        String notedFromPatient = treatmentDetailRequest.getNotedFromPatient();
        if(notedFromPatient != null){
            line11.add(String.valueOf(notedFromPatient));
        }
        String[] dates = profileDate.split("/");
        Paragraph line12 = new Paragraph("Phiếu này có giá trị trong 01 tháng",fontNormalItalic);
        line12.setIndentationLeft(100);
        line12.add("            Ngày "+dates[0]+" tháng "+dates[1] +" năm "+dates[2]);
        Paragraph line13 = new Paragraph("BỆNH NHÂN                                                                           ",fontBold);
        line13.setIndentationLeft(100);
        line13.add("BÁC SĨ");
        Paragraph line14 = new Paragraph("...........................                                                                  ",fontBold);
        line14.setIndentationLeft(100);
        line14.add("...........................");


        document.add(Chunk.NEWLINE);
        document.add(title);
        document.add(Chunk.NEWLINE);
        document.add(lineTwo);
        document.add(lineThree);
        document.add(lineFour);
        document.add(lineFive);
        document.add(Chunk.NEWLINE);
        document.add(pdfPTable);
        document.add(Chunk.NEWLINE);
        document.add(line6);
        document.add(line7);
        document.add(line8);
        document.add(line9);
        document.add(line10);
        document.add(line11);
        document.add(Chunk.NEWLINE);
        document.add(line12);
        document.add(Chunk.NEWLINE);
        document.add(line13);
        document.add(Chunk.NEWLINE);
        document.add(line14);
        document.close();
        return simpleDateFormat;
    }

    private String getFinalPriceString(float finalPrice){
        String finalPriceString = String.valueOf(finalPrice);
        if(finalPriceString.contains("E")){
            return String.format("%.0f",finalPrice);
        }
        return finalPriceString.replaceAll("\\.?0*$", "");
    }

    private Resource downloadFilePdf(String userId,String fileName) throws TreatmentDetailPdfException {
        String treatmentDetailPdf = directoryPathTreatmentServer + userId +"/" + fileName+".pdf";
        log.info("Start download files PDF with path {}",treatmentDetailPdf);
        try {
            Path path = Paths.get(treatmentDetailPdf);
            Resource resource = new UrlResource(path.toUri());
            log.info("Download files PDF success");
            return resource;
        } catch (Exception e) {
            log.error("Can't download Treatment Detail PDF with userId {} ,fileName {} : ",userId,fileName);
            throw new TreatmentDetailPdfException("Can't download Treatment Detail PDF ");
        }
    }

    private String generateDirectoryFolder(String userId) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(directoryPathTreatmentServer);
        stringBuilder.append(userId+"/");
        return stringBuilder.toString();
    }

    private void addTableHeader(PdfPTable table,Font fondBold){
        Paragraph paragraph1 = new Paragraph("STT",fondBold);
        PdfPCell headerTable1 = new PdfPCell(paragraph1);
        Paragraph paragraph2 = new Paragraph("DỊCH VỤ ĐIỀU TRỊ",fondBold);
        PdfPCell headerTable2 = new PdfPCell(paragraph2);
        Paragraph paragraph3 = new Paragraph("x",fondBold);
        PdfPCell headerTable3 = new PdfPCell(paragraph3);
        Paragraph paragraph4 = new Paragraph("RĂNG",fondBold);
        PdfPCell headerTable4 = new PdfPCell(paragraph4);
        Paragraph paragraph5 = new Paragraph("GIÁ",fondBold);
        PdfPCell headerTable5 = new PdfPCell(paragraph5);
        Paragraph paragraph6 = new Paragraph("SL",fondBold);
        PdfPCell headerTable6 = new PdfPCell(paragraph6);
        Paragraph paragraph7 = new Paragraph("TỔNG",fondBold);
        PdfPCell headerTable7 = new PdfPCell(paragraph7);
        addPaddingForCell(headerTable1);
        addPaddingForCell(headerTable2);
        addPaddingForCellSL(headerTable3);
        addPaddingForCell(headerTable4);
        addPaddingForCell(headerTable5);
        addPaddingForCellSL(headerTable6);
        addPaddingForCell(headerTable7);
        table.addCell(headerTable1);
        table.addCell(headerTable2);
        table.addCell(headerTable3);
        table.addCell(headerTable4);
        table.addCell(headerTable5);
        table.addCell(headerTable6);
        table.addCell(headerTable7);
    }

    private void addPaddingForCell(PdfPCell cell){
        cell.setPaddingLeft(10);
        cell.setPaddingTop(10);
        cell.setPaddingBottom(10);
    }

    private void addPaddingForCellSL(PdfPCell cell){
        cell.setPaddingLeft(3);
        cell.setPaddingTop(10);
        cell.setPaddingBottom(10);
    }


    private void addRows(PdfPTable table,Font fontNormal,TreatmentDetailRequest treatmentDetailRequest) throws ConvertMoneyFortmatException {
        List<TreatmentServicesRequest> services = treatmentDetailRequest.getServices();
        int i = 1;
        for(TreatmentServicesRequest service : services){
            String toothNames = String.join(",",service.getToothNames());
            Paragraph paragraphOne = new Paragraph(String.valueOf(i),fontNormal);
            PdfPCell cellOne = new PdfPCell(paragraphOne);
            Paragraph paragraphTwo = new Paragraph(service.getName(),fontNormal);
            PdfPCell cellTwo = new PdfPCell(paragraphTwo);

            Paragraph paragraphThree = new Paragraph("x",fontNormal);
            PdfPCell cellThree = new PdfPCell(paragraphThree);
            Paragraph paragraphFour = new Paragraph(toothNames,fontNormal);
            PdfPCell cellFour = new PdfPCell(paragraphFour);
            int amount = service.getAmounts();
            float price = service.getPrice();
            float sum = amount * price;
            String priceFortmat = NumberToMoneyCharactersUtil.convertMoneyFortmat(price);
            String sumFortmat = NumberToMoneyCharactersUtil.convertMoneyFortmat(sum);
            Paragraph paragraphFive = new Paragraph(priceFortmat,fontNormal);
            PdfPCell cellFive = new PdfPCell(paragraphFive);
            Paragraph paragraphSix = new Paragraph(String.valueOf(amount),fontNormal);
            PdfPCell cellSix = new PdfPCell(paragraphSix);
            Paragraph paragraphSeven = new Paragraph(sumFortmat,fontNormal);
            PdfPCell cellSeven = new PdfPCell(paragraphSeven);

            addPaddingForCell(cellOne);
            addPaddingForCell(cellTwo);
            addPaddingForCellSL(cellThree);
            addPaddingForCell(cellFour);
            addPaddingForCell(cellFive);
            addPaddingForCellSL(cellSix);
            addPaddingForCell(cellSeven);
            table.addCell(cellOne);
            table.addCell(cellTwo);
            table.addCell(cellThree);
            table.addCell(cellFour);
            table.addCell(cellFive);
            table.addCell(cellSix);
            table.addCell(cellSeven);
            i++;
        }
    }

    private String clinicInfo(String group,String name) throws GlobalSettingNotFoundException {
        GlobalSetting globalSetting = globalSettingRepository.findByGroupAndName(group,name);
        if(globalSetting == null){
            log.error("Get GlobalSetting not found with group {},name {}",group,name);
            throw new GlobalSettingNotFoundException("Get GlobalSetting not found");
        }
        return globalSetting.getValue();
    }
}
