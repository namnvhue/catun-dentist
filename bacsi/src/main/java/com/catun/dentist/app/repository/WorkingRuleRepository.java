package com.catun.dentist.app.repository;

import com.catun.dentist.app.entity.WorkingRuleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkingRuleRepository extends JpaRepository<WorkingRuleEntity, Long> {

}
