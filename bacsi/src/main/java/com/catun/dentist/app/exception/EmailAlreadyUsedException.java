package com.catun.dentist.app.exception;

public class EmailAlreadyUsedException extends Exception {

    public EmailAlreadyUsedException(String message) {
        super(message);
    }
}
