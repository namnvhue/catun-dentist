package com.catun.dentist.app.web.rest.response;

import java.util.List;

public class NotificationResponse {

    private List<NotificationDetailResponse> news;
    private List<NotificationDetailResponse> olds;

    public List<NotificationDetailResponse> getNews() {
        return news;
    }

    public void setNews(List<NotificationDetailResponse> news) {
        this.news = news;
    }

    public List<NotificationDetailResponse> getOlds() {
        return olds;
    }

    public void setOlds(List<NotificationDetailResponse> olds) {
        this.olds = olds;
    }
}
