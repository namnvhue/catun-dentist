package com.catun.dentist.app.web.rest.util;

import com.catun.dentist.app.exception.ParseDateException;
import com.catun.dentist.app.service.impl.SendGridEmailServiceImpl;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ParseDateUtil {

    private static final Logger log = LoggerFactory.getLogger(SendGridEmailServiceImpl.class);

    private static String[] splitDate(String dateJson) {
        return dateJson.split("/");
    }

    private static String[] splitHour(String hourJson) {
        return hourJson.split(":");
    }

    public static Date parseJsonToDate(String dateJson, String hourJson)
        throws ParseDateException {
        try {
            String[] partDays = splitDate(dateJson);
            String day = partDays[0];
            String month = partDays[1];
            if (month != null) {
                month = new DateFormatSymbols().getMonths()[Integer.valueOf(month) - 1];
            }
            String year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
            String[] partHours = splitHour(hourJson);
            String hour = partHours[0];
            String minute = partHours[1];
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MMM/yyyy HH:mm");
            Date bookingDate = formatter.parse(day + "/" + month + "/" + year + " " + hour + ":" + minute);
            Date oneDayeAgo = getOneDayAgo();
            if(!bookingDate.before(oneDayeAgo)){
                return bookingDate;
            }
            year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR) + 1);
            return formatter.parse(day + "/" + month + "/" + year + " " + hour + ":" + minute);
        } catch (Exception e) {
            log.error("Date parameter is invalid , Parse date Error :" + e.getMessage());
            throw new ParseDateException("Date parameter is invalid , Parse date Error");
        }
    }

    private static Date getOneDayAgo(){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH,-1);
        return calendar.getTime();
    }

    public static String fortmatDate(Date dateFormat,String fortmatType){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(fortmatType);
        return simpleDateFormat.format(dateFormat);
    }

    public static String getStringDateFromJson(String dateJson)
        throws JSONException, ParseDateException {
        try {
            String[] partDays = splitDate(dateJson);
            String day = partDays[0];
            String month = partDays[1];
            String year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
            return day + "_" + month + "_" + year;
        } catch (Exception e) {
            log.error("Date parameter is invalid , Parse date Error :" + e.getMessage());
            throw new ParseDateException("Date parameter is invalid , Parse date Error ");
        }
    }

    public static String getStringHourFromJson(String hourJson)
        throws JSONException, ParseDateException {
        try {
            String[] partHours = splitHour(hourJson);
            String hour = partHours[0];
            String minute = partHours[1];
            return hour + ":" + minute;
        } catch (Exception e) {
            log.error("Date parameter is invalid , Parse date Error :" + e.getMessage());
            throw new ParseDateException("Date parameter is invalid , Parse date Error");
        }
    }

    public static Date getBirthDayFormat(String birthDay)
        throws ParseException, ParseDateException {
        try {
            String[] partDays = birthDay.split("/");
            String day = partDays[0];
            String month = partDays[1];
            String year = partDays[2];
            if (partDays[1] != null) {
                DateFormatSymbols dfs = new DateFormatSymbols();
                dfs.setLocalPatternChars("en_US");
                month = dfs.getMonths()[Integer.valueOf(month) - 1];
            }
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MMM/yyyy HH:mm a");
            return formatter.parse(day + "/" + month + "/" + year + " 00:00 AM");
        } catch (Exception e) {
            log.error("Date parameter is invalid , Parse date Error :" + e.getMessage());
            throw new ParseDateException("Date parameter is invalid , Parse date Error");
        }
    }
}
