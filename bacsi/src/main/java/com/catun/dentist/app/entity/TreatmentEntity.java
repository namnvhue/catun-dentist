package com.catun.dentist.app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "treatment")
public class TreatmentEntity extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Column(name = "patient_id", nullable = false)
    private String patientId;

    @NotNull
    @Column(name = "booking_id", nullable = false)
    private long bookingId;

    @JoinColumn(name = "full_name")
    private String fullName;

    @Column(name = "birthday")
    private String birthday;

    @Column(name = "phone", length = 50)
    private String phone;

    @Column(name = "email")
    private String email;

    @Column(name = "doctor_id")
    private String doctorId;

    @Column(name = "doctor_name")
    private String doctorName;

    @JoinColumn(name = "related_services")
    private String relatedServices;

    @Column(name = "treatment_time_from")
    private Date treatmentTimeFrom;

    @Column(name = "treatment_time_to")
    private Date treatmentTimeTo;

    @NotNull
    @Column(name = "finished" ,nullable = false)
    private boolean finished = false;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }


    public long getBookingId() {
        return bookingId;
    }

    public void setBookingId(long bookingId) {
        this.bookingId = bookingId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getRelatedServices() {
        return relatedServices;
    }

    public void setRelatedServices(String relatedServices) {
        this.relatedServices = relatedServices;
    }

    public Date getTreatmentTimeFrom() {
        return treatmentTimeFrom;
    }

    public void setTreatmentTimeFrom(Date treatmentTimeFrom) {
        this.treatmentTimeFrom = treatmentTimeFrom;
    }

    public Date getTreatmentTimeTo() {
        return treatmentTimeTo;
    }

    public void setTreatmentTimeTo(Date treatmentTimeTo) {
        this.treatmentTimeTo = treatmentTimeTo;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }
}
