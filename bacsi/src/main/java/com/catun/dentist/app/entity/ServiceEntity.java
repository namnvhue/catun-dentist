package com.catun.dentist.app.entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * A ServiceEntity.
 */
@Entity
@Table(name = "service")
public class ServiceEntity extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "name", length = 50, nullable = false)
    private String name;

    @NotNull
    @Column(name = "base_price", nullable = false)
    private Float basePrice;

    @Column(name = "description")
    @Lob
    private String description;

    @Column(name = "related_products")
    private String relatedProducts;

    @ManyToOne
    private ServiceCategoryEntity serviceCategory;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ServiceEntity name(String name) {
        this.name = name;
        return this;
    }

    public Float getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(Float basePrice) {
        this.basePrice = basePrice;
    }

    public ServiceEntity basePrice(Float basePrice) {
        this.basePrice = basePrice;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ServiceEntity description(String description) {
        this.description = description;
        return this;
    }

    public String getRelatedProducts() {
        return relatedProducts;
    }

    public void setRelatedProducts(String relatedProducts) {
        this.relatedProducts = relatedProducts;
    }

    public ServiceEntity relatedProducts(String relatedProducts) {
        this.relatedProducts = relatedProducts;
        return this;
    }

    public ServiceCategoryEntity getServiceCategory() {
        return serviceCategory;
    }

    public void setServiceCategory(ServiceCategoryEntity serviceCategoryEntity) {
        this.serviceCategory = serviceCategoryEntity;
    }

    public ServiceEntity serviceCategory(ServiceCategoryEntity serviceCategoryEntity) {
        this.serviceCategory = serviceCategoryEntity;
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ServiceEntity serviceEntity = (ServiceEntity) o;
        if (serviceEntity.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), serviceEntity.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ServiceEntity{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", basePrice=" + getBasePrice() +
            ", description='" + getDescription() + "'" +
            ", relatedProducts='" + getRelatedProducts() + "'" +
            "}";
    }
}
