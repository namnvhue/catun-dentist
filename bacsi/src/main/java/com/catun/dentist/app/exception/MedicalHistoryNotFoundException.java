package com.catun.dentist.app.exception;

public class MedicalHistoryNotFoundException extends Exception{

    public MedicalHistoryNotFoundException(String message) {
        super(message);
    }
}
