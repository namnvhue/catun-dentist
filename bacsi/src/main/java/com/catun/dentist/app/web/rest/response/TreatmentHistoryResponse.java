package com.catun.dentist.app.web.rest.response;

import java.util.List;

public class TreatmentHistoryResponse {

    private List<TreatmentResponse> treatments;

    private PagingResponse pageable;

    public List<TreatmentResponse> getTreatments() {
        return treatments;
    }

    public void setTreatments(List<TreatmentResponse> treatments) {
        this.treatments = treatments;
    }

    public PagingResponse getPageable() {
        return pageable;
    }

    public void setPageable(PagingResponse pageable) {
        this.pageable = pageable;
    }
}
