package com.catun.dentist.app.web.rest.request;

public class DateOffRequest {

    private String dateOff;
    private String reason;

    public String getDateOff() {
        return dateOff;
    }

    public void setDateOff(String dateOff) {
        this.dateOff = dateOff;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
