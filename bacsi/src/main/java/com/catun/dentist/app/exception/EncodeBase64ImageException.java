package com.catun.dentist.app.exception;

public class EncodeBase64ImageException extends Exception {

    public EncodeBase64ImageException(String message) {
        super(message);
    }
}
