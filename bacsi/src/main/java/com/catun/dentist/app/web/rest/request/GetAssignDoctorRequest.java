package com.catun.dentist.app.web.rest.request;
public class GetAssignDoctorRequest {

    private String typeSelected;
    private String dateSelected;

    public String getTypeSelected() {
        return typeSelected;
    }

    public void setTypeSelected(String typeSelected) {
        this.typeSelected = typeSelected;
    }

    public String getDateSelected() {
        return dateSelected;
    }

    public void setDateSelected(String dateSelected) {
        this.dateSelected = dateSelected;
    }
}
