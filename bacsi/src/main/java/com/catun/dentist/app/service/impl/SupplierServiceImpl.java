package com.catun.dentist.app.service.impl;

import com.catun.dentist.app.entity.SupplierEntity;
import com.catun.dentist.app.exception.SupplierNotFoundException;
import com.catun.dentist.app.repository.SupplierRepository;
import com.catun.dentist.app.service.SupplierService;
import com.catun.dentist.app.web.rest.request.SupplierRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SupplierServiceImpl implements SupplierService {
    private final Logger log = LoggerFactory.getLogger(SupplierServiceImpl.class);

    private SupplierRepository supplierRepository;

    public SupplierServiceImpl(SupplierRepository supplierRepository){
        this.supplierRepository = supplierRepository;
    }

    @Override
    public List<SupplierEntity> getSupplier() throws SupplierNotFoundException {
        log.info("Start get Supplier");
        List<SupplierEntity> suppliers =  supplierRepository.findAll();
        if(suppliers.isEmpty()){
            log.error("Supplier not found");
            throw new SupplierNotFoundException("Supplier not found");
        }
        return suppliers;
    }

    @Override
    public void createSupplier(SupplierRequest request) {
        log.info("Start create Supplier");
        SupplierEntity supplier = new SupplierEntity();
        saveSupplier(supplier,request);
        log.info("End create Supplier Success");
    }

    @Override
    public void editSupplier(SupplierRequest request) throws SupplierNotFoundException {
        long supplierId = Long.parseLong(request.getId());
        log.info("Start edit Supplier with supplierId {}",supplierId);
        SupplierEntity supplier = supplierRepository.findOne(supplierId);
        if(supplier == null){
            log.error("Supplier not found with supplierId {}",supplierId);
            throw new SupplierNotFoundException("Supplier not found");
        }
        saveSupplier(supplier,request);
    }

    @Override
    public void deleteSupplier(long id) {
        log.info("Start delete supplier with supplierId {}",id);
        supplierRepository.delete(id);
        log.info("End delete supplier Success");
    }

    private void saveSupplier(SupplierEntity supplier,SupplierRequest request){
        supplier.setName(request.getName());
        supplier.setAddress(request.getAddress());
        supplier.setPhone(request.getPhone());
        supplier.setEmail(request.getEmail());
        supplierRepository.save(supplier);
    }
}
