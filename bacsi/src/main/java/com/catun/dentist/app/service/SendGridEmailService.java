package com.catun.dentist.app.service;

import com.catun.dentist.app.exception.SendGridEmailException;

public interface SendGridEmailService {

    String sendGridEmail(String subject, String toEmail, String contentEmail)
        throws SendGridEmailException;
}
