package com.catun.dentist.app.web.rest.response;

public class WareHouseDetailResponse {

    private long id;
    private long wareHouseId;
    private long userId;
    private String userName;
    private String type;
    private String timeIOput;
    private int amountIOput;
    private int amount;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getWareHouseId() {
        return wareHouseId;
    }

    public void setWareHouseId(long wareHouseId) {
        this.wareHouseId = wareHouseId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTimeIOput() {
        return timeIOput;
    }

    public void setTimeIOput(String timeIOput) {
        this.timeIOput = timeIOput;
    }

    public int getAmountIOput() {
        return amountIOput;
    }

    public void setAmountIOput(int amountIOput) {
        this.amountIOput = amountIOput;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
