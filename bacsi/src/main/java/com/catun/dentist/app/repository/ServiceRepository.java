package com.catun.dentist.app.repository;

import com.catun.dentist.app.entity.ServiceEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Spring Data JPA repository for the ServiceEntity entity.
 */
public interface ServiceRepository extends JpaRepository<ServiceEntity, Long> {

    ServiceEntity findByName(String name);
    List<ServiceEntity> findAllByServiceCategoryId(long categoryServiceId);

    List<ServiceEntity> findByRelatedProductsContaining(String productId);

}
