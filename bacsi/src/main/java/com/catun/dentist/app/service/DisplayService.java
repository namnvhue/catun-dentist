package com.catun.dentist.app.service;

import com.catun.dentist.app.entity.ExpenditureEntity;
import com.catun.dentist.app.exception.AppointmentNotFoundException;
import com.catun.dentist.app.exception.ConvertMoneyFortmatException;
import com.catun.dentist.app.exception.DoctorNotFoundException;
import com.catun.dentist.app.exception.ExpenditureEnumException;
import com.catun.dentist.app.exception.ExpenditureNotFoundException;
import com.catun.dentist.app.exception.InvalidParameterException;
import com.catun.dentist.app.exception.NumberPatientOneHourException;
import com.catun.dentist.app.exception.ParameterGetAssignDoctorInvalidException;
import com.catun.dentist.app.exception.ParseDateException;
import com.catun.dentist.app.exception.PaymentInvalidMoneyException;
import com.catun.dentist.app.exception.UserNotFoundException;
import com.catun.dentist.app.web.rest.request.AssignDoctorRequest;
import com.catun.dentist.app.web.rest.request.BirthdayToDayRequest;
import com.catun.dentist.app.web.rest.request.ConfirmBeforeRequest;
import com.catun.dentist.app.web.rest.request.EditCalendarBookingRequest;
import com.catun.dentist.app.web.rest.request.ExpenditureGetRequest;
import com.catun.dentist.app.web.rest.request.ExpenditureRequest;
import com.catun.dentist.app.web.rest.request.GetAssignDoctorRequest;
import com.catun.dentist.app.web.rest.request.ManagePatientRequest;
import com.catun.dentist.app.web.rest.response.AssignDoctorResponse;
import com.catun.dentist.app.web.rest.response.BirthdayToDayResponse;
import com.catun.dentist.app.web.rest.response.BookingTodayResponse;
import com.catun.dentist.app.web.rest.response.DoctorInfoResponse;
import com.catun.dentist.app.web.rest.response.ExpenditureResponse;
import com.catun.dentist.app.web.rest.response.ManagePatientsResponse;
import com.catun.dentist.app.web.rest.response.PatientIntervalTimeTreatmentResponse;

import java.text.ParseException;
import java.util.List;

public interface DisplayService {

    List<BookingTodayResponse> getConfirmBefore(ConfirmBeforeRequest confirmBeforeRequest) throws InvalidParameterException;

    List<BookingTodayResponse> getConfirmToday(ConfirmBeforeRequest confirmBeforeRequest) throws InvalidParameterException;

    List<PatientIntervalTimeTreatmentResponse> getPatientWithIntervalTimeTreatmented();
    List<BirthdayToDayResponse> getBirthday(BirthdayToDayRequest birthdayToDayRequest);

    List<AssignDoctorResponse> getAssignDoctorAppointments(GetAssignDoctorRequest getAssignDoctorRequest) throws AppointmentNotFoundException, ParseDateException, ParameterGetAssignDoctorInvalidException;

    void assignDoctorAppointments(AssignDoctorRequest assignDoctorRequest) throws AppointmentNotFoundException, ParseDateException, DoctorNotFoundException;

    List<DoctorInfoResponse> getDoctors();

    ManagePatientsResponse getPatientPaging(ManagePatientRequest managePatientRequest);

    void createExpenditure(ExpenditureRequest expenditureRequest) throws UserNotFoundException, ParseException, ExpenditureEnumException, PaymentInvalidMoneyException;

    ExpenditureResponse getExpenditure(ExpenditureGetRequest expenditureGetRequest) throws ExpenditureNotFoundException, ParseException, ExpenditureEnumException, ConvertMoneyFortmatException;

    void deleteExpenditure(long id);

    void editBooking(EditCalendarBookingRequest request) throws AppointmentNotFoundException, ParseDateException, NumberPatientOneHourException;
}
