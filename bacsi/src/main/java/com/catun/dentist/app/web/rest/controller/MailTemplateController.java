package com.catun.dentist.app.web.rest.controller;

import com.catun.dentist.app.exception.PlaceHolderSettingNotFoundException;
import com.catun.dentist.app.service.MailTeamplateService;
import com.catun.dentist.app.web.rest.request.MailTemplateRequest;
import com.catun.dentist.app.web.rest.response.DetailCommonResponse;
import com.catun.dentist.app.web.rest.response.MasterMailTemplateResponse;
import com.codahale.metrics.annotation.Timed;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin")
public class MailTemplateController {

    private final Logger log = LoggerFactory.getLogger(MailTemplateController.class);
    private MailTeamplateService mailTeamplateService;

    public MailTemplateController(MailTeamplateService mailTeamplateService) {
        this.mailTeamplateService = mailTeamplateService;
    }

    @GetMapping("/mail-template/masters")
    @Timed
    public ResponseEntity<MasterMailTemplateResponse> getMasterMailTemplate()
        throws PlaceHolderSettingNotFoundException {
        MasterMailTemplateResponse masterMailTemplateResponse = mailTeamplateService
            .getMasterMailTemplate();
        return new ResponseEntity<>(masterMailTemplateResponse, HttpStatus.CREATED);
    }

    @PostMapping("/mail-template")
    @Timed
    public ResponseEntity<DetailCommonResponse> createMailTemplate(
        @Valid @RequestBody MailTemplateRequest mailTemplateRequest) {
        log.debug("REST request to save MailTemplate : {}", mailTemplateRequest);
        mailTeamplateService.createMailTemplate(mailTemplateRequest);
        return new ResponseEntity<>(new DetailCommonResponse("201", "Success"), HttpStatus.CREATED);
    }
}
