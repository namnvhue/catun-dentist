package com.catun.dentist.app.service.impl;

import com.catun.dentist.app.exception.SendGridEmailException;
import com.catun.dentist.app.service.SendGridEmailService;
import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SendGridEmailServiceImpl implements SendGridEmailService {

    private final Logger log = LoggerFactory.getLogger(SendGridEmailServiceImpl.class);

    @Value("${catun.sendgrid.email.from}")
    private String fromEmail;

    @Value("${catun.sendgrid.email.key}")
    private String emailKeyApi;

    @Value("${catun.sendgrid.email.cc}")
    private String ccEmail;

    @Override
    public String sendGridEmail(String subject, String toEmail, String contentEmail)
        throws SendGridEmailException {
        log.info("Send GridEmail from {} , to {} , subject {}",contentEmail,toEmail,subject);
        Email from = new Email(fromEmail);
        Email to = new Email(toEmail);
        Email cc = new Email(ccEmail);
        Content content = new Content("text/html", contentEmail);
        Mail mail = new Mail(from, subject, to, content);
        mail.getPersonalization().get(0).addCc(cc);

        SendGrid sg = new SendGrid(emailKeyApi);
        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sg.api(request);
            log.info("SendGrid Mail Status :" + String.valueOf(response.getStatusCode()));
            log.info("SendGrid Mail Body :" + response.getBody());
            log.info("SendGrid Mail Header :" + String.valueOf(response.getHeaders()));
        } catch (IOException e) {
            log.error("Error SendGrid Email :" + e.getMessage());
            throw new SendGridEmailException("Error SendGrid Email");
        }
        return "Success";
    }

}
