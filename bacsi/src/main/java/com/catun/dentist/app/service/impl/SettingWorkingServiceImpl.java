package com.catun.dentist.app.service.impl;

import com.catun.dentist.app.constant.ClinicSettings;
import com.catun.dentist.app.domain.GlobalSetting;
import com.catun.dentist.app.entity.DateOffEntity;
import com.catun.dentist.app.entity.WorkingRuleEntity;
import com.catun.dentist.app.exception.DateOffInvalidException;
import com.catun.dentist.app.exception.GlobalSettingNotFoundException;
import com.catun.dentist.app.repository.DateOffRepository;
import com.catun.dentist.app.repository.GlobalSettingRepository;
import com.catun.dentist.app.repository.WorkingRuleRepository;
import com.catun.dentist.app.service.SettingWorkingService;
import com.catun.dentist.app.web.rest.request.DateOffRequest;
import com.catun.dentist.app.web.rest.request.SettingTimeWorkingRequest;
import com.catun.dentist.app.web.rest.response.SettingTimeWorkingResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class SettingWorkingServiceImpl implements SettingWorkingService {

    private final Logger log = LoggerFactory.getLogger(SettingWorkingServiceImpl.class);
    private DateOffRepository dateOffRepository;
    private WorkingRuleRepository workingRuleRepository;
    private GlobalSettingRepository globalSettingRepository;

    public SettingWorkingServiceImpl(DateOffRepository dateOffRepository,
        WorkingRuleRepository workingRuleRepository,GlobalSettingRepository globalSettingRepository) {
        this.dateOffRepository = dateOffRepository;
        this.workingRuleRepository = workingRuleRepository;
        this.globalSettingRepository = globalSettingRepository;
    }

    @Override
    public void settingTimeWorking(SettingTimeWorkingRequest settingTimeWorkingRequest)
        throws DateOffInvalidException {
        log.info("Start setting time working");
        List<DateOffRequest> dateOffs = settingTimeWorkingRequest.getDateOffs();
        List<DateOffRequest> holidateOffs = settingTimeWorkingRequest.getHolidayOffs();
        List<DateOffRequest> dateOffRequests = new ArrayList<>();
        dateOffRequests.addAll(dateOffs);
        dateOffRequests.addAll(holidateOffs);
        settingDateOff(dateOffRequests, dateOffs.size());
        settingWorkingRule(settingTimeWorkingRequest.getTimeWorking(),
            settingTimeWorkingRequest.getNumberPatientOneHour(),
            settingTimeWorkingRequest.getNameDateOff());
        log.info("End setting time working");
    }

    @Override
    public SettingTimeWorkingResponse getTimeWorking() throws GlobalSettingNotFoundException {
        log.info("Start Get time working");
        SettingTimeWorkingResponse settingTimeWorkingResponse = new SettingTimeWorkingResponse();
        List<DateOffEntity> dateOffEntities = dateOffRepository.findAll();
        List<DateOffRequest> dateOffRequests = new ArrayList<>();
        List<DateOffRequest> holidayOffRequests = new ArrayList<>();
        for (DateOffEntity dateOffEntity : dateOffEntities) {
            DateOffRequest dateOffRequest = new DateOffRequest();
            dateOffRequest.setDateOff(dateOffEntity.getDateOff());
            dateOffRequest.setReason(dateOffEntity.getReason());
            if (dateOffEntity.isHoliday()) {
                holidayOffRequests.add(dateOffRequest);
            } else {
                dateOffRequests.add(dateOffRequest);
            }
        }
        settingTimeWorkingResponse.setDateOffs(dateOffRequests);
        settingTimeWorkingResponse.setHolidayOffs(holidayOffRequests);
        List<WorkingRuleEntity> workingRuleEntitys = workingRuleRepository.findAll();
        if (!workingRuleEntitys.isEmpty()) {
            WorkingRuleEntity workingRuleEntity = workingRuleEntitys.get(0);
            settingTimeWorkingResponse.setTimeWorking(
                workingRuleEntity.getTimeWorkingFrom() + "," + workingRuleEntity
                    .getTimeWorkingTo());
            settingTimeWorkingResponse.setNameDateOff(workingRuleEntity.getNameDateOff());
            settingTimeWorkingResponse
                .setNumberPatientOneHour(workingRuleEntity.getNumberPatientOneHour());
        }

        List<Integer> integers = getNumberPatientSelected();
        settingTimeWorkingResponse.setNumberPatientSelected(integers);
        log.info("End time working");
        return settingTimeWorkingResponse;
    }

    private void settingDateOff(List<DateOffRequest> dateOffRequests, int dateOffNormalSize)
        throws DateOffInvalidException {
        log.info("Start settingDateOff with dateOffRequest size {} , dateOffNormalSize {}",dateOffRequests.size(),dateOffNormalSize);
        List<DateOffEntity> dateOffEntitys = new ArrayList<>();
        List<String> list = new ArrayList<>();
        for (DateOffRequest dateOffRequest : dateOffRequests) {
            dateOffNormalSize--;
            String dateOff = dateOffRequest.getDateOff();
            if (list.contains(dateOff)) {
                log.error("Param DateOff Invalid :");
                throw new DateOffInvalidException("Param DateOff Invalid :");
            }
            list.add(dateOff);
            try {
                new SimpleDateFormat("dd/MM/yyyy").parse(dateOff);
            } catch (ParseException e) {
                String errorMessage = e.getMessage();
                log.error("Param DateOff Invalid :" + errorMessage);
                throw new DateOffInvalidException(errorMessage);
            }
            DateOffEntity dateOffEntity = dateOffRepository
                .findByDateOff(dateOffRequest.getDateOff());
            if (dateOffEntity == null) {
                dateOffEntity = new DateOffEntity();
            }
            dateOffEntity.setDateOff(dateOff);
            dateOffEntity.setReason(dateOffRequest.getReason());
            if (dateOffNormalSize + 1 == 0) {
                dateOffNormalSize = 0;
                dateOffEntity.setHoliday(true);
            }
            dateOffEntitys.add(dateOffEntity);
        }
        dateOffRepository.deleteAll();
        dateOffRepository.save(dateOffEntitys);
        log.info("End settingDateOff ");
    }

    private void settingWorkingRule(String timeWorking, String numberPatientOneHour,
        String nameDateOff) {
        log.info("Start settingWorkingRule with timeWorking {} , numberPatientOneHour {} , nameDateOff {} ",timeWorking,numberPatientOneHour,nameDateOff);
        String[] timeWorkings = timeWorking.split(",");
        List<WorkingRuleEntity> workingRuleEntities = workingRuleRepository.findAll();
        WorkingRuleEntity workingRuleEntity;
        if (workingRuleEntities.isEmpty()) {
            workingRuleEntity = new WorkingRuleEntity();
        } else {
            workingRuleEntity = workingRuleEntities.get(0);
        }
        workingRuleEntity.setTimeWorkingFrom(timeWorkings[0]);
        workingRuleEntity.setTimeWorkingTo(timeWorkings[1]);
        workingRuleEntity.setNumberPatientOneHour(numberPatientOneHour);
        workingRuleEntity.setNameDateOff(nameDateOff);
        log.info("End settingWorkingRule ");
        workingRuleRepository.save(workingRuleEntity);
    }

    private List<Integer> getNumberPatientSelected() throws GlobalSettingNotFoundException {
        GlobalSetting globalSetting = globalSettingRepository.findByGroupAndName(ClinicSettings.groups.get(0),ClinicSettings.nameSettingClinics.get(2));
        if(globalSetting == null){
            log.error("Get GlobalSetting number patient in one hours failed");
            throw new GlobalSettingNotFoundException("Get GlobalSetting not found");
        }
        int numberPatientSelected = Integer.parseInt(globalSetting.getValue());
        List<Integer> integers = new ArrayList<>();
        for(int i = 1; i <= numberPatientSelected; i++){
            integers.add(i);
        }
        return integers;
    }
}
