package com.catun.dentist.app.enums;

import com.catun.dentist.app.exception.NotificationTypeException;

public enum NotificationType {

    REGISTER("REGISTER", 0),
    EDIT("EDIT", 1),
    FINISHED("FINISHED", 2);

    private String name;
    private int value;

    NotificationType(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public static NotificationType fromValue(int value) throws NotificationTypeException {
        for (NotificationType notificationType : NotificationType.values()) {
            if (notificationType.getValue() == value) {
                return notificationType;
            }
        }
        throw new NotificationTypeException("Can't find Expenditure with value {}");
    }

    public static NotificationType fromName(String name) throws NotificationTypeException {
        for (NotificationType notificationType : NotificationType.values()) {
            if (notificationType.getName().equals(name)) {
                return notificationType;
            }
        }
        throw new NotificationTypeException("Can't find Expenditure with name {}");
    }
}
