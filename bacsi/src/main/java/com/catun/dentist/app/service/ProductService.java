package com.catun.dentist.app.service;

import com.catun.dentist.app.entity.ProductEntity;
import com.catun.dentist.app.exception.ProductEntityExistException;
import com.catun.dentist.app.web.rest.request.ProductRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductService {

    ProductEntity createServiceCategory(ProductRequest productRequest)
        throws ProductEntityExistException;

    ProductEntity updateServiceCategory(ProductRequest productRequest);

    Page<ProductEntity> findAll(Pageable pageable);

    ProductEntity getDetailServiceCategory(long id);

    void deleteServiceCategory(long id);
}
