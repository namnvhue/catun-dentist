package com.catun.dentist.app.repository;

import com.catun.dentist.app.entity.MedicalHistoryEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

/**
 * Spring Data JPA repository for the MedicalHistoryEntity entity.
 */
public interface MedicalHistoryRepository extends JpaRepository<MedicalHistoryEntity, Long> {

    List<MedicalHistoryEntity> findAllByTimeTreatmentBetweenOrTimeTreatmentBetweenOrTimeTreatmentBetweenOrTimeTreatmentBetweenOrTimeTreatmentBetweenOrTimeTreatmentBetweenOrderByTimeTreatmentDesc(Date oneDayStart,Date oneDayEnd,Date threeDayStart,Date threeDayEnd,Date oneMonthStart,Date oneMonthEnd,Date threeMonthStart,Date threeMonthEnd,Date sixMonthStart,Date sixMonthEnd,Date oneYearStart,Date oneYearEnd);

    List<MedicalHistoryEntity> findAllByUserId(long userId);
    void deleteAllByUserId(long userId);
    MedicalHistoryEntity findTopByUserIdOrderByTimeTreatmentAsc(long userId);
    MedicalHistoryEntity findTopByUserIdOrderByTimeTreatmentDesc(long userId);

    MedicalHistoryEntity findByTreatmentDetailId(String treatmentDetailId);

}
