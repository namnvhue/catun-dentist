package com.catun.dentist.app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "warehouse_detail")
public class WareHouseDetailEntity extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "warehouse_id")
    private long wareHouseId;


    @Column(name = "user_id")
    private long userId;

    @NotNull
    @Column(name = "user_name", nullable = false)
    private String userName;

    @Column(name = "type")
    private int type;

    @Column(name = "time_i_o_put")
    private Date timeIOput;

    @Column(name = "amount_i_o_put")
    private int amountIOput;

    @Column(name = "amount")
    private int amount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getWareHouseId() {
        return wareHouseId;
    }

    public void setWareHouseId(long wareHouseId) {
        this.wareHouseId = wareHouseId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Date getTimeIOput() {
        return timeIOput;
    }

    public void setTimeIOput(Date timeIOput) {
        this.timeIOput = timeIOput;
    }

    public int getAmountIOput() {
        return amountIOput;
    }

    public void setAmountIOput(int amountIOput) {
        this.amountIOput = amountIOput;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
