package com.catun.dentist.app.exception;

public class ConvertMoneyFortmatException extends Exception{

    public ConvertMoneyFortmatException(String message) {
        super(message);
    }
}
