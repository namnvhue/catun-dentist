package com.catun.dentist.app.exception;

public class WaHouseExistException extends Exception {

    public WaHouseExistException(String message) {
        super(message);
    }

}
