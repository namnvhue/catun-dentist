package com.catun.dentist.app.exception;

public class MedicalHistoryUploadImagesException extends Exception{

    public MedicalHistoryUploadImagesException(String message) {
        super(message);
    }
}
