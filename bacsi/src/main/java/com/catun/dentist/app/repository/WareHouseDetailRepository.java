package com.catun.dentist.app.repository;

import com.catun.dentist.app.entity.WareHouseDetailEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface WareHouseDetailRepository extends JpaRepository<WareHouseDetailEntity, Long> {

    List<WareHouseDetailEntity> findAllByWareHouseIdOrderByTimeIOputDesc(long wareHouseId);

    List<WareHouseDetailEntity> findAllByWareHouseId(long id);

    List<WareHouseDetailEntity> findAllByTimeIOputBetweenOrderByWareHouseIdAsc(Date fromDate,Date toDate);

    WareHouseDetailEntity findTopByWareHouseIdAndTimeIOputBeforeOrderByLastModifiedDateDesc(long wareHouseId,Date timeIOput);

    List<WareHouseDetailEntity> findAllByWareHouseIdAndTimeIOputAfter(long wareHouseId,Date fromDate);

}
