package com.catun.dentist.app.repository;

import com.catun.dentist.app.entity.TreatmentItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Spring Data JPA repository for the TreatmentItemEntity entity.
 */
public interface TreatmentItemRepository extends JpaRepository<TreatmentItemEntity, Long> {

    TreatmentItemEntity findByServiceIdAndTreatmentDetailEntityId(String serviceId,
        long treatmentDetailId);

    void deleteByServiceIdAndTreatmentDetailEntityId(String serviceId, long treatmentDetailId);

    void deleteByTreatmentDetailEntityId(long treatmentDetailId);
}
