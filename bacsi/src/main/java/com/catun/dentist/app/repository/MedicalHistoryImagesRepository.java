package com.catun.dentist.app.repository;

import com.catun.dentist.app.entity.MedicalHistoryImagesEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MedicalHistoryImagesRepository extends JpaRepository<MedicalHistoryImagesEntity, Long> {

    List<MedicalHistoryImagesEntity> findAllByMedicalHistoryEntityId(long id);

    void deleteAllByMedicalHistoryEntityId(long id);

}
