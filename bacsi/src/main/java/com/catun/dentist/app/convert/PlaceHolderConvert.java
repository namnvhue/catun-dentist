package com.catun.dentist.app.convert;

public class PlaceHolderConvert {

    private String room;
    private String docter;
    private String receptionist;
    private String booking;
    private String patient;
    private String bookingStatus;
    private String subject;
    private String content;

    public PlaceHolderConvert(String room, String docter, String receptionist, String booking,
        String patient, String bookingStatus, String subject, String content) {
        this.room = room;
        this.docter = docter;
        this.receptionist = receptionist;
        this.booking = booking;
        this.patient = patient;
        this.bookingStatus = bookingStatus;
        this.subject = subject;
        this.content = content;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getDocter() {
        return docter;
    }

    public void setDocter(String docter) {
        this.docter = docter;
    }

    public String getReceptionist() {
        return receptionist;
    }

    public void setReceptionist(String receptionist) {
        this.receptionist = receptionist;
    }

    public String getBooking() {
        return booking;
    }

    public void setBooking(String booking) {
        this.booking = booking;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
