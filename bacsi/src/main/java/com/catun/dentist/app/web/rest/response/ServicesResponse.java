package com.catun.dentist.app.web.rest.response;

import com.catun.dentist.app.entity.ProductEntity;
import com.catun.dentist.app.entity.ServiceCategoryEntity;
import java.util.List;

public class ServicesResponse {

    private List<ProductEntity> products;
    private List<ServiceCategoryEntity> categoryServices;

    public ServicesResponse(List<ProductEntity> productEntityList,
        List<ServiceCategoryEntity> serviceCategoryEntities) {
        this.products = productEntityList;
        this.categoryServices = serviceCategoryEntities;
    }

    public List<ProductEntity> getProducts() {
        return products;
    }

    public void setProducts(List<ProductEntity> products) {
        this.products = products;
    }

    public List<ServiceCategoryEntity> getCategoryServices() {
        return categoryServices;
    }

    public void setCategoryServices(List<ServiceCategoryEntity> categoryServices) {
        this.categoryServices = categoryServices;
    }
}
