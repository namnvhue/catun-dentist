package com.catun.dentist.app.web.rest.response;

public class CalendarbookingResponse {

    private String code;

    public CalendarbookingResponse() {
        super();
    }

    ;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
