package com.catun.dentist.app.web.rest.request;


import java.util.List;

public class TreatmentServicesRequest {

    private String id;
    private String name;
    private float price;
    private List<String> toothNames;
    private int amounts;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public List<String> getToothNames() {
        return toothNames;
    }

    public void setToothNames(List<String> toothNames) {
        this.toothNames = toothNames;
    }

    public int getAmounts() {
        return amounts;
    }

    public void setAmounts(int amounts) {
        this.amounts = amounts;
    }
}
