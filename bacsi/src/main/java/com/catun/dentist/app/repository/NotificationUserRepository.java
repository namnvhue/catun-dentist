package com.catun.dentist.app.repository;

import com.catun.dentist.app.entity.NotificationUserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NotificationUserRepository extends JpaRepository<NotificationUserEntity, Long> {

    List<NotificationUserEntity> findTop20ByUserIdOrderByFinishViewFlgAsc(long userId);

    List<NotificationUserEntity> findTop100ByUserIdOrderByFinishViewFlgAsc(long userId);

    List<NotificationUserEntity> findAllByUserIdAndNotificationId(long userId,long notificationId);

    List<NotificationUserEntity> findAllByUserId(long userId);
}
