package com.catun.dentist.app.web.rest.request;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

public class ExpenditureRequest {

    private long treatmentDetailId;

    @NotNull
    @NotEmpty
    private String type;

    @NotNull
    @NotEmpty
    private String name;
    private String value;

    @NotNull
    private String createDate;

    public long getTreatmentDetailId() {
        return treatmentDetailId;
    }

    public void setTreatmentDetailId(long treatmentDetailId) {
        this.treatmentDetailId = treatmentDetailId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
}
