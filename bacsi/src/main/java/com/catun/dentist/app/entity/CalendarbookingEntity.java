package com.catun.dentist.app.entity;


import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "calendarbooking")
public class CalendarbookingEntity extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JoinColumn(name = "full_name")
    private String fullName;

    @Column(name = "birthday")
    private String birthDay;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UserEntity userEntity;

    @JoinColumn(name = "related_services")
    private String relatedServices;

    @JoinColumn(name = "orther_request")
    private String ortherRequest;

    @Column(name = "medical_history_description")
    @Lob
    private String medicalHistoryDescription;

    @Column(name = "status_current")
    @Lob
    private String statusCurrent;

    @Column(name = "appointment_time_from")
    private Date appointmentTimeFrom;

    @Column(name = "appointment_time_to")
    private Date appointmentTimeTo;

    @Column(name = "email")
    private String email;

    @Column(name = "code")
    private String code;

    @Column(name = "booked_by")
    private String bookingBy;

    @Column(name = "booked_for")
    private String bookingFor;

    @Column(name = "doctor_id")
    private String doctorId;

    @Column(name = "doctor_name")
    private String doctorName;

    @Column(name = "url_confirm")
    private String urlConfirm;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    public String getMedicalHistoryDescription() {
        return medicalHistoryDescription;
    }

    public void setMedicalHistoryDescription(String medicalHistoryDescription) {
        this.medicalHistoryDescription = medicalHistoryDescription;
    }

    public String getStatusCurrent() {
        return statusCurrent;
    }

    public void setStatusCurrent(String statusCurrent) {
        this.statusCurrent = statusCurrent;
    }

    public Date getAppointmentTimeFrom() {
        return appointmentTimeFrom;
    }

    public void setAppointmentTimeFrom(Date appointmentTimeFrom) {
        this.appointmentTimeFrom = appointmentTimeFrom;
    }

    public Date getAppointmentTimeTo() {
        return appointmentTimeTo;
    }

    public void setAppointmentTimeTo(Date appointmentTimeTo) {
        this.appointmentTimeTo = appointmentTimeTo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBookingBy() {
        return bookingBy;
    }

    public void setBookingBy(String bookingBy) {
        this.bookingBy = bookingBy;
    }

    public String getBookingFor() {
        return bookingFor;
    }

    public void setBookingFor(String bookingFor) {
        this.bookingFor = bookingFor;
    }


    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getRelatedServices() {
        return relatedServices;
    }

    public void setRelatedServices(String relatedServices) {
        this.relatedServices = relatedServices;
    }

    public String getOrtherRequest() {
        return ortherRequest;
    }

    public void setOrtherRequest(String ortherRequest) {
        this.ortherRequest = ortherRequest;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getUrlConfirm() {
        return urlConfirm;
    }

    public void setUrlConfirm(String urlConfirm) {
        this.urlConfirm = urlConfirm;
    }
}
