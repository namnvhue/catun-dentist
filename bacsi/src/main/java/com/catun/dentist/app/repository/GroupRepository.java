package com.catun.dentist.app.repository;

import com.catun.dentist.app.entity.GroupEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupRepository extends JpaRepository<GroupEntity, Long> {

}
