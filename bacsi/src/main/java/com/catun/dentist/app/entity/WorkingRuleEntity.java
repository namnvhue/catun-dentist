package com.catun.dentist.app.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "working_rule")
public class WorkingRuleEntity extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "time_working_from", length = 10)
    private String timeWorkingFrom;

    @Column(name = "time_working_to", length = 10)
    private String timeWorkingTo;

    @Column(name = "number_patient_one_hour", length = 2)
    private String numberPatientOneHour;

    @Column(name = "name_date_off", length = 255)
    private String nameDateOff;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTimeWorkingFrom() {
        return timeWorkingFrom;
    }

    public void setTimeWorkingFrom(String timeWorkingFrom) {
        this.timeWorkingFrom = timeWorkingFrom;
    }

    public String getTimeWorkingTo() {
        return timeWorkingTo;
    }

    public void setTimeWorkingTo(String timeWorkingTo) {
        this.timeWorkingTo = timeWorkingTo;
    }

    public String getNumberPatientOneHour() {
        return numberPatientOneHour;
    }

    public void setNumberPatientOneHour(String numberPatientOneHour) {
        this.numberPatientOneHour = numberPatientOneHour;
    }

    public String getNameDateOff() {
        return nameDateOff;
    }

    public void setNameDateOff(String nameDateOff) {
        this.nameDateOff = nameDateOff;
    }
}
