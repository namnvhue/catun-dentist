package com.catun.dentist.app.service.impl;

import com.catun.dentist.app.entity.NotificationEntity;
import com.catun.dentist.app.entity.NotificationUserEntity;
import com.catun.dentist.app.entity.UserEntity;
import com.catun.dentist.app.enums.NotificationType;
import com.catun.dentist.app.exception.NotificationTypeException;
import com.catun.dentist.app.repository.NotificationRepository;
import com.catun.dentist.app.repository.NotificationUserRepository;
import com.catun.dentist.app.repository.UserRepository;
import com.catun.dentist.app.security.AuthoritiesConstants;
import com.catun.dentist.app.security.SecurityUtils;
import com.catun.dentist.app.service.PushNotificationService;
import com.catun.dentist.app.web.rest.response.NotificationDetailResponse;
import com.catun.dentist.app.web.rest.response.NotificationResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class PushNotificationServiceImpl implements PushNotificationService{

    private final Logger log = LoggerFactory.getLogger(PushNotificationServiceImpl.class);

    private NotificationRepository notificationRepository;
    private NotificationUserRepository notificationUserRepository;
    private UserRepository userRepository;

    public PushNotificationServiceImpl(NotificationRepository notificationRepository,
                                       NotificationUserRepository notificationUserRepository,
                                       UserRepository userRepository){
        this.notificationRepository = notificationRepository;
        this.notificationUserRepository = notificationUserRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void pushNofitication(long userId,NotificationType notificationType,String content,String linkConfirm) {
        log.info("Start push notification with userId {} , notificationType {} , content {} , linkConfirm {}",userId,notificationType.getName(),content,linkConfirm);
        NotificationEntity notification = new NotificationEntity();
        notification.setCreateDate(new Date());
        notification.setCreateById(userId);
        notification.setContent(content);
        notification.setEventType(notificationType.getValue());
        notification.setLink_confirm(linkConfirm);
        notificationRepository.save(notification);
        long notificationId = notification.getId();
        List<UserEntity> userClinics = userRepository.findAllByAuthoritiesNameOrAuthoritiesNameAndIdNot(AuthoritiesConstants.RECEPTIONIST,AuthoritiesConstants.DOCTOR,userId);
        if(!userClinics.isEmpty()){
            log.info("Push notification for user of clinic size {}",userClinics.size());
            List<NotificationUserEntity> notificationUsers = new ArrayList<>();
            for(UserEntity user : userClinics){
                NotificationUserEntity notificationUser = new NotificationUserEntity();
                notificationUser.setNotificationId(notificationId);
                notificationUser.setUserId(user.getId());
                notificationUser.setFinishViewFlg(false);
                notificationUsers.add(notificationUser);
            }
            notificationUserRepository.save(notificationUsers);
            log.info("Finish push notification");
        }
    }

    private List<NotificationUserEntity> getNotifications(long clinicUserId ,String notificationNumber){
        if("20".equals(notificationNumber)){
            return notificationUserRepository.findTop20ByUserIdOrderByFinishViewFlgAsc(clinicUserId);
        }
        return notificationUserRepository.findTop100ByUserIdOrderByFinishViewFlgAsc(clinicUserId);
    }

    @Override
    public NotificationResponse getPushNotification(String notificationNumber) throws NotificationTypeException {
        UserEntity clinicUsers = SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).get();
        if(clinicUsers != null){
            long clinicUserId = clinicUsers.getId();
            List<NotificationUserEntity> notificationUsers = getNotifications(clinicUserId,notificationNumber);
            if(!notificationUsers.isEmpty()){
                List<Long> notificationIds = notificationUsers.stream().map(NotificationUserEntity::getNotificationId).collect(Collectors.toList());
                List<NotificationEntity> notifications = notificationRepository.findAllByIdIn(notificationIds);
                if(!notifications.isEmpty()){
                    Map<Long,Boolean> notificationInfos = notificationUsers.stream().collect(Collectors.toMap(x -> x.getNotificationId() , x-> x.isFinishViewFlg()));
                    NotificationResponse response = new NotificationResponse();
                    List<NotificationDetailResponse> news = new ArrayList<>();
                    List<NotificationDetailResponse> olds = new ArrayList<>();
                    for(NotificationEntity notification : notifications){
                        long notificationId = notification.getId();
                        boolean isViewed = notificationInfos.get(notificationId);
                        NotificationDetailResponse nt = new NotificationDetailResponse();
                        nt.setNotificationId(notification.getId());
                        nt.setUserId(clinicUserId);
                        nt.setEventType(NotificationType.fromValue(notification.getEventType()).getName());
                        nt.setContent(notification.getContent());
                        nt.setLinkConfirm(notification.getLink_confirm());
                        nt.setIntervalTime(caculartorMinutes(notification.getCreateDate()));
                        if(isViewed){
                            olds.add(nt);
                        }else{
                            news.add(nt);
                        }
                    }
                    response.setNews(news);
                    response.setOlds(olds);
                    return response;
                }
            }
        }
        return new NotificationResponse();
    }

    private List<NotificationUserEntity> getNotification(long userId,long notificationId){
        if(0 == notificationId){
            return notificationUserRepository.findAllByUserId(userId);
        }
        return notificationUserRepository.findAllByUserIdAndNotificationId(userId,notificationId);
    }

    @Override
    public void viewed(long notificationId) {
        UserEntity clinicUsers = SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).get();
        long userId = clinicUsers.getId();
        log.info("Start viewed userId {}, notificationId {}",userId,notificationId);
        List<NotificationUserEntity> notificationUsers = getNotification(userId,notificationId);
        if(!notificationUsers.isEmpty()){
            for(NotificationUserEntity notificationUser : notificationUsers){
                notificationUser.setFinishViewFlg(true);
                log.info("End viewed notification Success");
            }
            notificationUserRepository.save(notificationUsers);
        }
    }

    private String caculartorMinutes(Date createDate){
        long diff = Math.abs(new Date().getTime() - createDate.getTime());
        long diffDays = diff / (24 * 60 * 60 * 1000);
        if(diffDays >= 1){
            return diffDays +" days ago" ;
        }
        long diffHours = diff / (60 * 60 * 1000) % 24;
        if(diffHours >= 1){
            return diffHours +" hours ago" ;
        }
        long diffMinutes = diff / (60 * 1000) % 60;
        if(diffMinutes >= 1){
            return diffMinutes +" minutes ago" ;
        }
        long diffSeconds = diff / 1000 % 60;
        return diffSeconds + " seconds ago";
    }
}
