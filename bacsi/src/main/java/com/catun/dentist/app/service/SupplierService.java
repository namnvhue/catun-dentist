package com.catun.dentist.app.service;

import com.catun.dentist.app.entity.SupplierEntity;
import com.catun.dentist.app.exception.SupplierNotFoundException;
import com.catun.dentist.app.web.rest.request.SupplierRequest;

import java.util.List;

public interface SupplierService {

    List<SupplierEntity> getSupplier() throws SupplierNotFoundException;

    void createSupplier(SupplierRequest request);

    void editSupplier(SupplierRequest request) throws SupplierNotFoundException;

    void deleteSupplier(long id);
}
