package com.catun.dentist.app.service;

import com.catun.dentist.app.exception.GlobalSettingExistException;
import com.catun.dentist.app.service.dto.GlobalSettingDTO;
import com.catun.dentist.app.web.rest.response.GlobalSettingGroupResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Service Interface for managing GlobalSetting.
 */
public interface GlobalSettingService {

    /**
     * Save a globalSetting.
     *
     * @param globalSettingDTO the entity to save
     * @return the persisted entity
     */
    GlobalSettingDTO save(GlobalSettingDTO globalSettingDTO) throws GlobalSettingExistException;

    GlobalSettingDTO update(GlobalSettingDTO globalSettingDTO);

    /**
     * Get all the globalSettings.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<GlobalSettingDTO> findAll(Pageable pageable);

    /**
     * Get the "id" globalSetting.
     *
     * @param id the id of the entity
     * @return the entity
     */
    GlobalSettingDTO findOne(Long id);

    /**
     * Delete the "id" globalSetting.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    List<GlobalSettingGroupResponse> getGlobalSettingInfo();
}
