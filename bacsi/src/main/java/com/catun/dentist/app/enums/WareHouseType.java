package com.catun.dentist.app.enums;

import com.catun.dentist.app.exception.WareHouseEnumException;

public enum WareHouseType {

    XUAT("XUAT", 0),
    NHAP("NHAP", 1);

    private String name;
    private int value;

    WareHouseType(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public static WareHouseType fromValue(int value) throws WareHouseEnumException {
        for (WareHouseType wareHouseType : WareHouseType.values()) {
            if (wareHouseType.getValue() == value) {
                return wareHouseType;
            }
        }
        throw new WareHouseEnumException("Can't find Expenditure with value {}");
    }

    public static WareHouseType fromName(String name) throws WareHouseEnumException {
        for (WareHouseType wareHouseType : WareHouseType.values()) {
            if (wareHouseType.getName().equals(name)) {
                return wareHouseType;
            }
        }
        throw new WareHouseEnumException("Can't find Expenditure with name {}");
    }
}
