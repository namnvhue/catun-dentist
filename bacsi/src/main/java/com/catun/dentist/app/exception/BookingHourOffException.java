package com.catun.dentist.app.exception;

public class BookingHourOffException extends Exception {

    public BookingHourOffException(String message) {
        super(message);
    }
}
