package com.catun.dentist.app.service.impl;

import com.catun.dentist.app.entity.UserEntity;
import com.catun.dentist.app.entity.WareHouseDetailEntity;
import com.catun.dentist.app.entity.WareHouseEntity;
import com.catun.dentist.app.enums.WareHouseType;
import com.catun.dentist.app.exception.*;
import com.catun.dentist.app.repository.UserRepository;
import com.catun.dentist.app.repository.WareHouseDetailRepository;
import com.catun.dentist.app.repository.WarehouseRepository;
import com.catun.dentist.app.security.SecurityUtils;
import com.catun.dentist.app.service.WareHouseDetailService;
import com.catun.dentist.app.web.rest.request.WareHouseDetailRequest;
import com.catun.dentist.app.web.rest.response.WareHouseDetailResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class WareHouseDetailServiceImpl implements WareHouseDetailService {

    private final Logger log = LoggerFactory.getLogger(WareHouseDetailServiceImpl.class);
    private WareHouseDetailRepository wareHouseDetailRepository;
    private UserRepository userRepository;
    private WarehouseRepository warehouseRepository;

    public WareHouseDetailServiceImpl(WareHouseDetailRepository wareHouseDetailRepository,UserRepository userRepository,WarehouseRepository warehouseRepository){
        this.wareHouseDetailRepository = wareHouseDetailRepository;
        this.userRepository = userRepository;
        this.warehouseRepository = warehouseRepository;
    }

    @Override
    public List<WareHouseDetailResponse> getWareHouseDetail(long wareHouseId) throws WareHouseEnumException {
        log.info("Start get WarehouseDetail by warehouseId {}",wareHouseId);
        List<WareHouseDetailEntity> wareHouseDetails = wareHouseDetailRepository.findAllByWareHouseIdOrderByTimeIOputDesc(wareHouseId);
        log.info("Get WarehouseDetails with size {}",wareHouseDetails.size());
        List<WareHouseDetailResponse> responses = new ArrayList<>();
        for(WareHouseDetailEntity wareHouseDetail : wareHouseDetails){
            WareHouseDetailResponse response = new WareHouseDetailResponse();
            response.setId(wareHouseDetail.getId());
            response.setUserId(wareHouseDetail.getUserId());
            response.setUserName(wareHouseDetail.getUserName());
            response.setAmount(wareHouseDetail.getAmount());
            response.setAmountIOput(wareHouseDetail.getAmountIOput());
            response.setWareHouseId(wareHouseDetail.getWareHouseId());
            String type = WareHouseType.fromValue(wareHouseDetail.getType()).getName();
            response.setType(type);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
            response.setTimeIOput(simpleDateFormat.format(wareHouseDetail.getTimeIOput()));
            responses.add(response);

        }
        return responses;
    }

    @Override
    public void createWareHouseDetail(WareHouseDetailRequest request) throws UserNotFoundException, WareHouseNotFoundException, ParseException, WareHouseAmountInvalidException {
        log.info("Start create WareHouseDetail with type {} , amount {}",request.getType(),request.getAmountIOput());
        UserEntity reception = SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).get();
        long userId = reception.getId();
        String userName = reception.getFullName();
        log.info("Start create WareHouseDetail userId {} , userName {} ",userId,userName);
        if(reception == null){
            log.error("Reception not found with userId {} ",userId);
            throw new UserNotFoundException("User not found");
        }
        long wareHouseId = request.getWarehouseId();
        WareHouseEntity wareHouseEntity = warehouseRepository.findOne(wareHouseId);
        if(wareHouseEntity == null){
            log.error("WareHouse not found with warehouseId {}",wareHouseId);
            throw new WareHouseNotFoundException("WareHouse not found");
        }
        WareHouseDetailEntity wareHouseDetail = new WareHouseDetailEntity();
        wareHouseDetail.setWareHouseId(wareHouseId);
        WareHouseType wareHouseType = WareHouseType.valueOf(request.getType());
        wareHouseDetail.setType(wareHouseType.getValue());
        wareHouseDetail.setUserId(userId);
        wareHouseDetail.setUserName(userName);
        int amountIOput = request.getAmountIOput();
        wareHouseDetail.setAmountIOput(amountIOput);
        int amount = wareHouseEntity.getAmount();
        if(wareHouseType.getName().equals(WareHouseType.NHAP.getName())){
            amount += amountIOput;
        }else if(amount >= amountIOput){
            amount -= amountIOput;
            int amountUsed = wareHouseEntity.getAmountUsed();
            amountUsed += amountIOput;
            wareHouseEntity.setAmountUsed(amountUsed);
        }else{
            log.error("Can't Output amount than amount in warehouse");
            throw new WareHouseAmountInvalidException("");
        }
        wareHouseDetail.setAmount(amount);
        Date inputDate = new SimpleDateFormat("dd/MM/yyyy").parse(request.getTimeIOput());
        wareHouseDetail.setTimeIOput(inputDate);
        wareHouseDetailRepository.save(wareHouseDetail);
        wareHouseEntity.setAmount(amount);
        warehouseRepository.save(wareHouseEntity);
        log.info("End create WareHouseDetail Success");
    }

    @Override
    public void deleteWareHouseDetail(long wareHouseDetailId) throws WareHouseDetailNotFoundException, WareHouseEnumException {
        log.info("Start delete WareHouseDetail with warehouseDetailId {}",wareHouseDetailId);
        WareHouseDetailEntity wareHouseDetail = wareHouseDetailRepository.findOne(wareHouseDetailId);
        if(wareHouseDetail == null){
            log.error("WarehouseDetail not found");
            throw new WareHouseDetailNotFoundException("");
        }
        long wareHouseId = wareHouseDetail.getWareHouseId();
        WareHouseEntity wareHouse = warehouseRepository.findOne(wareHouseId);
        if(wareHouse == null){
            log.error("Warehouse not found");
            throw new WareHouseDetailNotFoundException("");
        }
        String type = WareHouseType.fromValue(wareHouseDetail.getType()).getName();
        int amountIOput = wareHouseDetail.getAmountIOput();
        if(type.equals(WareHouseType.NHAP.getName())){
            int amount = wareHouse.getAmount() - amountIOput;
            wareHouse.setAmount(amount);
        }else{
            int amount = wareHouse.getAmount() + amountIOput;
            int amountUsed = wareHouse.getAmountUsed() - amountIOput;
            wareHouse.setAmount(amount);
            wareHouse.setAmountUsed(amountUsed);
        }
        warehouseRepository.save(wareHouse);
        wareHouseDetailRepository.delete(wareHouseDetail);
        log.info("End delete WareHouseDetail Success");
    }
}
