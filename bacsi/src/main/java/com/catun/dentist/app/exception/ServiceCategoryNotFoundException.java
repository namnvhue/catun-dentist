package com.catun.dentist.app.exception;

public class ServiceCategoryNotFoundException extends Exception {

    public ServiceCategoryNotFoundException(String message) {
        super(message);
    }
}
