package com.catun.dentist.app.service.impl;

import com.catun.dentist.app.constant.ClinicSettings;
import com.catun.dentist.app.convert.PlaceHolderConvert;
import com.catun.dentist.app.domain.GlobalSetting;
import com.catun.dentist.app.entity.AuthorityEntity;
import com.catun.dentist.app.entity.CalendarbookingEntity;
import com.catun.dentist.app.entity.GroupEntity;
import com.catun.dentist.app.entity.MailTemplateEntity;
import com.catun.dentist.app.entity.MedicalHistoryEntity;
import com.catun.dentist.app.entity.MedicalHistoryImagesEntity;
import com.catun.dentist.app.entity.ServiceEntity;
import com.catun.dentist.app.entity.TreatmentDetailEntity;
import com.catun.dentist.app.entity.TreatmentEntity;
import com.catun.dentist.app.entity.UserEntity;
import com.catun.dentist.app.enums.MailTemplate;
import com.catun.dentist.app.enums.NotificationType;
import com.catun.dentist.app.exception.DoctorNotFoundException;
import com.catun.dentist.app.exception.GlobalSettingNotFoundException;
import com.catun.dentist.app.exception.MedicalHistoryNotFoundException;
import com.catun.dentist.app.exception.MedicalHistoryUploadImagesException;
import com.catun.dentist.app.exception.ParseDateException;
import com.catun.dentist.app.exception.SendGridEmailException;
import com.catun.dentist.app.exception.ServiceEntityNotFoundException;
import com.catun.dentist.app.exception.TreatmentExistException;
import com.catun.dentist.app.exception.TreatmentNotFoundException;
import com.catun.dentist.app.exception.UserNotFoundException;
import com.catun.dentist.app.repository.AuthorityRepository;
import com.catun.dentist.app.repository.CalendarbookingRepository;
import com.catun.dentist.app.repository.GlobalSettingRepository;
import com.catun.dentist.app.repository.GroupRepository;
import com.catun.dentist.app.repository.MailTemplateRepository;
import com.catun.dentist.app.repository.MedicalHistoryRepository;
import com.catun.dentist.app.repository.MedicalHistoryImagesRepository;
import com.catun.dentist.app.repository.ServiceRepository;
import com.catun.dentist.app.repository.TreatmentDetailRepository;
import com.catun.dentist.app.repository.TreatmentRepository;
import com.catun.dentist.app.repository.UserRepository;
import com.catun.dentist.app.security.AuthoritiesConstants;
import com.catun.dentist.app.security.SecurityUtils;
import com.catun.dentist.app.service.MedicalHistoryService;
import com.catun.dentist.app.service.PushNotificationService;
import com.catun.dentist.app.service.SendGridEmailService;
import com.catun.dentist.app.service.util.PlaceHolderUtil;
import com.catun.dentist.app.service.util.RandomCodeUtil;
import com.catun.dentist.app.web.rest.request.BookingServiceRequest;
import com.catun.dentist.app.web.rest.request.CalendarbookingRequest;
import com.catun.dentist.app.web.rest.request.MedicalHistoryAddNewRequest;
import com.catun.dentist.app.web.rest.request.MedicalHistoryPatientRequest;
import com.catun.dentist.app.web.rest.response.MedicalHistoryDetailResponse;
import com.catun.dentist.app.web.rest.response.PagingResponse;
import com.catun.dentist.app.web.rest.response.PatiensDetailResponse;
import com.catun.dentist.app.web.rest.response.TreatmentResponse;
import com.catun.dentist.app.web.rest.response.TreatmentHistoryResponse;
import com.catun.dentist.app.web.rest.response.TreatmentStatusRequest;
import com.catun.dentist.app.web.rest.util.ParseDateUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Service
public class MedicalHistoryServiceImpl implements MedicalHistoryService {

    private final Logger log = LoggerFactory.getLogger(MedicalHistoryServiceImpl.class);
    private final int NUMBER_RECORD_IN_PAGES = 20;
    private static final String MEDICALHISTORY_FOLDER = "/medicalhistorys/";
    private UserRepository userRepository;
    private MedicalHistoryRepository medicalHistoryRepository;
    private MedicalHistoryImagesRepository medicalHistoryImagesRepository;
    private MailTemplateRepository mailTemplateRepository;
    private SendGridEmailService sendGridEmailService;
    private GlobalSettingRepository globalSettingRepository;
    private TreatmentRepository treatmentRepository;
    private AuthorityRepository authorityRepository;
    private PasswordEncoder passwordEncoder;
    private GroupRepository groupRepository;
    private ServiceRepository serviceRepository;
    private TreatmentDetailRepository treatmentDetailRepository;
    private CalendarbookingRepository calendarbookingRepository;
    private PushNotificationService pushNotificationService;

    @Value("${catun.directory.path.server}")
    private String directoryPathServer;

    @Value("${catun.sendgrid.format.newuser.subject}")
    private String newuserSubjectEmail;

    @Value("${catun.sendgrid.format.newuser.content}")
    private String newuserContentEmail;

    public MedicalHistoryServiceImpl(UserRepository userRepository,
           MedicalHistoryRepository medicalHistoryRepository,
           MedicalHistoryImagesRepository medicalHistoryImagesRepository,
           MailTemplateRepository mailTemplateRepository,
           SendGridEmailService sendGridEmailService,
           GlobalSettingRepository globalSettingRepository,
           TreatmentRepository treatmentRepository,
           AuthorityRepository authorityRepository,
           PasswordEncoder passwordEncoder,
           GroupRepository groupRepository,
           ServiceRepository serviceRepository,
           TreatmentDetailRepository treatmentDetailRepository,
           CalendarbookingRepository calendarbookingRepository,
           PushNotificationService pushNotificationService){
        this.userRepository = userRepository;
        this.medicalHistoryRepository = medicalHistoryRepository;
        this.medicalHistoryImagesRepository = medicalHistoryImagesRepository;
        this.mailTemplateRepository = mailTemplateRepository;
        this.sendGridEmailService = sendGridEmailService;
        this.globalSettingRepository = globalSettingRepository;
        this.treatmentRepository = treatmentRepository;
        this.authorityRepository = authorityRepository;
        this.passwordEncoder = passwordEncoder;
        this.groupRepository = groupRepository;
        this.serviceRepository = serviceRepository;
        this.treatmentDetailRepository = treatmentDetailRepository;
        this.calendarbookingRepository = calendarbookingRepository;
        this.pushNotificationService = pushNotificationService;
    }

  private Page<TreatmentEntity> getTreatmentPagings(
      Pageable pageable, String searchCondition, boolean finishFlag) {
        log.info("Start get treatmentPaging with finishFlag {}",finishFlag);
    if (finishFlag) {
      if (StringUtils.isEmpty(searchCondition)) {
        return treatmentRepository.findAll(pageable);
      } else {
        return treatmentRepository
            .findAllByFinishedFalseAndFullNameContainingOrPhoneContainingOrEmailContainingOrBirthdayContaining(
                pageable, searchCondition, searchCondition, searchCondition, searchCondition);
      }
    } else {
      if (StringUtils.isEmpty(searchCondition)) {
        return treatmentRepository.findAllByFinishedFalse(pageable);
      } else {
        return treatmentRepository
            .findAllByFullNameContainingOrPhoneContainingOrEmailContainingOrBirthdayContaining(
                pageable, searchCondition, searchCondition, searchCondition, searchCondition);
      }
    }
    }

    @Override
    public TreatmentHistoryResponse getTreatmentPaging(MedicalHistoryPatientRequest medicalHistoryPatientRequest,boolean finishFlag) {
        log.info("Start get PatientsPaging with finishFlag {}",finishFlag);
        int page = medicalHistoryPatientRequest.getPage();
        Sort.Direction direction = getSortCondition(medicalHistoryPatientRequest.getSort());
        Pageable pageable  = new PageRequest(page - 1,NUMBER_RECORD_IN_PAGES, direction,"fullName");
        String searchCondition = medicalHistoryPatientRequest.getSearch();
        Page<TreatmentEntity> treatmentPagings = getTreatmentPagings(pageable,searchCondition,finishFlag);
        List<TreatmentEntity> treatments = treatmentPagings.getContent();
        log.info("Number treatments {}",treatments.size());
        List<TreatmentResponse> treatmentResponses = setInfoPatients(treatments);
        PagingResponse pagingResponse = createPageResponse(page,treatmentPagings.getTotalPages());
        TreatmentHistoryResponse treatmentHistoryResponse = new TreatmentHistoryResponse();
        treatmentHistoryResponse.setTreatments(treatmentResponses);
        treatmentHistoryResponse.setPageable(pagingResponse);
        log.info("Finish get PatientsPaging");
        return treatmentHistoryResponse;
    }

    @Override
    public TreatmentResponse getTreatmentByBookingId(long bookingId) throws TreatmentNotFoundException {
        log.info("Start get treatment by bookingId {}",bookingId);
        TreatmentEntity treatmentEntity = treatmentRepository.findByBookingId(bookingId);
        if(treatmentEntity == null){
            CalendarbookingEntity booking = calendarbookingRepository.findById(bookingId);
            if(booking == null){
                log.error("Treatment not found with booking Id {}",bookingId);
                throw new TreatmentNotFoundException("Treatment not found");
            }
            String patientId = String.valueOf(booking.getUserEntity().getId());
            treatmentEntity = treatmentRepository.findByPatientId(patientId);
            return createTreatmentResponse(treatmentEntity);
        }
        return createTreatmentResponse(treatmentEntity);
    }

    @Override
    public PatiensDetailResponse getPatientInfo(long id) throws UserNotFoundException {
        log.info("Start get Treatment-PatientInfo by patientId {}",id);
        UserEntity patient = userRepository.findByAuthoritiesNameAndId(AuthoritiesConstants.USER,id);
        if(patient == null){
            log.error("User not found with patientId {}",id);
            throw new UserNotFoundException("User not found");
        }
        PatiensDetailResponse response = new PatiensDetailResponse();
        response.setId(String.valueOf(patient.getId()));
        response.setName(patient.getFullName());
        response.setAddress(patient.getAddress());
        response.setBirthDay(patient.getBirthday());
        response.setEmail(patient.getEmail());
        response.setPhone(patient.getPhone());
        return response;
    }

    @Override
    @Transactional
    public void createMedicalHistory(MedicalHistoryAddNewRequest request) throws  MedicalHistoryUploadImagesException, ParseDateException, ParseException, SendGridEmailException, GlobalSettingNotFoundException, DoctorNotFoundException {
        log.info("Start create MedicalHistory");
        UserEntity doctor = SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).get();
        if(doctor == null){
            log.error("Can't find any Doctor is login");
            throw new DoctorNotFoundException("Can't find any Doctor is login");
        }
        log.info("Start create MedicalHistory with doctorName {}",doctor.getFullName());
        String treatmentDetailId = request.getTreatmentDetailId();
        String timeIn = request.getTimeIn();
        long userId = request.getUserId();
        log.info("Start upload MedicalHistory images for TreatmentDetailId {} ,timeIn {}",treatmentDetailId,timeIn);
        List<String> imageNames = uploadImages(request.getFiles(), String.valueOf(userId),timeIn);
        saveMedicalHistory(doctor.getFullName(),request,imageNames);
        log.info("Sendmail for patient then treatment ,End create MedicalHistory");
        UserEntity patient = userRepository.findOne(Long.valueOf(userId));
        if(patient != null){
            sendMail(doctor.getFullName(),patient);
        }
    }

    private List<String> getImagesContentBase64(List<MedicalHistoryImagesEntity> historyImages,String timeIn) throws ParseException, IOException {
        if(historyImages.isEmpty()){
            return null;
        }
        log.info("Start get  MedicalHistory image content base64 with size {}",historyImages.size());
        String userId = String.valueOf(historyImages.get(0).getUserId());
        String date = changeFormatTimeIn(timeIn);
        List<String> contentImageBase64s = new ArrayList<>();
        for(MedicalHistoryImagesEntity historyImage : historyImages){
            String historyPath = directoryPathServer + userId +MEDICALHISTORY_FOLDER+ date+"/"+historyImage.getImageName();
            Path path = Paths.get(historyPath);
            Resource resource = new UrlResource(path.toUri());
            File file = resource.getFile();
            int length = (int) file.length();
            BufferedInputStream reader = new BufferedInputStream(new FileInputStream(file));
            byte[] bytes = new byte[length];
            reader.read(bytes, 0, length);
            reader.close();
            String content = java.util.Base64.getEncoder().encodeToString(bytes);
            contentImageBase64s.add(content);
        }
        log.info("Ennd get  MedicalHistory image content base64");
        return contentImageBase64s;
    }

    @Override
    public MedicalHistoryDetailResponse getMedicalHistoryDetail(String treatmentDetailId) throws MedicalHistoryNotFoundException, ParseDateException {
        try {
            log.info("Start get medicalHistoryDetail with treatmentDetailId {}",treatmentDetailId);
            MedicalHistoryEntity history = getMedicalHistory(treatmentDetailId);
            List<MedicalHistoryImagesEntity> historyImages = medicalHistoryImagesRepository.findAllByMedicalHistoryEntityId(history.getId());
            List<String> contentImages = getImagesContentBase64(historyImages,history.getTimeIn());
            log.info("Number images finded {}",historyImages.size());
            MedicalHistoryDetailResponse response = new MedicalHistoryDetailResponse();
            response.setMedicalHistory(history);
            response.setImages(contentImages);
            return response;
        } catch (ParseException e) {
            log.error("Parse medical history failed , TimeIn invalid");
            throw new ParseDateException("Parse date failed");
        } catch (IOException e) {
            log.error("Parse medical history failed , TimeIn invalid");
            throw new ParseDateException("Parse date failed");
        }
    }

    @Override
    @Transactional
    public void deleteMedicalHistory(String treatmentDetailId) throws ParseException, MedicalHistoryNotFoundException {
        log.info("Start delete TreatmentDetail with id {} ",treatmentDetailId);
        MedicalHistoryEntity medicalHistoryEntity = medicalHistoryRepository.findByTreatmentDetailId(treatmentDetailId);
        if(medicalHistoryEntity != null){
            long medicalHistoryId = medicalHistoryEntity.getId();
            List<MedicalHistoryImagesEntity> historyImagess = medicalHistoryImagesRepository.findAllByMedicalHistoryEntityId(medicalHistoryId);
            if(!historyImagess.isEmpty()){
                String path = generateDirectoryFolder(String.valueOf(medicalHistoryEntity.getUserId()),medicalHistoryEntity.getTimeIn());
                CompletableFuture.runAsync(()->{
                    try{
                        deleteImages(historyImagess,path);
                    }catch (Exception e){
                        log.error("Delete medicalhistory images failed with id {}",medicalHistoryId);
                    }
                });
                medicalHistoryImagesRepository.deleteAllByMedicalHistoryEntityId(medicalHistoryId);
            }
            medicalHistoryRepository.delete(medicalHistoryId);
            log.info("Delete medicalhistory with id {} ",medicalHistoryId);
        }
        treatmentDetailRepository.delete(Long.valueOf(treatmentDetailId));
        log.info("End delete treatmentdetail with id {} ",treatmentDetailId);
    }

    @Override
    public void changeTreatmentStatus(TreatmentStatusRequest treatmentStatusRequest) throws TreatmentNotFoundException {
        long treatmentId = Long.parseLong(treatmentStatusRequest.getTreatmentId());
        log.info("Start change treatment status with treatmentId {}",treatmentId);
        TreatmentEntity treatmentEntity = getTreatent(treatmentId);
        if(treatmentStatusRequest.isFinishFlag()){
            treatmentEntity.setFinished(true);
            treatmentEntity.setTreatmentTimeTo(new Date());
            UserEntity doctor = SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).get();
            if(doctor != null){
                pushNotificationService.pushNofitication(doctor.getId(), NotificationType.FINISHED,doctor.getFullName() +" hoàn thành điều trị cho bệnh nhân "+treatmentEntity.getFullName(),"");
            }
        }else{
            treatmentEntity.setFinished(false);
            treatmentEntity.setTreatmentTimeTo(null);
        }
        treatmentRepository.save(treatmentEntity);
        log.info("End change treatment status Success");
    }

    @Override
    public void createTreatment(CalendarbookingRequest treatmentRequest) throws SendGridEmailException, ParseException, ParseDateException, TreatmentExistException, ServiceEntityNotFoundException {
        log.info("Start create treatment without booking");
        UserEntity user = registerUser(treatmentRequest);
        String appointmentDateJson = treatmentRequest.getDateVal();
        String appointmentHourJson = treatmentRequest.getTimeVal();
        Date appointmentDate = ParseDateUtil
            .parseJsonToDate(appointmentDateJson, appointmentHourJson);
        String patientId = String.valueOf(user.getId());
        createTreatment(patientId,treatmentRequest,appointmentDate);
    }

    @Override
    public void deleteTreatment(long treatmentId) throws TreatmentNotFoundException, ParseException, MedicalHistoryNotFoundException {
        log.info("Start delete Treatment with treatmentId {}",treatmentId);
        TreatmentEntity treatment = treatmentRepository.findOne(treatmentId);
        if(treatment == null){
            log.error("Treatment not exist with treatmentId {} , can't delete",treatmentId);
            throw new TreatmentNotFoundException("Treatment not exist");
        }
        List<TreatmentDetailEntity> treatmentDetails = treatmentDetailRepository.findAllByTreatmentId(treatment.getId());
        if(!treatmentDetails.isEmpty()){
            for(TreatmentDetailEntity treatmentDetail : treatmentDetails){
                deleteMedicalHistory(String.valueOf(treatmentDetail.getId()));
            }
        }
        treatmentRepository.delete(treatment);
        log.info("Delete treatment Success");
    }

    private UserEntity registerUser(CalendarbookingRequest treatmentRequest) throws SendGridEmailException, ParseException {
        log.info("Register new User");
        Optional<UserEntity> optionalUser = userRepository
            .findOneByLogin(treatmentRequest.getPhoneVal());
        String code = RandomCodeUtil.ramdomCode(5);
        if (optionalUser.isPresent()) {
            return optionalUser.get();
        } else {
            optionalUser = userRepository
                .findOneByEmailIgnoreCase(treatmentRequest.getEmailVal());
            if (!optionalUser.isPresent()) {
                return registerNewUser(treatmentRequest, code);
            } else {
                return optionalUser.get();
            }
        }
    }

    private void createTreatment(String patientId,CalendarbookingRequest treatmentRequest,Date appointmentDate) throws ServiceEntityNotFoundException, TreatmentExistException {
        log.info("Start create treatment with patientId {}",patientId);
        TreatmentEntity treatment = treatmentRepository.findByPatientId(patientId);
        if(treatment == null){
            treatment = new TreatmentEntity();
            treatment.setBookingId(0L);
            treatment.setPatientId(patientId);
            treatment.setFullName(treatmentRequest.getNameVal());
            treatment.setBirthday(treatmentRequest.getDobVal());
            treatment.setPhone(treatmentRequest.getPhoneVal());
            treatment.setEmail(treatmentRequest.getEmailVal());
            String relatedServices = convertRequestToSerivces(treatmentRequest.getServices());
            treatment.setRelatedServices(relatedServices);
            treatment.setTreatmentTimeFrom(appointmentDate);
            treatment.setFinished(false);
            treatmentRepository.save(treatment);
            log.info("End create treatment Success");
        }else{
            log.error("Current treatment for patientId {} exist in system ,please seacrh it again",patientId);
            throw new TreatmentExistException("Treatment is exist in system");
        }
    }

    private String convertRequestToSerivces(List<BookingServiceRequest> bookingServiceRequests)
        throws ServiceEntityNotFoundException {
        log.info("Start convert calendarbooking services with size {}",bookingServiceRequests.size());
        StringBuilder relatedServices = new StringBuilder();
        for (BookingServiceRequest bookingServiceRequest : bookingServiceRequests) {
            ServiceEntity serviceEntity = serviceRepository
                .findOne(Long.valueOf(bookingServiceRequest.getId()));
            if (serviceEntity == null) {
                log.error("ServiceEntity not found when booking");
                throw new ServiceEntityNotFoundException("ServiceEntity not found when booking");
            }
            relatedServices.append(serviceEntity.getId());
            relatedServices.append(":");
            relatedServices.append(serviceEntity.getName());
            relatedServices.append(",");
        }
        log.info("End convert calendarbooking services");
        return relatedServices.toString();
    }


    private UserEntity registerNewUser(CalendarbookingRequest calendarbookingRequest,
                                       String defaultPassword) throws SendGridEmailException, ParseException {
        log.info("Calendarbooking register new user with defaultPassword {}",defaultPassword);
        UserEntity newUser = new UserEntity();
        AuthorityEntity authorityEntity = authorityRepository.findOne(AuthoritiesConstants.USER);
        Set<AuthorityEntity> authorities = new HashSet<>();
        String encryptedPassword = passwordEncoder.encode(defaultPassword);
        String numberPhone = calendarbookingRequest.getPhoneVal();
        newUser.setLogin(numberPhone);
        newUser.setPassword(encryptedPassword);
        newUser.setFullName(calendarbookingRequest.getNameVal());
        String email = calendarbookingRequest.getEmailVal();
        if (checkEmail(email)) {
            newUser.setEmail(email);
        }
        newUser.setPhone(numberPhone);
        newUser.setLangKey("vi");
        newUser.setBirthday(calendarbookingRequest.getDobVal());
        newUser.setActivated(true);
        authorities.add(authorityEntity);
        newUser.setAuthorities(authorities);
        GroupEntity groupEntity = groupRepository.findAll().get(0);
        newUser.setGroupEntity(groupEntity);
        userRepository.save(newUser);
        log.info("Created Information for User: {}", newUser);
        if (checkEmail(email)) {
            log.info("SendEmail when calendarbooking register new user");
            CompletableFuture.runAsync(() ->{
                try {
                    sendGridEmailService.sendGridEmail(newuserSubjectEmail, email,
                        newuserContentEmail + numberPhone + " - Password :" + defaultPassword);
                } catch (SendGridEmailException e) {
                    log.error("Send mail failed from email {} to newUserSubjectEmail {}",email,newuserContentEmail);

                }
            });

        }
        log.info("End sendEmail when calendarbooking register new user");
        return newUser;
    }

    private TreatmentEntity getTreatent(long treatmentId) throws TreatmentNotFoundException {
        log.info("Start get Treatment with treatmentId {}",treatmentId);
        TreatmentEntity treatmentEntity = treatmentRepository.findOne(treatmentId);
        if(treatmentEntity == null){
            log.error("Treatment not found with treatmentId {}",treatmentId);
            throw new TreatmentNotFoundException("Treatment not found");
        }
        return treatmentEntity;
    }

    private UserEntity checkPatient(long userId) throws UserNotFoundException {
        UserEntity patient = userRepository.findOne(Long.valueOf(userId));
        if(patient == null){
            log.error("User not found userId {}",userId);
            throw new UserNotFoundException("User not found");
        }
        return patient;
    }

    private void saveMedicalHistory(String doctorName,MedicalHistoryAddNewRequest request,List<String> imageNames) throws ParseException {
        log.info("Start save Medical History with imageNames size {}",imageNames.size());
        String treatmentDetailId = request.getTreatmentDetailId();
        long userId = request.getUserId();
        MedicalHistoryEntity medicalHistory = medicalHistoryRepository.findByTreatmentDetailId(treatmentDetailId);
        if(medicalHistory == null){
            medicalHistory = new MedicalHistoryEntity();
        }else{
            log.info("Delete info old before update medicalhistory");
            long medicalHistoryId = medicalHistory.getId();
            List<MedicalHistoryImagesEntity> historyImagess = medicalHistoryImagesRepository.findAllByMedicalHistoryEntityId(medicalHistoryId);
            String path = generateDirectoryFolder(String.valueOf(userId),request.getTimeIn());
            CompletableFuture.runAsync(()->{
                try{
                    deleteImages(historyImagess,path);
                }catch (Exception e){
                    log.error("Delete medicalhistory images failed with medicalHistoryId {}",medicalHistoryId);
                }
            });
            medicalHistoryImagesRepository.deleteAllByMedicalHistoryEntityId(medicalHistoryId);
        }
        medicalHistory.setTreatmentDetailId(treatmentDetailId);
        medicalHistory.setUserId(userId);
        medicalHistory.setDoctorName(doctorName);
        medicalHistory.setTimeIn(request.getTimeIn());
        String timeOut = request.getTimeOut();
        medicalHistory.setTimeOut(timeOut);
        medicalHistory.setTimeAmount(request.getTimeWorking());
        medicalHistory.setDescription(request.getNoted());
        Date treatmentDate = new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(timeOut);
        medicalHistory.setTimeTreatment(treatmentDate);
        medicalHistoryRepository.save(medicalHistory);
        for(String imageName : imageNames){
            MedicalHistoryImagesEntity medicalHistoryImagesEntity = new MedicalHistoryImagesEntity();
            medicalHistoryImagesEntity.setImageName(imageName);
            medicalHistoryImagesEntity.setUserId(String.valueOf(userId));
            medicalHistoryImagesEntity.setMedicalHistoryEntity(medicalHistory);
            medicalHistoryImagesRepository.save(medicalHistoryImagesEntity);
        }
        log.info(" save Medical History Success ");
    }

    private List<String> uploadImages(MultipartFile[] files,String userId,String timeIn) throws MedicalHistoryUploadImagesException, ParseDateException {
        log.info("Start Upload images with files size {} , userId {} ,timeIn {}",files.length,userId,timeIn);
        String fileNames = Arrays.stream(files).map(x -> x.getOriginalFilename()).filter(x -> !StringUtils.isEmpty(x)).collect(Collectors.joining(","));
        List<String> imageNames = new ArrayList<>();
        if(!StringUtils.isEmpty(fileNames)){
            List<MultipartFile> images = Arrays.asList(files);
            for(MultipartFile file : images){
                if(file.isEmpty()){
                    continue;
                }
                try {
                    byte[] bytes = file.getBytes();
                    String generatePath = generateDirectoryFolder(userId,timeIn);
                    new File(generatePath).mkdirs();
                    String nameQrCodeImage = RandomCodeUtil.ramdomCode(25)+".png";
                    imageNames.add(nameQrCodeImage);
                    Path path = Paths.get(generatePath + nameQrCodeImage);
                    Files.write(path,bytes);
                } catch (IOException e) {
                    String messageError = "Upload medical history images failed";
                    log.error(messageError + e.getMessage());
                    throw new MedicalHistoryUploadImagesException(messageError);
                } catch (ParseException e) {
                    log.error("Parse format failed TimeIn {}",timeIn);
                    throw new ParseDateException("Parse date failed");
                }
            }
        }
        log.info("End upload images Success");
        return imageNames;
    }

    private String changeFormatTimeIn(String timeIn) throws ParseException {
        String oldFormat = "dd/MM/yyyy hh:mm";
        String newFormat = "dd_MM_yyyy";
        SimpleDateFormat dateFormat = new SimpleDateFormat(oldFormat);
        Date date = dateFormat.parse(timeIn);
        dateFormat.applyPattern(newFormat);
        return dateFormat.format(date);
    }

    private String generateDirectoryFolder(String patientId,String timeIn) throws ParseException {
        log.info("Start generate directory folder with patientId {} , timeIn {}",patientId,timeIn);
        String date = changeFormatTimeIn(timeIn);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(directoryPathServer);
        stringBuilder.append(patientId);
        stringBuilder.append(MEDICALHISTORY_FOLDER);
        stringBuilder.append(date + "/");
        return stringBuilder.toString();
    }

    private Sort.Direction getSortCondition(String sort){
        if(sort.equals("asc")){
           return Sort.Direction.ASC;
        }
        return Sort.Direction.DESC;
    }

    private PagingResponse createPageResponse(int page,int totalPages){
        log.info("Create page response with page {} , totalPages {}",page,totalPages);
        String[] numberPages = getNumberPages(totalPages,page);
        PagingResponse pagingResponse = new PagingResponse();
        pagingResponse.setCurrentPage(page);
        pagingResponse.setTotalPage(totalPages);
        pagingResponse.setNumberPages(numberPages);
        return pagingResponse;
    }

    private String[] getNumberPages(int totalPage,int currentPage){
        log.info("Get numberPages with totalPage {} , currentPage {}",totalPage,currentPage);
        List<Integer> integers = Arrays.asList(-2,-1,0,1,2);
        List<String> numberPages = new ArrayList<>();
        for(int a :integers){
            int numberPage = currentPage + a;
            if(numberPage > 0 && numberPage <= totalPage){
                numberPages.add(String.valueOf(numberPage));
            }
        }
        return numberPages.toArray(new String[0]);
    }

    private List<TreatmentResponse> setInfoPatients(List<TreatmentEntity> treatments){
        log.info("Set infoPatient with treatments size {}",treatments.size());
        List<TreatmentResponse> treatmentResponses = new ArrayList<>();
        for(TreatmentEntity treatment : treatments){
            TreatmentResponse treatmentResponse = createTreatmentResponse(treatment);
            treatmentResponses.add(treatmentResponse);
        }
        log.info("Set infoPatient Success");
        return treatmentResponses;
    }

    private TreatmentResponse createTreatmentResponse(TreatmentEntity treatment){
        TreatmentResponse treatmentResponse = new TreatmentResponse();
        treatmentResponse.setId(String.valueOf(treatment.getId()));
        treatmentResponse.setPatientId(treatment.getPatientId());
        treatmentResponse.setName(treatment.getFullName());
        treatmentResponse.setBirthDay(treatment.getBirthday());
        treatmentResponse.setPhone(treatment.getPhone());
        treatmentResponse.setEmail(treatment.getEmail());
        treatmentResponse.setDoctorName(treatment.getDoctorName());
        treatmentResponse.setServices(treatment.getRelatedServices());
        treatmentResponse.setStatus(treatment.isFinished());
        Date treatmentFrom = treatment.getTreatmentTimeFrom();
        Date treatmentTo = treatment.getTreatmentTimeTo();
        if(treatmentFrom != null){
            String dateFortmat = ParseDateUtil.fortmatDate(treatmentFrom,ClinicSettings.FORTMAT_DATE_TYPE);
            treatmentResponse.setTreatmentTimeFrom(dateFortmat);
        }
        if(treatmentTo != null){
            String dateFortmat = ParseDateUtil.fortmatDate(treatmentTo,ClinicSettings.FORTMAT_DATE_TYPE);
            treatmentResponse.setTreatmentTimeTo(dateFortmat);
        }
        return treatmentResponse;
    }

    private MedicalHistoryEntity getMedicalHistory(String treatmentDetailId) throws MedicalHistoryNotFoundException {
        log.info("Start get MedicalHistory with treatmentDetailId {}",treatmentDetailId);
        MedicalHistoryEntity medicalHistoryEntity = medicalHistoryRepository.findByTreatmentDetailId(treatmentDetailId);
        if(medicalHistoryEntity == null){
            log.error("Medicalhistory is not found with id {}",treatmentDetailId);
            throw new MedicalHistoryNotFoundException("MedicalHistory not found");
        }
        return medicalHistoryEntity;
    }

    private void deleteImages(List<MedicalHistoryImagesEntity> historyImagess,String path){
        log.info("Start delete Images with images size {} ,path {}",historyImagess.size(),path);
        for(MedicalHistoryImagesEntity historyImages : historyImagess){
            File file=new File(path+historyImages.getImageName());
            if(file.exists()){
                file.delete();
            }
        }
    }


    private void sendMail(String doctorName,UserEntity user)
        throws SendGridEmailException, GlobalSettingNotFoundException {
        String email = user.getEmail();
        log.info("Start sendMail email {} , userId {}",email,user.getId());
        MailTemplateEntity mailTemplateEntity = mailTemplateRepository
            .findByMasterTemplate(MailTemplate.TREATMENT.toString());
        if (mailTemplateEntity == null) {
            CompletableFuture.runAsync(() ->{
                try {
                    sendGridEmailService.sendGridEmail("Phòng Khám Nha Khoa", email,
                        "Chân thành cảm ơn bạn đã tin dùng và khám tại phòng khám chúng tôi, hẹn gặp lại bạn trong những lần khám định kỳ tới");
                } catch (SendGridEmailException e) {
                    log.error("SendEmail then treatment failed email {} , error {}",email,e.getMessage());
                }
            });

        } else {
            String address = addressClinic(ClinicSettings.groups.get(0),ClinicSettings.nameSettingClinics.get(1));
            PlaceHolderConvert placeHolderConvert = new PlaceHolderConvert(address,
                doctorName, "", "", user.getLogin(), "",
                mailTemplateEntity.getSubject(), mailTemplateEntity.getContent());
            PlaceHolderUtil placeHolderUtil = PlaceHolderUtil.placeHolder(placeHolderConvert);
            if (checkEmail(email)) {
                String subject = placeHolderUtil.getSubject();
                String newContent = placeHolderUtil.getContent();
                CompletableFuture.runAsync(() ->{
                    try {
                        sendGridEmailService.sendGridEmail(subject, email, newContent);
                    } catch (SendGridEmailException e) {
                        log.error("SendEmail treatment with mailTeamplate subject subject {} , email {}",subject,email);
                    }
                });
            }
        }
        log.info("End sendMail");
    }

    private boolean checkEmail(String email) {
        if (email == null || email.equals("")) {
            return false;
        }
        return true;
    }

    private String addressClinic(String group,String name) throws GlobalSettingNotFoundException {
        GlobalSetting globalSetting = globalSettingRepository.findByGroupAndName(group,name);
        if(globalSetting == null){
            log.error("Get GlobalSetting not found with group {},name {}",group,name);
            throw new GlobalSettingNotFoundException("Get GlobalSetting not found");
        }
        return globalSetting.getValue();
    }

}
