package com.catun.dentist.app.exception;

public class NotificationTypeException extends Exception{

    public NotificationTypeException(String message) {
        super(message);
    }
}
