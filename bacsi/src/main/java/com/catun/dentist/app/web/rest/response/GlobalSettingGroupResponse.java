package com.catun.dentist.app.web.rest.response;

import java.util.List;

public class GlobalSettingGroupResponse {
    private String groupName;
    private List<String> names;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }
}
