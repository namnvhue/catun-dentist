package com.catun.dentist.app.exception;

public class WareHouseNotFoundException extends Exception{

    public WareHouseNotFoundException(String message) {
        super(message);
    }
}
