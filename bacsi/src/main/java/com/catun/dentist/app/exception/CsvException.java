package com.catun.dentist.app.exception;

public class CsvException extends Exception{

    public CsvException(String message) {
        super(message);
    }

}
