package com.catun.dentist.app.service.util;

import com.catun.dentist.app.entity.ProductEntity;
import com.catun.dentist.app.exception.ProductEntityNotFoundException;
import com.catun.dentist.app.repository.ProductRepository;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProductUtil {

    private static final Logger log = LoggerFactory.getLogger(ProductUtil.class);

    public static String getNameProduct(String relatedProducts,
        ProductRepository productRepository) throws ProductEntityNotFoundException {
        if(StringUtils.isEmpty(relatedProducts)){
            return "";
        }
        String[] productIds = relatedProducts.split(",");
        StringBuilder listProducts = new StringBuilder();
        int length = productIds.length;
        for (int i = 0; i < length; i++) {
            ProductEntity productEntity = productRepository
                .findOne(Long.valueOf(productIds[i]));
            if (productEntity == null) {
                log.error("Product not found Exception");
                return "";
            }
            if (i + 1 < length) {
                listProducts.append(i + 1 + ":" + productEntity.getName() + ",");
            } else if (i + 1 == length) {
                listProducts.append(i + 1 + ":" + productEntity.getName());
            }
        }
        return listProducts.toString();
    }
}
