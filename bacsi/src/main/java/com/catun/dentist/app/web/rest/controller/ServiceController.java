package com.catun.dentist.app.web.rest.controller;

import com.catun.dentist.app.entity.ServiceEntity;
import com.catun.dentist.app.exception.ProductEntityNotFoundException;
import com.catun.dentist.app.exception.ServiceCategoryNotFoundException;
import com.catun.dentist.app.exception.ServiceEntityExistException;
import com.catun.dentist.app.exception.ServiceEntityNotFoundException;
import com.catun.dentist.app.service.Services;
import com.catun.dentist.app.web.rest.request.ServiceRequest;
import com.catun.dentist.app.web.rest.response.DetailCommonResponse;
import com.catun.dentist.app.web.rest.response.DetailServiceResponse;
import com.catun.dentist.app.web.rest.response.ServicesResponse;
import com.catun.dentist.app.web.rest.util.HeaderUtil;
import com.catun.dentist.app.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/admin")
public class ServiceController {

    private static final String ENTITY_NAME = "serviceEntity";
    private final Logger log = LoggerFactory.getLogger(ServiceController.class);
    private Services services;

    public ServiceController(Services services) {
        this.services = services;
    }


    @GetMapping("/service-entities/services")
    @Timed
    public ResponseEntity<ServicesResponse> getProducts(Pageable pageable)
        throws ServiceCategoryNotFoundException {
        log.debug("REST request to get list Products of ServiceEntities");
        ServicesResponse productEntities = services.getServices();
        return new ResponseEntity<>(productEntities, HttpStatus.OK);
    }

    @PostMapping("/service-entities")
    @Timed
    public ResponseEntity<DetailCommonResponse> createServiceEntity(
        @Valid @RequestBody ServiceRequest serviceRequest)
        throws ServiceCategoryNotFoundException, ServiceEntityExistException, ProductEntityNotFoundException {
        log.debug("REST request to save ServiceEntity : {}", serviceRequest);
        services.createService(serviceRequest);
        return new ResponseEntity<>(new DetailCommonResponse("200", "Success"), HttpStatus.CREATED);
    }

    @PutMapping("/service-entities")
    @Timed
    public ResponseEntity<DetailCommonResponse> updateServiceEntity(
        @Valid @RequestBody ServiceRequest serviceRequest)
        throws ServiceCategoryNotFoundException, ServiceEntityExistException, ServiceEntityNotFoundException, ProductEntityNotFoundException {
        log.debug("REST request to update ServiceEntity : {}", serviceRequest);
        if (serviceRequest.getId() == null) {
            return createServiceEntity(serviceRequest);
        }
        services.updateService(serviceRequest);
        return new ResponseEntity<>(new DetailCommonResponse("200", "Success"), HttpStatus.OK);
    }

    @GetMapping("/service-entities")
    @Timed
    public ResponseEntity<List<ServiceEntity>> getAllServiceEntities(Pageable pageable)
        throws ProductEntityNotFoundException {
        log.debug("REST request to get a page of ServiceEntities");
        Page<ServiceEntity> page = services.findAll(pageable);
        HttpHeaders headers = PaginationUtil
            .generatePaginationHttpHeaders(page, "/api/service-entities");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/service-entities/{id}")
    @Timed
    public ResponseEntity<DetailServiceResponse> getServiceEntity(@PathVariable Long id)
        throws ServiceEntityNotFoundException, ServiceCategoryNotFoundException, ProductEntityNotFoundException {
        log.debug("REST request to get ServiceEntity : {}", id);
        DetailServiceResponse detailService = services.getDetail(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(detailService));
    }

    @DeleteMapping("/service-entities/{id}")
    @Timed
    public ResponseEntity<Void> deleteServiceEntity(@PathVariable Long id)
        throws ServiceEntityNotFoundException {
        log.debug("REST request to delete ServiceEntity : {}", id);
        services.delete(id);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
