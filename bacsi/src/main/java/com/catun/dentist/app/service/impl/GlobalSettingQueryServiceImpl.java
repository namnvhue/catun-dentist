package com.catun.dentist.app.service.impl;


import com.catun.dentist.app.domain.GlobalSetting;
import com.catun.dentist.app.domain.GlobalSetting_;
import com.catun.dentist.app.repository.GlobalSettingRepository;
import com.catun.dentist.app.service.GlobalSettingQueryService;
import com.catun.dentist.app.service.dto.GlobalSettingCriteria;
import com.catun.dentist.app.service.dto.GlobalSettingDTO;
import com.catun.dentist.app.service.mapper.GlobalSettingMapper;
import io.github.jhipster.service.QueryService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service for executing complex queries for GlobalSetting entities in the database. The main input
 * is a {@link GlobalSettingCriteria} which get's converted to {@link Specifications}, in a way that
 * all the filters must apply. It returns a {@link List} of {@link GlobalSettingDTO} or a {@link
 * Page} of {@link GlobalSettingDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class GlobalSettingQueryServiceImpl extends QueryService<GlobalSetting> implements GlobalSettingQueryService {

    private final Logger log = LoggerFactory.getLogger(GlobalSettingQueryServiceImpl.class);


    private final GlobalSettingRepository globalSettingRepository;

    private final GlobalSettingMapper globalSettingMapper;

    public GlobalSettingQueryServiceImpl(GlobalSettingRepository globalSettingRepository,
                                         GlobalSettingMapper globalSettingMapper) {
        this.globalSettingRepository = globalSettingRepository;
        this.globalSettingMapper = globalSettingMapper;
    }

    /**
     * Return a {@link List} of {@link GlobalSettingDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<GlobalSettingDTO> findByCriteria(GlobalSettingCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<GlobalSetting> specification = createSpecification(criteria);
        return globalSettingMapper.toDto(globalSettingRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link GlobalSettingDTO} which matches the criteria from the database
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<GlobalSettingDTO> findByCriteria(GlobalSettingCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<GlobalSetting> specification = createSpecification(criteria);
        final Page<GlobalSetting> result = globalSettingRepository.findAll(specification, page);
        return result.map(globalSettingMapper::toDto);
    }

    /**
     * Function to convert GlobalSettingCriteria to a {@link Specifications}
     */
    private Specifications<GlobalSetting> createSpecification(GlobalSettingCriteria criteria) {
        Specifications<GlobalSetting> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification
                    .and(buildSpecification(criteria.getId(), GlobalSetting_.id));
            }
            if (criteria.getGroup() != null) {
                specification = specification
                    .and(buildStringSpecification(criteria.getGroup(), GlobalSetting_.group));
            }
            if (criteria.getName() != null) {
                specification = specification
                    .and(buildStringSpecification(criteria.getName(), GlobalSetting_.name));
            }
            if (criteria.getValue() != null) {
                specification = specification
                    .and(buildStringSpecification(criteria.getValue(), GlobalSetting_.value));
            }
        }
        return specification;
    }

}
