package com.catun.dentist.app.exception;

public class GlobalSettingNotFoundException extends Exception{

    public GlobalSettingNotFoundException(String message) {
        super(message);
    }
}
