package com.catun.dentist.app.exception;

public class WareHouseAmountInvalidException extends Exception{

    public WareHouseAmountInvalidException(String message) {
        super(message);
    }

}
