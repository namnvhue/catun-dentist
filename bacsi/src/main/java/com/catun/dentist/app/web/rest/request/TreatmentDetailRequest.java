package com.catun.dentist.app.web.rest.request;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.List;

public class TreatmentDetailRequest {

    @NotEmpty
    private List<TreatmentServicesRequest> services;
    private float totalPrice;
    private int discount;
    private float finalPrice;
    @NotNull
    @NotEmpty
    private String patientId;
    @NotNull
    @NotEmpty
    private String treatmentId;
    private String notedFromDoctor;
    private String notedFromPatient;

    public List<TreatmentServicesRequest> getServices() {
        return services;
    }

    public void setServices(List<TreatmentServicesRequest> services) {
        this.services = services;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public float getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(float finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getTreatmentId() {
        return treatmentId;
    }

    public void setTreatmentId(String treatmentId) {
        this.treatmentId = treatmentId;
    }

    public String getNotedFromDoctor() {
        return notedFromDoctor;
    }

    public void setNotedFromDoctor(String notedFromDoctor) {
        this.notedFromDoctor = notedFromDoctor;
    }

    public String getNotedFromPatient() {
        return notedFromPatient;
    }

    public void setNotedFromPatient(String notedFromPatient) {
        this.notedFromPatient = notedFromPatient;
    }
}
