package com.catun.dentist.app.web.rest.response;

import java.util.List;

public class TreatmentDetailServiceResponse {

    private List<TreatmentCategoryServicesResponse> categoryServices;
    List<String> toothNames;
    List<Integer> amounts;

    public List<TreatmentCategoryServicesResponse> getCategoryServices() {
        return categoryServices;
    }

    public void setCategoryServices(List<TreatmentCategoryServicesResponse> categoryServices) {
        this.categoryServices = categoryServices;
    }

    public List<String> getToothNames() {
        return toothNames;
    }

    public void setToothNames(List<String> toothNames) {
        this.toothNames = toothNames;
    }

    public List<Integer> getAmounts() {
        return amounts;
    }

    public void setAmounts(List<Integer> amounts) {
        this.amounts = amounts;
    }
}
