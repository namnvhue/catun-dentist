package com.catun.dentist.app.exception;

public class ServiceEntityNotFoundException extends Exception {

    public ServiceEntityNotFoundException(String message) {
        super(message);
    }
}
