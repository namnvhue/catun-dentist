package com.catun.dentist.app.repository;

import com.catun.dentist.app.entity.CalendarbookingEntity;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CalendarbookingRepository extends JpaRepository<CalendarbookingEntity, String> {

    CalendarbookingEntity findOneByCode(String code);

    List<CalendarbookingEntity> findAllByAppointmentTimeFrom(Date appointmentDate);

    List<CalendarbookingEntity> findAllByAppointmentTimeFromBetweenOrderByAppointmentTimeFromAsc(Date startDate,Date endDate);

    void deleteAllByUserEntityId(long userId);

    CalendarbookingEntity findById(long id);
}
