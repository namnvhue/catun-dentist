package com.catun.dentist.app.exception;

public class ServiceCategoryEntityExistException extends Exception {

    public ServiceCategoryEntityExistException(String message) {
        super(message);
    }
}
