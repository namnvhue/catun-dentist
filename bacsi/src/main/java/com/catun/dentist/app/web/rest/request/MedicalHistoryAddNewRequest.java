package com.catun.dentist.app.web.rest.request;

import org.springframework.web.multipart.MultipartFile;


public class MedicalHistoryAddNewRequest {

    private String treatmentDetailId;
    private long userId;
    private String timeIn;
    private String timeOut;
    private int timeWorking;
    private MultipartFile[] files;
    private String noted;

    public String getTreatmentDetailId() {
        return treatmentDetailId;
    }

    public void setTreatmentDetailId(String treatmentDetailId) {
        this.treatmentDetailId = treatmentDetailId;
    }


    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(String timeIn) {
        this.timeIn = timeIn;
    }

    public String getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }

    public int getTimeWorking() {
        return timeWorking;
    }

    public void setTimeWorking(int timeWorking) {
        this.timeWorking = timeWorking;
    }

    public MultipartFile[] getFiles() {
        return files;
    }

    public void setFiles(MultipartFile[] files) {
        this.files = files;
    }

    public String getNoted() {
        return noted;
    }

    public void setNoted(String noted) {
        this.noted = noted;
    }
}
