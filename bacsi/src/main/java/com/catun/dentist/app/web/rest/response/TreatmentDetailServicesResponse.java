package com.catun.dentist.app.web.rest.response;

public class TreatmentDetailServicesResponse {
    private Long id;
    private String relatedProducts;
    private float servicePrice;
    private String toothNames;
    private int amounts;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRelatedProducts() {
        return relatedProducts;
    }

    public void setRelatedProducts(String relatedProducts) {
        this.relatedProducts = relatedProducts;
    }

    public float getServicePrice() {
        return servicePrice;
    }

    public void setServicePrice(float servicePrice) {
        this.servicePrice = servicePrice;
    }

    public String getToothNames() {
        return toothNames;
    }

    public void setToothNames(String toothNames) {
        this.toothNames = toothNames;
    }

    public int getAmounts() {
        return amounts;
    }

    public void setAmounts(int amounts) {
        this.amounts = amounts;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
