package com.catun.dentist.app.web.rest.response;

import com.catun.dentist.app.entity.MedicalHistoryEntity;

import java.util.List;

public class MedicalHistoryDetailResponse {

    private MedicalHistoryEntity medicalHistory;
    private List<String> images;

    public MedicalHistoryEntity getMedicalHistory() {
        return medicalHistory;
    }

    public void setMedicalHistory(MedicalHistoryEntity medicalHistory) {
        this.medicalHistory = medicalHistory;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }
}
