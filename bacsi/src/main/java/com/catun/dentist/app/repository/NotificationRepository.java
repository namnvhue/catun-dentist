package com.catun.dentist.app.repository;

import com.catun.dentist.app.entity.NotificationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NotificationRepository extends JpaRepository<NotificationEntity, Long> {

    List<NotificationEntity> findAllByIdIn(List<Long> ids);
}
