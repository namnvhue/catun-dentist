package com.catun.dentist.app.exception;

public class DateOffInvalidException extends Exception {

    public DateOffInvalidException(String message) {
        super(message);
    }
}
