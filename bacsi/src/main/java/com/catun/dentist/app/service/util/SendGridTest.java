package com.catun.dentist.app.service.util;

import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import java.io.IOException;

/**
 * Test sendgrid API
 */
public class SendGridTest {

    public static void main(String[] args) throws IOException {
        Email from = new Email("nam@inlumina.work");
        String subject = "Sending with SendGrid is Fun";
        Email to = new Email("namnvhue@gmail.com");
        Email cc = new Email("nhat@inlumina.work");
        Content content = new Content("text/plain", "and easy to do anywhere, even with Java");
        Mail mail = new Mail(from, subject, to, content);
        mail.getPersonalization().get(0).addCc(cc);

        SendGrid sg = new SendGrid(
            "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");
        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sg.api(request);
            System.out.println(response.getStatusCode());
            System.out.println(response.getBody());
            System.out.println(response.getHeaders());
        } catch (IOException ex) {
            throw ex;
        }
    }
}
