package com.catun.dentist.app.exception;

public class SendGridEmailException extends Exception {

    public SendGridEmailException(String message) {
        super(message);
    }
}
