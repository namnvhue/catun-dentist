package com.catun.dentist.app.exception;

public class GlobalSettingExistException extends Exception{

    public GlobalSettingExistException(String message) {
        super(message);
    }
}
