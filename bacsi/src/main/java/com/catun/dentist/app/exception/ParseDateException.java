package com.catun.dentist.app.exception;

public class ParseDateException extends Exception {

    public ParseDateException(String message) {
        super(message);
    }
}
