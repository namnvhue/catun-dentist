package com.catun.dentist.app.repository;

import com.catun.dentist.app.entity.MailTemplateEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MailTemplateRepository extends JpaRepository<MailTemplateEntity, Long> {

    MailTemplateEntity findByMasterTemplate(String masterTemplate);
}
