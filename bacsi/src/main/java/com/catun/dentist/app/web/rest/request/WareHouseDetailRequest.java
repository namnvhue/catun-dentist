package com.catun.dentist.app.web.rest.request;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

public class WareHouseDetailRequest {

    private long warehouseId;

    @NotNull
    @NotEmpty
    private String type;

    @NotNull
    @NotEmpty
    private String timeIOput;
    private int amountIOput;

    public long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(long warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getAmountIOput() {
        return amountIOput;
    }

    public void setAmountIOput(int amountIOput) {
        this.amountIOput = amountIOput;
    }

    public String getTimeIOput() {
        return timeIOput;
    }

    public void setTimeIOput(String timeIOput) {
        this.timeIOput = timeIOput;
    }
}
