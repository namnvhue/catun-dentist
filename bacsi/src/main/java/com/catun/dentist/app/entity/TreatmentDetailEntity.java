package com.catun.dentist.app.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * A TreatmentDetailEntity.
 */
@Entity
@Table(name = "treatment_detail")
public class TreatmentDetailEntity extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "treatment_id", nullable = false)
    private long treatmentId;

    @NotNull
    @Column(name = "patient_id", nullable = false)
    private String patientId;

    @NotNull
    @Column(name = "doctor_id", nullable = false)
    private String doctorId;

    @NotNull
    @Column(name = "total_cost", nullable = false)
    private Float totalCost;

    @Column(name = "discount")
    private Integer discount;

    @NotNull
    @Column(name = "real_cost", nullable = false)
    private Float realCost;

    @OneToMany(mappedBy = "treatmentDetailEntity", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<TreatmentItemEntity> treatmentItem;

    @Column(name = "noted_from_doctor")
    @Lob
    private String notedFromDoctor;

    @Column(name = "noted_from_patient")
    @Lob
    private String notedFromPatient;

    @Column(name = "link_download_file")
    private String linkDownloadFile;

    @Column(name = "paymented")
    private float paymented;

    @Column(name = "finish_paymented")
    private boolean finishPaymented;


    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getTreatmentId() {
        return treatmentId;
    }

    public void setTreatmentId(long treatmentId) {
        this.treatmentId = treatmentId;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public TreatmentDetailEntity patientId(String patientId) {
        this.patientId = patientId;
        return this;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public TreatmentDetailEntity doctorId(String doctorId) {
        this.doctorId = doctorId;
        return this;
    }

    public Float getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Float totalCost) {
        this.totalCost = totalCost;
    }

    public TreatmentDetailEntity totalCost(Float totalCost) {
        this.totalCost = totalCost;
        return this;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public TreatmentDetailEntity discount(Integer discount) {
        this.discount = discount;
        return this;
    }

    public Float getRealCost() {
        return realCost;
    }

    public void setRealCost(Float realCost) {
        this.realCost = realCost;
    }

    public TreatmentDetailEntity realCost(Float realCost) {
        this.realCost = realCost;
        return this;
    }

    public List<TreatmentItemEntity> getTreatmentItem() {
        return treatmentItem;
    }

    public void setTreatmentItem(List<TreatmentItemEntity> treatmentItem) {
        this.treatmentItem = treatmentItem;
    }

// jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    public String getNotedFromDoctor() {
        return notedFromDoctor;
    }

    public void setNotedFromDoctor(String notedFromDoctor) {
        this.notedFromDoctor = notedFromDoctor;
    }

    public String getNotedFromPatient() {
        return notedFromPatient;
    }

    public void setNotedFromPatient(String notedFromPatient) {
        this.notedFromPatient = notedFromPatient;
    }

    public String getLinkDownloadFile() {
        return linkDownloadFile;
    }

    public void setLinkDownloadFile(String linkDownloadFile) {
        this.linkDownloadFile = linkDownloadFile;
    }

    public float getPaymented() {
        return paymented;
    }

    public void setPaymented(float paymented) {
        this.paymented = paymented;
    }

    public boolean isFinishPaymented() {
        return finishPaymented;
    }

    public void setFinishPaymented(boolean finishPaymented) {
        this.finishPaymented = finishPaymented;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TreatmentDetailEntity treatmentDetailEntity = (TreatmentDetailEntity) o;
        if (treatmentDetailEntity.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), treatmentDetailEntity.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TreatmentDetailEntity{" +
            "id=" + getId() +
            ", patientId='" + getPatientId() + "'" +
            ", doctorId='" + getDoctorId() + "'" +
            ", totalCost=" + getTotalCost() +
            ", discount=" + getDiscount() +
            ", realCost=" + getRealCost() +
            "}";
    }
}
