package com.catun.dentist.app.exception;

public class ProductEntityNotFoundException extends Exception {

    public ProductEntityNotFoundException(String message) {
        super(message);
    }
}
