package com.catun.dentist.app.web.rest.response;

import java.util.List;

public class AssignDoctorResponse {

    private String day;
    private List<AssignDoctorDetailResponse> appointments;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public List<AssignDoctorDetailResponse> getAppointments() {
        return appointments;
    }

    public void setAppointments(List<AssignDoctorDetailResponse> appointments) {
        this.appointments = appointments;
    }
}
