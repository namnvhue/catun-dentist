package com.catun.dentist.app.service.impl;

import com.catun.dentist.app.config.Constants;
import com.catun.dentist.app.entity.AuthorityEntity;
import com.catun.dentist.app.entity.GroupEntity;
import com.catun.dentist.app.entity.MedicalHistoryEntity;
import com.catun.dentist.app.entity.UserEntity;
import com.catun.dentist.app.exception.EmailAlreadyUsedException;
import com.catun.dentist.app.exception.InvalidPasswordException;
import com.catun.dentist.app.exception.LoginAlreadyUsedException;
import com.catun.dentist.app.exception.SendGridEmailException;
import com.catun.dentist.app.repository.AuthorityRepository;
import com.catun.dentist.app.repository.CalendarbookingRepository;
import com.catun.dentist.app.repository.GroupRepository;
import com.catun.dentist.app.repository.MedicalHistoryImagesRepository;
import com.catun.dentist.app.repository.MedicalHistoryRepository;
import com.catun.dentist.app.repository.TreatmentDetailRepository;
import com.catun.dentist.app.repository.UserRepository;
import com.catun.dentist.app.security.AuthoritiesConstants;
import com.catun.dentist.app.security.SecurityUtils;
import com.catun.dentist.app.service.SendGridEmailService;
import com.catun.dentist.app.service.UserService;
import com.catun.dentist.app.service.dto.UserDTO;
import com.catun.dentist.app.service.util.RandomUtil;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import com.catun.dentist.app.web.rest.vm.ManagedUserVM;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserServiceImpl implements UserService{

    private final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final AuthorityRepository authorityRepository;
    private GroupRepository groupRepository;
    private CalendarbookingRepository calendarbookingRepository;
    private MedicalHistoryRepository medicalHistoryRepository;
    private MedicalHistoryImagesRepository medicalHistoryImagesRepository;
    private TreatmentDetailRepository treatmentDetailRepository;
    private SendGridEmailService sendGridEmailService;

    @Value("${catun.sendgrid.format.newuser.subject}")
    private String newuserSubjectEmail;
    @Value("${catun.sendgrid.format.newuser.content}")
    private String newuserContentEmail;

    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder,
                            AuthorityRepository authorityRepository,
                            GroupRepository groupRepository,
                            CalendarbookingRepository calendarbookingRepository,
                            MedicalHistoryRepository medicalHistoryRepository,
                            MedicalHistoryImagesRepository medicalHistoryImagesRepository,
                            TreatmentDetailRepository treatmentDetailRepository,
                            SendGridEmailService sendGridEmailService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.authorityRepository = authorityRepository;
        this.groupRepository = groupRepository;
        this.calendarbookingRepository = calendarbookingRepository;
        this.medicalHistoryRepository = medicalHistoryRepository;
        this.medicalHistoryImagesRepository = medicalHistoryImagesRepository;
        this.treatmentDetailRepository = treatmentDetailRepository;
        this.sendGridEmailService = sendGridEmailService;
    }

    @Override
    public Optional<UserEntity> activateRegistration(String key) {
        log.debug("Activating user for activation key {}", key);
        return userRepository.findOneByActivationKey(key)
            .map(user -> {
                // activate given user for the registration key.
                user.setActivated(true);
                user.setActivationKey(null);
                log.debug("Activated user: {}", user);
                return user;
            });
    }

    @Override
    public Optional<UserEntity> completePasswordReset(String newPassword, String key) {
        log.debug("Reset user password for reset key {}", key);

        return userRepository.findOneByResetKey(key)
            .filter(user -> user.getResetDate().isAfter(Instant.now().minusSeconds(86400)))
            .map(user -> {
                user.setPassword(passwordEncoder.encode(newPassword));
                user.setResetKey(null);
                user.setResetDate(null);
                return user;
            });
    }

    @Override
    public Optional<UserEntity> requestPasswordReset(String mail) {
        return userRepository.findOneByEmailIgnoreCase(mail)
            .filter(UserEntity::getActivated)
            .map(user -> {
                user.setResetKey(RandomUtil.generateResetKey());
                user.setResetDate(Instant.now());
                return user;
            });
    }

    @Override
    public UserEntity registerUser(UserDTO userDTO, String password) throws InvalidPasswordException, LoginAlreadyUsedException, EmailAlreadyUsedException {
        if (!checkPasswordLength(password)) {
            throw new InvalidPasswordException("Invalid password");
        }
        Optional<UserEntity> userEntityOptional = userRepository.findOneByLogin(userDTO.getLogin().toLowerCase());
        if(userEntityOptional.isPresent()){
            throw new LoginAlreadyUsedException("User exist in system");
        }
        String email = userDTO.getEmail();
        if (email != null && !email.equals("")) {
            Optional<UserEntity> userEntityOptionals = userRepository.findOneByEmailIgnoreCase(email);
            if(userEntityOptionals.isPresent()){
                throw new EmailAlreadyUsedException("Email exist in system");
            }
        }
        UserEntity newUser = new UserEntity();
        AuthorityEntity authorityEntity = authorityRepository.findOne(AuthoritiesConstants.USER);
        Set<AuthorityEntity> authorities = new HashSet<>();
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setLogin(userDTO.getLogin());
        // new user gets initially a generated password
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(userDTO.getFirstName());
        newUser.setLastName(userDTO.getLastName());
        newUser.setFullName(userDTO.getFullName());
        newUser.setBirthday(userDTO.getBirthday());
        newUser.setAddress(userDTO.getAddress());
        newUser.setCity(userDTO.getCity());
        if (email != null && !email.equals("")) {
            newUser.setEmail(email);
        }
        newUser.setImageUrl(userDTO.getImageUrl());
        newUser.setLangKey(userDTO.getLangKey());
        newUser.setPhone(userDTO.getLogin());
        newUser.setChannel(userDTO.getChannel());
        // new user is not active
        newUser.setActivated(true);
        // new user gets registration key
        authorities.add(authorityEntity);
        newUser.setAuthorities(authorities);
        GroupEntity groupEntity = groupRepository.findAll().get(0);
        newUser.setGroupEntity(groupEntity);
        userRepository.save(newUser);
        log.debug("Created Information for User: {}", newUser);

        if (checkEmail(email)) {
            log.info("SendEmail when calendarbooking register new user");
            CompletableFuture.runAsync(() ->{
                try {
                    sendGridEmailService.sendGridEmail(newuserSubjectEmail, email,
                        newuserContentEmail + newUser.getLogin() + " - Password :" + password);
                } catch (SendGridEmailException e) {
                    log.error("Send mail failed from email {} to newUserSubjectEmail {}",email,newuserContentEmail);

                }
            });
        }
        return newUser;
    }

    private boolean checkEmail(String email) {
        if (email == null || email.equals("")) {
            return false;
        }
        return true;
    }

    @Override
    public UserEntity createUser(UserDTO userDTO) {
        UserEntity user = new UserEntity();
        user.setLogin(userDTO.getLogin());
        String fullName = userDTO.getFullName();
        user.setFirstName(fullName);
        user.setLastName(fullName);
        user.setFullName(fullName);
        user.setEmail(userDTO.getEmail());
        user.setImageUrl(userDTO.getImageUrl());
        if (userDTO.getLangKey() == null) {
            user.setLangKey(Constants.DEFAULT_LANGUAGE); // default language
        } else {
            user.setLangKey(userDTO.getLangKey());
        }
        if (userDTO.getAuthorities() != null) {
            Set<AuthorityEntity> authorities = userDTO.getAuthorities().stream()
                .map(authorityRepository::findOne)
                .collect(Collectors.toSet());
            user.setAuthorities(authorities);
        }
        String password = RandomUtil.generatePassword();
        String encryptedPassword = passwordEncoder.encode(password);
        user.setPassword(encryptedPassword);
        user.setResetKey(RandomUtil.generateResetKey());
        user.setResetDate(Instant.now());
        user.setActivated(true);
        GroupEntity groupEntity = groupRepository.findAll().get(0);
        user.setGroupEntity(groupEntity);
        userRepository.save(user);
        String email = userDTO.getEmail();
        if (checkEmail(email)) {
            log.info("SendEmail when calendarbooking register new user");
            CompletableFuture.runAsync(() ->{
                try {
                    sendGridEmailService.sendGridEmail(newuserSubjectEmail, email,
                        newuserContentEmail + userDTO.getLogin() + " - Password :" + password);
                } catch (SendGridEmailException e) {
                    log.error("Send mail failed from email {} to newUserSubjectEmail {}",email,newuserContentEmail);

                }
            });
        }
        log.debug("Created Information for User: {}", user);
        return user;
    }

    /**
     * Update basic information (first name, last name, email, language) for the current user.
     *
     * @param firstName first name of user
     * @param lastName last name of user
     * @param email email id of user
     * @param langKey language key
     * @param imageUrl image URL of user
     */
    @Override
    public void updateUser(String firstName, String lastName, String email, String langKey,
        String imageUrl) {
        SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin)
            .ifPresent(user -> {
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setEmail(email);
                user.setLangKey(langKey);
                user.setImageUrl(imageUrl);
                log.debug("Changed Information for User: {}", user);
            });
    }

    /**
     * Update all information for a specific user, and return the modified user.
     *
     * @param userDTO user to update
     * @return updated user
     */
    @Override
    public Optional<UserDTO> updateUser(UserDTO userDTO) {
        return Optional.of(userRepository
            .findOne(userDTO.getId()))
            .map(user -> {
                user.setLogin(userDTO.getLogin());
                user.setFirstName(userDTO.getFirstName());
                user.setLastName(userDTO.getLastName());
                user.setEmail(userDTO.getEmail());
                user.setImageUrl(userDTO.getImageUrl());
                user.setActivated(userDTO.isActivated());
                user.setLangKey(userDTO.getLangKey());
                Set<AuthorityEntity> managedAuthorities = user.getAuthorities();
                managedAuthorities.clear();
                userDTO.getAuthorities().stream()
                    .map(authorityRepository::findOne)
                    .forEach(managedAuthorities::add);
                log.debug("Changed Information for User: {}", user);
                return user;
            })
            .map(UserDTO::new);
    }

    @Override
    @Transactional
    public void deleteUser(String login) {
        log.warn("Start delete user with login {}",login);
        userRepository.findOneByLogin(login).ifPresent(user -> {
            long userId = user.getId();
            List<MedicalHistoryEntity> medicalHistoryEntities = medicalHistoryRepository.findAllByUserId(userId);
            for(MedicalHistoryEntity medicalHistoryEntity : medicalHistoryEntities){
                medicalHistoryImagesRepository.deleteAllByMedicalHistoryEntityId(medicalHistoryEntity.getId());
            }
            treatmentDetailRepository.deleteAllByPatientId(String.valueOf(userId));
            calendarbookingRepository.deleteAllByUserEntityId(userId);
            medicalHistoryRepository.deleteAllByUserId(userId);
            userRepository.delete(user);
            log.debug("Deleted User: {}", user);
        });
    }

    @Override
    public void changePassword(String password) {
        SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin)
            .ifPresent(user -> {
                String encryptedPassword = passwordEncoder.encode(password);
                user.setPassword(encryptedPassword);
                log.debug("Changed password for User: {}", user);
            });
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserDTO> getAllManagedUsers(Pageable pageable) {
        return userRepository.findAllByLoginNot(pageable, Constants.ANONYMOUS_USER)
            .map(UserDTO::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<UserEntity> getUserWithAuthoritiesByLogin(String login) {
        return userRepository.findOneWithAuthoritiesByLogin(login);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<UserEntity> getUserWithAuthorities(Long id) {
        return userRepository.findOneWithAuthoritiesById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<UserEntity> getUserWithAuthorities() {
        return SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneWithAuthoritiesByLogin);
    }

    /**
     * Not activated users should be automatically deleted after 3 days.
     * <p>
     * This is scheduled to get fired everyday, at 01:00 (am).
     */
    @Override
    @Scheduled(cron = "0 0 1 * * ?")
    public void removeNotActivatedUsers() {
        List<UserEntity> users = userRepository
            .findAllByActivatedIsFalseAndCreatedDateBefore(Instant.now().minus(3, ChronoUnit.DAYS));
        for (UserEntity user : users) {
            log.debug("Deleting not activated user {}", user.getLogin());
            userRepository.delete(user);
        }
    }

    /**
     * @return a list of all the authorities
     */
    @Override
    public List<String> getAuthorities() {
        return authorityRepository.findAll().stream().map(AuthorityEntity::getName)
            .collect(Collectors.toList());
    }

    private static boolean checkPasswordLength(String password) {
        return !StringUtils.isEmpty(password) &&
            password.length() >= ManagedUserVM.PASSWORD_MIN_LENGTH &&
            password.length() <= ManagedUserVM.PASSWORD_MAX_LENGTH;
    }

}
