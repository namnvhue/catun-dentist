package com.catun.dentist.app.repository;

import com.catun.dentist.app.entity.BookingCodeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookingCodeRepository extends JpaRepository<BookingCodeEntity, String> {

    BookingCodeEntity findOneByCode(String code);
}
