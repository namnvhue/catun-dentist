package com.catun.dentist.app.web.rest.response;

public class TreatmentStatusRequest {

    private String treatmentId;
    private boolean finishFlag;

    public String getTreatmentId() {
        return treatmentId;
    }

    public void setTreatmentId(String treatmentId) {
        this.treatmentId = treatmentId;
    }

    public boolean isFinishFlag() {
        return finishFlag;
    }

    public void setFinishFlag(boolean finishFlag) {
        this.finishFlag = finishFlag;
    }
}
