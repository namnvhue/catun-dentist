package com.catun.dentist.app.web.rest.response;

public class PatiensDetailResponse {

    private String id;
    private String name;
    private String phone;
    private String address;
    private String email;
    private String birthDay;
    private String firstTreatmentTime;
    private String lastTreatmentTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getFirstTreatmentTime() {
        return firstTreatmentTime;
    }

    public void setFirstTreatmentTime(String firstTreatmentTime) {
        this.firstTreatmentTime = firstTreatmentTime;
    }

    public String getLastTreatmentTime() {
        return lastTreatmentTime;
    }

    public void setLastTreatmentTime(String lastTreatmentTime) {
        this.lastTreatmentTime = lastTreatmentTime;
    }
}
