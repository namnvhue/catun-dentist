/**
 * View Models used by Spring MVC REST controllers.
 */
package com.catun.dentist.app.web.rest.vm;
