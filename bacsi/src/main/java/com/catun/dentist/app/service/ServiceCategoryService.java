package com.catun.dentist.app.service;

import com.catun.dentist.app.entity.ServiceCategoryEntity;
import com.catun.dentist.app.exception.ServiceCategoryEntityExistException;
import com.catun.dentist.app.web.rest.request.ServiceCategoryRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ServiceCategoryService {

    ServiceCategoryEntity createServiceCategory(ServiceCategoryRequest serviceCategoryRequest)
        throws ServiceCategoryEntityExistException;

    ServiceCategoryEntity updateServiceCategory(ServiceCategoryRequest serviceCategoryRequest);

    Page<ServiceCategoryEntity> findAll(Pageable pageable);

    ServiceCategoryEntity getDetailServiceCategory(long id);

    void deleteServiceCategory(long id);
}
