package com.catun.dentist.app.service;

import com.catun.dentist.app.exception.*;
import com.catun.dentist.app.web.rest.request.ReportWareHouseRequest;
import com.catun.dentist.app.web.rest.request.WareHouseRequest;
import com.catun.dentist.app.web.rest.response.ReportWareHouseResponse;
import com.catun.dentist.app.web.rest.response.WareHouseResponse;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.core.io.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.util.List;

public interface WareHouseService {

    List<WareHouseResponse> getWareHouse() throws WareHouseNotFoundException, SupplierNotFoundException, ConvertMoneyFortmatException;

    void createWareHouse(WareHouseRequest request) throws UserNotFoundException, ParseException, SupplierNotFoundException, WaHouseExistException, WareHouseReportDateException;

    void deleteWareHouse(long id);

    WareHouseResponse getDetailWareHouse(long id) throws WareHouseNotFoundException, SupplierNotFoundException, ConvertMoneyFortmatException;

    ReportWareHouseResponse reportWareHouse(ReportWareHouseRequest request) throws ParseException, WareHouseEnumException, CsvException, WareHouseReportDateException, ExcelFileException;

    Resource downloadCsv(String fileName) throws CsvException;

    void downloadCsvTest(String fileName , HttpServletRequest httpServletRequest,HttpServletResponse response);

    HSSFWorkbook createFilesExcel(String directoryPathCsvServer, String fileName, ReportWareHouseResponse responses) throws ExcelFileException;

    void downloadExcel(HttpServletResponse response,String fromDate,String toDate) throws ExcelFileException, WareHouseEnumException, CsvException, ParseException, WareHouseReportDateException;
}
