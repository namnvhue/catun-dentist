package com.catun.dentist.app.web.rest.request;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

public class ServiceRequest {

    private String id;

    @NotNull
    @NotEmpty
    private String serviceCategoryId;

    @NotNull
    @NotEmpty
    private String name;

    private float basePrice;
    private String description;
    private String relatedProducts;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getServiceCategoryId() {
        return serviceCategoryId;
    }

    public void setServiceCategoryId(String serviceCategoryId) {
        this.serviceCategoryId = serviceCategoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(float basePrice) {
        this.basePrice = basePrice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRelatedProducts() {
        return relatedProducts;
    }

    public void setRelatedProducts(String relatedProducts) {
        this.relatedProducts = relatedProducts;
    }
}
