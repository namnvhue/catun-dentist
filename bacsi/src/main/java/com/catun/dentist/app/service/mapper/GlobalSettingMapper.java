package com.catun.dentist.app.service.mapper;

import com.catun.dentist.app.domain.GlobalSetting;
import com.catun.dentist.app.service.dto.GlobalSettingDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity GlobalSetting and its DTO GlobalSettingDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface GlobalSettingMapper extends EntityMapper<GlobalSettingDTO, GlobalSetting> {


    default GlobalSetting fromId(Long id) {
        if (id == null) {
            return null;
        }
        GlobalSetting globalSetting = new GlobalSetting();
        globalSetting.setId(id);
        return globalSetting;
    }
}
