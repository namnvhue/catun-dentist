package com.catun.dentist.app.repository;

import com.catun.dentist.app.entity.ServiceCategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Spring Data JPA repository for the ServiceCategoryEntity entity.
 */
public interface ServiceCategoryRepository extends
    JpaRepository<ServiceCategoryEntity, Long> {

    ServiceCategoryEntity findByName(String name);
}
