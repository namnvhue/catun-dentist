package com.catun.dentist.app.service.util;

import java.util.Locale;
import org.springframework.context.MessageSource;

public class GetMessageUtil {

    public static String getMessage(MessageSource messageSource, String titleKey, String langKey) {
        Locale locale = Locale.forLanguageTag(langKey);
        return messageSource.getMessage(titleKey, null, locale);
    }
}
