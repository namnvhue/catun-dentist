package com.catun.dentist.app.repository;

import com.catun.dentist.app.entity.WareHouseEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WarehouseRepository extends JpaRepository<WareHouseEntity, Long> {

    List<WareHouseEntity> findAllByOrderByExpiryDateAsc();

    WareHouseEntity findByCode(String code);
}
