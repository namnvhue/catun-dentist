package com.catun.dentist.app.exception;

public class ExpenditureNotFoundException extends Exception{

    public ExpenditureNotFoundException(String message) {
        super(message);
    }

}
