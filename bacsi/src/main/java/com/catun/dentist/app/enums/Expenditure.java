package com.catun.dentist.app.enums;

import com.catun.dentist.app.exception.ExpenditureEnumException;

public enum Expenditure {

    THU("THU", 0),
    CHI("CHI", 1);

    private String name;
    private int value;

    Expenditure(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public static Expenditure fromValue(int value) throws ExpenditureEnumException {
        for (Expenditure executeStatus : Expenditure.values()) {
            if (executeStatus.getValue() == value) {
                return executeStatus;
            }
        }
        throw new ExpenditureEnumException("Can't find Expenditure with value {}");
    }

    public static Expenditure fromName(String name) throws ExpenditureEnumException {
        for (Expenditure executeStatus : Expenditure.values()) {
            if (executeStatus.getName().equals(name)) {
                return executeStatus;
            }
        }
        throw new ExpenditureEnumException("Can't find Expenditure with name {}");
    }
}
