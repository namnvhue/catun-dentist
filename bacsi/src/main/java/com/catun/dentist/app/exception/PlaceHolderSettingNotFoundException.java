package com.catun.dentist.app.exception;

public class PlaceHolderSettingNotFoundException extends Exception {

    public PlaceHolderSettingNotFoundException(String message) {
        super(message);
    }
}
