package com.catun.dentist.app.repository;

import com.catun.dentist.app.entity.AuthorityEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Authority entity.
 */
public interface AuthorityRepository extends JpaRepository<AuthorityEntity, String> {

}
