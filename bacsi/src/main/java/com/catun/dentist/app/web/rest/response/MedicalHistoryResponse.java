package com.catun.dentist.app.web.rest.response;

import java.util.List;

public class MedicalHistoryResponse {

    List<MedicalHistoryListResponse> medicalHistoryListResponses;
    private PagingResponse pageable;
    private TreatmentResponse user;

    public List<MedicalHistoryListResponse> getMedicalHistoryListResponses() {
        return medicalHistoryListResponses;
    }

    public void setMedicalHistoryListResponses(List<MedicalHistoryListResponse> medicalHistoryListResponses) {
        this.medicalHistoryListResponses = medicalHistoryListResponses;
    }

    public PagingResponse getPageable() {
        return pageable;
    }

    public void setPageable(PagingResponse pageable) {
        this.pageable = pageable;
    }

    public TreatmentResponse getUser() {
        return user;
    }

    public void setUser(TreatmentResponse user) {
        this.user = user;
    }
}
