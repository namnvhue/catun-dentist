package com.catun.dentist.app.exception;

public class TreatmentDetailNotFoundException extends Exception {

    public TreatmentDetailNotFoundException(String message) {
        super(message);
    }
}
