package com.catun.dentist.app.exception;

public class MedicalHistoryParamInvalidException extends Exception{

    public MedicalHistoryParamInvalidException(String message) {
        super(message);
    }
}
