package com.catun.dentist.app.service;

import com.catun.dentist.app.exception.DoctorNotFoundException;
import com.catun.dentist.app.exception.GlobalSettingNotFoundException;
import com.catun.dentist.app.exception.MedicalHistoryNotFoundException;
import com.catun.dentist.app.exception.MedicalHistoryUploadImagesException;
import com.catun.dentist.app.exception.ParseDateException;
import com.catun.dentist.app.exception.SendGridEmailException;
import com.catun.dentist.app.exception.ServiceEntityNotFoundException;
import com.catun.dentist.app.exception.TreatmentExistException;
import com.catun.dentist.app.exception.TreatmentNotFoundException;
import com.catun.dentist.app.exception.UserNotFoundException;
import com.catun.dentist.app.web.rest.request.CalendarbookingRequest;
import com.catun.dentist.app.web.rest.request.MedicalHistoryAddNewRequest;
import com.catun.dentist.app.web.rest.request.MedicalHistoryPatientRequest;
import com.catun.dentist.app.web.rest.response.MedicalHistoryDetailResponse;
import com.catun.dentist.app.web.rest.response.PatiensDetailResponse;
import com.catun.dentist.app.web.rest.response.TreatmentHistoryResponse;
import com.catun.dentist.app.web.rest.response.TreatmentResponse;
import com.catun.dentist.app.web.rest.response.TreatmentStatusRequest;

import java.text.ParseException;


public interface MedicalHistoryService {
    TreatmentHistoryResponse getTreatmentPaging(MedicalHistoryPatientRequest medicalHistoryPatientRequest,boolean finishFlag);
    TreatmentResponse getTreatmentByBookingId(long bookingId) throws TreatmentNotFoundException;
    PatiensDetailResponse getPatientInfo(long id) throws UserNotFoundException;
    void createMedicalHistory(MedicalHistoryAddNewRequest medicalHistoryAddNewRequest) throws UserNotFoundException, MedicalHistoryUploadImagesException, ParseDateException, ParseException, SendGridEmailException, GlobalSettingNotFoundException, DoctorNotFoundException;
    MedicalHistoryDetailResponse getMedicalHistoryDetail(String treatmentDetailId) throws MedicalHistoryNotFoundException, ParseDateException;
    void deleteMedicalHistory(String treatmentDetailId) throws ParseException, MedicalHistoryNotFoundException;
    void changeTreatmentStatus(TreatmentStatusRequest treatmentStatusRequest) throws TreatmentNotFoundException;
    void createTreatment(CalendarbookingRequest treatmentRequest) throws SendGridEmailException, ParseException, ParseDateException, TreatmentExistException, ServiceEntityNotFoundException;
    void deleteTreatment(long treatmentId) throws TreatmentNotFoundException, ParseException, MedicalHistoryNotFoundException;
}
