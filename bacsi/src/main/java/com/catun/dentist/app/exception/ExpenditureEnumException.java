package com.catun.dentist.app.exception;

public class ExpenditureEnumException extends Exception{

    public ExpenditureEnumException(String message) {
        super(message);
    }
}
