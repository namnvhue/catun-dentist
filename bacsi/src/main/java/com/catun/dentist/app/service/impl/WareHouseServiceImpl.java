package com.catun.dentist.app.service.impl;

import com.catun.dentist.app.constant.CsvConstants;
import com.catun.dentist.app.entity.*;
import com.catun.dentist.app.enums.WareHouseType;
import com.catun.dentist.app.exception.*;
import com.catun.dentist.app.repository.*;
import com.catun.dentist.app.security.SecurityUtils;
import com.catun.dentist.app.service.WareHouseService;
import com.catun.dentist.app.service.util.NumberToMoneyCharactersUtil;
import com.catun.dentist.app.web.rest.request.ReportWareHouseRequest;
import com.catun.dentist.app.web.rest.request.WareHouseRequest;
import com.catun.dentist.app.web.rest.response.ReportWareHouseDetailResponse;
import com.catun.dentist.app.web.rest.response.ReportWareHouseResponse;
import com.catun.dentist.app.web.rest.response.WareHouseResponse;
import com.catun.dentist.app.web.rest.util.ExcelData;
import com.catun.dentist.app.web.rest.util.ExportExcelUtils;
import liquibase.util.csv.opencsv.CSVReader;
import net.logstash.logback.encoder.org.apache.commons.lang.math.NumberUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class WareHouseServiceImpl implements WareHouseService {
    private final Logger log = LoggerFactory.getLogger(WareHouseServiceImpl.class);

    private WarehouseRepository warehouseRepository;
    private UserRepository userRepository;
    private SupplierRepository supplierRepository;
    private WareHouseDetailRepository wareHouseDetailRepository;
    private WareHouseReportRepository wareHouseReportRepository;

    public WareHouseServiceImpl(WarehouseRepository warehouseRepository,UserRepository userRepository,SupplierRepository supplierRepository,WareHouseDetailRepository wareHouseDetailRepository,WareHouseReportRepository wareHouseReportRepository){
        this.warehouseRepository = warehouseRepository;
        this.userRepository = userRepository;
        this.supplierRepository = supplierRepository;
        this.wareHouseDetailRepository = wareHouseDetailRepository;
        this.wareHouseReportRepository = wareHouseReportRepository;
    }

    @Value("${catun.directory.pathcsv.server}")
    private String directoryPathCsvServer;

    @Override
    public List<WareHouseResponse> getWareHouse() throws WareHouseNotFoundException, SupplierNotFoundException, ConvertMoneyFortmatException {
        log.info("Start get WareHouse info");
        List<WareHouseEntity> wareHouses = warehouseRepository.findAllByOrderByExpiryDateAsc();
        if(wareHouses.isEmpty()){
            log.error("WareHouse not found");
            throw new WareHouseNotFoundException("WareHouse not found");
        }
        log.info("End get WareHouse Success with size {}",wareHouses.size());
        List<WareHouseResponse> responses = new ArrayList<>();
        for(WareHouseEntity wareHouse : wareHouses){
            WareHouseResponse response = convertWareHouseResponse(wareHouse);
            responses.add(response);
        }
        return responses;
    }

    @Override
    public void createWareHouse(WareHouseRequest request) throws UserNotFoundException, ParseException, SupplierNotFoundException, WaHouseExistException, WareHouseReportDateException {
        UserEntity reception = SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).get();
        long userId = reception.getId();
        String userName = reception.getFullName();
        log.info("Start create WareHouse userId {} , userName {} ",userId,userName);
        if(reception == null){
            log.error("Reception not found with userId {} ",userId);
            throw new UserNotFoundException("User not found");
        }
        String code = request.getCode();
        WareHouseEntity wareHouseEntity = warehouseRepository.findByCode(code);
        if(wareHouseEntity != null){
            log.error("WareHouse exist in system with code {}",code);
            throw new WaHouseExistException("");
        }
        WareHouseEntity wareHouse = new WareHouseEntity();
        wareHouse.setCode(code);
        long supplierId = request.getSupplierId();
        SupplierEntity supplier = supplierRepository.findOne(supplierId);
        if(supplier == null){
            log.error("Supplier not found with supplierId {}",supplierId);
            throw new SupplierNotFoundException("Supplier not found");
        }
        wareHouse.setSupplierId(supplierId);
        wareHouse.setName(request.getName());
        wareHouse.setUserId(userId);
        wareHouse.setUserName(userName);
        int amount = request.getAmount();
        wareHouse.setAmount(amount);
        wareHouse.setPrice(request.getPrice());
        wareHouse.setAmountUsed(0);
        Date expiryDate = new SimpleDateFormat("dd/MM/yyyy").parse(request.getExpiryDate());
        wareHouse.setAmountExpiryDate(0);
        Date inputDate = new SimpleDateFormat("dd/MM/yyyy").parse(request.getDateInput());
        if(inputDate.after(expiryDate)){
            log.error("FromDate ,ToDate Invalid inputDate {} , expiryDate {}",inputDate,expiryDate);
            throw new WareHouseReportDateException("FromDate ,ToDate Invalid");
        }
        wareHouse.setExpiryDate(expiryDate);
        wareHouse.setDateInput(inputDate);
        warehouseRepository.save(wareHouse);

        WareHouseDetailEntity wareHouseDetail = new WareHouseDetailEntity();
        wareHouseDetail.setWareHouseId(wareHouse.getId());
        wareHouseDetail.setUserId(userId);
        wareHouseDetail.setUserName(userName);
        wareHouseDetail.setAmount(amount);
        wareHouseDetail.setAmountIOput(amount);
        wareHouseDetail.setType(WareHouseType.NHAP.getValue());
        wareHouseDetail.setTimeIOput(inputDate);
        wareHouseDetailRepository.save(wareHouseDetail);
        log.info("End create warehouse Success");
    }

    @Override
    public void deleteWareHouse(long id) {
        log.info("Start delete WareHouse with warehouseId {}",id);
        List<WareHouseDetailEntity> wareHouseDetails = wareHouseDetailRepository.findAllByWareHouseId(id);
        if(!wareHouseDetails.isEmpty()){
            log.info("Start delete WareHouseDetail with size {}",wareHouseDetails.size());
            for(WareHouseDetailEntity wareHouseDetail : wareHouseDetails){
                wareHouseDetailRepository.delete(wareHouseDetail);
            }
        }
        warehouseRepository.delete(id);
        log.info("End delete WareHouse Success");
    }

    @Override
    public WareHouseResponse getDetailWareHouse(long id) throws WareHouseNotFoundException, SupplierNotFoundException, ConvertMoneyFortmatException {
        log.info("Start getDetailWareHouse with wareHouseId {}",id);
        WareHouseEntity wareHouse = warehouseRepository.findOne(id);
        if(wareHouse == null){
            log.error("WareHouse not found with wareHouseId {}",id);
            throw new WareHouseNotFoundException("WareHouse not found ");
        }
        return convertWareHouseResponse(wareHouse);
    }

    @Override
    public ReportWareHouseResponse reportWareHouse(ReportWareHouseRequest request) throws ParseException, WareHouseEnumException, CsvException, WareHouseReportDateException, ExcelFileException {
        UserEntity reception = SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).get();
        String fromDate = request.getFromDate();
        String toDate = request.getToDate();
        log.info("Start get Report WareHouse with fromDate {} , toDate {} , userId {} , userName {}",fromDate,toDate,reception.getId(),reception.getFullName());
        Date from = new SimpleDateFormat("dd/MM/yyyy").parse(fromDate);
        Date to = new SimpleDateFormat("dd/MM/yyyy").parse(toDate);
        if(from.after(to)){
            log.error("FromDate ,ToDate Invalid");
            throw new WareHouseReportDateException("FromDate ,ToDate Invalid");
        }
        ReportWareHouseResponse responses = getWareHouseReport(from,to);
//        String csvContent = createCsvStr(responses);
//        Date d = new Date();
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
//        String dateFortmat = simpleDateFormat.format(d);
//        String fileName = dateFortmat;
//        saveExcelFile(directoryPathCsvServer,fileName,csvContent);
//        createExcelFile(directoryPathCsvServer,fileName,responses);
        createWareHouseReport(reception,from,to);
        return responses;
    }

    @Override
    public Resource downloadCsv(String fileName) throws CsvException {
        String csvPath = directoryPathCsvServer+ fileName+CsvConstants.CSV_EXT;
        log.info("Start download files CSV with path {}",csvPath);
        try {
            Path path = Paths.get(csvPath);
            Resource resource = new UrlResource(path.toUri());
            log.info("Download files CSV success");
            return resource;
        } catch (Exception e) {
            log.error("Can't download WareHouseReport CSV with fileName {} : ",fileName);
            throw new CsvException("Can't download Treatment Detail PDF ");
        }
    }

    @Override
    public void downloadCsvTest(String fileName, HttpServletRequest httpServletRequest,HttpServletResponse response) {
        String csvPath = directoryPathCsvServer+ fileName+".xls";
        try {
            File downloadFile = new File(csvPath);
            if (downloadFile.isFile()) {
                FileInputStream inputStream = new FileInputStream(downloadFile);
                // get MIME type of the file
                final int BUFFER_SIZE = 4096;
                // get absolute path of the application
                ServletContext context = httpServletRequest.getServletContext();
                String mimeType = context.getMimeType(csvPath);
                if (mimeType == null) {
                    // set to binary type if MIME mapping not found
                    mimeType = "application/vnd.ms-excel";
                }
                System.out.println("MIME type: " + mimeType);
                // set content attributes for the response
                response.setContentType("application/vnd.openxml");
                response.setContentLength((int) downloadFile.length());

                // set headers for the response
                String headerKey = "Content-Disposition";
                String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
                response.setHeader(headerKey, headerValue);

                // get output stream of the response
                OutputStream outStream = response.getOutputStream();

                byte[] buffer = new byte[BUFFER_SIZE];
                int bytesRead = -1;

                // write bytes read from the input stream into the output stream
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outStream.write(buffer, 0, bytesRead);
                }
                inputStream.close();
                outStream.close();
//                downloadFile.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public HSSFWorkbook createFilesExcel(String directoryPathCsvServer,String fileName,ReportWareHouseResponse responses) throws ExcelFileException {
        try
        {
            HSSFWorkbook workbook = new HSSFWorkbook();
            CreationHelper createHelper = workbook.getCreationHelper();
            // Create a Sheet
            Sheet sheet = workbook.createSheet("Employee");
            sheet.setDefaultColumnWidth(30);
            int counter = 0;
            for (int i = 0;i<5;i++) {
                Row row = sheet.createRow(counter);
                Cell cellDate = row.createCell(0);
                cellDate.setCellValue("Hello world 1");
                Cell cellStart = row.createCell(1);
                cellStart.setCellValue("Hello world 2");
                Cell cellEnd = row.createCell(2);
                cellEnd.setCellValue("Hello world 3");
                Cell cellActivity = row.createCell(3);
                cellActivity.setCellValue("Hello world 4");
                counter ++;
            }
            return workbook;
        }
        catch (Exception e)
        {
            log.error("Create files Excel failed {}",e.getMessage());
            throw new ExcelFileException("");
        }
    }

    @Override
    public void downloadExcel(HttpServletResponse response,String fromDate,String toDate) throws ExcelFileException, WareHouseEnumException, CsvException, ParseException, WareHouseReportDateException {
        try {
            log.info("Start download file Excel fromDate {} , toDate {}");
            Date from = new SimpleDateFormat("dd-MM-yyyy").parse(fromDate);
            Date to = new SimpleDateFormat("dd-MM-yyyy").parse(toDate);
            if(from.after(to)){
                log.error("FromDate ,ToDate Invalid");
                throw new WareHouseReportDateException("FromDate ,ToDate Invalid");
            }
            log.info("Start get WareHouseReport in DB");
            ReportWareHouseResponse responseData = getWareHouseReport(from,to);
            ExcelData data = new ExcelData();
            data.setName("hello");
            List<String> titles = new ArrayList();
            titles.add("Tên Sản Phẩm");
            titles.add("Tháng Trước Còn Lại");
            titles.add("Nhập Vào");
            titles.add("Xuất Ra");
            titles.add("Còn Lại");
            data.setTitles(titles);
            List<List<Object>> rows = new ArrayList();
            List<ReportWareHouseDetailResponse> wareHouses = responseData.getWareHouses();
            log.info("Warehouse rows size {}",wareHouses.size());
            for(ReportWareHouseDetailResponse wareHouse : wareHouses){
                List<Object> row = new ArrayList();
                row.add(wareHouse.getName());
                row.add(wareHouse.getRestAmountMonthAgo());
                row.add(wareHouse.getInputAmount());
                row.add(wareHouse.getOutputAmount());
                row.add(wareHouse.getRestAmount());
                rows.add(row);
                data.setRows(rows);
            }
            List<Object> row = new ArrayList();
            row.add("Tổng Số");
            row.add(responseData.getTotalRestAmountMonthAgo());
            row.add(responseData.getTotalInputAmount());
            row.add(responseData.getTotalOutputAmount());
            row.add(responseData.getTotalRestAmount());
            rows.add(row);
            data.setRows(rows);
            //生成本地
        /*File f = new File("c:/test.xlsx");
        FileOutputStream out = new FileOutputStream(f);
        ExportExcelUtils.exportExcel(data, out);
        out.close();*/
            Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
        String fileName = simpleDateFormat.format(date) +CsvConstants.FILE_EXTN;
        log.info("Start ExportExcel data with fileName {} , data size {}",fileName,data.getRows().size());
            ExportExcelUtils.exportExcel(response,fileName,data);
        } catch (Exception e) {
            log.error("Download file Excel failed {}",e.getMessage());
            throw new ExcelFileException("Download Excel failed");
        }
    }

    private ReportWareHouseResponse getWareHouseReport(Date from,Date to) throws WareHouseEnumException {
        List<WareHouseDetailEntity> wareHouseDetails = wareHouseDetailRepository.findAllByTimeIOputBetweenOrderByWareHouseIdAsc(from,to);
        log.info("Get WareHouse report size {}",wareHouseDetails.size());
        Map<Long,List<WareHouseDetailEntity>> maps = wareHouseDetails.stream().flatMap(wareHouseDetail -> {
            Map<Long,WareHouseDetailEntity> map = new HashMap<>();
            map.put(wareHouseDetail.getWareHouseId(),wareHouseDetail);
            return map.entrySet().stream();
        }).collect(Collectors.groupingBy(Map.Entry::getKey,Collectors.mapping(Map.Entry::getValue,Collectors.toList())));
        ReportWareHouseResponse responses = new ReportWareHouseResponse();
        List<ReportWareHouseDetailResponse> responseList = new ArrayList<>();
        int totalRestAmountMonthAgo = 0 ;
        int totalInputAmount = 0;
        int totalOutputAmount = 0;
        int totalRestAmount =  0;
        for(Map.Entry<Long,List<WareHouseDetailEntity>> entry : maps.entrySet()){
            long wareHouseId = entry.getKey();
            WareHouseEntity wareHouse = warehouseRepository.findOne(wareHouseId);
            List<WareHouseDetailEntity> wareHouseDts = entry.getValue();
            int inputAmount = 0;
            int outputAmount = 0;
            for(WareHouseDetailEntity wareHouseDetail : wareHouseDts){
                int ioputAmount = wareHouseDetail.getAmountIOput();
                if(WareHouseType.NHAP.getName().equals(WareHouseType.fromValue(wareHouseDetail.getType()).getName())){
                    inputAmount += ioputAmount;
                }else{
                    outputAmount += ioputAmount;
                }
            }
            ReportWareHouseDetailResponse detailResponse = new ReportWareHouseDetailResponse();
            //Implement statistics rest month ago
            List<WareHouseDetailEntity> wareHouseDetailFroms = wareHouseDetailRepository.findAllByWareHouseIdAndTimeIOputAfter(wareHouseId,from);
            if(!wareHouseDetailFroms.isEmpty()){
                int amountInputFrom = 0;
                int amountOutputFrom = 0;
                for(WareHouseDetailEntity wareHouseDetailFrom : wareHouseDetailFroms){
                    if(WareHouseType.NHAP.getName().equals(WareHouseType.fromValue(wareHouseDetailFrom.getType()).getName())){
                        amountInputFrom += wareHouseDetailFrom.getAmountIOput();
                    }else{
                        amountOutputFrom += wareHouseDetailFrom.getAmountIOput();
                    }
                }
                int restAmountAgo = wareHouse.getAmount() + amountOutputFrom - amountInputFrom;
                detailResponse.setRestAmountMonthAgo(restAmountAgo);
                totalRestAmountMonthAgo += restAmountAgo;
            }else{
                detailResponse.setRestAmountMonthAgo(0);
                totalRestAmountMonthAgo += 0;
            }
            //End statistics rest month ago
            detailResponse.setName(wareHouse.getName());
            detailResponse.setInputAmount(inputAmount);
            detailResponse.setOutputAmount(outputAmount);
            int restAmount = wareHouse.getAmount();
            detailResponse.setRestAmount(restAmount);
            responseList.add(detailResponse);
            totalInputAmount += inputAmount;
            totalOutputAmount += outputAmount;
            totalRestAmount += restAmount;
        }
        responses.setWareHouses(responseList);
        responses.setTotalRestAmountMonthAgo(totalRestAmountMonthAgo);
        responses.setTotalInputAmount(totalInputAmount);
        responses.setTotalOutputAmount(totalOutputAmount);
        responses.setTotalRestAmount(totalRestAmount);
        return responses;
    }

    private void createWareHouseReport(UserEntity reception,Date fromDate,Date toDate){
        log.info("Start Create WareHouseReport");
        WareHouseReportEntity wareHouseReport =  new WareHouseReportEntity();
        wareHouseReport.setUserId(reception.getId());
        wareHouseReport.setUserName(reception.getFullName());
        wareHouseReport.setFromDate(fromDate);
        wareHouseReport.setToDate(toDate);
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
        String fileName = simpleDateFormat.format(date) +CsvConstants.FILE_EXTN;
        wareHouseReport.setFileName(fileName);
        wareHouseReportRepository.save(wareHouseReport);
        log.info("End Create WareHouseReport Success");
    }

    private String createCsvStr(ReportWareHouseResponse response) {
        StringBuilder csvStr = new StringBuilder();
        if (response != null) {
            csvStr.append(String.join(",", CsvConstants.ware_house));
            List<ReportWareHouseDetailResponse> wareHouseReports = response.getWareHouses();
            for (ReportWareHouseDetailResponse wareHouseReport : wareHouseReports) {
                csvStr.append(System.lineSeparator());
                csvStr.append(wareHouseReport.getName());
                csvStr.append(CsvConstants.CSV_DELIMITER);
                csvStr.append(wareHouseReport.getRestAmountMonthAgo());
                csvStr.append(CsvConstants.CSV_DELIMITER);
                csvStr.append(wareHouseReport.getInputAmount());
                csvStr.append(CsvConstants.CSV_DELIMITER);
                csvStr.append(wareHouseReport.getOutputAmount());
                csvStr.append(CsvConstants.CSV_DELIMITER);
                csvStr.append(wareHouseReport.getRestAmount());
            }
            csvStr.append(System.lineSeparator());
            csvStr.append("Tổng Số");
            csvStr.append(CsvConstants.CSV_DELIMITER);
            csvStr.append(response.getTotalRestAmountMonthAgo());
            csvStr.append(CsvConstants.CSV_DELIMITER);
            csvStr.append(response.getTotalInputAmount());
            csvStr.append(CsvConstants.CSV_DELIMITER);
            csvStr.append(response.getTotalOutputAmount());
            csvStr.append(CsvConstants.CSV_DELIMITER);
            csvStr.append(response.getTotalRestAmount());
        }
        return csvStr.toString();
    }

    private void saveExcelFile(String targetFolder, String csvName, String csvContent)
        throws CsvException {
        try{
            Path path = Paths.get(targetFolder);
            if(!Files.exists(path)){
                new File(targetFolder).mkdirs();
            }
            String filePath = targetFolder + csvName + CsvConstants.CSV_EXT;
            File fileCSV = new File(filePath);
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath), "UTF-8"));
            out.append(csvContent);
            out.flush();
            out.close();
            log.info("Write csv file done. File: {} , Start convert file CSV to Excel .xlsx", filePath);
            convertCsvToXls(targetFolder,filePath,csvName);
            fileCSV.delete();
        }catch (Exception e){
            log.error("Create warehouse report CSV file failed {}",e.getMessage());
            throw new CsvException("");
        }
    }

    private String convertCsvToXls(String xlsFileLocation, String csvFilePath,String fileName) {
        SXSSFSheet sheet = null;
        CSVReader reader = null;
        Workbook workBook = null;
        String generatedXlsFilePath = "";
        FileOutputStream fileOutputStream = null;
        try {
            /**** Get the CSVReader Instance & Specify The Delimiter To Be Used ****/
            String[] nextLine;
            reader = new CSVReader(new FileReader(csvFilePath), CsvConstants.FILE_DELIMITER);
            workBook = new SXSSFWorkbook();
            sheet = (SXSSFSheet) workBook.createSheet("Sheet");
            int rowNum = 0;
            log.info("Creating New .Xls File From The Already Generated .Csv File");
            while((nextLine = reader.readNext()) != null) {
                Row currentRow = sheet.createRow(rowNum++);
                for(int i=0; i < nextLine.length; i++) {
                    if(NumberUtils.isDigits(nextLine[i])) {
                        currentRow.createCell(i).setCellValue(Integer.parseInt(nextLine[i]));
                    } else if (NumberUtils.isNumber(nextLine[i])) {
                        currentRow.createCell(i).setCellValue(Double.parseDouble(nextLine[i]));
                    } else {
                        currentRow.createCell(i).setCellValue(nextLine[i]);
                    }
                }
            }
            generatedXlsFilePath = xlsFileLocation + fileName + CsvConstants.FILE_EXTN;
            log.info("The File Is Generated At The Following Location?= " + generatedXlsFilePath);
            fileOutputStream = new FileOutputStream(generatedXlsFilePath.trim());
            workBook.write(fileOutputStream);
        } catch(Exception exObj) {
            log.error("Exception In convertCsvToXls() Method?=  " + exObj);
        } finally {
            try {
                /**** Closing The Excel Workbook Object ****/
                workBook.close();
                /**** Closing The File-Writer Object ****/
                fileOutputStream.close();
                /**** Closing The CSV File-ReaderObject ****/
                reader.close();
            } catch (IOException ioExObj) {
                log.error("Exception While Closing I/O Objects In convertCsvToXls() Method?=  " + ioExObj);
            }
        }
        return generatedXlsFilePath;
    }

    private WareHouseResponse convertWareHouseResponse(WareHouseEntity wareHouse) throws SupplierNotFoundException, ConvertMoneyFortmatException {
        WareHouseResponse response = new WareHouseResponse();
        response.setId(wareHouse.getId());
        response.setCode(wareHouse.getCode());
        response.setName(wareHouse.getName());
        response.setUserId(wareHouse.getUserId());
        response.setUserName(wareHouse.getUserName());
        long supplierId = wareHouse.getSupplierId();
        response.setSupplierId(supplierId);
        SupplierEntity supplier = supplierRepository.findOne(supplierId);
        if(supplier == null){
            log.error("Can't find Supplier with supplierId {}",supplierId);
            throw new SupplierNotFoundException("Supplier not found");
        }
        response.setSupplierName(supplier.getName());
        response.setAmount(wareHouse.getAmount());
        response.setAmountUsed(wareHouse.getAmountUsed());
        String fortmatMoney = NumberToMoneyCharactersUtil.convertMoneyFortmat(wareHouse.getPrice());
        response.setPrice(fortmatMoney);
        response.setAmountExpiryDate(wareHouse.getAmountExpiryDate());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String inputDate = simpleDateFormat.format(wareHouse.getDateInput());
        String expiryDate = simpleDateFormat.format(wareHouse.getExpiryDate());
        response.setDateInput(inputDate);
        response.setExpiryDate(expiryDate);
        return response;
    }

    private void createExcelFile(String directoryPathCsvServer,String fileName,ReportWareHouseResponse responses) throws ExcelFileException {
        try
        {
            List<ReportWareHouseDetailResponse> wareHouses = responses.getWareHouses();
            log.info("Start create files Excel with directoryPathCSVServer {} , fileName {} , recordSize {}",directoryPathCsvServer,fileName,wareHouses.size());
            //Blank workbook
            XSSFWorkbook workbook = new XSSFWorkbook();
            //Create a blank sheet
            XSSFSheet sheet = workbook.createSheet("Employee Data");
            createCell(sheet, 0, "Tên Sản Phẩm", "Tháng Trước Còn Lại", "Nhập Vào", "Xuất Ra", "Còn Lại");
            int rownum = 1;
            for (ReportWareHouseDetailResponse wareHouse : wareHouses) {
                log.info("Start create Excel with numberRow {}",rownum);
                String name = wareHouse.getName();
                String rstAmountMonthAgo = String.valueOf(wareHouse.getRestAmountMonthAgo());
                String inputAmount = String.valueOf(wareHouse.getInputAmount());
                String outputAmount = String.valueOf(wareHouse.getOutputAmount());
                String restAmount = String.valueOf(wareHouse.getRestAmount());
                createCell(sheet, rownum++, name, rstAmountMonthAgo, inputAmount, outputAmount, restAmount);
                log.info("End create row Success");
            }
            createCell(sheet, rownum++, "Tổng Số", String.valueOf(responses.getTotalRestAmountMonthAgo()), String.valueOf(responses.getTotalInputAmount()), String.valueOf(responses.getTotalOutputAmount()), String.valueOf(responses.getTotalRestAmount()));
            String filePath = directoryPathCsvServer + fileName + CsvConstants.FILE_EXTN;
            //Write the workbook in file system
            log.info("Create file Excel name {}",filePath);
            FileOutputStream out = new FileOutputStream(new File(filePath));
            workbook.write(out);
            workbook.close();
            out.close();
            log.info("Create file Excel Success");
        }
        catch (Exception e)
        {
            log.error("Create files Excel failed {}",e.getMessage());
            throw new ExcelFileException("");
        }
    }

    private void createCell(XSSFSheet sheet,int rowNum,String value0,String value1,String value2,String value3,String value4){
        Row row = sheet.createRow(rowNum);
        Cell cellZero = row.createCell(0);
        cellZero.setCellValue(value0);
        Cell cellOne = row.createCell(1);
        cellOne.setCellValue(value1);
        Cell cellTwo = row.createCell(2);
        cellTwo.setCellValue(value2);
        Cell cellThree = row.createCell(3);
        cellThree.setCellValue(value3);
        Cell cellFour = row.createCell(4);
        cellFour.setCellValue(value4);
    }
}
