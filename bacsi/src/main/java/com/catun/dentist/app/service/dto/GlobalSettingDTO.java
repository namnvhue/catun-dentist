package com.catun.dentist.app.service.dto;


import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.NotNull;

/**
 * A DTO for the GlobalSetting entity.
 */
public class GlobalSettingDTO implements Serializable {

    private Long id;

    private String group;

    @NotNull
    private String name;

    @NotNull
    private String value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GlobalSettingDTO globalSettingDTO = (GlobalSettingDTO) o;
        if (globalSettingDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), globalSettingDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "GlobalSettingDTO{" +
            "id=" + getId() +
            ", group='" + getGroup() + "'" +
            ", name='" + getName() + "'" +
            ", value='" + getValue() + "'" +
            "}";
    }
}
