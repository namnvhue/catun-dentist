package com.catun.dentist.app.repository;

import com.catun.dentist.app.entity.ExpenditureEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface ExpenditureRepository extends JpaRepository<ExpenditureEntity, Long> {

    List<ExpenditureEntity> findAllByCreateDateExpenditureBetweenOrderByCreateDateExpenditureAsc(Date fromDate, Date toDate);
}
