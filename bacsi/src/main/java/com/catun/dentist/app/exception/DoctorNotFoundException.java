package com.catun.dentist.app.exception;

public class DoctorNotFoundException extends Exception {

    public DoctorNotFoundException(String message) {
        super(message);
    }
}
