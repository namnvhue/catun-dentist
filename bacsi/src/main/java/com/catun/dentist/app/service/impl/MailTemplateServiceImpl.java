package com.catun.dentist.app.service.impl;

import com.catun.dentist.app.entity.MailTemplateEntity;
import com.catun.dentist.app.entity.PlaceHolderSettingEntity;
import com.catun.dentist.app.enums.MailTemplate;
import com.catun.dentist.app.exception.PlaceHolderSettingNotFoundException;
import com.catun.dentist.app.repository.MailTemplateRepository;
import com.catun.dentist.app.repository.PlaceHolderSettingRepository;
import com.catun.dentist.app.service.MailTeamplateService;
import com.catun.dentist.app.web.rest.request.MailTemplateRequest;
import com.catun.dentist.app.web.rest.response.ContentEmailResponse;
import com.catun.dentist.app.web.rest.response.ListMailTemplateResponse;
import com.catun.dentist.app.web.rest.response.MasterMailTemplateResponse;
import com.catun.dentist.app.web.rest.response.PlaceHolderResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class MailTemplateServiceImpl implements MailTeamplateService {

    private final Logger log = LoggerFactory.getLogger(MailTemplateServiceImpl.class);

    private PlaceHolderSettingRepository placeHolderSettingRepository;
    private MailTemplateRepository mailTemplateRepository;

    public MailTemplateServiceImpl(PlaceHolderSettingRepository placeHolderSettingRepository,
        MailTemplateRepository mailTemplateRepository) {
        this.placeHolderSettingRepository = placeHolderSettingRepository;
        this.mailTemplateRepository = mailTemplateRepository;
    }

    @Override
    public void createMailTemplate(MailTemplateRequest mailTemplateRequest) {
        String masterTemplate = mailTemplateRequest.getMasterTemplate();
        log.info("Start create mail teamplate with template for {}",masterTemplate);
        MailTemplateEntity mailTemplateEntity = mailTemplateRepository
            .findByMasterTemplate(masterTemplate);
        if (mailTemplateEntity == null) {
            mailTemplateEntity = new MailTemplateEntity();
            mailTemplateEntity.setMasterTemplate(masterTemplate);
        }
        mailTemplateEntity.setSubject(mailTemplateRequest.getSubject());
        mailTemplateEntity.setContent(mailTemplateRequest.getContent());
        mailTemplateRepository.save(mailTemplateEntity);
        log.info("End create mail teamplate");
    }

    @Override
    public MasterMailTemplateResponse getMasterMailTemplate()
        throws PlaceHolderSettingNotFoundException {
        log.info("Start get mail teamplate");
        MasterMailTemplateResponse masterMailTemplateResponse = new MasterMailTemplateResponse();
        List<PlaceHolderSettingEntity> placeHolderSettingEntities = placeHolderSettingRepository
            .findAll();
        if (placeHolderSettingEntities.isEmpty()) {
            log.error("PlaceHolderSetting is empty");
            throw new PlaceHolderSettingNotFoundException("PlaceHolder is empty");
        }
        List<ListMailTemplateResponse> listMailTemplateResponses = getMailTemplateResponses(
            placeHolderSettingEntities);
        masterMailTemplateResponse.setMasterTemplate(listMailTemplateResponses);
        log.info("End get mail teamplate");
        return masterMailTemplateResponse;
    }

    private List<ListMailTemplateResponse> getMailTemplateResponses(
        List<PlaceHolderSettingEntity> placeHolderSettingEntities) {
        log.info("Start get MailTemplate with placeHolderSetting size {}",placeHolderSettingEntities.size());
        List<ListMailTemplateResponse> listMailTemplateResponses = new ArrayList<>();
        List<MailTemplate> mailTemplates = Arrays.asList(MailTemplate.values());
        for (MailTemplate mailTemplate : mailTemplates) {
            ListMailTemplateResponse listMailTemplateResponse = new ListMailTemplateResponse();
            listMailTemplateResponse.setName(mailTemplate.name());
            String mailTemplateName = mailTemplate.name();
            List<PlaceHolderResponse> placeHolderResponses = getPlaceHolder(
                placeHolderSettingEntities, mailTemplateName);
            getContentEmail(listMailTemplateResponse, mailTemplateName);
            listMailTemplateResponse.setPlaceHolder(placeHolderResponses);
            listMailTemplateResponses.add(listMailTemplateResponse);
        }
        log.info("get list mail teamplate with size {} ",listMailTemplateResponses.size());
        return listMailTemplateResponses;
    }

    private List<PlaceHolderResponse> getPlaceHolder(
        List<PlaceHolderSettingEntity> placeHolderSettingEntities, String mailTemplateName) {
        log.info("get Placeholder with placeHolderSetting size {} , mailTemplateName {}",placeHolderSettingEntities.size(),mailTemplateName);
        List<PlaceHolderResponse> placeHolderResponses = new ArrayList<>();
        for (PlaceHolderSettingEntity placeHolderSettingEntity : placeHolderSettingEntities) {
            if (mailTemplateName.equals(MailTemplate.BOOKING.name()) && (
                placeHolderSettingEntity.getName().equals("[[BAC_SI]]") || placeHolderSettingEntity
                    .getName().equals("[[LE_TAN]]"))) {
                continue;
            }
            PlaceHolderResponse placeHolderResponse = new PlaceHolderResponse();
            placeHolderResponse.setKeyword(placeHolderSettingEntity.getName());
            placeHolderResponse.setSample(placeHolderSettingEntity.getSample());
            placeHolderResponses.add(placeHolderResponse);
        }
        log.info("End get Placeholder ");
        return placeHolderResponses;
    }

    private void getContentEmail(ListMailTemplateResponse listMailTemplateResponse,
        String mailTemplateName) {
        log.info("Start get ContentEmail with string mailTemplateName {}",mailTemplateName);
        List<MailTemplateEntity> mailTemplateEntitys = mailTemplateRepository.findAll();
        for (MailTemplateEntity mailTemplateEntity : mailTemplateEntitys) {
            if (mailTemplateName.equals(mailTemplateEntity.getMasterTemplate())) {
                ContentEmailResponse contentEmailResponse = new ContentEmailResponse();
                contentEmailResponse.setSubject(mailTemplateEntity.getSubject());
                contentEmailResponse.setContent(mailTemplateEntity.getContent());
                listMailTemplateResponse.setContentEmail(contentEmailResponse);
            }
        }
    }
}
