package com.catun.dentist.app.web.rest.controller;

import com.catun.dentist.app.exception.*;
import com.catun.dentist.app.service.WareHouseService;
import com.catun.dentist.app.web.rest.request.ReportWareHouseRequest;
import com.catun.dentist.app.web.rest.request.WareHouseRequest;
import com.catun.dentist.app.web.rest.response.DetailCommonResponse;
import com.catun.dentist.app.web.rest.response.ReportWareHouseResponse;
import com.catun.dentist.app.web.rest.response.WareHouseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;


@RestController
@RequestMapping("/receptionist")
public class WareHouseController{

    @Autowired
    private WareHouseService wareHouseService;

    @GetMapping
    @RequestMapping("/warehouse-get")
    public ResponseEntity<List<WareHouseResponse>> getWareHouse() throws WareHouseNotFoundException, SupplierNotFoundException, ConvertMoneyFortmatException {
        List<WareHouseResponse> wareHouses = wareHouseService.getWareHouse();
        return new ResponseEntity<>(wareHouses, HttpStatus.OK) ;
    }

    @GetMapping
    @RequestMapping("/warehouse-get/{id}")
    public ResponseEntity<WareHouseResponse> getDetailWareHouse(@PathVariable("id") long id) throws WareHouseNotFoundException, SupplierNotFoundException, ConvertMoneyFortmatException {
        WareHouseResponse wareHouse = wareHouseService.getDetailWareHouse(id);
        return new ResponseEntity<>(wareHouse, HttpStatus.OK) ;
    }

    @PostMapping
    @RequestMapping("/warehouse-create")
    public ResponseEntity<DetailCommonResponse> createWareHouse(@Valid @RequestBody WareHouseRequest request) throws UserNotFoundException, ParseException, SupplierNotFoundException, WaHouseExistException, WareHouseReportDateException {
        wareHouseService.createWareHouse(request);
        return new ResponseEntity<>(new DetailCommonResponse("200","Success"), HttpStatus.OK) ;
    }

    @DeleteMapping
    @RequestMapping("/warehouse-delete/{id}")
    public ResponseEntity<DetailCommonResponse> editWareHouse(@PathVariable("id") long id){
        wareHouseService.deleteWareHouse(id);
        return new ResponseEntity<>(new DetailCommonResponse("200","Success"), HttpStatus.OK) ;
    }

    @PostMapping
    @RequestMapping("/warehouse-report")
    public ResponseEntity<ReportWareHouseResponse> reportWareHouse(@Valid @RequestBody ReportWareHouseRequest request) throws WareHouseNotFoundException, ParseException, WareHouseEnumException, CsvException, WareHouseReportDateException, ExcelFileException {
        ReportWareHouseResponse rePortWareHouses = wareHouseService.reportWareHouse(request);
        return new ResponseEntity<>(rePortWareHouses, HttpStatus.OK) ;
    }

    @GetMapping
    @RequestMapping("/warehouse-report-download/{fileName}")
    public ResponseEntity<Resource> downloadReportCSVFile(HttpServletRequest httpServletRequest, @PathVariable("fileName") String fileName) throws IOException, CsvException {
        Resource resource = wareHouseService.downloadCsv(fileName);
        return ResponseEntity.ok()
            .contentType(MediaType.parseMediaType("text/csv"))
            .header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + resource.getFilename() + "\"")
            .body(resource);
    }
}
