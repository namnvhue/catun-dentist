package com.catun.dentist.app.service.impl;

import com.catun.dentist.app.entity.ProductEntity;
import com.catun.dentist.app.entity.ServiceCategoryEntity;
import com.catun.dentist.app.entity.ServiceEntity;
import com.catun.dentist.app.exception.ProductEntityNotFoundException;
import com.catun.dentist.app.exception.ServiceCategoryNotFoundException;
import com.catun.dentist.app.exception.ServiceEntityExistException;
import com.catun.dentist.app.exception.ServiceEntityNotFoundException;
import com.catun.dentist.app.repository.ProductRepository;
import com.catun.dentist.app.repository.ServiceCategoryRepository;
import com.catun.dentist.app.repository.ServiceRepository;
import com.catun.dentist.app.service.Services;
import com.catun.dentist.app.service.util.ProductUtil;
import com.catun.dentist.app.web.rest.request.ServiceRequest;
import com.catun.dentist.app.web.rest.response.DetailServiceResponse;
import com.catun.dentist.app.web.rest.response.ProductResponse;
import com.catun.dentist.app.web.rest.response.ServiceResponse;
import com.catun.dentist.app.web.rest.response.ServicesResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ServiceImpl implements Services {

    private final Logger log = LoggerFactory.getLogger(ServiceImpl.class);

    private ProductRepository productRepository;
    private ServiceCategoryRepository serviceCategoryRepository;
    private ServiceRepository serviceRepository;

    public ServiceImpl(ProductRepository productRepository,
                       ServiceCategoryRepository serviceCategoryRepository,
                       ServiceRepository serviceRepository) {
        this.productRepository = productRepository;
        this.serviceCategoryRepository = serviceCategoryRepository;
        this.serviceRepository = serviceRepository;
    }


    @Override
    public ServicesResponse getServices() throws ServiceCategoryNotFoundException {
        List<ProductEntity> productEntities = productRepository.findAll();
        List<ServiceCategoryEntity> serviceCategoryEntities = serviceCategoryRepository
            .findAll();
        if (serviceCategoryEntities.isEmpty()) {
            log.error("CategoryServices is empty , please create");
            throw new ServiceCategoryNotFoundException("CategoryServices is empty , please create");
        }
        return new ServicesResponse(productEntities, serviceCategoryEntities);
    }

    @Override
    public void createService(ServiceRequest serviceRequest)
        throws ServiceCategoryNotFoundException, ServiceEntityExistException, ProductEntityNotFoundException {
        String nameService = serviceRequest.getName();
        ServiceEntity serviceEntityExist = serviceRepository.findByName(nameService);
        if (serviceEntityExist != null) {
            log.error("Services is exit in system");
            throw new ServiceEntityExistException("Services is exit in system");
        }
        ServiceEntity serviceEntity = new ServiceEntity();
        serviceEntity.setName(nameService);
        serviceEntity.setDescription(serviceRequest.getDescription());
        serviceEntity.setBasePrice(serviceRequest.getBasePrice());
        ServiceCategoryEntity serviceCategoryEntity = serviceCategoryRepository
            .findOne(Long.valueOf(serviceRequest.getServiceCategoryId()));
        if (serviceCategoryEntity == null) {
            log.error("CategoryServices is empty , please create");
            throw new ServiceCategoryNotFoundException("CategoryServices is empty , please create");
        }
        serviceEntity.setServiceCategory(serviceCategoryEntity);
        String productIds = serviceRequest.getRelatedProducts();
        if (checkProducts(productIds)) {
            serviceEntity.setRelatedProducts(productIds);
        }
        serviceRepository.save(serviceEntity);
    }

    private boolean checkProducts(String relatedProducts) throws ProductEntityNotFoundException {
        if(StringUtils.isEmpty(relatedProducts)){
            return false;
        }
        log.info("Start checkProducts with relatedProducts {}",relatedProducts);
        String[] productIds = relatedProducts.split(",");
        for (int i = 0; i < productIds.length; i++) {
            ProductEntity productEntity = productRepository
                .findOne(Long.valueOf(productIds[i]));
            if (productEntity == null) {
                log.error("Product entity not found in system");
                throw new ProductEntityNotFoundException("Product entity not found in system");
            }
        }
        return true;
    }

    @Override
    public void updateService(ServiceRequest serviceRequest)
        throws ServiceEntityNotFoundException, ServiceCategoryNotFoundException {
        ServiceEntity serviceEntity = serviceRepository
            .findOne(Long.valueOf(serviceRequest.getId()));
        if (serviceEntity == null) {
            log.error("ServiceEntity not found in system");
            throw new ServiceEntityNotFoundException("ServiceEntity not found in system");
        }
        serviceEntity = convertServiceRequest(serviceEntity, serviceRequest);
        serviceRepository.save(serviceEntity);
    }

    @Override
    public Page<ServiceEntity> findAll(Pageable pageable) throws ProductEntityNotFoundException {
        Page<ServiceEntity> serviceEntities = serviceRepository.findAll(pageable);
        Iterator<ServiceEntity> serviceEntityIterator = serviceEntities.iterator();
        while (serviceEntityIterator.hasNext()) {
            ServiceEntity serviceEntity = serviceEntityIterator.next();
            String relatedProduct = ProductUtil
                .getNameProduct(serviceEntity.getRelatedProducts(), productRepository);
            serviceEntity.setRelatedProducts(relatedProduct);
        }
        return serviceEntities;
    }

    @Override
    public DetailServiceResponse getDetail(long id)
        throws ServiceEntityNotFoundException, ServiceCategoryNotFoundException, ProductEntityNotFoundException {
        log.info("Start get Service Detail with id {}",id);
        ServiceEntity serviceEntity = serviceRepository.findOne(id);
        if (serviceEntity == null) {
            log.error("Service entity not found in system");
            throw new ServiceEntityNotFoundException("Service entity not found in system");
        }
        DetailServiceResponse detailServiceResponse = new DetailServiceResponse();
        detailServiceResponse.setId(String.valueOf(serviceEntity.getId()));
        detailServiceResponse.setName(serviceEntity.getName());
        detailServiceResponse.setBasePrice(serviceEntity.getBasePrice());
        detailServiceResponse.setDescription(serviceEntity.getDescription());
        detailServiceResponse.setRelatedProducts(getProducts(serviceEntity.getRelatedProducts()));
        ServiceCategoryEntity serviceCategoryEntity = serviceEntity.getServiceCategory();
        if (serviceCategoryEntity == null) {
            log.error("SeviceCategory can't find in Service");
            throw new ServiceCategoryNotFoundException("SeviceCategory can't find in Service");
        }
        ServiceResponse serviceResponse = new ServiceResponse();
        serviceResponse.setId(String.valueOf(serviceCategoryEntity.getId()));
        serviceResponse.setName(serviceCategoryEntity.getName());
        serviceResponse.setDescription(serviceCategoryEntity.getDescription());
        detailServiceResponse.setServiceCategory(serviceResponse);
        return detailServiceResponse;

    }

    private List<ProductResponse> getProducts(String relatedProducts)
        throws ProductEntityNotFoundException {
        if(StringUtils.isEmpty(relatedProducts)){
            return null;
        }
        String[] productIds = relatedProducts.split(",");
        List<ProductResponse> productResponses = new ArrayList<>();
        for (String productId : productIds) {
            ProductEntity productEntity = productRepository.findOne(Long.valueOf(productId));
            if (productEntity == null) {
                log.error("Product not found exception {}", productId);
                throw new ProductEntityNotFoundException("Product not found exception");
            }
            ProductResponse productResponse = new ProductResponse();
            productResponse.setId(productId);
            productResponse.setName(productEntity.getName());
            ;
            productResponse.setPrice(productEntity.getPrice());
            productResponses.add(productResponse);

        }
        return productResponses;
    }

    @Override
    public void delete(long id) throws ServiceEntityNotFoundException {
        log.info("Start delete service with id {} ",id);
        ServiceEntity serviceEntity = serviceRepository.findOne(id);
        if (serviceEntity == null) {
            log.error("Service entity not found in system");
            throw new ServiceEntityNotFoundException("Service entity not found in system");
        }
        serviceRepository.delete(id);
        log.info("End delete service Success");
    }

    private ServiceEntity convertServiceRequest(ServiceEntity serviceEntity,
        ServiceRequest serviceRequest) throws ServiceCategoryNotFoundException {
        ServiceCategoryEntity serviceCategoryEntity = serviceCategoryRepository
            .findOne(Long.valueOf(serviceRequest.getServiceCategoryId()));
        if (serviceCategoryEntity == null) {
            log.error("ServiceCategory not found in system");
            throw new ServiceCategoryNotFoundException("ServiceCategory not found in system");
        }
        serviceEntity.setServiceCategory(serviceCategoryEntity);
        serviceEntity.setName(serviceRequest.getName());
        serviceEntity.setBasePrice(serviceRequest.getBasePrice());
        serviceEntity.setDescription(serviceRequest.getDescription());
        serviceEntity.setRelatedProducts(serviceRequest.getRelatedProducts());
        return serviceEntity;
    }
}
