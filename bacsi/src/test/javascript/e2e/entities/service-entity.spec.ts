import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('ServiceEntity e2e test', () => {

    let navBarPage: NavBarPage;
    let serviceEntityDialogPage: ServiceEntityDialogPage;
    let serviceEntityComponentsPage: ServiceEntityComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load ServiceEntities', () => {
        navBarPage.goToEntity('service-entity');
        serviceEntityComponentsPage = new ServiceEntityComponentsPage();
        expect(serviceEntityComponentsPage.getTitle())
            .toMatch(/cantunDentistApp.serviceEntity.home.title/);

    });

    it('should load create ServiceEntity dialog', () => {
        serviceEntityComponentsPage.clickOnCreateButton();
        serviceEntityDialogPage = new ServiceEntityDialogPage();
        expect(serviceEntityDialogPage.getModalTitle())
            .toMatch(/cantunDentistApp.serviceEntity.home.createOrEditLabel/);
        serviceEntityDialogPage.close();
    });

    it('should create and save ServiceEntities', () => {
        serviceEntityComponentsPage.clickOnCreateButton();
        serviceEntityDialogPage.setNameInput('name');
        expect(serviceEntityDialogPage.getNameInput()).toMatch('name');
        serviceEntityDialogPage.setBasePriceInput('5');
        expect(serviceEntityDialogPage.getBasePriceInput()).toMatch('5');
        serviceEntityDialogPage.setDescriptionInput('description');
        expect(serviceEntityDialogPage.getDescriptionInput()).toMatch('description');
        serviceEntityDialogPage.setRelatedProductsInput('relatedProducts');
        expect(serviceEntityDialogPage.getRelatedProductsInput()).toMatch('relatedProducts');
        serviceEntityDialogPage.serviceCategorySelectLastOption();
        serviceEntityDialogPage.treatmentItemSelectLastOption();
        serviceEntityDialogPage.save();
        expect(serviceEntityDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ServiceEntityComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-service-entity div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ServiceEntityDialogPage {
    modalTitle = element(by.css('h4#myServiceEntityLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));
    basePriceInput = element(by.css('input#field_basePrice'));
    descriptionInput = element(by.css('input#field_description'));
    relatedProductsInput = element(by.css('input#field_relatedProducts'));
    serviceCategorySelect = element(by.css('select#field_serviceCategory'));
    treatmentItemSelect = element(by.css('select#field_treatmentItem'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    setBasePriceInput = function(basePrice) {
        this.basePriceInput.sendKeys(basePrice);
    };

    getBasePriceInput = function() {
        return this.basePriceInput.getAttribute('value');
    };

    setDescriptionInput = function(description) {
        this.descriptionInput.sendKeys(description);
    };

    getDescriptionInput = function() {
        return this.descriptionInput.getAttribute('value');
    };

    setRelatedProductsInput = function(relatedProducts) {
        this.relatedProductsInput.sendKeys(relatedProducts);
    };

    getRelatedProductsInput = function() {
        return this.relatedProductsInput.getAttribute('value');
    };

    serviceCategorySelectLastOption = function() {
        this.serviceCategorySelect.all(by.tagName('option')).last().click();
    };

    serviceCategorySelectOption = function(option) {
        this.serviceCategorySelect.sendKeys(option);
    };

    getServiceCategorySelect = function() {
        return this.serviceCategorySelect;
    };

    getServiceCategorySelectedOption = function() {
        return this.serviceCategorySelect.element(by.css('option:checked')).getText();
    };

    treatmentItemSelectLastOption = function() {
        this.treatmentItemSelect.all(by.tagName('option')).last().click();
    };

    treatmentItemSelectOption = function(option) {
        this.treatmentItemSelect.sendKeys(option);
    };

    getTreatmentItemSelect = function() {
        return this.treatmentItemSelect;
    };

    getTreatmentItemSelectedOption = function() {
        return this.treatmentItemSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
