import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('GlobalSetting e2e test', () => {

    let navBarPage: NavBarPage;
    let globalSettingDialogPage: GlobalSettingDialogPage;
    let globalSettingComponentsPage: GlobalSettingComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load GlobalSettings', () => {
        navBarPage.goToEntity('global-setting');
        globalSettingComponentsPage = new GlobalSettingComponentsPage();
        expect(globalSettingComponentsPage.getTitle())
            .toMatch(/cantunDentistApp.globalSetting.home.title/);

    });

    it('should load create GlobalSetting dialog', () => {
        globalSettingComponentsPage.clickOnCreateButton();
        globalSettingDialogPage = new GlobalSettingDialogPage();
        expect(globalSettingDialogPage.getModalTitle())
            .toMatch(/cantunDentistApp.globalSetting.home.createOrEditLabel/);
        globalSettingDialogPage.close();
    });

    it('should create and save GlobalSettings', () => {
        globalSettingComponentsPage.clickOnCreateButton();
        globalSettingDialogPage.setGroupInput('group');
        expect(globalSettingDialogPage.getGroupInput()).toMatch('group');
        globalSettingDialogPage.setNameInput('name');
        expect(globalSettingDialogPage.getNameInput()).toMatch('name');
        globalSettingDialogPage.setValueInput('value');
        expect(globalSettingDialogPage.getValueInput()).toMatch('value');
        globalSettingDialogPage.save();
        expect(globalSettingDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class GlobalSettingComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-global-setting div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class GlobalSettingDialogPage {
    modalTitle = element(by.css('h4#myGlobalSettingLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    groupInput = element(by.css('input#field_group'));
    nameInput = element(by.css('input#field_name'));
    valueInput = element(by.css('input#field_value'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setGroupInput = function(group) {
        this.groupInput.sendKeys(group);
    };

    getGroupInput = function() {
        return this.groupInput.getAttribute('value');
    };

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    setValueInput = function(value) {
        this.valueInput.sendKeys(value);
    };

    getValueInput = function() {
        return this.valueInput.getAttribute('value');
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
