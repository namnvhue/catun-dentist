import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('ServiceCategoryEntity e2e test', () => {

    let navBarPage: NavBarPage;
    let serviceCategoryEntityDialogPage: ServiceCategoryEntityDialogPage;
    let serviceCategoryEntityComponentsPage: ServiceCategoryEntityComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load ServiceCategoryEntities', () => {
        navBarPage.goToEntity('service-category-entity');
        serviceCategoryEntityComponentsPage = new ServiceCategoryEntityComponentsPage();
        expect(serviceCategoryEntityComponentsPage.getTitle())
            .toMatch(/cantunDentistApp.serviceCategoryEntity.home.title/);

    });

    it('should load create ServiceCategoryEntity dialog', () => {
        serviceCategoryEntityComponentsPage.clickOnCreateButton();
        serviceCategoryEntityDialogPage = new ServiceCategoryEntityDialogPage();
        expect(serviceCategoryEntityDialogPage.getModalTitle())
            .toMatch(/cantunDentistApp.serviceCategoryEntity.home.createOrEditLabel/);
        serviceCategoryEntityDialogPage.close();
    });

    it('should create and save ServiceCategoryEntities', () => {
        serviceCategoryEntityComponentsPage.clickOnCreateButton();
        serviceCategoryEntityDialogPage.setNameInput('name');
        expect(serviceCategoryEntityDialogPage.getNameInput()).toMatch('name');
        serviceCategoryEntityDialogPage.setDescriptionInput('description');
        expect(serviceCategoryEntityDialogPage.getDescriptionInput()).toMatch('description');
        serviceCategoryEntityDialogPage.save();
        expect(serviceCategoryEntityDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ServiceCategoryEntityComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-service-category-entity div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ServiceCategoryEntityDialogPage {
    modalTitle = element(by.css('h4#myServiceCategoryEntityLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));
    descriptionInput = element(by.css('input#field_description'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    setDescriptionInput = function(description) {
        this.descriptionInput.sendKeys(description);
    };

    getDescriptionInput = function() {
        return this.descriptionInput.getAttribute('value');
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
