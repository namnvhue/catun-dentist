import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('ProductEntity e2e test', () => {

    let navBarPage: NavBarPage;
    let productEntityDialogPage: ProductEntityDialogPage;
    let productEntityComponentsPage: ProductEntityComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load ProductEntities', () => {
        navBarPage.goToEntity('product-entity');
        productEntityComponentsPage = new ProductEntityComponentsPage();
        expect(productEntityComponentsPage.getTitle())
            .toMatch(/cantunDentistApp.productEntity.home.title/);

    });

    it('should load create ProductEntity dialog', () => {
        productEntityComponentsPage.clickOnCreateButton();
        productEntityDialogPage = new ProductEntityDialogPage();
        expect(productEntityDialogPage.getModalTitle())
            .toMatch(/cantunDentistApp.productEntity.home.createOrEditLabel/);
        productEntityDialogPage.close();
    });

    it('should create and save ProductEntities', () => {
        productEntityComponentsPage.clickOnCreateButton();
        productEntityDialogPage.setNameInput('name');
        expect(productEntityDialogPage.getNameInput()).toMatch('name');
        productEntityDialogPage.setPriceInput('5');
        expect(productEntityDialogPage.getPriceInput()).toMatch('5');
        productEntityDialogPage.serviceSelectLastOption();
        productEntityDialogPage.save();
        expect(productEntityDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ProductEntityComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-product-entity div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ProductEntityDialogPage {
    modalTitle = element(by.css('h4#myProductEntityLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));
    priceInput = element(by.css('input#field_price'));
    serviceSelect = element(by.css('select#field_service'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    setPriceInput = function(price) {
        this.priceInput.sendKeys(price);
    };

    getPriceInput = function() {
        return this.priceInput.getAttribute('value');
    };

    serviceSelectLastOption = function() {
        this.serviceSelect.all(by.tagName('option')).last().click();
    };

    serviceSelectOption = function(option) {
        this.serviceSelect.sendKeys(option);
    };

    getServiceSelect = function() {
        return this.serviceSelect;
    };

    getServiceSelectedOption = function() {
        return this.serviceSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
