/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { CatunDentistAppTestModule } from '../../../test.module';
import { ServiceCategoryEntityDetailComponent } from '../../../../../../main/webapp/app/entities/service-category-entity/service-category-entity-detail.component';
import { ServiceCategoryEntityService } from '../../../../../../main/webapp/app/entities/service-category-entity/service-category-entity.service';
import { ServiceCategoryEntity } from '../../../../../../main/webapp/app/entities/service-category-entity/service-category-entity.model';

describe('Component Tests', () => {

    describe('ServiceCategoryEntity Management Detail Component', () => {
        let comp: ServiceCategoryEntityDetailComponent;
        let fixture: ComponentFixture<ServiceCategoryEntityDetailComponent>;
        let service: ServiceCategoryEntityService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [CatunDentistAppTestModule],
                declarations: [ServiceCategoryEntityDetailComponent],
                providers: [
                    ServiceCategoryEntityService
                ]
            })
            .overrideTemplate(ServiceCategoryEntityDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ServiceCategoryEntityDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ServiceCategoryEntityService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new ServiceCategoryEntity(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.serviceCategoryEntity).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
