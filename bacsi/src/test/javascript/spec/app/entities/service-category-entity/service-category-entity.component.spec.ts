/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { CatunDentistAppTestModule } from '../../../test.module';
import { ServiceCategoryEntityComponent } from '../../../../../../main/webapp/app/entities/service-category-entity/service-category-entity.component';
import { ServiceCategoryEntityService } from '../../../../../../main/webapp/app/entities/service-category-entity/service-category-entity.service';
import { ServiceCategoryEntity } from '../../../../../../main/webapp/app/entities/service-category-entity/service-category-entity.model';

describe('Component Tests', () => {

    describe('ServiceCategoryEntity Management Component', () => {
        let comp: ServiceCategoryEntityComponent;
        let fixture: ComponentFixture<ServiceCategoryEntityComponent>;
        let service: ServiceCategoryEntityService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [CatunDentistAppTestModule],
                declarations: [ServiceCategoryEntityComponent],
                providers: [
                    ServiceCategoryEntityService
                ]
            })
            .overrideTemplate(ServiceCategoryEntityComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ServiceCategoryEntityComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ServiceCategoryEntityService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new ServiceCategoryEntity(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.serviceCategoryEntities[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
