/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { CatunDentistAppTestModule } from '../../../test.module';
import { ServiceCategoryEntityDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/service-category-entity/service-category-entity-delete-dialog.component';
import { ServiceCategoryEntityService } from '../../../../../../main/webapp/app/entities/service-category-entity/service-category-entity.service';

describe('Component Tests', () => {

    describe('ServiceCategoryEntity Management Delete Component', () => {
        let comp: ServiceCategoryEntityDeleteDialogComponent;
        let fixture: ComponentFixture<ServiceCategoryEntityDeleteDialogComponent>;
        let service: ServiceCategoryEntityService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [CatunDentistAppTestModule],
                declarations: [ServiceCategoryEntityDeleteDialogComponent],
                providers: [
                    ServiceCategoryEntityService
                ]
            })
            .overrideTemplate(ServiceCategoryEntityDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ServiceCategoryEntityDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ServiceCategoryEntityService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
