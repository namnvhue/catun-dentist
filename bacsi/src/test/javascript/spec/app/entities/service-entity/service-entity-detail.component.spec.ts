/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { CatunDentistAppTestModule } from '../../../test.module';
import { ServiceEntityDetailComponent } from '../../../../../../main/webapp/app/entities/service-entity/service-entity-detail.component';
import { ServiceEntityService } from '../../../../../../main/webapp/app/entities/service-entity/service-entity.service';
import { ServiceEntity } from '../../../../../../main/webapp/app/entities/service-entity/service-entity.model';

describe('Component Tests', () => {

    describe('ServiceEntity Management Detail Component', () => {
        let comp: ServiceEntityDetailComponent;
        let fixture: ComponentFixture<ServiceEntityDetailComponent>;
        let service: ServiceEntityService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [CatunDentistAppTestModule],
                declarations: [ServiceEntityDetailComponent],
                providers: [
                    ServiceEntityService
                ]
            })
            .overrideTemplate(ServiceEntityDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ServiceEntityDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ServiceEntityService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new ServiceEntity(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.serviceEntity).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
