/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { CatunDentistAppTestModule } from '../../../test.module';
import { ServiceEntityDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/service-entity/service-entity-delete-dialog.component';
import { ServiceEntityService } from '../../../../../../main/webapp/app/entities/service-entity/service-entity.service';

describe('Component Tests', () => {

    describe('ServiceEntity Management Delete Component', () => {
        let comp: ServiceEntityDeleteDialogComponent;
        let fixture: ComponentFixture<ServiceEntityDeleteDialogComponent>;
        let service: ServiceEntityService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [CatunDentistAppTestModule],
                declarations: [ServiceEntityDeleteDialogComponent],
                providers: [
                    ServiceEntityService
                ]
            })
            .overrideTemplate(ServiceEntityDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ServiceEntityDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ServiceEntityService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
