/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { CatunDentistAppTestModule } from '../../../test.module';
import { ServiceEntityComponent } from '../../../../../../main/webapp/app/entities/service-entity/service-entity.component';
import { ServiceEntityService } from '../../../../../../main/webapp/app/entities/service-entity/service-entity.service';
import { ServiceEntity } from '../../../../../../main/webapp/app/entities/service-entity/service-entity.model';

describe('Component Tests', () => {

    describe('ServiceEntity Management Component', () => {
        let comp: ServiceEntityComponent;
        let fixture: ComponentFixture<ServiceEntityComponent>;
        let service: ServiceEntityService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [CatunDentistAppTestModule],
                declarations: [ServiceEntityComponent],
                providers: [
                    ServiceEntityService
                ]
            })
            .overrideTemplate(ServiceEntityComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ServiceEntityComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ServiceEntityService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new ServiceEntity(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.serviceEntities[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
