/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { CatunDentistAppTestModule } from '../../../test.module';
import { ServiceEntityDialogComponent } from '../../../../../../main/webapp/app/entities/service-entity/service-entity-dialog.component';
import { ServiceEntityService } from '../../../../../../main/webapp/app/entities/service-entity/service-entity.service';
import { ServiceEntity } from '../../../../../../main/webapp/app/entities/service-entity/service-entity.model';
import { ServiceCategoryEntityService } from '../../../../../../main/webapp/app/entities/service-category-entity';

describe('Component Tests', () => {

    describe('ServiceEntity Management Dialog Component', () => {
        let comp: ServiceEntityDialogComponent;
        let fixture: ComponentFixture<ServiceEntityDialogComponent>;
        let service: ServiceEntityService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [CatunDentistAppTestModule],
                declarations: [ServiceEntityDialogComponent],
                providers: [
                    ServiceCategoryEntityService,
                    ServiceEntityService
                ]
            })
            .overrideTemplate(ServiceEntityDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ServiceEntityDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ServiceEntityService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new ServiceEntity(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.serviceEntity = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'serviceEntityListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new ServiceEntity();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.serviceEntity = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'serviceEntityListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
