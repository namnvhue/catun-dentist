/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { CatunDentistAppTestModule } from '../../../test.module';
import { ProductEntityDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/product-entity/product-entity-delete-dialog.component';
import { ProductEntityService } from '../../../../../../main/webapp/app/entities/product-entity/product-entity.service';

describe('Component Tests', () => {

    describe('ProductEntity Management Delete Component', () => {
        let comp: ProductEntityDeleteDialogComponent;
        let fixture: ComponentFixture<ProductEntityDeleteDialogComponent>;
        let service: ProductEntityService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [CatunDentistAppTestModule],
                declarations: [ProductEntityDeleteDialogComponent],
                providers: [
                    ProductEntityService
                ]
            })
            .overrideTemplate(ProductEntityDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ProductEntityDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProductEntityService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
