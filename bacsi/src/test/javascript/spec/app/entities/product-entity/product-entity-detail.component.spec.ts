/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { CatunDentistAppTestModule } from '../../../test.module';
import { ProductEntityDetailComponent } from '../../../../../../main/webapp/app/entities/product-entity/product-entity-detail.component';
import { ProductEntityService } from '../../../../../../main/webapp/app/entities/product-entity/product-entity.service';
import { ProductEntity } from '../../../../../../main/webapp/app/entities/product-entity/product-entity.model';

describe('Component Tests', () => {

    describe('ProductEntity Management Detail Component', () => {
        let comp: ProductEntityDetailComponent;
        let fixture: ComponentFixture<ProductEntityDetailComponent>;
        let service: ProductEntityService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [CatunDentistAppTestModule],
                declarations: [ProductEntityDetailComponent],
                providers: [
                    ProductEntityService
                ]
            })
            .overrideTemplate(ProductEntityDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ProductEntityDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProductEntityService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new ProductEntity(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.productEntity).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
