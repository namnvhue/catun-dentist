/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { CatunDentistAppTestModule } from '../../../test.module';
import { ProductEntityComponent } from '../../../../../../main/webapp/app/entities/product-entity/product-entity.component';
import { ProductEntityService } from '../../../../../../main/webapp/app/entities/product-entity/product-entity.service';
import { ProductEntity } from '../../../../../../main/webapp/app/entities/product-entity/product-entity.model';

describe('Component Tests', () => {

    describe('ProductEntity Management Component', () => {
        let comp: ProductEntityComponent;
        let fixture: ComponentFixture<ProductEntityComponent>;
        let service: ProductEntityService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [CatunDentistAppTestModule],
                declarations: [ProductEntityComponent],
                providers: [
                    ProductEntityService
                ]
            })
            .overrideTemplate(ProductEntityComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ProductEntityComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProductEntityService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new ProductEntity(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.productEntities[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
