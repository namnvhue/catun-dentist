/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { CatunDentistAppTestModule } from '../../../test.module';
import { ProductEntityDialogComponent } from '../../../../../../main/webapp/app/entities/product-entity/product-entity-dialog.component';
import { ProductEntityService } from '../../../../../../main/webapp/app/entities/product-entity/product-entity.service';
import { ProductEntity } from '../../../../../../main/webapp/app/entities/product-entity/product-entity.model';
import { ServiceEntityService } from '../../../../../../main/webapp/app/entities/service-entity';

describe('Component Tests', () => {

    describe('ProductEntity Management Dialog Component', () => {
        let comp: ProductEntityDialogComponent;
        let fixture: ComponentFixture<ProductEntityDialogComponent>;
        let service: ProductEntityService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [CatunDentistAppTestModule],
                declarations: [ProductEntityDialogComponent],
                providers: [
                    ServiceEntityService,
                    ProductEntityService
                ]
            })
            .overrideTemplate(ProductEntityDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ProductEntityDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProductEntityService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new ProductEntity(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.productEntity = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'productEntityListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new ProductEntity();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.productEntity = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'productEntityListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
